# TODO: Copyright
from UM.Tool import Tool
from UM.Scene.Selection import Selection
from UM.Application import Application


class AmeTool(Tool):
    """
    This is a one-level inheritance class that allows us to subclass tools that need to work on our models (PCB or AME)

    For the most part it is a null-op class, with some utilities that are common to several of our tools but not to the
    generic Cura tools
    """

    def __init__(self) -> None:
        super().__init__()

        Selection.selectionChanged.connect(self.propertyChanged)
        Selection.selectionChanged.connect(self._updateEnabled)
        self._updateEnabled()

    def isPCBTool(self):
        return True

    def _updateEnabled(self):
        """
        Update enabled/disabled status of the tool
        :return:
        """

        selected_objects = Selection.getAllSelectedObjects()
        answer = False
        if len(selected_objects) == 1:
            selected = selected_objects[0]
            if not selected.isCopyOrMaster():
                answer = (selected.hasDecoration("isPcbModel") == self.isPCBTool())
        Application.getInstance().getController().toolEnabledChanged.emit(self._plugin_id, answer)
