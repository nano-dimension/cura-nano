# Copyright (c) 2020 Ultimaker B.V.
# Cura is released under the terms of the LGPLv3 or higher.

import os
import shutil
import sys
import time
from collections import Iterable
from tempfile import TemporaryDirectory
from typing import cast, TYPE_CHECKING, Optional, Callable, List, Any, Dict


import cura.Settings.cura_empty_instance_containers
import numpy
from PyQt5.QtCore import QObject, QTimer, QUrl, pyqtSignal, pyqtProperty, QEvent, Q_ENUMS
from PyQt5.QtGui import QColor, QIcon
from PyQt5.QtQml import qmlRegisterUncreatableType, qmlRegisterSingletonType, qmlRegisterType
from PyQt5.QtWidgets import QMessageBox

import UM.Util
from UM.Application import Application
from UM.Decorators import override
from UM.FileHandler.ReadFileJob import ReadFileJob
from UM.FlameProfiler import pyqtSlot
from UM.Logger import Logger
from UM.Math.AxisAlignedBox import AxisAlignedBox
from UM.Math.Matrix import Matrix
from UM.Math.Quaternion import Quaternion
from UM.Math.Vector import Vector
from UM.Mesh.ReadMeshJob import ReadMeshJob
from UM.Message import Message
from UM.Operations import Operation, OperationStack
from UM.Operations.AddSceneNodeOperation import AddSceneNodeOperation
from UM.Operations.GroupedOperation import GroupedOperation
from UM.Operations.SetTransformOperation import SetTransformOperation
from UM.Operations.TranslateOperation import TranslateOperation
from UM.Platform import Platform
from UM.PluginError import PluginNotFoundError
from UM.Preferences import Preferences
from UM.Qt.QtApplication import QtApplication  # The class we're inheriting from.
from UM.Resources import Resources
from UM.Scene.Camera import Camera
from UM.Scene.GroupDecorator import GroupDecorator
from UM.Scene.Iterator.DepthFirstIterator import DepthFirstIterator
from UM.Scene.SceneNode import SceneNode
from UM.Scene.SceneNodeSettings import SceneNodeSettings
from UM.Scene.Selection import Selection
from UM.Scene.ToolHandle import ToolHandle
from UM.Settings.ContainerRegistry import ContainerRegistry
from UM.Settings.InstanceContainer import InstanceContainer
from UM.Settings.SettingDefinition import SettingDefinition, DefinitionPropertyType
from UM.Settings.SettingFunction import SettingFunction
from UM.Settings.Validator import Validator
from UM.View.SelectionPass import SelectionPass  # For typing.
from UM.Workspace.WorkspaceReader import WorkspaceReader
from UM.i18n import i18nCatalog
from UM.Workspace.WorkspaceFileHandler import WorkspaceFileHandler

from cura import ApplicationMetadata
from cura.API import CuraAPI
from cura.API.Account import Account
from cura.Arranging.Arrange import Arrange
from cura.Arranging.ArrangeObjectsAllBuildPlatesJob import ArrangeObjectsAllBuildPlatesJob
from cura.Arranging.ArrangeObjectsJob import ArrangeObjectsJob
from cura.Arranging.Nest2DArrange import arrange
from cura.Machines.MachineErrorChecker import MachineErrorChecker
from cura.Machines.Models.BuildPlateModel import BuildPlateModel
from cura.Machines.Models.CustomQualityProfilesDropDownMenuModel import CustomQualityProfilesDropDownMenuModel
from cura.Machines.Models.DiscoveredCloudPrintersModel import DiscoveredCloudPrintersModel
from cura.Machines.Models.DiscoveredPrintersModel import DiscoveredPrintersModel
from cura.Machines.Models.ExtrudersModel import ExtrudersModel
from cura.Machines.Models.FirstStartMachineActionsModel import FirstStartMachineActionsModel
from cura.Machines.Models.GlobalStacksModel import GlobalStacksModel
from cura.Machines.Models.IntentCategoryModel import IntentCategoryModel
from cura.Machines.Models.IntentModel import IntentModel
from cura.Machines.Models.MaterialBrandsModel import MaterialBrandsModel
from cura.Machines.Models.MaterialManagementModel import MaterialManagementModel
from cura.Machines.Models.MultiBuildPlateModel import MultiBuildPlateModel
from cura.Machines.Models.NozzleModel import NozzleModel
from cura.Machines.Models.QualityManagementModel import QualityManagementModel
from cura.Machines.Models.QualityProfilesDropDownMenuModel import QualityProfilesDropDownMenuModel
from cura.Machines.Models.QualitySettingsModel import QualitySettingsModel
from cura.Machines.Models.SettingVisibilityPresetsModel import SettingVisibilityPresetsModel
from cura.Machines.Models.UserChangesModel import UserChangesModel
from cura.Operations.SetParentOperation import SetParentOperation
from cura.PrinterOutput.NetworkMJPGImage import NetworkMJPGImage
from cura.PrinterOutput.PrinterOutputDevice import PrinterOutputDevice
from cura.Scene import ZOffsetDecorator
from cura.Scene.BlockSlicingDecorator import BlockSlicingDecorator
from cura.Scene.BuildPlateDecorator import BuildPlateDecorator
from cura.Scene.ConvexHullDecorator import ConvexHullDecorator
from cura.Scene.CuraSceneController import CuraSceneController
from cura.Scene.CuraSceneNode import CuraSceneNode
from cura.Scene.SliceableObjectDecorator import SliceableObjectDecorator
from cura.Settings.ContainerManager import ContainerManager
from cura.Settings.CuraContainerRegistry import CuraContainerRegistry
from cura.Settings.CuraFormulaFunctions import CuraFormulaFunctions
from cura.Settings.ExtruderManager import ExtruderManager
from cura.Settings.ExtruderStack import ExtruderStack
from cura.Settings.GlobalStack import GlobalStack
from cura.Settings.IntentManager import IntentManager
from cura.Settings.MachineManager import MachineManager
from cura.Settings.MachineNameValidator import MachineNameValidator
from cura.Settings.MaterialSettingsVisibilityHandler import MaterialSettingsVisibilityHandler
from cura.Settings.SettingInheritanceManager import SettingInheritanceManager
from cura.Settings.SidebarCustomMenuItemsModel import SidebarCustomMenuItemsModel
from cura.Settings.SimpleModeSettingsManager import SimpleModeSettingsManager
from cura.TaskManagement.OnExitCallbackManager import OnExitCallbackManager
from cura.TrackJobsFinishedJob import TrackJobsFinishedJob
from cura.UI import CuraSplashScreen, MachineActionManager, PrintInformation
from cura.UI.AddPrinterPagesModel import AddPrinterPagesModel
from cura.UI.MachineSettingsManager import MachineSettingsManager
from cura.UI.ObjectsModel import ObjectsModel
from cura.UI.RecommendedMode import RecommendedMode
from cura.UI.TextManager import TextManager
from cura.UI.WelcomePagesModel import WelcomePagesModel
from cura.UI.WhatsNewPagesModel import WhatsNewPagesModel
from cura.UltimakerCloud import UltimakerCloudConstants
from cura.Utils.NetworkingUtil import NetworkingUtil
from cura.Utils.SceneNodeUtils import groupNodesOperation, ungroupNodesOperation
from cura.CrashHandler import CrashHandler


from hasplib import haspChecker

#-------------------------------
from cura.Utils.PCBHandler.PCBUtils.ContentValidation import ContentValidation
#-------------------------------



try:
    from cura.Utils.PCBHandler.Gerber.GerberFileHandler import GerberFileHandler
    from cura.Utils.PCBHandler.Gerber.ReadGerberJob import ReadPCBJob
except:
    print("no Nano-Dimension plugins")

from . import BuildVolume
from . import CameraAnimation
from . import CuraActions
from . import PlatformPhysics
from . import PrintJobPreviewImageProvider
from .AutoSave import AutoSave
from .SingleInstance import SingleInstance

if TYPE_CHECKING:
    from UM.Settings.EmptyInstanceContainer import EmptyInstanceContainer

numpy.seterr(all="ignore")



class CuraApplication(QtApplication):
    # SettingVersion represents the set of settings available in the machine/extruder definitions.
    # You need to make sure that this version number needs to be increased if there is any non-backwards-compatible
    # changes of the settings.
    SettingVersion = 17

    Created = False

    class ResourceTypes:
        QmlFiles = Resources.UserType + 1
        Firmware = Resources.UserType + 2
        QualityInstanceContainer = Resources.UserType + 3
        QualityChangesInstanceContainer = Resources.UserType + 4
        MaterialInstanceContainer = Resources.UserType + 5
        VariantInstanceContainer = Resources.UserType + 6
        UserInstanceContainer = Resources.UserType + 7
        MachineStack = Resources.UserType + 8
        ExtruderStack = Resources.UserType + 9
        DefinitionChangesContainer = Resources.UserType + 10
        SettingVisibilityPreset = Resources.UserType + 11
        IntentInstanceContainer = Resources.UserType + 12

    Q_ENUMS(ResourceTypes)

    def __init__(self, *args, **kwargs):
        super().__init__(name=ApplicationMetadata.CuraAppName,
                         app_display_name=ApplicationMetadata.CuraAppDisplayName,
                         version=ApplicationMetadata.CuraVersion,
                         api_version=ApplicationMetadata.CuraSDKVersion,
                         build_type=ApplicationMetadata.CuraBuildType,
                         is_debug_mode=ApplicationMetadata.CuraDebugMode,
                         tray_icon_name="switch-icon-32.png",
                         **kwargs)

        self.default_theme = "dark-nano-dimension"

        self.change_log_url = "https://ultimaker.com/ultimaker-cura-latest-features"

        self._boot_loading_time = time.time()

        self._on_exit_callback_manager = OnExitCallbackManager(self)

        # Variables set from CLI
        self._files_to_open = []
        self._use_single_instance = False
        self._skip_user_interaction = False  # This should be used to skip opening dialogs
        self._process_tray_after_loading = False
        self._save_tray_to = ""
        self._quit_after_processing = False

        self._single_instance = None

        self._cura_formula_functions = None  # type: Optional[CuraFormulaFunctions]

        self._machine_action_manager = None  # type: Optional[MachineActionManager.MachineActionManager]

        self._crash_handler = None  # type: Optional[CrashHandler]

        self.empty_container = None  # type: EmptyInstanceContainer
        self.empty_definition_changes_container = None  # type: EmptyInstanceContainer
        self.empty_variant_container = None  # type: EmptyInstanceContainer
        self.empty_intent_container = None  # type: EmptyInstanceContainer
        self.empty_material_container = None  # type: EmptyInstanceContainer
        self.empty_quality_container = None  # type: EmptyInstanceContainer
        self.empty_quality_changes_container = None  # type: EmptyInstanceContainer

        self._material_manager = None
        self._machine_manager = None
        self._extruder_manager = None
        self._container_manager = None

        self._object_manager = None
        self._extruders_model = None
        self._extruders_model_with_optional = None
        self._build_plate_model = None
        self._multi_build_plate_model = None
        self._setting_visibility_presets_model = None
        self._setting_inheritance_manager = None
        self._simple_mode_settings_manager = None
        self._cura_scene_controller = None
        self._machine_error_checker = None

        self._licenses = {}  # Keeps handles of all acquired licenses - {name: handle}

        self._machine_settings_manager = MachineSettingsManager(self, parent=self)
        self._material_management_model = None
        self._quality_management_model = None

        self._discovered_printer_model = DiscoveredPrintersModel(self, parent=self)
        self._discovered_cloud_printers_model = DiscoveredCloudPrintersModel(self, parent=self)
        self._first_start_machine_actions_model = None
        self._welcome_pages_model = WelcomePagesModel(self, parent=self)
        self._add_printer_pages_model = AddPrinterPagesModel(self, parent=self)
        self._add_printer_pages_model_without_cancel = AddPrinterPagesModel(self, parent=self)
        self._whats_new_pages_model = WhatsNewPagesModel(self, parent=self)
        self._text_manager = TextManager(parent=self)

        self._quality_profile_drop_down_menu_model = None
        self._custom_quality_profile_drop_down_menu_model = None
        self._cura_API = CuraAPI(self)

        self._temp_dir = TemporaryDirectory()
        Logger.log("d", "Created a temporary directory at {}".format(self._temp_dir))

        self._physics = None
        self._volume = None
        self._output_devices = {}
        self._print_information = None
        self._previous_active_tool = None
        self._platform_activity = False
        self._scene_bounding_box = AxisAlignedBox.Null

        self._center_after_select = False
        self._camera_animation = None
        self._cura_actions = None
        self.started = False

        self._message_box_callback = None
        self._message_box_callback_arguments = []
        self._i18n_catalog = None

        self._currently_loading_files = []
        self._non_sliceable_extensions = []
        self._additional_components = {}  # Components to add to certain areas in the interface

        self._open_file_queue = []  # A list of files to open (after the application has started)

        self._update_platform_activity_timer = None

        self._sidebar_custom_menu_items = []  # type: list # Keeps list of custom menu items for the side bar

        self._plugins_loaded = False

        # Backups
        self._auto_save = None  # type: Optional[AutoSave]
        self._enable_save = True

        self._container_registry_class = CuraContainerRegistry
        # Redefined here in order to please the typing.
        self._container_registry = None  # type: CuraContainerRegistry
        from cura.CuraPackageManager import CuraPackageManager
        self._package_manager_class = CuraPackageManager


    @pyqtProperty(str, constant=True)
    def ultimakerCloudApiRootUrl(self) -> str:
        return UltimakerCloudConstants.CuraCloudAPIRoot

    @pyqtProperty(str, constant=True)
    def ultimakerCloudAccountRootUrl(self) -> str:
        return UltimakerCloudConstants.CuraCloudAccountAPIRoot

    @pyqtProperty(str, constant=True)
    def ultimakerDigitalFactoryUrl(self) -> str:
        return UltimakerCloudConstants.CuraDigitalFactoryURL

    def addCommandLineOptions(self):
        """Adds command line options to the command line parser.

        This should be called after the application is created and before the pre-start.
        """

        super().addCommandLineOptions()
        self._cli_parser.add_argument("--help", "-h",
                                      action="store_true",
                                      default=False,
                                      help="Show this help message and exit.")

        self._cli_parser.add_argument("--single-instance",
                                      dest="single_instance",
                                      action="store_true",
                                      default=False)
        # >> For debugging
        # Trigger an early crash, i.e. a crash that happens before the application enters its event loop.
        self._cli_parser.add_argument("--trigger-early-crash",
                                      dest="trigger_early_crash",
                                      action="store_true",
                                      default=False,
                                      help="FOR TESTING ONLY. Trigger an early crash to show the crash dialog.")

        self._cli_parser.add_argument("--skip-user-interaction",
                                      dest="skip_user_interaction",
                                      action="store_true",
                                      default=False,
                                      help="Set this flag to skip dialogs requiring user interaction")

        self._cli_parser.add_argument("--process-tray-after-loading",
                                      dest="process_tray_after_loading",
                                      action="store_true",
                                      default=False,
                                      help="Set this flag to process the tray after it finished loading")

        self._cli_parser.add_argument("--save-tray-to",
                                      dest="save_tray_to",
                                      type=str,
                                      default="",
                                      help="Set this variable to save the processed tray at this path")

        self._cli_parser.add_argument("--quit-after-processing",
                                      dest="quit_after_processing",
                                      action="store_true",
                                      default=False,
                                      help="Set this flag to exit the application after processing (and saving) tray")
        
        self._cli_parser.add_argument("file", nargs="*", help="Files to load after starting the application.")

    def getContainerRegistry(self) -> "CuraContainerRegistry":
        return self._container_registry

    def getDoSkipUserInteraction(self) -> bool:
        return self._skip_user_interaction

    def parseCliOptions(self):
        super().parseCliOptions()

        if self._cli_args.help:
            self._cli_parser.print_help()
            sys.exit(0)

        self._use_single_instance = self._cli_args.single_instance
        # FOR TESTING ONLY
        if self._cli_args.trigger_early_crash:
            assert not "This crash is triggered by the trigger_early_crash command line argument."

        self._skip_user_interaction = self._cli_args.skip_user_interaction

        self._process_tray_after_loading = self._cli_args.process_tray_after_loading

        self._save_tray_to = self._cli_args.save_tray_to

        self._quit_after_processing = self._cli_args.quit_after_processing

        for filename in self._cli_args.file:
            self._files_to_open.append(os.path.abspath(filename))

    def initialize(self) -> None:
        self.__addExpectedResourceDirsAndSearchPaths()  # Must be added before init of super

        super().initialize()

        self._preferences.addPreference("cura/single_instance", False)
        self._use_single_instance = self._preferences.getValue("cura/single_instance")
        self._gerber_file_handler = GerberFileHandler(self)
        self.__sendCommandToSingleInstance()
        self._initializeSettingDefinitions()
        self._initializeSettingFunctions()
        self.__addAllResourcesAndContainerResources()
        self.__addAllEmptyContainers()
        self.__setLatestResouceVersionsForVersionUpgrade()

        self._machine_action_manager = MachineActionManager.MachineActionManager(self)
        self._machine_action_manager.initialize()

    def __sendCommandToSingleInstance(self):
        self._single_instance = SingleInstance(self, self._files_to_open)

        # If we use single instance, try to connect to the single instance server, send commands, and then exit.
        # If we cannot find an existing single instance server, this is the only instance, so just keep going.
        if self._use_single_instance:
            if self._single_instance.startClient():
                Logger.log("i", "Single instance commands were sent, exiting")
                sys.exit(0)

    def __addExpectedResourceDirsAndSearchPaths(self):
        """Adds expected directory names and search paths for Resources."""

        # this list of dir names will be used by UM to detect an old cura directory
        for dir_name in ["extruders", "machine_instances", "materials", "plugins", "quality", "quality_changes", "user",
                         "variants", "intent"]:
            Resources.addExpectedDirNameInData(dir_name)

        app_root = os.path.abspath(os.path.join(os.path.dirname(sys.executable)))
        Resources.addSearchPath(os.path.join(app_root, "share", "cura", "resources"))

        Resources.addSearchPath(os.path.join(self._app_install_dir, "share", "cura", "resources"))
        if not hasattr(sys, "frozen"):
            resource_path = os.path.join(os.path.abspath(os.path.dirname(__file__)), "..", "resources")
            Resources.addSearchPath(resource_path)

    @classmethod
    def _initializeSettingDefinitions(cls):
        # Need to do this before ContainerRegistry tries to load the machines
        SettingDefinition.addSupportedProperty("settable_per_mesh", DefinitionPropertyType.Any, default=True,
                                               read_only=True)
        SettingDefinition.addSupportedProperty("settable_per_extruder", DefinitionPropertyType.Any, default=True,
                                               read_only=True)
        # this setting can be changed for each group in one-at-a-time mode
        SettingDefinition.addSupportedProperty("settable_per_meshgroup", DefinitionPropertyType.Any, default=True,
                                               read_only=True)
        SettingDefinition.addSupportedProperty("settable_globally", DefinitionPropertyType.Any, default=True,
                                               read_only=True)

        # From which stack the setting would inherit if not defined per object (handled in the engine)
        # AND for settings which are not settable_per_mesh:
        # which extruder is the only extruder this setting is obtained from
        SettingDefinition.addSupportedProperty("limit_to_extruder", DefinitionPropertyType.Function, default="-1",
                                               depends_on="value")

        # For settings which are not settable_per_mesh and not settable_per_extruder:
        # A function which determines the glabel/meshgroup value by looking at the values of the setting in all (used) extruders
        SettingDefinition.addSupportedProperty("resolve", DefinitionPropertyType.Function, default=None,
                                               depends_on="value")

        SettingDefinition.addSettingType("extruder", None, str, Validator)
        SettingDefinition.addSettingType("optional_extruder", None, str, None)
        SettingDefinition.addSettingType("[int]", None, str, None)

    def _initializeSettingFunctions(self):
        """Adds custom property types, settings types, and extra operators (functions).

        Whom need to be registered in SettingDefinition and SettingFunction.
        """

        self._cura_formula_functions = CuraFormulaFunctions(self)

        SettingFunction.registerOperator("extruderValue", self._cura_formula_functions.getValueInExtruder)
        SettingFunction.registerOperator("extruderValues", self._cura_formula_functions.getValuesInAllExtruders)
        SettingFunction.registerOperator("resolveOrValue", self._cura_formula_functions.getResolveOrValue)
        SettingFunction.registerOperator("defaultExtruderPosition",
                                         self._cura_formula_functions.getDefaultExtruderPosition)
        SettingFunction.registerOperator("valueFromContainer",
                                         self._cura_formula_functions.getValueFromContainerAtIndex)
        SettingFunction.registerOperator("extruderValueFromContainer",
                                         self._cura_formula_functions.getValueFromContainerAtIndexInExtruder)

    def __addAllResourcesAndContainerResources(self) -> None:
        """Adds all resources and container related resources."""

        Resources.addStorageType(self.ResourceTypes.QualityInstanceContainer, "quality")
        Resources.addStorageType(self.ResourceTypes.QualityChangesInstanceContainer, "quality_changes")
        Resources.addStorageType(self.ResourceTypes.VariantInstanceContainer, "variants")
        Resources.addStorageType(self.ResourceTypes.MaterialInstanceContainer, "materials")
        Resources.addStorageType(self.ResourceTypes.UserInstanceContainer, "user")
        Resources.addStorageType(self.ResourceTypes.ExtruderStack, "extruders")
        Resources.addStorageType(self.ResourceTypes.MachineStack, "machine_instances")
        Resources.addStorageType(self.ResourceTypes.DefinitionChangesContainer, "definition_changes")
        Resources.addStorageType(self.ResourceTypes.SettingVisibilityPreset, "setting_visibility")
        Resources.addStorageType(self.ResourceTypes.IntentInstanceContainer, "intent")

        self._container_registry.addResourceType(self.ResourceTypes.QualityInstanceContainer, "quality")
        self._container_registry.addResourceType(self.ResourceTypes.QualityChangesInstanceContainer, "quality_changes")
        self._container_registry.addResourceType(self.ResourceTypes.VariantInstanceContainer, "variant")
        self._container_registry.addResourceType(self.ResourceTypes.MaterialInstanceContainer, "material")
        self._container_registry.addResourceType(self.ResourceTypes.UserInstanceContainer, "user")
        self._container_registry.addResourceType(self.ResourceTypes.ExtruderStack, "extruder_train")
        self._container_registry.addResourceType(self.ResourceTypes.MachineStack, "machine")
        self._container_registry.addResourceType(self.ResourceTypes.DefinitionChangesContainer, "definition_changes")
        self._container_registry.addResourceType(self.ResourceTypes.IntentInstanceContainer, "intent")

        Resources.addType(self.ResourceTypes.QmlFiles, "qml")
        Resources.addType(self.ResourceTypes.Firmware, "firmware")

    def __addAllEmptyContainers(self) -> None:
        """Adds all empty containers."""

        # Add empty variant, material and quality containers.
        # Since they are empty, they should never be serialized and instead just programmatically created.
        # We need them to simplify the switching between materials.
        self.empty_container = cura.Settings.cura_empty_instance_containers.empty_container

        self._container_registry.addContainer(
            cura.Settings.cura_empty_instance_containers.empty_definition_changes_container)
        self.empty_definition_changes_container = cura.Settings.cura_empty_instance_containers.empty_definition_changes_container

        self._container_registry.addContainer(cura.Settings.cura_empty_instance_containers.empty_variant_container)
        self.empty_variant_container = cura.Settings.cura_empty_instance_containers.empty_variant_container

        self._container_registry.addContainer(cura.Settings.cura_empty_instance_containers.empty_intent_container)
        self.empty_intent_container = cura.Settings.cura_empty_instance_containers.empty_intent_container

        self._container_registry.addContainer(cura.Settings.cura_empty_instance_containers.empty_material_container)
        self.empty_material_container = cura.Settings.cura_empty_instance_containers.empty_material_container

        self._container_registry.addContainer(cura.Settings.cura_empty_instance_containers.empty_quality_container)
        self.empty_quality_container = cura.Settings.cura_empty_instance_containers.empty_quality_container

        self._container_registry.addContainer(
            cura.Settings.cura_empty_instance_containers.empty_quality_changes_container)
        self.empty_quality_changes_container = cura.Settings.cura_empty_instance_containers.empty_quality_changes_container

    def __setLatestResouceVersionsForVersionUpgrade(self):
        """Initializes the version upgrade manager with by providing the paths for each resource type and the latest
        versions. """

        self._version_upgrade_manager.setCurrentVersions(
            {
                ("quality", InstanceContainer.Version * 1000000 + self.SettingVersion): (
                    self.ResourceTypes.QualityInstanceContainer, "application/x-uranium-instancecontainer"),
                ("quality_changes", InstanceContainer.Version * 1000000 + self.SettingVersion): (
                    self.ResourceTypes.QualityChangesInstanceContainer, "application/x-uranium-instancecontainer"),
                ("intent", InstanceContainer.Version * 1000000 + self.SettingVersion): (
                    self.ResourceTypes.IntentInstanceContainer, "application/x-uranium-instancecontainer"),
                ("machine_stack", GlobalStack.Version * 1000000 + self.SettingVersion): (
                    self.ResourceTypes.MachineStack, "application/x-cura-globalstack"),
                ("extruder_train", ExtruderStack.Version * 1000000 + self.SettingVersion): (
                    self.ResourceTypes.ExtruderStack, "application/x-cura-extruderstack"),
                ("preferences", Preferences.Version * 1000000 + self.SettingVersion): (
                    Resources.Preferences, "application/x-uranium-preferences"),
                ("user", InstanceContainer.Version * 1000000 + self.SettingVersion): (
                    self.ResourceTypes.UserInstanceContainer, "application/x-uranium-instancecontainer"),
                ("definition_changes", InstanceContainer.Version * 1000000 + self.SettingVersion): (
                    self.ResourceTypes.DefinitionChangesContainer, "application/x-uranium-instancecontainer"),
                ("variant", InstanceContainer.Version * 1000000 + self.SettingVersion): (
                    self.ResourceTypes.VariantInstanceContainer, "application/x-uranium-instancecontainer"),
            }
        )

    def startSplashWindowPhase(self) -> None:
        """Runs preparations that needs to be done before the starting process."""

        super().startSplashWindowPhase()

        acquired = haspChecker.acquireArtwork()
        self._licenses.update(acquired)  # Acquire artwork license
        haspChecker.AcquiredLicenses.update(acquired)

        if not self.getIsHeadLess():
            try:
                self.setWindowIcon(QIcon(Resources.getPath(Resources.Images, "switch-icon.png")))
            except FileNotFoundError:
                Logger.log("w", "Unable to find the window icon.")

        self.setRequiredPlugins([
            # Misc.:
            "CuraNanoBackend",
            "ConsoleLogger",  # You want to be able to read the log if something goes wrong.
            "FileLogger",  # You want to be able to read the log if something goes wrong.
            "XmlMaterialProfile",  # Cura crashes without this one.
            "Toolbox",
            # This contains the interface to enable/disable plug-ins, so if you disable it you can't enable it back.
            "GerberReader",
            "ODBReader",
            "Routes",
            "Stackup",

            "PrepareStage",  # Cura is useless without this one since you can't load models.
            # "PreviewStage",  # This shows the list of the plugin views that are installed in Cura.
            # "MonitorStage",  # Major part of Cura's functionality.
            "LocalFileOutputDevice",  # Major part of Cura's functionality.
            "LocalContainerProvider",  # Cura is useless without any profiles or setting definitions.

            # Views:
            "SimpleView",  # Dependency of SolidView.
            "SolidView",  # Displays models. Cura is useless without it.

            # Readers & Writers:
            # "GCodeWriter",  # Cura is useless if it can't write its output.
            "STLReader",  # Most common model format, so disabling this makes Cura 90% useless.
            "3MFWriter",  # Required for writing project files.

            # Tools:
            "CameraTool",  # Needed to see the scene. Cura is useless without it.
            "SelectionTool",  # Dependency of the rest of the tools.
            "TranslateTool",  # You'll need this for almost every print.
        ])
        self._i18n_catalog = i18nCatalog("cura")

        self._update_platform_activity_timer = QTimer()
        self._update_platform_activity_timer.setInterval(500)
        self._update_platform_activity_timer.setSingleShot(True)
        self._update_platform_activity_timer.timeout.connect(self.updatePlatformActivity)

        self.getController().getScene().sceneChanged.connect(self.updatePlatformActivityDelayed)
        self.getController().toolOperationStopped.connect(self._onToolOperationStopped)
        self.getController().contextMenuRequested.connect(self._onContextMenuRequested)
        self.getCuraSceneController().activeBuildPlateChanged.connect(self.updatePlatformActivityDelayed)

        self._setLoadingHint(self._i18n_catalog.i18nc("@info:progress", "Loading machines..."))

        self._container_registry.allMetadataLoaded.connect(ContainerRegistry.getInstance)

        with self._container_registry.lockFile():
            self._container_registry.loadAllMetadata()

        self._setLoadingHint(self._i18n_catalog.i18nc("@info:progress", "Setting up preferences..."))
        # Set the setting version for Preferences
        preferences = self.getPreferences()
        preferences.addPreference("metadata/setting_version", 0)
        preferences.setValue("metadata/setting_version",
                             self.SettingVersion)  # Don't make it equal to the default so that the setting version always gets written to the file.

        preferences.addPreference("cura/active_mode", "simple")

        preferences.addPreference("cura/categories_expanded", "")
        preferences.addPreference("cura/jobname_prefix", True)
        preferences.addPreference("cura/select_models_on_load", False)
        preferences.addPreference("view/center_on_select", False)
        preferences.addPreference("mesh/scale_to_fit", False)
        preferences.addPreference("mesh/scale_tiny_meshes", True)
        preferences.addPreference("cura/dialog_on_project_save", True)
        preferences.addPreference("cura/asked_dialog_on_project_save", False)
        preferences.addPreference("cura/choice_on_profile_override", "always_ask")
        preferences.addPreference("cura/choice_on_open_project", "open_as_project")
        preferences.addPreference("cura/use_multi_build_plate", False)
        preferences.addPreference("cura/show_list_of_objects", False)
        preferences.addPreference("view/settings_list_height", 400)
        preferences.addPreference("view/settings_visible", False)
        preferences.addPreference("view/preview_visible", False)
        preferences.addPreference("view/settings_xpos", 0)
        preferences.addPreference("view/settings_ypos", 56)
        preferences.addPreference("view/preview_xpos", 0)
        preferences.addPreference("view/preview_ypos", 56)
        preferences.addPreference("view/colorscheme_xpos", 0)
        preferences.addPreference("view/colorscheme_ypos", 56)
        preferences.addPreference("cura/currency", "€")
        preferences.addPreference("cura/material_settings", "{}")

        preferences.addPreference("cura/center_models_on_load", True)
        preferences.addPreference("cura/arrange_models_on_load", False)

        preferences.addPreference("view/invert_zoom", False)
        preferences.addPreference("view/filter_current_build_plate", False)
        preferences.addPreference("cura/sidebar_collapsed", False)

        preferences.addPreference("cura/favorite_materials", "")
        preferences.addPreference("cura/expanded_brands", "")
        preferences.addPreference("cura/expanded_types", "")

        preferences.addPreference("general/accepted_user_agreement", False)

        for key in [
            "dialog_load_path",  # dialog_save_path is in LocalFileOutputDevicePlugin
            "dialog_profile_path",
            "dialog_material_path"]:
            preferences.addPreference("local_file/%s" % key, os.path.expanduser("~/"))

        preferences.setDefault("local_file/last_used_type", "text/x-gcode")

        self.applicationShuttingDown.connect(self.saveSettings)
        self.engineCreatedSignal.connect(self._onEngineCreated)

        self.getCuraSceneController().setActiveBuildPlate(0)  # Initialize

        CuraApplication.Created = True

    def _onEngineCreated(self):
        self._qml_engine.addImageProvider("print_job_preview",
                                          PrintJobPreviewImageProvider.PrintJobPreviewImageProvider())

    @pyqtProperty(bool)
    def needToShowUserAgreement(self) -> bool:
        return not UM.Util.parseBool(self.getPreferences().getValue("general/accepted_user_agreement"))

    @pyqtSlot(bool)
    def setNeedToShowUserAgreement(self, set_value: bool = True) -> None:
        self.getPreferences().setValue("general/accepted_user_agreement", str(not set_value))

    @pyqtSlot(str, str)
    def writeToLog(self, severity: str, message: str) -> None:
        Logger.log(severity, message)

    # DO NOT call this function to close the application, use checkAndExitApplication() instead which will perform
    # pre-exit checks such as checking for in-progress USB printing, etc.
    # Except for the 'Decline and close' in the 'User Agreement'-step in the Welcome-pages, that should be a hard exit.
    @pyqtSlot()
    def closeApplication(self) -> None:
        Logger.log("i", "Close application")
        Logger.log("d", "removing the temporary directory at {}".format(self._temp_dir))

        if self.backend is not None:
            self.backend.stopProcessing()
        kill_all_subprocess()
        self._temp_dir.cleanup()
        main_window = self.getMainWindow()
        self._release_licenses()
        if main_window is not None:
            main_window.close()
        else:
            self.exit(0)

    def _release_licenses(self):
        haspChecker.AcquiredLicenses = {}
        for lic, handle in self._licenses.items():
            haspChecker.releaseLicense(handle)

    # This function first performs all upon-exit checks such as USB printing that is in progress.
    # Use this to close the application.
    @pyqtSlot()
    def checkAndExitApplication(self) -> None:
        self._on_exit_callback_manager.resetCurrentState()
        self._on_exit_callback_manager.triggerNextCallback()

    @pyqtSlot(result=bool)
    def getIsAllChecksPassed(self) -> bool:
        return self._on_exit_callback_manager.getIsAllChecksPassed()

    def getOnExitCallbackManager(self) -> "OnExitCallbackManager":
        return self._on_exit_callback_manager

    def getTempDir(self) -> TemporaryDirectory:
        return self._temp_dir

    def triggerNextExitCheck(self) -> None:
        self._on_exit_callback_manager.triggerNextCallback()

    showConfirmExitDialog = pyqtSignal(str, arguments=["message"])

    def setConfirmExitDialogCallback(self, callback: Callable) -> None:
        self._confirm_exit_dialog_callback = callback

    @pyqtSlot(bool)
    def callConfirmExitDialogCallback(self, yes_or_no: bool) -> None:
        self._confirm_exit_dialog_callback(yes_or_no)

    showPreferencesWindow = pyqtSignal()
    """Signal to connect preferences action in QML"""

    @pyqtSlot()
    def showPreferences(self) -> None:
        """Show the preferences window"""

        self.showPreferencesWindow.emit()

    # This is called by drag-and-dropping curapackage files.
    @pyqtSlot(QUrl)
    def installPackageViaDragAndDrop(self, file_url: str) -> Optional[str]:
        filename = QUrl(file_url).toLocalFile()
        return self._package_manager.installPackage(filename)

    @override(Application)
    def getGlobalContainerStack(self) -> Optional["GlobalStack"]:
        return self._global_container_stack

    @override(Application)
    def setGlobalContainerStack(self, stack: Optional["GlobalStack"]) -> None:
        self._setLoadingHint(self._i18n_catalog.i18nc("@info:progress", "Initializing Active Machine..."))
        super().setGlobalContainerStack(stack)

    showMessageBox = pyqtSignal(str, str, str, str, int, int,
                                arguments=["title", "text", "informativeText", "detailedText", "buttons", "icon"])
    """A reusable dialogbox"""

    def messageBox(self, title, text,
                   informativeText="",
                   detailedText="",
                   buttons=QMessageBox.Ok,
                   icon=QMessageBox.NoIcon,
                   callback=None,
                   callback_arguments=[]
                   ):
        self._message_box_callback = callback
        self._message_box_callback_arguments = callback_arguments
        self.showMessageBox.emit(title, text, informativeText, detailedText, buttons, icon)

    showDiscardOrKeepProfileChanges = pyqtSignal()

    def discardOrKeepProfileChanges(self) -> bool:
        has_user_interaction = False
        choice = self.getPreferences().getValue("cura/choice_on_profile_override")
        if choice == "always_discard":
            # don't show dialog and DISCARD the profile
            self.discardOrKeepProfileChangesClosed("discard")
        elif choice == "always_keep":
            # don't show dialog and KEEP the profile
            self.discardOrKeepProfileChangesClosed("keep")
        elif not self._is_headless:
            # ALWAYS ask whether to keep or discard the profile
            self.showDiscardOrKeepProfileChanges.emit()
            has_user_interaction = True
        return has_user_interaction

    @pyqtSlot(str)
    def discardOrKeepProfileChangesClosed(self, option: str) -> None:
        global_stack = self.getGlobalContainerStack()
        if option == "discard":
            for extruder in global_stack.extruderList:
                extruder.userChanges.clear()
            global_stack.userChanges.clear()

        # if the user decided to keep settings then the user settings should be re-calculated and validated for errors
        # before slicing. To ensure that slicer uses right settings values
        elif option == "keep":
            for extruder in global_stack.extruderList:
                extruder.userChanges.update()
            global_stack.userChanges.update()

    @pyqtSlot(int)
    def messageBoxClosed(self, button):
        if self._message_box_callback:
            self._message_box_callback(button, *self._message_box_callback_arguments)
            self._message_box_callback = None
            self._message_box_callback_arguments = []

    def enableSave(self, enable: bool):
        self._enable_save = enable

    # Cura has multiple locations where instance containers need to be saved, so we need to handle this differently.
    def saveSettings(self) -> None:
        if not self.started or not self._enable_save:
            # Do not do saving during application start or when data should not be saved on quit.
            return
        ContainerRegistry.getInstance().saveDirtyContainers()
        self.savePreferences()

    def saveStack(self, stack):
        if not self._enable_save:
            return
        ContainerRegistry.getInstance().saveContainer(stack)

    @pyqtSlot(str, result=QUrl)
    def getDefaultPath(self, key):
        default_path = self.getPreferences().getValue("local_file/%s" % key)
        return QUrl.fromLocalFile(default_path)

    @pyqtSlot(str, str)
    def setDefaultPath(self, key, default_path):
        self.getPreferences().setValue("local_file/%s" % key, QUrl(default_path).toLocalFile())

    def _loadPlugins(self) -> None:
        """Handle loading of all plugin types (and the backend explicitly)

        :py:class:`Uranium.UM.PluginRegistry`
        """

        self._plugin_registry.setCheckIfTrusted(ApplicationMetadata.IsEnterpriseVersion)

        self._plugin_registry.addType("profile_reader", self._addProfileReader)
        self._plugin_registry.addType("profile_writer", self._addProfileWriter)

        if Platform.isLinux():
            lib_suffixes = {"", "64", "32", "x32"}  # A few common ones on different distributions.
        else:
            lib_suffixes = {""}
        for suffix in lib_suffixes:
            self._plugin_registry.addPluginLocation(
                os.path.join(QtApplication.getInstallPrefix(), "lib" + suffix, "cura"))
        if not hasattr(sys, "frozen"):
            self._plugin_registry.addPluginLocation(
                os.path.join(os.path.abspath(os.path.dirname(__file__)), "..", "plugins"))
            self._plugin_registry.preloaded_plugins.append("ConsoleLogger")

        self._plugin_registry.loadPlugins()

        self._plugins_loaded = True

        # these tools are too big to be opened every time, we will black-list them
        controller = self.getController()
        all_tools = controller.getAllTools()
        self._large_area_tools = [controller.getTool(tool) for tool in ["Stackup", "Routes", "Materials"]
                                  if tool in all_tools]

    def _setLoadingHint(self, hint: str):
        """Set a short, user-friendly hint about current loading status.

        The way this message is displayed depends on application state
        """

        if self.started:
            Logger.info(hint)
        else:
            self.showSplashMessage(hint)

    def run(self):
        super().run()

        Logger.log("i", "Initializing machine error checker")
        self._machine_error_checker = MachineErrorChecker(self)
        self._machine_error_checker.initialize()
        self.processEvents()

        Logger.log("i", "Initializing machine manager")
        self._setLoadingHint(self._i18n_catalog.i18nc("@info:progress", "Initializing machine manager..."))
        self.getMachineManager()
        self.processEvents()

        Logger.log("i", "Initializing container manager")
        self._container_manager = ContainerManager(self)
        self.processEvents()

        # Check if we should run as single instance or not. If so, set up a local socket server which listener which
        # coordinates multiple Cura instances and accepts commands.
        if self._use_single_instance:
            self.__setUpSingleInstanceServer()

        # Setup scene and build volume
        self._setLoadingHint(self._i18n_catalog.i18nc("@info:progress", "Initializing build volume..."))
        root = self.getController().getScene().getRoot()
        self._volume = BuildVolume.BuildVolume(self, root)

        # Ensure that the old style arranger still works.
        Arrange.build_volume = self._volume

        # initialize info objects
        self._print_information = PrintInformation.PrintInformation(self)
        self._cura_actions = CuraActions.CuraActions(self)
        self.processEvents()
        # Initialize setting visibility presets model.
        self._setting_visibility_presets_model = SettingVisibilityPresetsModel(self.getPreferences(), parent=self)

        # Initialize Cura API
        self._cura_API.initialize()
        self.processEvents()
        self._output_device_manager.start()
        self._welcome_pages_model.initialize()
        self._add_printer_pages_model.initialize()
        self._add_printer_pages_model_without_cancel.initialize(cancellable=False)
        self._whats_new_pages_model.initialize()

        ####### Flight Code
        ContentValidation.ValidateAndQuitOnIssue_WCAD()
        ####### Flight Code


        # Detect in which mode to run and execute that mode
        if self._is_headless:
            self.runWithoutGUI()
        else:
            self.runWithGUI()

        self.started = True
        self.initializationFinished.emit()
        Logger.log("d", "Booting Cura took %s seconds", time.time() - self._boot_loading_time)

        # For now use a timer to postpone some things that need to be done after the application and GUI are
        # initialized, for example opening files because they may show dialogs which can be closed due to incomplete
        # GUI initialization.
        self._post_start_timer = QTimer(self)
        self._post_start_timer.setInterval(1000)
        self._post_start_timer.setSingleShot(True)
        self._post_start_timer.timeout.connect(self._onPostStart)
        self._post_start_timer.start()

        self._auto_save = AutoSave(self)
        self._auto_save.initialize()

        self.exec_()

    def __setUpSingleInstanceServer(self):
        if self._use_single_instance:
            self._single_instance.startServer()

    def _onPostStart(self):
        self.callLater(self._openFiles, self._files_to_open)

        # Open all the files that were queued up while plug-ins were loading.
        self.callLater(self._openFiles, self._open_file_queue)

    initializationFinished = pyqtSignal()
    showAddPrintersUncancellableDialog = pyqtSignal()  # Used to show the add printers dialog with a greyed background

    def runWithoutGUI(self):
        """Run Cura without GUI elements and interaction (server mode)."""

        self.closeSplash()

    def runWithGUI(self):
        """Run Cura with GUI (desktop mode)."""

        self._setLoadingHint(self._i18n_catalog.i18nc("@info:progress", "Setting up scene..."))

        controller = self.getController()

        t = controller.getTool("TranslateTool")
        if t:
            t.setEnabledAxis([ToolHandle.XAxis, ToolHandle.YAxis, ToolHandle.ZAxis])

        Selection.selectionChanged.connect(self.onSelectionChanged)

        # Set default background color for scene
        self.getRenderer().setBackgroundColor(QColor(245, 245, 245))
        self.processEvents()
        # Initialize platform physics
        self._physics = PlatformPhysics.PlatformPhysics(controller, self._volume)

        # Initialize camera
        root = controller.getScene().getRoot()
        camera = Camera("3d", root)
        diagonal = self.getBuildVolume().getDiagonalSize()
        if diagonal < 1:  # No printer added yet. Set a default camera distance for normal-sized printers.
            diagonal = 375
        camera.setPosition(Vector(-80, 250, 700) * diagonal / 375)
        camera.lookAt(Vector(0, 0, 0))
        controller.getScene().setActiveCamera("3d")

        # Initialize camera tool
        camera_tool = controller.getTool("CameraTool")
        if camera_tool:
            camera_tool.setOrigin(Vector(0, 100, 0))
            camera_tool.setZoomRange(0.1, 2000)

        # Initialize camera animations
        self._camera_animation = CameraAnimation.CameraAnimation()
        self._camera_animation.setCameraTool(self.getController().getTool("CameraTool"))

        self._setLoadingHint(self._i18n_catalog.i18nc("@info:progress", "Loading interface..."))

        # Initialize QML engine
        self.setMainQml(Resources.getPath(self.ResourceTypes.QmlFiles, "Cura.qml"))
        self._qml_import_paths.append(Resources.getPath(self.ResourceTypes.QmlFiles))
        self._setLoadingHint(self._i18n_catalog.i18nc("@info:progress", "Initializing engine..."))
        self.initializeEngine()

        # Initialize UI state
        controller.setActiveStage("PrepareStage")
        controller.setActiveView("SolidView")
        controller.setCameraTool("CameraTool")
        controller.setSelectionTool("SelectionTool")

        # Hide the splash screen
        self.closeSplash()

    @pyqtSlot(result=CrashHandler)
    def getCrashHandler(self, *args):
        return self._crash_handler

    def setCrashHandler(self, crash_handler: CrashHandler, *args) -> None:
        self._crash_handler = crash_handler

    @pyqtSlot(result=QObject)
    def getDiscoveredPrintersModel(self, *args) -> "DiscoveredPrintersModel":
        return self._discovered_printer_model

    @pyqtSlot(result=QObject)
    def getDiscoveredCloudPrintersModel(self, *args) -> "DiscoveredCloudPrintersModel":
        return self._discovered_cloud_printers_model

    @pyqtSlot(result=QObject)
    def getFirstStartMachineActionsModel(self, *args) -> "FirstStartMachineActionsModel":
        if self._first_start_machine_actions_model is None:
            self._first_start_machine_actions_model = FirstStartMachineActionsModel(self, parent=self)
            if self.started:
                self._first_start_machine_actions_model.initialize()
        return self._first_start_machine_actions_model

    @pyqtSlot(result=QObject)
    def getSettingVisibilityPresetsModel(self, *args) -> SettingVisibilityPresetsModel:
        return self._setting_visibility_presets_model

    @pyqtSlot(result=QObject)
    def getWelcomePagesModel(self, *args) -> "WelcomePagesModel":
        return self._welcome_pages_model

    @pyqtSlot(result=QObject)
    def getAddPrinterPagesModel(self, *args) -> "AddPrinterPagesModel":
        return self._add_printer_pages_model

    @pyqtSlot(result=QObject)
    def getAddPrinterPagesModelWithoutCancel(self, *args) -> "AddPrinterPagesModel":
        return self._add_printer_pages_model_without_cancel

    @pyqtSlot(result=QObject)
    def getWhatsNewPagesModel(self, *args) -> "WhatsNewPagesModel":
        return self._whats_new_pages_model

    @pyqtSlot(result=QObject)
    def getMachineSettingsManager(self, *args) -> "MachineSettingsManager":
        return self._machine_settings_manager

    @pyqtSlot(result=QObject)
    def getTextManager(self, *args) -> "TextManager":
        return self._text_manager

    def getCuraFormulaFunctions(self, *args) -> "CuraFormulaFunctions":
        if self._cura_formula_functions is None:
            self._cura_formula_functions = CuraFormulaFunctions(self)
        return self._cura_formula_functions

    def getMachineErrorChecker(self, *args) -> MachineErrorChecker:
        return self._machine_error_checker

    def getMachineManager(self, *args) -> MachineManager:
        if self._machine_manager is None:
            self._machine_manager = MachineManager(self, parent=self)
        return self._machine_manager

    def getExtruderManager(self, *args) -> ExtruderManager:
        if self._extruder_manager is None:
            self._extruder_manager = ExtruderManager()
        return self._extruder_manager

    def getIntentManager(self, *args) -> IntentManager:
        return IntentManager.getInstance()

    def getObjectsModel(self, *args):
        if self._object_manager is None:
            self._object_manager = ObjectsModel(self)
        return self._object_manager

    @pyqtSlot(result=QObject)
    def getExtrudersModel(self, *args) -> "ExtrudersModel":
        if self._extruders_model is None:
            self._extruders_model = ExtrudersModel(self)
        return self._extruders_model

    @pyqtSlot(result=QObject)
    def getExtrudersModelWithOptional(self, *args) -> "ExtrudersModel":
        if self._extruders_model_with_optional is None:
            self._extruders_model_with_optional = ExtrudersModel(self)
            self._extruders_model_with_optional.setAddOptionalExtruder(True)
        return self._extruders_model_with_optional

    @pyqtSlot(result=QObject)
    def getMultiBuildPlateModel(self, *args) -> MultiBuildPlateModel:
        if self._multi_build_plate_model is None:
            self._multi_build_plate_model = MultiBuildPlateModel(self)
        return self._multi_build_plate_model

    @pyqtSlot(result=QObject)
    def getBuildPlateModel(self, *args) -> BuildPlateModel:
        if self._build_plate_model is None:
            self._build_plate_model = BuildPlateModel(self)
        return self._build_plate_model

    def getCuraSceneController(self, *args) -> CuraSceneController:
        if self._cura_scene_controller is None:
            self._cura_scene_controller = CuraSceneController.createCuraSceneController()
        return self._cura_scene_controller

    def getSettingInheritanceManager(self, *args) -> SettingInheritanceManager:
        if self._setting_inheritance_manager is None:
            self._setting_inheritance_manager = SettingInheritanceManager.createSettingInheritanceManager()
        return self._setting_inheritance_manager

    def getMachineActionManager(self, *args: Any) -> MachineActionManager.MachineActionManager:
        """Get the machine action manager

        We ignore any *args given to this, as we also register the machine manager as qml singleton.
        It wants to give this function an engine and script engine, but we don't care about that.
        """

        return cast(MachineActionManager.MachineActionManager, self._machine_action_manager)

    @pyqtSlot(result=QObject)
    def getMaterialManagementModel(self) -> MaterialManagementModel:
        if not self._material_management_model:
            self._material_management_model = MaterialManagementModel(parent=self)
        return self._material_management_model

    @pyqtSlot(result=QObject)
    def getQualityManagementModel(self) -> QualityManagementModel:
        if not self._quality_management_model:
            self._quality_management_model = QualityManagementModel(parent=self)
        return self._quality_management_model

    def getSimpleModeSettingsManager(self, *args):
        if self._simple_mode_settings_manager is None:
            self._simple_mode_settings_manager = SimpleModeSettingsManager()
        return self._simple_mode_settings_manager

    def event(self, event):
        """Handle Qt events"""

        if event.type() == QEvent.FileOpen:
            if self._plugins_loaded:
                self._openFiles([event.file()])
            else:
                self._open_file_queue.append(event.file())

        return super().event(event)

    def getAutoSave(self) -> Optional[AutoSave]:
        return self._auto_save

    def getPrintInformation(self):
        """Get print information (duration / material used)"""

        return self._print_information

    def getQualityProfilesDropDownMenuModel(self, *args, **kwargs):
        if self._quality_profile_drop_down_menu_model is None:
            self._quality_profile_drop_down_menu_model = QualityProfilesDropDownMenuModel(self)
        return self._quality_profile_drop_down_menu_model

    def getCustomQualityProfilesDropDownMenuModel(self, *args, **kwargs):
        if self._custom_quality_profile_drop_down_menu_model is None:
            self._custom_quality_profile_drop_down_menu_model = CustomQualityProfilesDropDownMenuModel(self)
        return self._custom_quality_profile_drop_down_menu_model

    def getCuraAPI(self, *args, **kwargs) -> "CuraAPI":
        return self._cura_API

    def registerObjects(self, engine):
        """Registers objects for the QML engine to use.

        :param engine: The QML engine.
        """

        super().registerObjects(engine)

        # global contexts
        self.processEvents()
        engine.rootContext().setContextProperty("Printer", self)
        engine.rootContext().setContextProperty("CuraApplication", self)
        engine.rootContext().setContextProperty("PrintInformation", self._print_information)
        engine.rootContext().setContextProperty("CuraActions", self._cura_actions)
        engine.rootContext().setContextProperty("CuraSDKVersion", ApplicationMetadata.CuraSDKVersion)

        self.processEvents()
        qmlRegisterUncreatableType(CuraApplication, "Cura", 1, 0, "ResourceTypes", "Just an Enum type")

        self.processEvents()
        qmlRegisterSingletonType(CuraSceneController, "Cura", 1, 0, "SceneController", self.getCuraSceneController)
        qmlRegisterSingletonType(ExtruderManager, "Cura", 1, 0, "ExtruderManager", self.getExtruderManager)
        qmlRegisterSingletonType(MachineManager, "Cura", 1, 0, "MachineManager", self.getMachineManager)
        qmlRegisterSingletonType(IntentManager, "Cura", 1, 6, "IntentManager", self.getIntentManager)
        qmlRegisterSingletonType(SettingInheritanceManager, "Cura", 1, 0, "SettingInheritanceManager",
                                 self.getSettingInheritanceManager)
        qmlRegisterSingletonType(SimpleModeSettingsManager, "Cura", 1, 0, "SimpleModeSettingsManager",
                                 self.getSimpleModeSettingsManager)
        qmlRegisterSingletonType(MachineActionManager.MachineActionManager, "Cura", 1, 0, "MachineActionManager",
                                 self.getMachineActionManager)
        qmlRegisterSingletonType(GerberFileHandler, "Cura", 1, 0, "GerberFileHandler", GerberFileHandler.getInstance)

        self.processEvents()
        qmlRegisterType(NetworkingUtil, "Cura", 1, 5, "NetworkingUtil")
        qmlRegisterType(WelcomePagesModel, "Cura", 1, 0, "WelcomePagesModel")
        qmlRegisterType(WhatsNewPagesModel, "Cura", 1, 0, "WhatsNewPagesModel")
        qmlRegisterType(AddPrinterPagesModel, "Cura", 1, 0, "AddPrinterPagesModel")
        qmlRegisterType(TextManager, "Cura", 1, 0, "TextManager")
        qmlRegisterType(RecommendedMode, "Cura", 1, 0, "RecommendedMode")

        self.processEvents()
        qmlRegisterType(NetworkMJPGImage, "Cura", 1, 0, "NetworkMJPGImage")
        qmlRegisterType(ObjectsModel, "Cura", 1, 0, "ObjectsModel")
        qmlRegisterType(BuildPlateModel, "Cura", 1, 0, "BuildPlateModel")
        qmlRegisterType(MultiBuildPlateModel, "Cura", 1, 0, "MultiBuildPlateModel")
        qmlRegisterType(InstanceContainer, "Cura", 1, 0, "InstanceContainer")
        qmlRegisterType(ExtrudersModel, "Cura", 1, 0, "ExtrudersModel")
        qmlRegisterType(GlobalStacksModel, "Cura", 1, 0, "GlobalStacksModel")

        self.processEvents()
        # qmlRegisterType(FavoriteMaterialsModel, "Cura", 1, 0, "FavoriteMaterialsModel")
        # qmlRegisterType(GenericMaterialsModel, "Cura", 1, 0, "GenericMaterialsModel")
        qmlRegisterType(MaterialBrandsModel, "Cura", 1, 0, "MaterialBrandsModel")
        qmlRegisterSingletonType(QualityManagementModel, "Cura", 1, 0, "QualityManagementModel",
                                 self.getQualityManagementModel)
        qmlRegisterSingletonType(MaterialManagementModel, "Cura", 1, 5, "MaterialManagementModel",
                                 self.getMaterialManagementModel)

        self.processEvents()
        qmlRegisterType(DiscoveredPrintersModel, "Cura", 1, 0, "DiscoveredPrintersModel")
        qmlRegisterType(DiscoveredCloudPrintersModel, "Cura", 1, 7, "DiscoveredCloudPrintersModel")
        qmlRegisterSingletonType(QualityProfilesDropDownMenuModel, "Cura", 1, 0,
                                 "QualityProfilesDropDownMenuModel", self.getQualityProfilesDropDownMenuModel)
        qmlRegisterSingletonType(CustomQualityProfilesDropDownMenuModel, "Cura", 1, 0,
                                 "CustomQualityProfilesDropDownMenuModel",
                                 self.getCustomQualityProfilesDropDownMenuModel)
        qmlRegisterType(NozzleModel, "Cura", 1, 0, "NozzleModel")
        qmlRegisterType(IntentModel, "Cura", 1, 6, "IntentModel")
        qmlRegisterType(IntentCategoryModel, "Cura", 1, 6, "IntentCategoryModel")

        self.processEvents()
        qmlRegisterType(MaterialSettingsVisibilityHandler, "Cura", 1, 0, "MaterialSettingsVisibilityHandler")
        qmlRegisterType(SettingVisibilityPresetsModel, "Cura", 1, 0, "SettingVisibilityPresetsModel")
        qmlRegisterType(QualitySettingsModel, "Cura", 1, 0, "QualitySettingsModel")
        qmlRegisterType(FirstStartMachineActionsModel, "Cura", 1, 0, "FirstStartMachineActionsModel")
        qmlRegisterType(MachineNameValidator, "Cura", 1, 0, "MachineNameValidator")
        qmlRegisterType(UserChangesModel, "Cura", 1, 0, "UserChangesModel")
        qmlRegisterSingletonType(ContainerManager, "Cura", 1, 0, "ContainerManager", ContainerManager.getInstance)
        qmlRegisterType(SidebarCustomMenuItemsModel, "Cura", 1, 0, "SidebarCustomMenuItemsModel")

        qmlRegisterType(PrinterOutputDevice, "Cura", 1, 0, "PrinterOutputDevice")

        from cura.API import CuraAPI
        qmlRegisterSingletonType(CuraAPI, "Cura", 1, 1, "API", self.getCuraAPI)
        qmlRegisterUncreatableType(Account, "Cura", 1, 0, "AccountSyncState", "Could not create AccountSyncState")

        # As of Qt5.7, it is necessary to get rid of any ".." in the path for the singleton to work.
        actions_url = QUrl.fromLocalFile(
            os.path.abspath(Resources.getPath(CuraApplication.ResourceTypes.QmlFiles, "Actions.qml")))
        qmlRegisterSingletonType(actions_url, "Cura", 1, 0, "Actions")

        for path in Resources.getAllResourcesOfType(CuraApplication.ResourceTypes.QmlFiles):
            type_name = os.path.splitext(os.path.basename(path))[0]
            if type_name in ("Cura", "Actions"):
                continue

            # Ignore anything that is not a QML file.
            if not path.endswith(".qml"):
                continue

            qmlRegisterType(QUrl.fromLocalFile(path), "Cura", 1, 0, type_name)
            self.processEvents()

    def onSelectionChanged(self):
        if Selection.hasSelection():  # Selecting a new object
            active_tool = self.getController().getActiveTool()
            if not active_tool:
                if self._previous_active_tool:
                    active_tool = self.getController().getTool(self._previous_active_tool)
                    self._previous_active_tool = None

            # If the tool is active, relevant to the the new selection and not too big to diplay each time
            if active_tool and active_tool.getEnabled() and active_tool not in self._large_area_tools:
                self.getController().setActiveTool(active_tool)

            else:  # revert to the default tool
                self.getController().setActiveTool("TranslateTool")

            if self.getPreferences().getValue("view/center_on_select"):
                self._center_after_select = True
                
        else:  # Deselecting a previously selected object
            if self.getController().getActiveTool():
                self._previous_active_tool = self.getController().getActiveTool().getPluginId()
                self.getController().setActiveTool(None)

    def _onToolOperationStopped(self, event):
        if self._center_after_select and Selection.getSelectedObject(0) is not None:
            self._center_after_select = False
            self._camera_animation.setStart(self.getController().getTool("CameraTool").getOrigin())
            self._camera_animation.setTarget(Selection.getSelectedObject(0).getWorldPosition())
            self._camera_animation.start()

    activityChanged = pyqtSignal()
    sceneBoundingBoxChanged = pyqtSignal()

    @pyqtProperty(bool, notify=activityChanged)
    def platformActivity(self):
        return self._platform_activity

    @pyqtProperty(str, notify=sceneBoundingBoxChanged)
    def getSceneBoundingBoxString(self):
        return self._i18n_catalog.i18nc(
            "@info 'width', 'depth' and 'height' are variable names that must NOT be translated; just translate the format of ##x##x## mm.",
            "%(width).1f x %(depth).1f x %(height).1f mm") % {'width': self._scene_bounding_box.width.item(),
                                                              'depth': self._scene_bounding_box.depth.item(),
                                                              'height': self._scene_bounding_box.height.item()}

    def updatePlatformActivityDelayed(self, node=None):
        if node is not None and (node.getMeshData() is not None or node.callDecoration("getLayerData")):
            self._update_platform_activity_timer.start()

    def updatePlatformActivity(self, node=None):
        """Update scene bounding box for current build plate"""

        count = 0
        scene_bounding_box = None
        is_block_slicing_node = False
        active_build_plate = self.getMultiBuildPlateModel().activeBuildPlate

        print_information = self.getPrintInformation()
        for node in DepthFirstIterator(self.getController().getScene().getRoot()):
            if (
                    not issubclass(type(node), CuraSceneNode) or
                    (not node.getMeshData() and not node.callDecoration("getLayerData")) or
                    (node.callDecoration("getBuildPlateNumber") != active_build_plate)):
                continue
            if node.callDecoration("isBlockSlicing"):
                is_block_slicing_node = True

            count += 1

            # After clicking the Undo button, if the build plate empty the project name needs to be set
            if print_information.baseName == '':
                print_information.setBaseName(node.getName())

            if not scene_bounding_box:
                scene_bounding_box = node.getBoundingBox()
            else:
                other_bb = node.getBoundingBox()
                if other_bb is not None:
                    scene_bounding_box = scene_bounding_box + node.getBoundingBox()

        if print_information:
            print_information.setPreSliced(is_block_slicing_node)

        if not scene_bounding_box:
            scene_bounding_box = AxisAlignedBox.Null

        if repr(self._scene_bounding_box) != repr(scene_bounding_box):
            self._scene_bounding_box = scene_bounding_box
            self.sceneBoundingBoxChanged.emit()

        self._platform_activity = True if count > 0 else False
        self.activityChanged.emit()

    @pyqtSlot()
    def selectAll(self):
        """Select all nodes containing mesh data in the scene."""

        if not self.getController().getToolsEnabled():
            return

        Selection.clear()
        for node in DepthFirstIterator(self.getController().getScene().getRoot()):
            if not isinstance(node, SceneNode):
                continue
            if not node.getMeshData() and not node.callDecoration("isGroup"):
                continue  # Node that doesnt have a mesh and is not a group.
            if node.getParent() and node.getParent().callDecoration("isGroup") or node.getParent().callDecoration(
                    "isSliceable"):
                continue  # Grouped nodes don't need resetting as their parent (the group) is resetted)
            if not node.isSelectable():
                continue  # i.e. node with layer data
            if not node.callDecoration("isSliceable") and not node.callDecoration("isGroup"):
                continue  # i.e. node with layer data

            Selection.add(node)

    @pyqtSlot()
    def resetAllTranslation(self):
        """Reset all translation on nodes with mesh data."""

        Logger.log("i", "Resetting all scene translations")
        nodes = []
        for node in DepthFirstIterator(self.getController().getScene().getRoot()):
            if not isinstance(node, SceneNode):
                continue
            if not node.getMeshData() and not node.callDecoration("isGroup"):
                continue  # Node that doesnt have a mesh and is not a group.
            if node.getParent() and node.getParent().callDecoration("isGroup"):
                continue  # Grouped nodes don't need resetting as their parent (the group) is resetted)
            if not node.isSelectable():
                continue  # i.e. node with layer data
            nodes.append(node)

        if nodes:
            op = GroupedOperation()
            for node in nodes:
                # Ensure that the object is above the build platform
                node.removeDecorator(ZOffsetDecorator.ZOffsetDecorator)
                if node.getBoundingBox():
                    center_y = node.getWorldPosition().y - node.getBoundingBox().bottom
                else:
                    center_y = 0
                op.addOperation(SetTransformOperation(node, Vector(0, center_y, 0)))
            op.push()

    @pyqtSlot()
    def resetAll(self):
        """Reset all transformations on nodes with mesh data."""

        Logger.log("i", "Resetting all scene transformations")
        nodes = []
        for node in DepthFirstIterator(self.getController().getScene().getRoot()):
            if not isinstance(node, SceneNode):
                continue
            if not node.getMeshData() and not node.callDecoration("isGroup"):
                continue  # Node that doesnt have a mesh and is not a group.
            if node.getParent() and node.getParent().callDecoration("isGroup"):
                continue  # Grouped nodes don't need resetting as their parent (the group) is resetted)
            if not node.callDecoration("isSliceable") and not node.callDecoration("isGroup"):
                continue  # i.e. node with layer data
            nodes.append(node)

        if nodes:
            op = GroupedOperation()
            for node in nodes:
                # Ensure that the object is above the build platform
                node.removeDecorator(ZOffsetDecorator.ZOffsetDecorator)
                if node.getBoundingBox():
                    center_y = node.getWorldPosition().y - node.getBoundingBox().bottom
                else:
                    center_y = 0
                op.addOperation(SetTransformOperation(node, Vector(0, center_y, 0), Quaternion(), Vector(1, 1, 1)))
            op.push()

    @pyqtSlot()
    def arrangeObjectsToAllBuildPlates(self) -> None:
        """Arrange all objects."""

        nodes_to_arrange = []
        for node in DepthFirstIterator(self.getController().getScene().getRoot()):
            if not isinstance(node, SceneNode):
                continue

            if not node.getMeshData() and not node.callDecoration("isGroup"):
                continue  # Node that doesnt have a mesh and is not a group.

            parent_node = node.getParent()
            if parent_node and parent_node.callDecoration("isGroup"):
                continue  # Grouped nodes don't need resetting as their parent (the group) is reset)

            if not node.callDecoration("isSliceable") and not node.callDecoration("isGroup"):
                continue  # i.e. node with layer data

            bounding_box = node.getBoundingBox()
            # Skip nodes that are too big
            if bounding_box is None or bounding_box.width < self._volume.getBoundingBox().width or bounding_box.depth < self._volume.getBoundingBox().depth:
                nodes_to_arrange.append(node)
        job = ArrangeObjectsAllBuildPlatesJob(nodes_to_arrange)
        job.start()
        self.getCuraSceneController().setActiveBuildPlate(0)  # Select first build plate

    # Single build plate
    @pyqtSlot()
    def arrangeAll(self) -> None:
        nodes_to_arrange = []
        active_build_plate = self.getMultiBuildPlateModel().activeBuildPlate
        locked_nodes = []
        for node in DepthFirstIterator(self.getController().getScene().getRoot()):
            if not isinstance(node, SceneNode):
                continue

            if not node.getMeshData() and not node.callDecoration("isGroup"):
                continue  # Node that doesnt have a mesh and is not a group.

            parent_node = node.getParent()
            if parent_node and parent_node.callDecoration("isGroup"):
                continue  # Grouped nodes don't need resetting as their parent (the group) is resetted)

            if not node.isSelectable():
                continue  # i.e. node with layer data

            if not node.callDecoration("isSliceable") and not node.callDecoration("isGroup"):
                continue  # i.e. node with layer data

            if node.callDecoration("getBuildPlateNumber") == active_build_plate:
                # Skip nodes that are too big
                bounding_box = node.getBoundingBox()
                if bounding_box is None or bounding_box.width < self._volume.getBoundingBox().width or bounding_box.depth < self._volume.getBoundingBox().depth:
                    # Arrange only the unlocked nodes and keep the locked ones in place
                    if UM.Util.parseBool(node.getSetting(SceneNodeSettings.LockPosition)):
                        locked_nodes.append(node)
                    else:
                        nodes_to_arrange.append(node)
        self.arrange(nodes_to_arrange, locked_nodes)

    def arrange(self, nodes: List[SceneNode], fixed_nodes: List[SceneNode]) -> None:
        """Arrange a set of nodes given a set of fixed nodes

        :param nodes: nodes that we have to place
        :param fixed_nodes: nodes that are placed in the arranger before finding spots for nodes
        """

        min_offset = self.getBuildVolume().getEdgeDisallowedSize() + 2  # Allow for some rounding errors
        job = ArrangeObjectsJob(nodes, fixed_nodes, min_offset=max(min_offset, 8))
        job.start()

    @pyqtSlot()
    def reloadAll(self) -> None:
        """Reload all mesh data on the screen from file."""

        Logger.log("i", "Reloading all loaded mesh data.")
        nodes = []
        has_merged_nodes = False
        gcode_filename = None  # type: Optional[str]
        for node in DepthFirstIterator(self.getController().getScene().getRoot()):
            # Objects loaded from Gcode should also be included.
            gcode_filename = node.callDecoration("getGcodeFileName")
            if gcode_filename is not None:
                break

            if not isinstance(node, CuraSceneNode) or not node.getMeshData():
                if node.getName() == "MergedMesh":
                    has_merged_nodes = True
                continue

            nodes.append(node)

        # We can open only one gcode file at the same time. If the current view has a gcode file open, just reopen it
        # for reloading.
        if gcode_filename:
            self._openFile(gcode_filename)

        if not nodes:
            return

        objects_in_filename = {}  # type: Dict[str, List[CuraSceneNode]]
        for node in nodes:
            mesh_data = node.getMeshData()
            if mesh_data:
                file_name = mesh_data.getFileName()
                if file_name:
                    if file_name not in objects_in_filename:
                        objects_in_filename[file_name] = []
                    if file_name in objects_in_filename:
                        objects_in_filename[file_name].append(node)
                else:
                    Logger.log("w", "Unable to reload data because we don't have a filename.")

        for file_name, nodes in objects_in_filename.items():
            for node in nodes:
                job = ReadMeshJob(file_name)
                job._node = node  # type: ignore
                job.finished.connect(self._reloadMeshFinished)
                if has_merged_nodes:
                    job.finished.connect(self.updateOriginOfMergedMeshes)

                job.start()

    @pyqtSlot("QStringList")
    def setExpandedCategories(self, categories: List[str]) -> None:
        categories = list(set(categories))
        categories.sort()
        joined = ";".join(categories)
        if joined != self.getPreferences().getValue("cura/categories_expanded"):
            self.getPreferences().setValue("cura/categories_expanded", joined)
            self.expandedCategoriesChanged.emit()

    expandedCategoriesChanged = pyqtSignal()

    @pyqtProperty("QStringList", notify=expandedCategoriesChanged)
    def expandedCategories(self) -> List[str]:
        return self.getPreferences().getValue("cura/categories_expanded").split(";")

    @pyqtSlot()
    def mergeSelected(self):
        self.groupSelected()
        try:
            group_node = Selection.getAllSelectedObjects()[0]
        except Exception as e:
            Logger.log("e", "mergeSelected: Exception: %s", e)
            return

        meshes = [node.getMeshData() for node in group_node.getAllChildren() if node.getMeshData()]

        # Compute the center of the objects
        object_centers = []
        # Forget about the translation that the original objects have
        zero_translation = Matrix(data=numpy.zeros(3))
        for mesh, node in zip(meshes, group_node.getChildren()):
            transformation = node.getLocalTransformation()
            transformation.setTranslation(zero_translation)
            transformed_mesh = mesh.getTransformed(transformation)
            center = transformed_mesh.getCenterPosition()
            if center is not None:
                object_centers.append(center)

        if object_centers:
            middle_x = sum([v.x for v in object_centers]) / len(object_centers)
            middle_y = sum([v.y for v in object_centers]) / len(object_centers)
            middle_z = sum([v.z for v in object_centers]) / len(object_centers)
            offset = Vector(middle_x, middle_y, middle_z)
        else:
            offset = Vector(0, 0, 0)

        # Move each node to the same position.
        for mesh, node in zip(meshes, group_node.getChildren()):
            node.setTransformation(Matrix())
            # Align the object around its zero position
            # and also apply the offset to center it inside the group.
            node.setPosition(-mesh.getZeroPosition() - offset)

        # Use the previously found center of the group bounding box as the new location of the group
        group_node.setPosition(group_node.getBoundingBox().center)
        group_node.setName("MergedMesh")  # add a specific name to distinguish this node

    def updateOriginOfMergedMeshes(self, _):
        """Updates origin position of all merged meshes"""

        group_nodes = []
        for node in DepthFirstIterator(self.getController().getScene().getRoot()):
            if isinstance(node, CuraSceneNode) and node.getName() == "MergedMesh":
                # Checking by name might be not enough, the merged mesh should has "GroupDecorator" decorator
                for decorator in node.getDecorators():
                    if isinstance(decorator, GroupDecorator):
                        group_nodes.append(node)
                        break

        for group_node in group_nodes:
            meshes = [node.getMeshData() for node in group_node.getAllChildren() if node.getMeshData()]

            # Compute the center of the objects
            object_centers = []
            # Forget about the translation that the original objects have
            zero_translation = Matrix(data=numpy.zeros(3))
            for mesh, node in zip(meshes, group_node.getChildren()):
                transformation = node.getLocalTransformation()
                transformation.setTranslation(zero_translation)
                transformed_mesh = mesh.getTransformed(transformation)
                center = transformed_mesh.getCenterPosition()
                if center is not None:
                    object_centers.append(center)

            if object_centers:
                middle_x = sum([v.x for v in object_centers]) / len(object_centers)
                middle_y = sum([v.y for v in object_centers]) / len(object_centers)
                middle_z = sum([v.z for v in object_centers]) / len(object_centers)
                offset = Vector(middle_x, middle_y, middle_z)
            else:
                offset = Vector(0, 0, 0)

            # Move each node to the same position.
            for mesh, node in zip(meshes, group_node.getChildren()):
                transformation = node.getLocalTransformation()
                transformation.setTranslation(zero_translation)
                transformed_mesh = mesh.getTransformed(transformation)

                # Align the object around its zero position
                # and also apply the offset to center it inside the group.
                node.setPosition(-transformed_mesh.getZeroPosition() - offset)

            # Use the previously found center of the group bounding box as the new location of the group
            group_node.setPosition(group_node.getBoundingBox().center)

    @pyqtSlot()
    def groupSelected(self) -> Operation:
        group_node, group_ops = groupNodesOperation(nodes=Selection.getAllSelectedObjects(),
                                                    parent_of_group_node=self.getController().getScene().getRoot(),
                                                    active_build_plate=self.getMultiBuildPlateModel().activeBuildPlate)

        group_ops.push()

        # center the group node correctly
        center = group_node.getBoundingBox().center
        group_node.setPosition(center)
        group_node.setCenterPosition(center)

        # Deselect individual nodes and select the group-node instead
        for node in group_node.getChildren():
            Selection.remove(node)
        Selection.add(group_node)

    @pyqtSlot()
    def ungroupSelected(self) -> Operation:
        new_nodes, ungroup_ops = ungroupNodesOperation(Selection.getAllSelectedObjects())

        ungroup_ops.push()

        # Add all individual nodes to the selection
        for node in new_nodes:
            Selection.add(node)
        # Note: The group removes itself from the scene once all its children have left it,
        # see GroupDecorator._onChildrenChanged

    def _createSplashScreen(self) -> Optional[CuraSplashScreen.CuraSplashScreen]:
        if self._is_headless:
            return None
        return CuraSplashScreen.CuraSplashScreen()

    def _onActiveMachineChanged(self):
        pass

    fileLoaded = pyqtSignal(str)
    fileCompleted = pyqtSignal(str)

    def _reloadMeshFinished(self, job) -> None:
        """
        Function called whenever a ReadMeshJob finishes in the background. It reloads a specific node object in the
        scene from its source file. The function gets all the nodes that exist in the file through the job result, and
        then finds the scene node that it wants to refresh by its object id. Each job refreshes only one node.

        :param job: The :py:class:`Uranium.UM.ReadMeshJob.ReadMeshJob` running in the background that reads all the
        meshes in a file
        """

        job_result = job.getResult()  # nodes that exist inside the file read by this job
        if len(job_result) == 0:
            Logger.log("e", "Reloading the mesh failed.")
            return
        object_found = False
        mesh_data = None
        # Find the node to be refreshed based on its id
        for job_result_node in job_result:
            if job_result_node.getId() == job._node.getId():
                mesh_data = job_result_node.getMeshData()
                object_found = True
                break
        if not object_found:
            Logger.warning("The object with id {} no longer exists! Keeping the old version in the scene.".format(
                job_result_node.getId()))
            return
        if not mesh_data:
            Logger.log("w", "Could not find a mesh in reloaded node.")
            return
        job._node.setMeshData(mesh_data)

    def _openFile(self, filename):
        self.readLocalFile(QUrl.fromLocalFile(filename))

    def _openFiles(self, filenames):
        self.readLocalFilesAndGroup([QUrl.fromLocalFile(filename) for filename in filenames])

    def _addProfileReader(self, profile_reader):
        # TODO: Add the profile reader to the list of plug-ins that can be used when importing profiles.
        pass

    def _addProfileWriter(self, profile_writer):
        pass

    @pyqtSlot("QSize")
    def setMinimumWindowSize(self, size):
        main_window = self.getMainWindow()
        if main_window:
            main_window.setMinimumSize(size)

    def getBuildVolume(self):
        return self._volume

    additionalComponentsChanged = pyqtSignal(str, arguments=["areaId"])

    @pyqtProperty("QVariantMap", notify=additionalComponentsChanged)
    def additionalComponents(self):
        return self._additional_components

    @pyqtSlot(str, "QVariant")
    def addAdditionalComponent(self, area_id: str, component):
        """Add a component to a list of components to be reparented to another area in the GUI.

        The actual reparenting is done by the area itself.
        :param area_id: dentifying name of the area to which the component should be reparented
        :param (QQuickComponent) component: The component that should be reparented
        """

        if area_id not in self._additional_components:
            self._additional_components[area_id] = []
        self._additional_components[area_id].append(component)

        self.additionalComponentsChanged.emit(area_id)

    @pyqtSlot(str)
    def log(self, msg):
        Logger.log("d", msg)

    openProjectFile = pyqtSignal(QUrl, arguments=["project_file"])  # Emitted when a project file is about to open.

    @pyqtSlot(QUrl, str)
    @pyqtSlot(QUrl)
    def readLocalFile(self, file: QUrl, project_mode: Optional[str] = None):
        """Open a local file

        :param project_mode: How to handle project files. Either None(default): Follow user preference, "open_as_model"
         or "open_as_project". This parameter is only considered if the file is a project file.
        """
        Logger.log("i", "Attempting to read file %s", file.toString())
        if not file.isValid():
            return

        scene = self.getController().getScene()

        for node in DepthFirstIterator(scene.getRoot()):
            if node.callDecoration("isBlockSlicing"):
                self.deleteAll()
                break

        is_project_file = self.checkIsValidProjectFile(file)

        if project_mode is None:
            project_mode = self.getPreferences().getValue("cura/choice_on_open_project")

        if is_project_file and project_mode == "open_as_project":
            # open as project immediately without presenting a dialog
            workspace_handler = self.getWorkspaceFileHandler()
            workspace_handler.readLocalFile(file)
            return

        if is_project_file and project_mode == "always_ask":
            # present a dialog asking to open as project or import models
            self.callLater(self.openProjectFile.emit, file)
            return

        # Either the file is a model file or we want to load only models from project. Continue to load models.

        if self.getPreferences().getValue("cura/select_models_on_load"):
            Selection.clear()

        f = file.toLocalFile()
        extension = os.path.splitext(f)[1]
        extension = extension.lower()
        filename = os.path.basename(f)
        if self._currently_loading_files:
            # If a non-slicable file is already being loaded, we prevent loading of any further non-slicable files
            if extension in self._non_sliceable_extensions:
                message = Message(
                    self._i18n_catalog.i18nc("@info:status",
                                             "Only one G-code file can be loaded at a time. Skipped importing {0}",
                                             filename), title=self._i18n_catalog.i18nc("@info:title", "Warning"))
                message.show()
                return
            # If file being loaded is non-slicable file, then prevent loading of any other files
            extension = os.path.splitext(self._currently_loading_files[0])[1]
            extension = extension.lower()
            if extension in self._non_sliceable_extensions:
                message = Message(
                    self._i18n_catalog.i18nc("@info:status",
                                             "Can't open any other file if G-code is loading. Skipped importing {0}",
                                             filename), title=self._i18n_catalog.i18nc("@info:title", "Error"))
                message.show()
                return

        self._currently_loading_files.append(f)
        if extension in self._non_sliceable_extensions:
            self.deleteAll(only_selectable=False)

        job = ReadMeshJob(f)
        job.finished.connect(self._readMeshFinished)
        job.start()

    def getGerberFileHandler(self) -> WorkspaceFileHandler:
        return self._gerber_file_handler

    def _readMeshFinished(self, job):
        global_container_stack = self.getGlobalContainerStack()
        if not global_container_stack:
            Logger.log("w", "Can't load meshes before a printer is added.")
            return
        if not self._volume:
            Logger.log("w", "Can't load meshes before the build volume is initialized")
            return

        nodes = job.getResult()
        if nodes is None:
            Logger.error("Read mesh job returned None. Mesh loading must have failed.")
            return
        file_name = job.getFileName()
        file_name_lower = file_name.lower()
        file_extension = file_name_lower.split(".")[-1]
        self._currently_loading_files.remove(file_name)

        self.fileLoaded.emit(file_name)
        target_build_plate = self.getMultiBuildPlateModel().activeBuildPlate

        root = self.getController().getScene().getRoot()
        fixed_nodes = []
        for node_ in DepthFirstIterator(root):
            if node_.callDecoration("isSliceable") and node_.callDecoration(
                    "getBuildPlateNumber") == target_build_plate:
                fixed_nodes.append(node_)

        default_extruder_position = self.getMachineManager().defaultExtruderPosition
        default_extruder_id = self._global_container_stack.extruderList[int(default_extruder_position)].getId()

        select_models_on_load = self.getPreferences().getValue("cura/select_models_on_load")

        nodes_to_arrange = []  # type: List[CuraSceneNode]

        fixed_nodes = []
        for node_ in DepthFirstIterator(self.getController().getScene().getRoot()):
            # Only count sliceable objects
            if node_.callDecoration("isSliceable"):
                fixed_nodes.append(node_)

        for original_node in nodes:
            # Create a CuraSceneNode just if the original node is not that type
            if isinstance(original_node, CuraSceneNode):
                node = original_node
            else:
                node = CuraSceneNode()
                node.setMeshData(original_node.getMeshData())

                # Setting meshdata does not apply scaling.
                if original_node.getScale() != Vector(1.0, 1.0, 1.0):
                    node.scale(original_node.getScale())

            node.setSelectable(True)
            node.setName(os.path.basename(file_name))
            self.getBuildVolume().checkBoundsAndUpdate(node)

            is_non_sliceable = "." + file_extension in self._non_sliceable_extensions

            if is_non_sliceable:
                # Need to switch first to the preview stage and then to layer view
                self.callLater(lambda: (self.getController().setActiveStage("PreviewStage"),
                                        self.getController().setActiveView("SimulationView")))

                block_slicing_decorator = BlockSlicingDecorator()
                node.addDecorator(block_slicing_decorator)
            else:
                sliceable_decorator = SliceableObjectDecorator()
                node.addDecorator(sliceable_decorator)

            scene = self.getController().getScene()

            # If there is no convex hull for the node, start calculating it and continue.
            if not node.getDecorator(ConvexHullDecorator):
                node.addDecorator(ConvexHullDecorator())
            for child in node.getAllChildren():
                if not child.getDecorator(ConvexHullDecorator):
                    child.addDecorator(ConvexHullDecorator())

            if file_extension != "3mf":
                if node.callDecoration("isSliceable"):
                    # Ensure that the bottom of the bounding box is on the build plate
                    if node.getBoundingBox():
                        center_y = node.getWorldPosition().y - node.getBoundingBox().bottom
                    else:
                        center_y = 0

                    node.translate(Vector(0, center_y, 0))

                    nodes_to_arrange.append(node)

            # This node is deep copied from some other node which already has a BuildPlateDecorator, but the deepcopy
            # of BuildPlateDecorator produces one that's associated with build plate -1. So, here we need to check if
            # the BuildPlateDecorator exists or not and always set the correct build plate number.
            build_plate_decorator = node.getDecorator(BuildPlateDecorator)
            if build_plate_decorator is None:
                build_plate_decorator = BuildPlateDecorator(target_build_plate)
                node.addDecorator(build_plate_decorator)
            build_plate_decorator.setBuildPlateNumber(target_build_plate)

            operation = AddSceneNodeOperation(node, scene.getRoot())
            operation.push()

            node.callDecoration("setActiveExtruder", default_extruder_id)
            scene.sceneChanged.emit(node)

            if select_models_on_load:
                Selection.add(node)
        try:
            arrange(nodes_to_arrange, self.getBuildVolume(), fixed_nodes)
        except:
            Logger.logException("e", "Failed to arrange the models")
        self.fileCompleted.emit(file_name)

    @pyqtSlot(list, str)
    @pyqtSlot(list)
    def readLocalFilesAndGroup(self, urls: List[QUrl], project_mode: Optional[str] = None):
        """
        Open local files and group them into one group at the end

        NB: This is similar to the existing readLocalFile, with the main
            difference that we group all the loaded files when their loading is finished.
        :param urls: The list of file urls to read
        
        :param project_mode: How to handle project files. Either None(default): Follow user preference, "open_as_model"
         or "open_as_project". This parameter is only considered if the file is a project file.
        """

        # validate input:
        invalid_urls = []
        err_msg = ""
        for url in urls:
            qurl = QUrl(url)
            if not qurl.isValid:
                err_msg += "url {} is not valid, skipping\n\n".format(url)
                invalid_urls.append(url)

            if os.path.isdir(qurl.toLocalFile()):
                err_msg += "reading folders is not supported, skipping {}\n\n".format(url)
                invalid_urls.append(url)  # TODO: Add loading all files in a folder (recursively?)
                continue

            if not os.path.isfile(qurl.toLocalFile()):
                err_msg += "File {} is not valid, skipping\n\n".format(url)

        urls = [url for url in urls if url not in invalid_urls]  #remove invalid urls
        if err_msg:
            Message(err_msg).show()

        # For now, we are ignoring project_mode and always trying to open as a tray, until we work out how to
        #  open trays as models without things breaking. See AMT-830
        # if project_mode is None:
        #     project_mode = self.getPreferences().getValue("cura/choice_on_open_project")
        project_mode = "open_as_project"

        projects = [url for url in urls if self.checkIsValidProjectFile(url)]

        if len(projects) > 1:
            Message("Cannot load more than one project").show()
            return
        
        if len(projects) == 1:

            if len(urls) > 1:
                Message("Cannot load a tray and models at the same time").show()
                return

            if project_mode == "open_as_project":
                # open as project immediately without presenting a dialog
                job = ReadFileJob(projects[0].toLocalFile(), handler=self.getWorkspaceFileHandler())
                job.finished.connect(self._readWorkspaceFinished)
                job.start()
                return

            elif project_mode == "always_ask":
                # present a dialog asking to open as project or import models
                self.callLater(self.openProjectFile.emit, projects[0])
                return

        # If we reached here, we are opening a list of models
        # initialize array of jobs
        jobs = []
        urls_and_readers = dict()
        for url in map(QUrl, urls):
            Logger.log("i", "Attempting to read file %s", url.toString())

            if not url.isValid():
                Message("Model {} is not valid. Skipping...".format(url)).show()
                continue

            file = url.toLocalFile()
            reader = self.getMeshFileHandler().getReaderForFile(file)
            if reader is None:
                reader = self.getGerberFileHandler().getReaderForFile(file)
            if reader is None:
                Message("Could not find a reader for file {}".format(url)).show()
                continue

            if reader not in urls_and_readers.keys():
                urls_and_readers[reader] = [file]
            else:
                urls_and_readers[reader] += [file]

        # readers_needed = set(attr[1] for attr in urls_and_readers)
        for reader, urls in urls_and_readers.items():
            # gerber need to be treated by a different Handler and different Job type
            if reader.getPluginId() == 'GerberReader':
                # files_to_read = set(attr[0] for attr in urls_and_readers if attr[1] == reader)
                job = ReadPCBJob(urls)
                job.finished.connect(self._readPcbFinished)
                job.start()

            # ODB need to be traded by different connection of .finished
            elif reader.getPluginId() == "ODBReader":
                # files_to_read = set(attr[0] for attr in urls_and_readers if attr[1] == reader)
                for file in urls:
                    job = ReadMeshJob(file)
                    job.finished.connect(self._readPcbFinished)
                    job.start()

            # any other probably 3D model go to mesh reader as usually
            else:
                # files_to_read = set(attr[0] for attr in urls_and_readers if attr[1] == reader)
                for file in urls:
                    job = ReadMeshJob(file)
                    job.finished.connect(self._readMeshFromGroupFinished)
                    jobs.append(job)
                    job.start()
                group_job = TrackJobsFinishedJob(jobs, job_finished_condition=self._isJobResultRepopulated)
                group_job.finished.connect(self._groupAndAddMeshesToScene)
                group_job.start()

    def _readPcbFinished(self, job):
        # We really should not have reached this method without a printer (and thus settings) loaded
        global_container_stack = self.getGlobalContainerStack()
        if not global_container_stack:
            Message("Can't load meshes before a printer is added.").show()
            return
        if not self._volume:
            Message("Can't load meshes before the build volume is initialized").show()
            return
        if len(job.getResult()) == 0:
            Message("Gerber Reader : loading Failed").show()
            return

        pcb_node = job.getResult()[0]
        if pcb_node is None:
            Message("Read mesh job returned None. Mesh loading must have failed.").show()
            return

        file_names = job.getFileName()

        if isinstance(file_names, Iterable):
            for file in file_names:
                self.fileLoaded.emit(file)
        else:
            self.fileLoaded.emit(file_names)

        node_name = pcb_node.getName()

        if isinstance(pcb_node, CuraSceneNode):
            updated_result = pcb_node
            updated_result.setSelectable(True)  # This is used e.g. when deciding whether we can export the mesh
            updated_result.setName(os.path.basename(node_name))
            self.getBuildVolume().checkBoundsAndUpdate(updated_result)

            # If there is no convex hull for the node, start calculating it and continue.
            for child in DepthFirstIterator(updated_result):
                if not child.getDecorator(ConvexHullDecorator):
                    child.addDecorator(ConvexHullDecorator())

            # This node is deep copied from some other node which already has a BuildPlateDecorator,
            # but the deepcopy of BuildPlateDecorator produces one that's associated with build plate -1.
            # So, here we need to check if the BuildPlateDecorator exists or not
            # and always set the correct build plate number.
            target_build_plate = self.getMultiBuildPlateModel().activeBuildPlate
            build_plate_decorator = updated_result.getDecorator(BuildPlateDecorator)
            if build_plate_decorator is None:
                build_plate_decorator = BuildPlateDecorator(target_build_plate)
                updated_result.addDecorator(build_plate_decorator)
            build_plate_decorator.setBuildPlateNumber(target_build_plate)

            default_extruder_position = self.getMachineManager().defaultExtruderPosition
            default_extruder_id = global_container_stack.extruderList[int(default_extruder_position)].getId()
            updated_result.callDecoration("setActiveExtruder", default_extruder_id)
            updated_result.addDecorator(SliceableObjectDecorator())  # Has to be sliceable so that it can be moved
        else:
            return

        try:
            some_object_iterator = iter(file_names)
            for file in file_names:
                self.fileCompleted.emit(file)
        except:
            self.fileCompleted.emit(file_names)

        scene = self.getController().getScene()

        AddSceneNodeOperation(updated_result, scene.getRoot()).push()

        # Select the new node if required
        if self.getPreferences().getValue("cura/select_models_on_load"):
            Selection.clear()
            Selection.add(updated_result)
            
        scene.sceneChanged.emit(updated_result)

        # Wait for things to finish happening before proceeding with the next actions
        pcb_loading_finished_timer = QTimer(self)
        pcb_loading_finished_timer.setInterval(1000)
        pcb_loading_finished_timer.setSingleShot(True)
        pcb_loading_finished_timer.timeout.connect(self._processTrayAfterLoading)
        pcb_loading_finished_timer.start()

    def _readMeshFromGroupFinished(self, job):
        """
        This is called by each job (thread) as its meshReader finishes reading a 3d Mesh from a file.
        The reader returns SceneNode objects
        """

        # We really should not have reached this method without a printer (and thus settings) loaded
        global_container_stack = self.getGlobalContainerStack()
        if not global_container_stack:
            Logger.log("w", "Can't load meshes before a printer is added.")
            return
        if not self._volume:
            Logger.log("w", "Can't load meshes before the build volume is initialized")
            return

        job_results = job.getResult()
        if job_results is None:
            Logger.error("Read mesh job returned None. Mesh loading must have failed.")
            return  # TODO: Error message?

        file_name = job.getFileName()
        self.fileLoaded.emit(file_name)

        updated_results = []
        for jobResult in job_results:
            # Create a CuraSceneNode just if the original node is not that type (i.e. just a SceneNode)
            if isinstance(jobResult, CuraSceneNode):
                updated_result = jobResult
            else:
                updated_result = CuraSceneNode()
                updated_result.setMeshData(jobResult.getMeshData())

                # Setting meshdata does not apply scaling.
                if jobResult.getScale() != Vector(1.0, 1.0, 1.0):
                    updated_result.scale(jobResult.getScale())

            updated_result.setSelectable(True)  # This is used e.g. when deciding whether we can export the mesh

            # The SceneNode name is the base filename, and the filePath setting is the full path
            updated_result.setName(os.path.basename(file_name))
            updated_result.setSetting('filePath', file_name)

            self.getBuildVolume().checkBoundsAndUpdate(updated_result)

            # If there is no convex hull for the node, start calculating it and continue.
            for child in DepthFirstIterator(updated_result):
                if not child.getDecorator(ConvexHullDecorator):
                    child.addDecorator(ConvexHullDecorator())

            # This node is deep copied from some other node which already has a BuildPlateDecorator, but the deepcopy
            # of BuildPlateDecorator produces one that's associated with build plate -1. So, here we need to check if
            # the BuildPlateDecorator exists or not and always set the correct build plate number.
            target_build_plate = self.getMultiBuildPlateModel().activeBuildPlate
            build_plate_decorator = updated_result.getDecorator(BuildPlateDecorator)
            if build_plate_decorator is None:
                build_plate_decorator = BuildPlateDecorator(target_build_plate)
                updated_result.addDecorator(build_plate_decorator)
            build_plate_decorator.setBuildPlateNumber(target_build_plate)

            default_extruder_position = self.getMachineManager().defaultExtruderPosition
            default_extruder_id = global_container_stack.extruderList[int(default_extruder_position)].getId()
            updated_result.callDecoration("setActiveExtruder", default_extruder_id)

            updated_result.addDecorator(SliceableObjectDecorator())  # All of our meshes are sliceable

            updated_results.append(updated_result)

        job.setResult(updated_results)  # replace the result for the benefit of the job that waits on all the other jobs

        self.fileCompleted.emit(file_name)  # S.K.: Is this even connected anywhere?

    def _readWorkspaceFinished(self, job: ReadFileJob) -> None:
        """
        This is called the workspace-loading job (thread) as it's done reading a tray from a file.
        The reader returns SceneNode objects.

        The reason for the duplication w.r.t. WorkspaceReader.read is because we want (based on CLI flags)
        to follow up with some actions at the end, like processing the tray, and closing the application
        """
        # Add the nodes to the scene.
        result = job.getResult()
        if isinstance(result, tuple):
            nodes, metadata = result
        else:
            nodes = result
            metadata = {}

        if nodes:  # Job did not fail.
            self.resetWorkspace()
            for node in nodes:
                AddSceneNodeOperation(node, self.getController().getScene().getRoot()).push()

            self.getWorkspaceMetadataStorage().setAllData(metadata)
            self.workspaceLoaded.emit(cast(WorkspaceReader, job.reader).workspaceName())

            # Wait for things to finish happening before proceeding with the next actions
            workspace_loading_finished_timer = QTimer(self)
            workspace_loading_finished_timer.setInterval(1000)
            workspace_loading_finished_timer.setSingleShot(True)
            workspace_loading_finished_timer.timeout.connect(self._processTrayAfterLoading)
            workspace_loading_finished_timer.start()

        else:
            if self._quit_after_processing:  # If Loading fails, we still want to quit
                sys.exit(1)

    def _processTrayAfterLoading(self) -> None:
        """
        This handles additional actions to be performed after workspace loading is complete,
        based on CLI arguments
        """
        if self._process_tray_after_loading:

            if self._save_tray_to:
                self.backend.setPostProcessCallback(self._saveProcessedJob,
                                                    dest_path = self._save_tray_to,
                                                    do_quit_on_completion = self._quit_after_processing)

            self.backend.startProcessing()

    def _saveProcessedJob(self, *args, **kwargs) -> None:
        """
        Save processed job file into the path given in the kwarg 'dest_path'
        """

        try:

            # Where is the file now?
            src_path = self.backend.getLastJobFilename()

            # Sanity checking
            if not os.path.isfile(src_path):
                raise Exception(self._i18n_catalog.i18nc("@info:status",
                                                         "Invalid source path {} for processed job",
                                                         src_path))

            # Where do we want to save it?
            dst_path = kwargs.get("dest_path")

            # Some more sanity checking
            if dst_path is None:  # This should not happen
                raise Exception(self._i18n_catalog.i18nc("@info:status",
                                                         "Destination path for processed job missing"))

            if os.path.isdir(dst_path):  # Got a directory rather than a filename
                dst_path = os.path.join(dst_path, os.path.basename(src_path))

            if not os.path.isdir(os.path.dirname(dst_path)):
                raise Exception(self._i18n_catalog.i18nc("@info:status",
                                                         "Invalid destination path {} for processed job",
                                                         dst_path))

            # Do the deed
            shutil.copy(src_path, dst_path)

        except Exception as e:
            Message("Cannot save processed file, {}".format(e),
                    title=self._i18n_catalog.i18nc("@info:title", "Error")).show()

        finally:
            # Do we need to quit when done?
            if kwargs.get("do_quit_on_completion", False):
                sys.exit(0)

    @staticmethod
    def _isJobResultRepopulated(job):
        """
        This is the condition we want to test for when loading jobs.
        1. job is finished
        2. we changed its result from SceneNode to CuraSceneNode
        """
        return job.isFinished() and all(isinstance(node, CuraSceneNode) for node in job.getResult())

    def guess(self, nodes):
        if len(nodes) != 2:
            return
        node0 = nodes[0]
        node1 = nodes[1]

        name0 = os.path.basename(node0.getSetting('filePath'))
        name1 = os.path.basename(node1.getSetting('filePath'))
        if len(name0) != len(name1):
            return

        if not name0.lower().endswith(".stl") or not name0.lower().endswith(".stl"):
            return

        diff_indexes = []
        for i, (a, b) in enumerate(zip(name0, name1)):
            if a != b:
                diff_indexes.append(i)
        if len(diff_indexes) != 1:
            return

        index = diff_indexes[0]

        node_ci, node_di = node0, node1
        name_ci, name_di = name0, name1
        if name0[index] + name1[index] in ["dc", "DC"]:
            node_ci, node_di = node1, node0
            name_ci, name_di = name1, name0

        if name_ci[index:index + 2] not in ["ci", "CI"]:
            return

        extruders = self.getGlobalContainerStack().extruderList
        my_extruders = {extruder.name: extruder.id for extruder in extruders}
        if len(my_extruders) != 2 or len(extruders) != 2:
            return
        conductive = "Conductive"
        dielectric = "Dielectric"
        if set(my_extruders.keys()) != {conductive, dielectric}:
            return
        node_di.callDecoration("setActiveExtruder", my_extruders[dielectric])
        node_ci.callDecoration("setActiveExtruder", my_extruders[conductive])


    def _groupAndAddMeshesToScene(self, job: TrackJobsFinishedJob):
        """
        This is run when all the mesh-loading jobs are finished,
        Collects their SceneNodes, places them on the tray, groups and arranges them if needed,
        s.t. it is undoable and redoable in a single step.
        If required, it also selects the final node
        """

        job_error = job.getError()
        if job_error is not None:
            Message("Failed loading Meshes: \n" + job_error.args[0]).show()

            if self._quit_after_processing:
                sys.exit(1)
            else:
                return

        nodes = job.getResult()

        if nodes is None or len(nodes) == 0:  # We might get here with an empty list of files
            if self._quit_after_processing:
                sys.exit(1)
            else:
                return

        scene = self.getController().getScene()
        scene_root = scene.getRoot()
        do_group_nodes = True

        # Init the grouped operation we will be activating in the end
        grouped_op = GroupedOperation()

        self.guess(nodes)
        # ----- First iteration to see if we have ungroupable nodes
        for node in nodes:
            if node.callDecoration('isUngroupable'):  # TODO: Can this happen? This calback should only happen with 3D models...
                do_group_nodes = False  # If one of the new nodes is not groupable, we won't group them
                break

        # ----- Second iteration: add the nodes to the scene, group and select them *As a single operation*-----#
        for node in nodes:
            # Add the node to the scene
            grouped_op.addOperation(AddSceneNodeOperation(node, scene_root))

            # TODO: Should we center the nodes here, like below in do_group_nodes?

        new_node = None  # To be used for grouping models together
        if do_group_nodes:
            # TODO: Shouldn't everything be grouped once we finished loading?...

            active_build_plate = self.getMultiBuildPlateModel().activeBuildPlate
            new_node, group_selected_op = groupNodesOperation(nodes=nodes,
                                                              parent_of_group_node=scene_root,
                                                              active_build_plate=active_build_plate)

            grouped_op.addOperation(group_selected_op)

            # center the group node correctly, so that it's later rotated around its center
            # First we need to calculate to total bounding box
            bounding_box = None
            for node in nodes:
                if bounding_box is None:
                    bounding_box = node.getBoundingBox()
                else:
                    bounding_box = bounding_box + node.getBoundingBox()

            center = Vector(bounding_box.center.x, bounding_box.bottom, bounding_box.center.z) # where to center
            new_node.setPosition(center)
            new_node.setCenterPosition(center)

            # Move to center of tray (if required)
            if self.getPreferences().getValue("cura/center_models_on_load"):
                grouped_op.addOperation(TranslateOperation(new_node, Vector(), set_position=True))

            # let's do this thang
            grouped_op.push()

        # ----- Now arrange the node on the tray (optional) -----#
        # TODO: Arrange is not working for groups atm
        #  if self.getPreferences().getValue("cura/arrange_models_on_load"):
        #     #Fixed nodes = nodes that arrange does not move later.
        #     #  As with everything, nodes that are not sliceable are ignored (we don't have those at all...)
        #     fixed_nodes = []
        #     for node_ in DepthFirstIterator(sceneRoot):
        #         # Only count sliceable objects
        #         if node_.callDecoration("isSliceable"):
        #             fixed_nodes.append(node_)
        #     #Arrange the node
        #     try:
        #         arrange([newNode], self.getBuildVolume(), fixed_nodes)
        #     except:
        #         Logger.logException("e", "Failed to arrange the new model on the existing tray")

         #Let the listeners know something happened on the scene, and select the new node(s) if required
        if self.getPreferences().getValue("cura/select_models_on_load"):
            Selection.clear()

        if do_group_nodes:
            if self.getPreferences().getValue("cura/select_models_on_load"):
                Selection.add(new_node)

            scene.sceneChanged.emit(new_node)

        else:
            for node in nodes:  # ----- Third iteration: let the scene know those nodes changed ----- #

                if self.getPreferences().getValue("cura/select_models_on_load"):
                    Selection.add(node)

                scene.sceneChanged.emit(node)

        # Wait for things to finish happening before proceeding with the next actions
        mesh_group_loading_finished_timer = QTimer(self)
        mesh_group_loading_finished_timer.setInterval(1000)
        mesh_group_loading_finished_timer.setSingleShot(True)
        mesh_group_loading_finished_timer.timeout.connect(self._processTrayAfterLoading)
        mesh_group_loading_finished_timer.start()

    def readLocalFilesAndAddToSelectedGroup(self,
                                            urls: List[QUrl],
                                            on_finished_callback: Callable = None) -> None:
        """
        This is made specifically to read a group of meshes given in the list of filenames and add them
        to the node representing a group, aligning their coordinates

        :param urls: the list of files to read
        :param on_finished_callback: a callback to call after loading and adding has finished
        """
        # initialize array of jobs
        jobs = []
        for url in urls:

            if not url.isValid():
                Message("Failed to load file {}, the url is invalid".format(url)).show()
                continue

            # For now we only support STL loading here (enforced on the QML side by the file dialog)
            filename = url.toLocalFile()
            if os.path.splitext(filename)[1].lower() != '.stl':
                Message("Cannot handle non-STL files at the moment. Skipping {}".format(filename)).show()
                continue

            # Verify this particular file was not loaded yet
            # We assume there is one selected node (This was checked earlier)
            group_node = Selection.getSelectedObject(0)

            if not group_node.callDecoration('isGroup'):
                Message("Non grouped nodes not supported ATM!").show()  # DEBUG?
                return

            if filename in (child.getSetting('filePath') for child in group_node.getChildren()):
                Message("Model {} was already added to this group. Skipping.".format(filename)).show()
                continue

            # TODO: Think about supporting 3mf files here
            #  The issue is that the 3mf files are loaded as a group, and we don't support sub-groups at the moment
            reader = self.getMeshFileHandler().getReaderForFile(filename)

            if not reader:
                Message("Could not find a reader for file {}".format(filename)).show()
                continue

            job = ReadMeshJob(filename)
            job.finished.connect(self._readMeshFromGroupFinished)
            jobs.append(job)
            job.start()


        group_job = TrackJobsFinishedJob(jobs,
                                         job_finished_condition=self._isJobResultRepopulated)

        group_job.finished.connect(self.wrapAddMeshesToGroupWithCallback(callback=on_finished_callback))
        group_job.start()

    # A bit of black magic here...
    def wrapAddMeshesToGroupWithCallback(self, callback: Callable) -> Callable:
        """
        This method wraps _addMeshesToSelectedGroup, such that it is called with the callback we want to pass to it,
          when in fact the method is called without it.
        In practice, due to the way signal connection works (we want to connect it to job.finished), it won't accept
          a function, but has to accept a method. Hence we store it in self.wrapped and remove it after using
        Moreover, for additional safety against multiple calls, we add the id of the specific 'wrapped' method created

        :param callback: The callback we want to pass into _addMeshesToSelectedGroup along with the job it already
                         receives from the signal connection in the JobQueue

        :return: The method that will be called from the JobQueue, and already contains the callback embedded in it
        """
        def wrapped(job: TrackJobsFinishedJob):
            ret = self._addMeshesToSelectedGroup(job, callback)
            delattr(self, 'wrapped' + str(id(wrapped)))
            return ret
        
        setattr(self, 'wrapped' + str(id(wrapped)), wrapped)
        return wrapped

    def _addMeshesToSelectedGroup(self, job: TrackJobsFinishedJob, callback: Callable = None) -> None:
        """
        This is run when all the mesh-loading jobs are finished,
        Collects their SceneNodes, places them on the tray, and adds them to the previously selected group,
        s.t. it is undoable and redoable in a single step.
        """

        job_error = job.getError()
        if job_error is not None:
            Message("Failed loading Meshes: \n" + job_error.args[0]).show()

        # Get all the nodes we need - each job retains a list of nodes...
        nodes = [job_result for job_result in job.getResult()]

        if nodes is None or len(nodes) == 0:  # We might get here with an empty list of files
            return

        # We assume there is one selected node (This was checked earlier)
        group_node = Selection.getSelectedObject(0)

        if not group_node.callDecoration('isGroup'):
            Message("Non grouped nodes not supported ATM!").show()         #      DEBUG?
            return

        # cache the transform of our current group
        group_transform = group_node.getChildren()[0].getLocalTransformation(copy=True)
        # this will help to reset the tranform later
        transform = Vector()


        group_position = group_node.getChildren()[0].getPosition()


        # Init the grouped operation we will be activating in the end
        grouped_op = GroupedOperation()


        for node in nodes:
            transform = group_transform.copy()
            # node_center is the center of the bounding box of the node
            # we want to reset the z axis to 0
            node_center = node.getBoundingBox().center
            node_center = node_center.set(None,0.0,None)
            # we want to set the position of the node to the group node by their center
            # for that we are using the calculation of the translate funcation
            translation_matrix = Matrix()
            translation_matrix.setByTranslation(- node_center - group_position)
            transform.multiply(translation_matrix)
            # apply the transform of our current group to the new node, so that they come together
            # so that it fits correctly in the group

            node.setTransformation(transform)
            # add the node to the group - first aligning it around the tray center,
            # so that it fits correctly in the group
            grouped_op.addOperation(SetParentOperation(node=node,
                                                       parent_node=group_node,
                                                       callback=callback))

        # let's do this thang
        grouped_op.push()

        # Finally let the listeners know something happened on the scene
        self.getController().getScene().sceneChanged.emit(group_node)

        # make an educated guess about the new materials (Note this might change currently-set materials)
        self.guess(group_node.getChildren())

        # and call the callback again, because things might have changed in the meanwhile
        if callback:
            callback()

    def addNonSliceableExtension(self, extension):
        self._non_sliceable_extensions.append(extension)

    @pyqtSlot(str, result=bool)
    def checkIsValidProjectFile(self, file_url):
        """Checks if the given file URL is a valid project file. """

        file_path = QUrl(file_url).toLocalFile()
        workspace_reader = self.getWorkspaceFileHandler().getReaderForFile(file_path)
        if workspace_reader is None:
            return False  # non-project files won't get a reader
        try:
            result = workspace_reader.preRead(file_path, show_dialog=False)
            return result == WorkspaceReader.PreReadResult.accepted
        except Exception:
            Logger.logException("e", "Could not check file %s", file_url)
            return False

    def _onContextMenuRequested(self, x: float, y: float) -> None:
        # Ensure we select the object if we request a context menu over an object without having a selection.
        if Selection.hasSelection():
            return
        selection_pass = cast(SelectionPass, self.getRenderer().getRenderPass("selection"))
        if not selection_pass:  # If you right-click before the rendering has been initialised there might not be a selection pass yet.
            return
        node = self.getController().getScene().findObject(selection_pass.getIdAtPosition(x, y))
        if not node:
            return
        parent = node.getParent()
        while parent and parent.callDecoration("isGroup"):
            node = parent
            parent = node.getParent()

        Selection.add(node)

    @pyqtSlot()
    def showMoreInformationDialogForAnonymousDataCollection(self):
        try:
            slice_info = self._plugin_registry.getPluginObject("SliceInfoPlugin")
            slice_info.showMoreInfoDialog()
        except PluginNotFoundError:
            Logger.log("w", "Plugin SliceInfo was not found, so not able to show the info dialog.")

    def addSidebarCustomMenuItem(self, menu_item: dict) -> None:
        self._sidebar_custom_menu_items.append(menu_item)

    def getSidebarCustomMenuItems(self) -> list:
        return self._sidebar_custom_menu_items

    @pyqtSlot(result=bool)
    def shouldShowWelcomeDialog(self) -> bool:
        # Only show the complete flow if there is no printer yet.
        return self._machine_manager.activeMachine is None

    @pyqtSlot(result=bool)
    def shouldShowWhatsNewDialog(self) -> bool:
        has_active_machine = self._machine_manager.activeMachine is not None
        has_app_just_upgraded = self.hasJustUpdatedFromOldVersion()

        # Only show the what's new dialog if there's no machine and we have just upgraded
        show_whatsnew_only = has_active_machine and has_app_just_upgraded
        return show_whatsnew_only

    @pyqtSlot(result=int)
    def appWidth(self) -> int:
        main_window = QtApplication.getInstance().getMainWindow()
        if main_window:
            return main_window.width()
        return 0

    @pyqtSlot(result=int)
    def appHeight(self) -> int:
        main_window = QtApplication.getInstance().getMainWindow()
        if main_window:
            return main_window.height()
        return 0

    @pyqtSlot()
    def deleteAll(self, only_selectable: bool = True) -> None:
        super().deleteAll(only_selectable=only_selectable)

        # Also remove nodes with LayerData
        self._removeNodesWithLayerData(only_selectable=only_selectable)

    def _removeNodesWithLayerData(self, only_selectable: bool = True) -> None:
        Logger.log("i", "Clearing scene")
        nodes = []
        for node in DepthFirstIterator(self.getController().getScene().getRoot()):
            if not isinstance(node, SceneNode):
                continue
            if not node.isEnabled():
                continue
            if (not node.getMeshData() and not node.callDecoration("getLayerData")) and not node.callDecoration(
                    "isGroup"):
                continue  # Node that doesnt have a mesh and is not a group.
            if only_selectable and not node.isSelectable():
                continue  # Only remove nodes that are selectable.
            if not node.callDecoration("isSliceable") and not node.callDecoration(
                    "getLayerData") and not node.callDecoration("isGroup"):
                continue  # Grouped nodes don't need resetting as their parent (the group) is resetted)
            nodes.append(node)
        if nodes:
            from UM.Operations.GroupedOperation import GroupedOperation
            op = GroupedOperation()

            for node in nodes:
                from UM.Operations.RemoveSceneNodeOperation import RemoveSceneNodeOperation
                op.addOperation(RemoveSceneNodeOperation(node))

                # Reset the print information
                self.getController().getScene().sceneChanged.emit(node)

            op.push()
            from UM.Scene.Selection import Selection
            Selection.clear()

    @classmethod
    def getInstance(cls, *args, **kwargs) -> "CuraApplication":
        return cast(CuraApplication, super().getInstance(**kwargs))


def kill_all_subprocess():
    return
    try:
        pid_m = psutil.Process(os.getpid())
    except (psutil.AccessDenied, psutil.ZombieProcess, psutil.NoSuchProcess):
        pass
    names2kill = pid_m.children(recursive=True)
    if len(names2kill) == 0:
        return
    for p in names2kill:
        try:
            p.kill()
        except (psutil.AccessDenied, psutil.ZombieProcess, psutil.NoSuchProcess):
            pass
