from PyQt5.QtWidgets import QDialog, QPlainTextEdit, QFileDialog, \
                            QVBoxLayout, QDialogButtonBox, QCheckBox, \
                            QHBoxLayout, QLabel
import sys, os
from datetime import datetime


class BugReportDialog(QDialog):
    def __init__(self, parent=None):

        super().__init__(parent)

        self.parent = parent
        self.setMinimumWidth(640)
        self.setMinimumHeight(480)
        self.setWindowTitle("Bug Description")
        self.label = QLabel("Please describe the bug and whatever led to it below:")

        self._my_textbox = self._textbox()  # Make textbox available out of scope
        self.save_design_checkbox = QCheckBox("TBD design")  # Make checkbox available out of scope
        self.save_logs_checkbox = QCheckBox("TBD logs")  # Make checkbox available out of scope

        layout = QVBoxLayout(self)  # Add layout

        layout.addWidget(self.label)
        layout.addWidget(self._my_textbox)
        layout.addLayout(self._buttons())

    def _buttons(self):
        """
        Buttons for closing the dialog and saving the bug and its description
        """

        def check_checkboxes():
            if not self.save_logs_checkbox.isChecked():
                buttons.button(QDialogButtonBox.Save).setEnabled(False)
            elif self.save_logs_checkbox.isChecked() and \
                  not buttons.button(QDialogButtonBox.Save).isEnabled():
                buttons.button(QDialogButtonBox.Save).setEnabled(True)

        layout = QHBoxLayout()

        self.save_design_checkbox.setChecked(True)
        self.save_logs_checkbox.setChecked(True)
        self.save_design_checkbox.clicked.connect(check_checkboxes)
        self.save_logs_checkbox.clicked.connect(check_checkboxes)

        buttons = QDialogButtonBox()

        buttons.addButton(QDialogButtonBox.Save)
        buttons.addButton(QDialogButtonBox.Close)

        buttons.accepted.connect(self._save_bug_report)
        buttons.rejected.connect(self.close)

        layout.addWidget(self.save_design_checkbox)
        layout.addWidget(self.save_logs_checkbox)
        layout.addWidget(buttons)

        return layout

    def _textbox(self):
        """
        Simple textbox
        """
        text_box = QPlainTextEdit()
        text_box.setMinimumWidth(640)
        text_box.setMinimumHeight(480)
        text_box.setPlaceholderText("You can write text here.")

        return text_box

    def _prompt_save_location(self):
        """
        This prompts the user for path location and returns it
        """
        dialog = QFileDialog(self)
        dialog.setWindowTitle("Crash report path")
        dialog.setFileMode(QFileDialog.Directory)
        dialog.setAcceptMode(QFileDialog.AcceptOpen)
        dialog.setDirectory(os.path.expanduser('~'))

        if sys.platform == "linux" and "KDE_FULL_SESSION" in os.environ:
            dialog.setOption(QFileDialog.DontUseNativeDialog)

        return dialog.getExistingDirectory()

    def pack_design(self, destination):
        from cura.CuraApplication import CuraApplication

        design_path = CuraApplication.getInstance().getTempDir()
        design_name = os.path.split(design_path.name)[-1]

        # --- Zip tempDir into our dir --- #
        if self.save_design_checkbox.isChecked():
            BugReportDialog.zip_directory(design_path.name,
                                          os.path.join(destination, design_name + '.zip'))

    def _save_bug_report(self):
        """
        Saves the bug and its description
        """

        # --- Prompt user for path --- #
        report_destination = self._prompt_save_location()

        # --- Make dir for bug info --- #
        bug_dir_path = os.path.join(report_destination, str(datetime.now()).replace(':', '-'))
        os.makedirs(bug_dir_path)

        # --- Save description --- #
        with open(bug_dir_path + '/bug_description.txt', 'w') as f:
            f.write(self._my_textbox.toPlainText())

        # --- Get bug info --- #
        if self.save_design_checkbox.isChecked():
            self.pack_design(bug_dir_path)

        BugReportDialog.retrieve_logs(bug_dir_path)

        # --- Close dialog --- #
        self.close()

    @staticmethod
    def retrieve_logs(destination: str):
        from cura.CuraApplication import CuraApplication
        from shutil import copy
        app_instance = CuraApplication.getInstance()
        logger = app_instance.getPluginRegistry().getPluginObject('FileLogger').getLogger()
        log_paths = [handler.baseFilename for handler in logger.handlers if hasattr(handler, 'baseFilename')]
        if sys.stderr != sys.__stderr__: log_paths.append(sys.stderr.name)
        if sys.stdout != sys.__stdout__: log_paths.append(sys.stdout.name)
        for path in log_paths:
            copy(path, destination)

    @staticmethod
    def zip_directory(folder_path, zip_file):
        """
        Zips folder in folder_path into zip_file path

        Parameters
        ----------
        folder_path : str
            The path for the dir to be zippped
        zip_file: str
            The dest path in which the zip will be created

        Returns
        -------
        None
        """
        import zipfile, os
        with zipfile.ZipFile(zip_file, mode='w') as zipf:
            len_dir_path = len(folder_path)
            for root, _, files in os.walk(folder_path):
                for file in files:
                    file_path = os.path.join(root, file)
                    zipf.write(file_path, file_path[len_dir_path:])


if __name__ == '__main__':
    print("Don't run me")

