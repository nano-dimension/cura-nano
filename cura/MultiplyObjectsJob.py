# Copyright (c) 2018 Ultimaker B.V.
# Cura is released under the terms of the LGPLv3 or higher.

import copy
from typing import List

from UM.Application import Application
from UM.Job import Job
from UM.Math.AxisAlignedBox import AxisAlignedBox
from UM.Math.Vector import Vector
from UM.Message import Message
from UM.Operations.AddSceneNodeOperation import AddSceneNodeOperation
from UM.Operations.Operation import Operation
from UM.Operations.GroupedOperation import GroupedOperation
from UM.Scene.ExactCopyDecorator import ExactCopyDecorator
from UM.Scene.Iterator.DepthFirstIterator import DepthFirstIterator
from UM.Scene.SceneNode import SceneNode
from UM.i18n import i18nCatalog

from cura.Utils.SceneNodeUtils import groupNodesOperation, ungroupNodesOperation

i18n_catalog = i18nCatalog("cura")


class MultiplyObjectsJob(Job):
    base_error = "Fix the stackup of \"{}\" before multiply"
    error_message = None

    def __init__(self,
                 nodes: List[SceneNode],
                 count_x: int, offset_x: int,
                 count_y: int, offset_y: int):
        super().__init__()
        self._nodes = nodes
        self._count_x = count_x
        self._count_y = count_y
        self._offset_x = offset_x
        self._offset_y = offset_y

        self.status_message = Message(i18n_catalog.i18nc("@info:status", ""), lifetime=0,
                                      dismissable=False, progress=0,
                                      title=i18n_catalog.i18nc("@info:title", "Multiplying Objects"))

    def run(self) -> None:
        application = Application.getInstance()
        application.getController().setActiveTool("TranslateTool")

        self.status_message.show()

        total_count = self._count_x * self._count_y
        if total_count == 1:  # Nothing to do here, move along
            self._allDone()

        # application = Application.getInstance()

        global_container_stack = application.getGlobalContainerStack()
        if global_container_stack is None:
            self._allDone()  # We can't do anything in this case. #TODO: Show error message?

        root = application.getController().getScene().getRoot()
        active_build_plate = application.getMultiBuildPlateModel().activeBuildPlate

        processed_nodes = []  # type: List[SceneNode]

        # Preprocessing: if the node is a part of a group (When would that happen?), use the whole group
        for node in self._nodes:
            current_node = node
            while (current_node.getParent() and
                   (current_node.getParent().callDecoration("isGroup") or
                    current_node.getParent().callDecoration("isSliceable"))):
                current_node = current_node.getParent()

            if current_node in processed_nodes:
                continue
            processed_nodes.append(current_node)

        # Start the work order
        group_op = GroupedOperation()
        group_nodes_op = Operation()

        # If more than one node was selected - group them, so that they're multiplied as a group
        do_ungroup_nodes = False
        if len(processed_nodes) > 1:
            do_ungroup_nodes = True
            node, group_nodes_op = groupNodesOperation(nodes=processed_nodes,
                                                       parent_of_group_node=root,
                                                       active_build_plate=active_build_plate)

            # In order for the next operations to succeed, we need to apply this operation
            # But we want it to be undoable and redo-able in one go, so we will undo it later
            group_nodes_op.push()

        else:
            node = processed_nodes[0]

        bbox = self._calculateDeepBoundingBox(node)

        offset_x = bbox.width + self._offset_x
        offset_y = bbox.depth + self._offset_y

        self.status_message.setText("Multiplying Objects")

        new_nodes = []  # type: List[SceneNode]

        # This is a bit complicated. In the case of multiplying multiple models we want to group them,
        #   multiply the group and then ungroup. This is best done by applying the group operation and then undoing it.
        #   However, in the case of a simple multiply on a single model we don't need to bother with it.
        #   To accommodate that, we have a double buffer: a list of operations that would then be transcribed
        #   into a GroupedOperation
        operations = []  # type: List[Operation]

        def declare_new_node(new_node):
            new_nodes.append(new_node)
            operations.append(AddSceneNodeOperation(new_node, root))

            if do_ungroup_nodes:
                for new_child in new_node.getChildren():
                    for child in node.getChildren():
                        if new_child.getName() == child.getName():
                            new_child.addDecorator(ExactCopyDecorator(child))
                            break
            else:
                new_node.addDecorator(ExactCopyDecorator(node))

        for copy_x_idx in range(self._count_x):
            is_new_node_x, new_node_x = (False, node) if copy_x_idx == 0 else (True, self._deepCopyNode(node))

            if is_new_node_x:
                new_node_x.translate(translation=Vector(offset_x * copy_x_idx, 0, 0),
                                     transform_space=SceneNode.TransformSpace.World)
                declare_new_node(new_node_x)

            count_base = copy_x_idx * self._count_y
            for copy_y_idx in range(self._count_y):
                is_new_node_y, new_node_y = (False, new_node_x) if copy_y_idx == 0 else (True,
                                                                                         self._deepCopyNode(new_node_x))

                if is_new_node_y:
                    new_node_y.translate(translation=Vector(0, 0, -offset_y * copy_y_idx),  # Note the axes inversion
                                         transform_space=SceneNode.TransformSpace.World)

                    declare_new_node(new_node_y)

                self.status_message.setProgress(progress=float(count_base + copy_y_idx) / total_count * 100)

        # Finally, if we grouped the original node we have to ungroup it and all the copies back
        if do_ungroup_nodes:
            # Remember that thing where we applied the operations and said we would undo it? Here we go...
            application.getOperationStack().undo()

            # And also prepare the operation to ungroup the duplicated...
            _, ungroup_nodes_op = ungroupNodesOperation(new_nodes)

            # ...and add it to the queue
            operations.append(ungroup_nodes_op)

        self.status_message.setText("Applying Operations")

        # Now transcribe our queue into a GroupedOperation
        for op in operations:
            group_op.addOperation(op)

        # ...and NOW we push it to the stack
        group_op.push()

        # Finished, let's go home
        self._allDone()

    @staticmethod
    def _deepCopyNode(node: SceneNode) -> SceneNode:
        new_node = copy.deepcopy(node)

        # This is important, since __deepcopy__ is specifically implemented in SceneNode and CuraSceneNode, and does not
        #   contain setting the parent. But in our case if we don't set the parent here we might remain with removed
        #   nodes at the stage of later ungrouping
        new_node.setParent(node.getParent())

        # Same build plate
        build_plate_number = node.callDecoration("getBuildPlateNumber")
        for child in DepthFirstIterator(new_node):
            child.callDecoration("setBuildPlateNumber", build_plate_number)

        # Same settings for the children
        for child, new_child in zip(node.getChildren(), new_node.getChildren()):
            new_child.setSettings(child.getSettings())

        # Same settings for the parent
        settings = dict(node.getSettings())
        new_node.setSettings(settings)

        return new_node

    def _allDone(self):
        self.status_message.setProgress(self.status_message.getMaxProgress())
        self.status_message.hide()

    @staticmethod
    def _calculateDeepBoundingBox(node: SceneNode) -> AxisAlignedBox:
        """
        This calculates the full bounding box of a node and all its children, no matter how deep.
        Its raison d'être is because CuraSceneNodes do not handle meta groups well, and tend to miscalculate the
        AABB in those cases
        """
        mesh_data = node.getMeshData()
        if mesh_data:
            aabb = mesh_data.getExtents(node.getWorldTransformation(copy=False))
        else:
            aabb = None

        for child in node.getAllChildren():
            if aabb is None:
                aabb = child.getBoundingBox()
            else:
                aabb = aabb + child.getBoundingBox()  # For some reason += disabled in AxisAlignedBox

        return aabb
