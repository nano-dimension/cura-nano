from cura.Operations.OperationWithCallback import OperationWithCallback
from UM.Operations.OperationStack import OperationStack
from cura.Scene.CuraSceneNode import CuraSceneNode


class UpdateLayerTypeOperation(OperationWithCallback):
    def __init__(self, operation_stack: OperationStack, node: CuraSceneNode, layer_index, layer_type, undo_callback=None, redo_callback=None):
        super().__init__(operation_stack, undo_callback=undo_callback, redo_callback=redo_callback)
        self._node = node
        self._layer_index = layer_index
        self._layer_type = layer_type
        self._prev_layer_type = None

    def _undo(self) -> None:
        self._node.updateLayerType(self._layer_index, self._prev_layer_type)

    def _redo(self) -> None:
        self._prev_layer_type = self._node.updateLayerType(self._layer_index, self._layer_type)
