from cura.Operations.OperationWithCallback import OperationWithCallback
from UM.Operations.OperationStack import OperationStack
from cura.Scene.CuraSceneNode import CuraSceneNode


class UpdateLayerThicknessOperation(OperationWithCallback):
    def __init__(self, operation_stack: OperationStack, node: CuraSceneNode, layer_index, thickness, undo_callback=None, redo_callback=None):
        super().__init__(operation_stack, undo_callback=undo_callback, redo_callback=redo_callback)
        self._node = node
        self._layer_index = layer_index
        self._thickness = thickness
        self._prev_thickness = None

    def _undo(self) -> None:
        self._node.updateLayerThickness(self._layer_index, self._prev_thickness)

    def _redo(self) -> None:
        self._prev_thickness = self._node.updateLayerThickness(self._layer_index, self._thickness)
