import copy
from cura.Operations.OperationWithCallback import OperationWithCallback
from UM.Operations.OperationStack import OperationStack
from cura.Utils.PCBHandler.PCBUtils.PcbUtils import Layer


class AddLayerOperation(OperationWithCallback):
    def __init__(self, operation_stack: OperationStack, node, layer: Layer, layer_index=-1,
                 undo_callback=None, redo_callback=None):
        super().__init__(operation_stack, undo_callback=undo_callback, redo_callback=redo_callback)
        self._node = node
        self._layer = copy.deepcopy(layer)
        self._layer_index = self._node.getStackupCount() if layer_index == -1 else layer_index

    def _undo(self) -> None:
        self._node.removeLayerByIndex(self._layer_index)

    def _redo(self) -> None:
        # some operations (update layer type/thickness) update the layer in place. so we store and add back
        # a copy of the layer
        self._node.addLayerToStackup(copy.deepcopy(self._layer), self._layer_index)

