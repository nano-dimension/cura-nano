from UM.Operations.Operation import Operation
from UM.Operations.OperationStack import OperationStack

class OperationWithCallback(Operation):
    def __init__(self, operation_stack: OperationStack, undo_callback=None, redo_callback=None) -> None:
        super().__init__()
        self._operation_stack = operation_stack
        self._undo_callback = undo_callback
        self._redo_callback = redo_callback

    def _undo(self):
        raise NotImplementedError("Must implement _undo and _redo")

    def _redo(self):
        raise NotImplementedError("Must implement _undo and _redo")

    def undo(self) -> None:
        self._undo()
        if self._undo_callback:
            self._undo_callback()

    def redo(self) -> None:
        self._redo()
        if self._redo_callback:
            self._redo_callback()

    def push(self) -> None:
        self._operation_stack.push(self)
