from cura.Operations.OperationWithCallback import OperationWithCallback
from UM.Operations.OperationStack import OperationStack
from cura.Scene.CuraSceneNode import CuraSceneNode


class SwapLayersOperation(OperationWithCallback):
    def __init__(self, operation_stack: OperationStack, node: CuraSceneNode, from_index, to_index, undo_callback=None, redo_callback=None):
        super().__init__(operation_stack, undo_callback=undo_callback, redo_callback=redo_callback)
        self._node = node
        self._from_index = from_index
        self._to_index = to_index

    def _undo(self) -> None:
        self._node.swapLayers(self._to_index, self._from_index)

    def _redo(self) -> None:
        self._node.swapLayers(self._from_index, self._to_index)
