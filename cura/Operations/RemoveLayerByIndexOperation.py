import copy
from typing import List
from cura.Operations.OperationWithCallback import OperationWithCallback
from UM.Operations.OperationStack import OperationStack


class RemoveLayerByIndexOperation(OperationWithCallback):
    def __init__(self, operation_stack: OperationStack, node, layer_index: int, add_to_drills: List[int],
                 undo_callback=None, redo_callback=None):
        super().__init__(operation_stack, undo_callback=undo_callback, redo_callback=redo_callback)
        self._node = node
        self._layer_index = layer_index
        self._add_to_drills = add_to_drills
        self._layer = None

    def _undo(self) -> None:
        if self._layer is None:
            return
        # some operations (update layer type/thickness) update the layer in place. so we store and add back
        # a copy of the layer
        self._node.addLayerToStackup(copy.deepcopy(self._layer), layer_index=self._layer_index,
                                     add_to_drills=self._add_to_drills)

    def _redo(self) -> None:
        self._layer = copy.deepcopy(self._node.removeLayerByIndex(self._layer_index, need_it_back=True))
