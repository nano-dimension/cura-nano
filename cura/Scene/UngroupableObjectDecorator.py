from UM.Scene.SceneNodeDecorator import SceneNodeDecorator


class UngroupableObjectDecorator(SceneNodeDecorator):
    def __init__(self) -> None:
        super().__init__()

    def isUngroupable(self) -> bool:
        return True

    def __deepcopy__(self, memo) -> "UngroupableObjectDecorator":
        return type(self)()
