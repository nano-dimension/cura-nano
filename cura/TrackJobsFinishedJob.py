# TODO: Copyright?
# Cura is released under the terms of the LGPLv3 or higher.

import time
from typing import List, Callable

from UM.Job import Job
from UM.Logger import Logger


class TrackJobsFinishedJob(Job):
  
    def __init__(self,
                 jobs_to_track: List[Job],
                 job_finished_condition: Callable = None,
                 sleep_interval_in_sec: float = 0.1,
                 do_cancel_all_on_error: bool = True):
        """
        This job will wait for all other jobs to finish and only then execute its onFinished callback

        :param jobs_to_track: the list of jobs to wait on

        :param job_finished_condition: the condition that has to be fulfilled on the job (defaults to job.isFinished())
        
        :param sleep_interval_in_sec: the time to wait between rechecking the jobs

        :param do_cancel_all_on_error: whether to cancel all jobs and return an error in case of error on a job

        The result of this job is the concatenation of the results of all the jobs it was waiting on
        """
        super().__init__()

        self._jobsToTrack = jobs_to_track  # Type: List[Job]

        self._job_finished_condition = job_finished_condition or self.isJobFinished  # Type: Callable

        self._sleep_interval_in_sec = sleep_interval_in_sec  # Type: float

        self._do_cancel_on_all_on_error = do_cancel_all_on_error  # Type: bool

        # NB: This only cancels the job itself. Cancelling the tracked jobs is done via the cancel() method
        self._cancelled = False  # Type: bool

        self._result = []  # Type: List[Any]

    def run(self) -> None:
        """
        This will wait for each job to finish in its turn and add the job's result to its own
        """
        Logger.log("i", "Started waiting on jobs.")
        for job in self._jobsToTrack:

            Logger.log("i", "waiting on job {}".format(job))

            while True:

                if self._cancelled:
                    return  # dang this, let's go home...

                if job.hasError():
                    if self._do_cancel_on_all_on_error:
                        self.setError(job.getError())  # propagate the error
                        self.cancel()  # cancel all
                        return

                    else:  # Append the error
                           # TODO: This is not a good solution, since _error is defined as an Exception, not a list.
                           #  Design an aggregate exception later
                        if not isinstance(self._error, list):
                            if self._error is None:
                                self._error = []
                            else:
                                self._error = [self._error]

                        self._error.append(job.getError())
                        break

                elif self._job_finished_condition(job):  # are we there yet?
                    Logger.log("i", "done, getting result.")
                    self._result += job.getResult()  # append the result
                    break
                    
                else:
                    time.sleep(self._sleep_interval_in_sec)  # catch some z-s

    @staticmethod
    def isJobFinished(job):
        return job.isFinished()

    def cancel(self):
        """
        This cancels the job itself as well as all the jobs it's waiting on
        :return:
        """
        super().cancel()

        for job in self._jobsToTrack:
            job.cancel()

        self._cancelled = True

    def isCancelled(self):
        return self._cancelled
