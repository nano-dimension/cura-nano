#TODO: License

from typing import List, Tuple

from UM.Operations.Operation import Operation
from UM.Operations.GroupedOperation import GroupedOperation
from UM.Scene.GroupDecorator import GroupDecorator
from UM.Scene.SceneNode import SceneNode

from cura.Scene.BuildPlateDecorator import BuildPlateDecorator
from cura.Scene.ConvexHullDecorator import ConvexHullDecorator
from cura.Scene.CuraSceneNode import CuraSceneNode
from cura.Operations.SetParentOperation import SetParentOperation


def groupNodesOperation(nodes: List[SceneNode],
                        parent_of_group_node: SceneNode = None,
                        active_build_plate: int = None) -> Tuple[CuraSceneNode, Operation]:
    """
    Creates a group_node that would group all the nodes in the input under it, and generates the GroupedOperation
    that would make it so.
    NB: It is the responsibility of the caller to apply (push) those operations

    :param nodes:
    :param active_build_plate: The active build plate the group_node should be assigned
    :param parent_of_group_node: The parent the group_node should be placed under

    :return: the group_node, and the GroupedOperation that needs to be applied to place all the nodes under it
    """

    # Create a group-node
    group_node = CuraSceneNode()
    group_node.addDecorator(GroupDecorator())
    group_node.addDecorator(ConvexHullDecorator())
    group_node.setSelectable(True)

    if active_build_plate is not None:
        group_node.addDecorator(BuildPlateDecorator(active_build_plate))

    if parent_of_group_node is not None:
        group_node.setParent(parent_of_group_node)

    # First iteration: Remove nodes that are directly parented to another node from the input so they remain parented
    processed_nodes = []
    for node in nodes:
        parent = node.getParent()
        if parent is None or parent not in nodes or parent.callDecoration("isGroup"):
            processed_nodes.append(node)

    # Second iteration: Move selected nodes into the group-node
    set_parent_gr_op = GroupedOperation()

    for node in processed_nodes:
        set_parent_gr_op.addOperation(SetParentOperation(node, group_node))

    return group_node, set_parent_gr_op


def ungroupNodesOperation(nodes: List[SceneNode]) -> Tuple[List[CuraSceneNode], Operation]:
    """
    Removes one level of grouping from all the nodes in the list of nodes passed to the method.

    NB: This method returns the GroupedOperation that does that, it is the responsibility of the caller
        to apply (push) those operations

    :param nodes: the list of nodes to ungroup (each one separately, and only if it's a group)

    :return: the newly ungrouped nodes and the GroupedOperation that needs to be applied to remove one layer of grouping
    """

    new_nodes = []
    ungroup_ops = GroupedOperation()
    for node in nodes:
        if node.callDecoration("isGroup"):
            group_parent = node.getParent()
            children = node.getChildren().copy()
            for child in children:
                # Ungroup only 1 level deep
                if child.getParent() != node:
                    continue

                # Set the parent of the children to the parent of the group-node
                ungroup_ops.addOperation(SetParentOperation(child, group_parent))

                # Add them to the list to be returned
                new_nodes.append(child)

    return new_nodes, ungroup_ops
