# TODO: Copyright
import os
import sys
from enum import Enum
from shutil import copyfile
from typing import Tuple
from cura.Utils.PCBHandler.PCBUtils.ContentValidation import ContentValidation
from cura.Utils.PCBHandler.PCBUtils.Global.GlobalParamsHandler import GlobalParamsHandler
from cura.Utils.PCBHandler.PCBUtils.Global.Consts import Consts


def add_to_filename(filename, addition):
    if addition == "":
        return filename
    wo, ext = os.path.splitext(filename)
    filename = "%s_%s%s" % (wo, addition, ext)
    return filename


def copyFileWithIncreasingNameIfExist(src, dst):
    counter = 1
    dst_copy = dst
    while os.path.exists(dst):
        dst = add_to_filename(dst_copy, str(counter))
        counter += 1

    copyfile(src, dst)
    return dst
