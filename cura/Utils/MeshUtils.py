# TODO: License
from copy import deepcopy
from typing import List
from trimesh import Trimesh
import numpy as np

from UM.Mesh.MeshData import MeshData, calculateNormalsFromIndexedVertices


def meshDataToTrimesh(mesh_data: MeshData,
                      do_swap_axes: bool = False) -> Trimesh:
    """
    Convert Uranium MeshData to Trimesh
    :param mesh_data: MeshData to convert
    :param do_swap_axes: whether to swap the axes from Cura to Cartesian
    :return: Trimesh
    """
    vertices = mesh_data.getVertices()
    faces = mesh_data.getIndices()

    if not faces:
        faces = []
        for vertex_id in range(0, len(vertices), 3):
            faces.append([vertex_id, vertex_id + 1, vertex_id + 2])

    if do_swap_axes:
        # Invert values of second column
        vertices[:, 1] *= -1

        # Swap column 1 and 2 (We have a different coordinate system)
        swapColumns(vertices, 1, 2)

    return Trimesh(vertices=vertices,
                   faces=faces)


def combineTwoTrimeshes(mesh1: Trimesh,
                        mesh2: Trimesh,
                        offset: List[float] = [0, 0, 0],
                        auto_z: bool = False) -> Trimesh:
    """
    :param mesh1: trimesh to be combined
    :param mesh2: trimesh to be combined(need to be related to first mesh coordinates)
    :param offset: where to put the second mesh related to 1 mesh
    :param auto_z: if True- second mesh will be automatically moved up by the
                    height of first mesh
    :return: a combined mesh
    """
    mesh1_copy = deepcopy(mesh1)
    mesh2_copy = deepcopy(mesh2)
    if auto_z:
        offset[2] = max(mesh1_copy.vertices, key=lambda v: v[2])[2]
    index_offset = len(mesh1_copy.vertices)

    vertex_list = [mesh1_copy.vertices, mesh2_copy.vertices]
    for vertex in vertex_list[1]:
        vertex += offset

    faces_list = [mesh1_copy.faces, mesh2_copy.faces]
    for face in faces_list[1]:
        face += index_offset
    vertices = np.vstack(vertex_list)
    faces = np.vstack(faces_list)
    return Trimesh(vertices, faces)


def trimeshToMeshData(tri_node: Trimesh,
                      file_name: str = "",
                      do_swap_axes: bool = True) -> MeshData:
    """Converts a Trimesh to Uranium's MeshData.

    :param tri_node: A Trimesh containing the contents of a file that was just read.
    :param file_name: The full original filename used to watch for changes
    :param do_swap_axes: whether to swap axes from cartesian to Cura
    :return: Mesh data from the Trimesh in a way that Uranium can understand it.
    """

    tri_faces = tri_node.faces
    tri_vertices = tri_node.vertices

    indices = []
    vertices = []

    index_count = 0
    face_count = 0
    for tri_face in tri_faces:
        face = []
        for tri_index in tri_face:
            vertices.append(tri_vertices[tri_index])
            face.append(index_count)
            index_count += 1
        indices.append(face)
        face_count += 1

    vertices = np.asarray(vertices, dtype=np.float32)

    if do_swap_axes:
        # Invert values of second column
        vertices[:, 1] *= -1

        # Swap column 1 and 2 (We have a different coordinate system)
        swapColumns(vertices, 1, 2)

    indices = np.asarray(indices, dtype=np.int32)
    normals = calculateNormalsFromIndexedVertices(vertices, indices, face_count)

    mesh_data = MeshData(vertices=vertices, indices=indices, normals=normals, file_name=file_name)
    return mesh_data


def swapColumns(array, frm, to):
    array[:, [frm, to]] = array[:, [to, frm]]