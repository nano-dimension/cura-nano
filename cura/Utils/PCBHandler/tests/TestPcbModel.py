import os
import tempfile

from UM.Settings.Interfaces import ContainerInterface
from artworkengine.pcbProcessUtils import LayerType
from cura.Utils.PCBHandler.PCBUtils.GerberReadUtils import readGerberFile
from cura.Utils.PCBHandler.PCBUtils.PcbModel import PcbModel
from cura.Utils.PCBHandler.gerberTools.cam import FileSettings
from plugins.GerberReader.GerberReader import GerberReader
from plugins.ODBReader.ODBReader import ODBReader
from tests.Settings.MockContainer import MockContainer

# TODO organize all tests in a suite and create a temp directory for each test based on test name
temp_dir = tempfile.TemporaryDirectory(prefix="TestPcbModel")

TESTS_PATH = os.path.dirname(__file__)


# TODO get the default settings from the printer json
def getDefaultSettings() -> ContainerInterface:
    mock_settings = MockContainer()
    mock_settings.setProperty('annotation_top_layer_thickness', 'annotation_top_layer_thickness', 3)
    mock_settings.setProperty('annotation_bottom_layer_thickness', 'annotation_bottom_layer_thickness', 3)
    mock_settings.setProperty('soldermask_top_layer_thickness', 'soldermask_top_layer_thickness', 50)
    mock_settings.setProperty('soldermask_bottom_layer_thickness', 'soldermask_bottom_layer_thickness', 50)
    mock_settings.setProperty('signal_layer_thickness', 'signal_layer_thickness', 17)
    mock_settings.setProperty('min_size_for_pth', 'min_size_for_pth', 0.7)
    mock_settings.setProperty('prepreg_layer_thickness', 'prepreg_layer_thickness', 50)
    mock_settings.setProperty('scale', 'scale', 100.0)
    mock_settings.setProperty('create_mesh_from_route', 'create_mesh_from_route', True)
    mock_settings.setProperty('enlarge_all_apertures_by_constant', 'enlarge_all_apertures_by_constant', 0.0)

    return mock_settings


class PCBModelTest:
    def __init__(self, pcb_model: PcbModel, settings: ContainerInterface = None):
        self._pcb_model = pcb_model
        settings = settings or getDefaultSettings()
        self._settings = settings

    @property
    def pcb_model(self):
        return self._pcb_model

    @property
    def settings(self):
        return self._settings


class ODBTest(PCBModelTest):
    def __init__(self, odb_path, expected_layers, expected_drills, expected_recipe, expected_thickness,
                 settings: ContainerInterface = None):
        settings = settings or getDefaultSettings()

        odb_reader = ODBReader()
        odb_reader.setGlobalContainerStack(settings)

        self._odb_path = os.path.abspath(os.path.join(odb_path))
        pcb_model = odb_reader._read(self._odb_path, no_setting_override=True, temp_dir=temp_dir)
        assert pcb_model is not None
        assert pcb_model.getStackupCount() == expected_layers
        assert pcb_model.getDrillsCount() == expected_drills
        assert pcb_model.getPreferredRecipe() == expected_recipe
        assert pcb_model.getTotalThikness() == expected_thickness
        PCBModelTest.__init__(self, pcb_model, settings)

    @property
    def odb_path(self):
        return self._odb_path


def test_ODBReader_updateThickness():
    odb_test = ODBTest(os.path.join(TESTS_PATH, "resources", "25-99-397-0_PCB.tgz"), 19, 3, None, 1.9)
    odb_model = odb_test.pcb_model
    layers_to_remove = ["comp_+_top", "top_paste", "bottom_paste", "comp_+_bot", "keep-out_layer",
                        "3d_body", "board_outline", "gerber_marking"]
    for layer_to_remove in layers_to_remove:
        assert odb_model.removeLayerByName(layer_to_remove) is not None, "Failed to remove layer {}".format(
            layer_to_remove)
    top_overlay_layer_index = odb_model.getLayerIndexByName("top_overlay")
    assert top_overlay_layer_index == 0
    assert odb_model.updateLayerThickness(top_overlay_layer_index, 3) == 35
    bottom_overlay_layer_index = odb_model.getLayerIndexByName("bottom_overlay")
    assert bottom_overlay_layer_index == 10
    assert odb_model.updateLayerThickness(bottom_overlay_layer_index, 3) == 35
    for layer_index in range(odb_model.getStackupCount()):
        layer = odb_model.getLayerCopyByIndex(layer_index)
        assert layer.thickness != 0, "layer {} thickness is 0".format(layer.name)


def test_ODBReader_PHC_PCB_missingRoute():
    odb_test = ODBTest(os.path.join(TESTS_PATH, "resources", "PHC_PCB.tgz"), 25, 8, None, 2.002)
    odb_model = odb_test.pcb_model

    route_layer = odb_model.removeLayerByName("mechanical1", need_it_back=True)
    odb_model.addRoute(path=route_layer.path)
    layers_to_remove = ["comp_+_top", "toppaste", "bottompaste", "comp_+_bot",
                        "mechanical3", "mechanical8", "mechanical13", "mechanical15", "mechanical16"]

    for layer_to_remove in layers_to_remove:
        assert odb_model.removeLayerByName(layer_to_remove) is not None, "Failed to remove layer {}".format(
            layer_to_remove)
    assert odb_model.getStackupCount() == 15
    assert odb_model.getDrillsCount() == 8
    assert odb_model.getPreferredRecipe() is None
    assert odb_model.getTotalThikness() == 1.722

    top_overlay_layer_index = odb_model.getLayerIndexByName("topoverlay")
    assert top_overlay_layer_index == 0
    assert odb_model.updateLayerThickness(top_overlay_layer_index, 3) == 35
    bottom_overlay_layer_index = odb_model.getLayerIndexByName("bottomoverlay")
    assert bottom_overlay_layer_index == 14
    assert odb_model.updateLayerThickness(bottom_overlay_layer_index, 3) == 35
    for layer_index in range(odb_model.getStackupCount()):
        layer = odb_model.getLayerCopyByIndex(layer_index)
        assert layer.thickness != 0, "layer {} thickness is 0".format(layer.name)


def test_ODBReader_12elic_d_coupon():
    odb_test = ODBTest(os.path.join(TESTS_PATH, "resources", "12elic_d-coupon.tgz"), 29, 12, None, 0.894)
    odb_model = odb_test.pcb_model

    layers_to_remove = ["gold_top", "gold_bottom"]

    for layer_to_remove in layers_to_remove:
        assert odb_model.removeLayerByName(layer_to_remove) is not None, "Failed to remove layer {}".format(
            layer_to_remove)
    assert odb_model.getStackupCount() == 27
    assert odb_model.getDrillsCount() == 12
    assert odb_model.getPreferredRecipe() is None
    assert odb_model.getTotalThikness() == 0.86

    for layer_index in range(odb_model.getStackupCount()):
        layer = odb_model.getLayerCopyByIndex(layer_index)
        assert layer.thickness != 0, "layer {} thickness is 0".format(layer.name)


def test_ODBReader_coil_dc2dc():
    odb_test = ODBTest(os.path.join(TESTS_PATH, "resources", "Coil_DC-DC-USB.tgz"), 44, 26, None, 1.77)
    odb_model = odb_test.pcb_model

    route_layer = odb_model.removeLayerByName("board", need_it_back=True)
    odb_model.addRoute(path=route_layer.path)
    layers_to_remove = ["comp_+_top"]
    for layer_to_remove in layers_to_remove:
        assert odb_model.removeLayerByName(layer_to_remove) is not None, "Failed to remove layer {}".format(
            layer_to_remove)
    silk_screen_top_index = 1
    assert odb_model.updateLayerThickness(silk_screen_top_index, 3) == 35
    top_solder_mask_index = 2
    assert odb_model.updateLayerThickness(top_solder_mask_index, 50) == 34
    bottom_solder_mask_index = 41
    assert odb_model.updateLayerThickness(bottom_solder_mask_index, 50) == 35

    assert odb_model.getStackupCount() == 42
    assert odb_model.getDrillsCount() == 26
    assert odb_model.getPreferredRecipe() is None
    assert odb_model.getTotalThikness() == 1.714
    for layer_index in range(odb_model.getStackupCount()):
        layer = odb_model.getLayerCopyByIndex(layer_index)
        assert layer.thickness != 0, "layer {} thickness is 0".format(layer.name)


class GerbersTest(PCBModelTest):
    def __init__(self, gerbers_path, expected_layers, expected_drills, expected_recipe, expected_thickness,
                 settings: ContainerInterface = None):
        settings = settings or getDefaultSettings()

        gerber_reader = GerberReader()
        gerber_reader.setGlobalContainerStack(settings)

        self._gerbers_path = gerbers_path
        gerber_files = [os.path.abspath(os.path.join(self._gerbers_path, gerber_file)) for
                        gerber_file in os.listdir(self._gerbers_path)
                        if os.path.isfile(os.path.join(self._gerbers_path, gerber_file))]
        gerber_model = gerber_reader._read(gerber_files, no_setting_override=True, temp_dir=temp_dir)
        assert gerber_model is not None
        assert gerber_model.getStackupCount() == expected_layers
        assert gerber_model.getDrillsCount() == expected_drills
        assert gerber_model.getPreferredRecipe() == expected_recipe
        assert gerber_model.getTotalThikness() == expected_thickness
        PCBModelTest.__init__(self, gerber_model, settings)

    @property
    def gerbers_path(self):
        return self._gerbers_path


def test_HATS_moveLayerToDrill():
    gerbers_test = GerbersTest(os.path.join(TESTS_PATH, "resources", "HATS"), 18, 0, None, 0.586)
    gerber_model = gerbers_test.pcb_model
    # DRILLS.drl is actually a gerber file of drills
    drills_name = "DRILLS.drl"
    drills_file_path = os.path.abspath(os.path.join(gerbers_test.gerbers_path, drills_name))
    new_drills, _ = gerber_model.addDrill(path=drills_file_path)
    assert len(new_drills) > 0

    assert gerber_model.removeLayerByName(drills_name) is not None
    assert gerber_model.getStackupCount() == 17
    assert gerber_model.getDrillsCount() == 5
    assert gerber_model.getPreferredRecipe() == 'High Quality Fine Vias'
    assert gerber_model.getTotalThikness() == 0.586


def test_Gerbers_Arduino():
    gerbers_test = GerbersTest(os.path.join(TESTS_PATH, "resources", "arduino"), 5, 2, None, 0.184)
    assert gerbers_test is not None


def test_Gerbers_080_326():
    gerbers_test = GerbersTest(os.path.join(TESTS_PATH, "resources", "080-326_opt"), 13, 7, None, 0.452)
    assert gerbers_test is not None


def test_addDrill_Existing_Jabil_Emerson():
    gerbers_test = GerbersTest(os.path.join(TESTS_PATH, 'resources', 'Jabil_Emerson'), 13, 0, None, 0.358)
    gerber_model = gerbers_test.pcb_model
    model_folder = gerbers_test.gerbers_path

    ## drill file loading
    drill_path = os.path.join(model_folder, 'drill', 'drilling.drl')
    drill_name = os.path.basename(drill_path)

    # first add should succeed
    new_drills, _ = gerber_model.addDrill(path=drill_path)
    assert len(new_drills) > 0

    # double add - should succeed
    new_drills, _ = gerber_model.addDrill(path=drill_path)
    assert len(new_drills) == 0

    ## again
    new_drills, _ = gerber_model.addDrill(path=drill_path)
    assert len(new_drills) == 0

    ## --------- add other drill to test remove -----------
    drills_count_before = gerber_model.getDrillsCount()
    drill_path2 = os.path.join(model_folder, 'drill', 'Arduino Nano15-Plated.TXT')
    drill_name2 = os.path.basename(drill_path2)
    new_drills, _ = gerber_model.addDrill(path=drill_path2)
    drill_count_after_add1 = gerber_model.getDrillsCount()
    assert drills_count_before < drill_count_after_add1
    new_drills, _ = gerber_model.addDrill(path=drill_path2)
    drill_count_after_add2 = gerber_model.getDrillsCount()
    assert drill_count_after_add1 == drill_count_after_add2
    gerber_model.removeDrillByName(drill_name2)
    drill_count_after_remove = gerber_model.getDrillsCount()
    assert drill_count_after_remove == drill_count_after_add1 - 1


def test_ExcellonChange_For_Warning_Arduino():
    gerbers_test = GerbersTest(os.path.join(TESTS_PATH, "resources", "arduino"), 5, 2, None, 0.184)
    assert gerbers_test is not None
    gerber_model = gerbers_test.pcb_model

    # lets see if we have warning when opening the model because drills are out of routes-shouldn't be
    gerber_model.readStackupFromFiles()
    for drill in gerber_model.getDrills():
        assert drill.isOutOfRoute is False

    # now lets change the settings to format 2,5 and check again - should be warning
    bad_settings = FileSettings(format=(2, 5))
    updated_drill = gerber_model.updateDrillSettings(0, bad_settings)
    assert updated_drill.isOutOfRoute
    # check also that the other drills are ok
    for drill in gerber_model.getDrills():
        if drill.original_path == updated_drill.original_path:
            assert drill.isOutOfRoute
        else:
            assert not drill.isOutOfRoute

    # and now lets change to the loading settings (the good one) should not be a warning
    good_settings = FileSettings(notation='absolute', units='metric',
                                 zero_suppression='leading', format=(4, 4),
                                 angle_units='degrees')
    updated_drill = gerber_model.updateDrillSettings(0, good_settings)
    assert not updated_drill.isOutOfRoute
    for drill in gerber_model.getDrills():
        assert drill.isOutOfRoute is False


def test_ExcellonChange_For_Warning_ofOvalDriils_Iot():
    gerbers_test = GerbersTest(os.path.join(TESTS_PATH, "resources", "IOT"), 6, 6, None, 0.187)

    assert gerbers_test is not None

    gerber_model = gerbers_test.pcb_model

    # lets see if we have warning when opening the model because drills are out of routes-shouldn't be
    gerber_model.readStackupFromFiles()
    for drill in gerber_model.getDrills():
        assert drill.isOutOfRoute is False

    # now lets change the settings to format 2,5 and check again - should be warning
    bad_settings = FileSettings(format=(2, 5))
    updated_drill = gerber_model.updateDrillSettings(2, bad_settings)
    assert updated_drill.isOutOfRoute
    # check also that the other drills are ok
    for drill in gerber_model.getDrills():
        if drill.original_path == updated_drill.original_path:
            assert drill.isOutOfRoute
        else:
            assert not drill.isOutOfRoute

    # and now lets change to the loading settings (the good one) should not be a warning
    good_settings = FileSettings(notation='absolute', units='metric',
                                 zero_suppression='leading', format=(4, 4),
                                 angle_units='degrees')
    updated_drill = gerber_model.updateDrillSettings(2, good_settings)
    assert not updated_drill.isOutOfRoute
    for drill in gerber_model.getDrills():
        assert drill.isOutOfRoute is False


def test_UpdateDrillDiameter_Iot():
    gerbers_test = GerbersTest(os.path.join(TESTS_PATH, "resources", "IOT"), 6, 6, None, 0.187)

    assert gerbers_test is not None

    gerber_model = gerbers_test.pcb_model

    pcb_model = readGerberFile(gerber_model.getDrills()[0].path)
    assert len(pcb_model.cam_source.apertures) == 1
    assert pcb_model.cam_source.apertures[200].diameter == 0.3

    # change drill 0 diameter 0.3 -> 0.4
    assert gerber_model.getDrills()[0].diameter == 0.3
    updated_drill, _ = gerber_model.updateDrillDiameter(0, 0.4)
    assert updated_drill is not None
    assert updated_drill.diameter == 0.4

    pcb_model = readGerberFile(gerber_model.getDrills()[0].path)
    assert len(pcb_model.cam_source.apertures) == 1
    assert pcb_model.cam_source.apertures[200].diameter == 0.4

    # change drill 0 diameter 0.4 -> 0.5
    assert gerber_model.getDrills()[0].diameter == 0.4
    updated_drill, _ = gerber_model.updateDrillDiameter(0, 0.5)
    assert updated_drill is not None
    assert updated_drill.diameter == 0.5

    pcb_model = readGerberFile(gerber_model.getDrills()[0].path)
    assert len(pcb_model.cam_source.apertures) == 1
    assert pcb_model.cam_source.apertures[200].diameter == 0.5

    # test failing to update a non existed drill, or invalid values
    assert gerber_model.updateDrillDiameter(-1, 0.5)[0] is None
    assert gerber_model.updateDrillDiameter(11, 0.5)[0] is None
    assert gerber_model.updateDrillDiameter(0, - 0.5)[0] is None
    assert gerber_model.updateDrillDiameter(0, 370)[0] is None

    # update a drill with DCode != 200
    pcb_model = readGerberFile(gerber_model.getDrills()[5].path)
    assert len(pcb_model.cam_source.apertures) == 1
    assert pcb_model.cam_source.apertures[203].diameter == 1.1

    # change drill 5 diameter 1.1 -> 0.9
    assert gerber_model.getDrills()[5].diameter == 1.1
    updated_drill, _ = gerber_model.updateDrillDiameter(5, 0.9)
    assert updated_drill is not None
    assert updated_drill.diameter == 0.9

    pcb_model = readGerberFile(gerber_model.getDrills()[5].path)
    assert len(pcb_model.cam_source.apertures) == 1
    assert pcb_model.cam_source.apertures[203].diameter == 0.9


def testForDrillMarkTO_aviram2():
    gerbers_test = GerbersTest(os.path.join(TESTS_PATH, "resources", "aviram2"), 11, 2, None, 0.324)
    assert gerbers_test is not None
    # lets check how changing the layer's type influencing the number of signals
    assert gerbers_test.pcb_model.getNumberOfSignalLayers() == 4
    gerbers_test.pcb_model.updateLayerType(1, LayerType.Signal)
    assert gerbers_test.pcb_model.getNumberOfSignalLayers() == 5
    gerbers_test.pcb_model.removeLayerByIndex(2, False)
    assert gerbers_test.pcb_model.getNumberOfSignalLayers() == 4
    assert gerbers_test.pcb_model.getStackupCount() == 10
    layer_path = os.path.join(TESTS_PATH, "resources", "aviram2", "Gerber_Inner1.G1")
    gerbers_test.pcb_model.appendLayerFromPathToStackup(layer_path)
    assert gerbers_test.pcb_model.getNumberOfSignalLayers() == 5
    # lets update drills with mark from to
    current_list1 = [{'state_bool': False}, {'state_bool': False}, {'state_bool': False}, {'state_bool': False},
                     {'state_bool': True}, {'state_bool': True}, {'state_bool': True}, {'state_bool': True},
                     {'state_bool': True}, {'state_bool': True}, {'state_bool': True}]
    current_list2 = [{'state_bool': True}, {'state_bool': True}, {'state_bool': True}, {'state_bool': True},
                     {'state_bool': True}, {'state_bool': True}, {'state_bool': False}, {'state_bool': False},
                     {'state_bool': False}, {'state_bool': False}, {'state_bool': False}]
    drills_list = gerbers_test.pcb_model.getDrillsAsDict()
    drills_list[0]['layers'] = current_list1
    drills_list[1]['layers'] = current_list2
    gerbers_test.pcb_model.updateDrillsFromGui(drills_list)
    gerber_model = gerbers_test.pcb_model
    assert gerber_model._drills[0].layers == current_list1
    assert gerber_model._drills[1].layers == current_list2


def testForEmptyLayersAndDrills_LED_MATRIX_odb():
    odb_test = ODBTest(os.path.join(TESTS_PATH, "resources", "LED_MATRIX.tgz"), 16, 8, None, 1.6)
    odb_model = odb_test.pcb_model

    route_layer = odb_model.removeLayerByName("board", need_it_back=True)
    odb_model.addRoute(path=route_layer.path)


def test_ODB_SupportInchAndMetricThickness():
    odb_test_mm = ODBTest(os.path.join(TESTS_PATH, "resources", "ASDC150V1_1_mm.tgz"), 10, 3, None, 1.076)
    odb_model_mm = odb_test_mm.pcb_model

    route_layer = odb_model_mm.removeLayerByName("mechanical_1", need_it_back=True)
    odb_model_mm.addRoute(path=route_layer.path)

    odb_test_inch = ODBTest(os.path.join(TESTS_PATH, "resources", "ASDC150V1_1_inch.tgz"), 10, 3, None, 1.076)
    odb_model_inch = odb_test_inch.pcb_model

    route_layer = odb_model_inch.removeLayerByName("mechanical_1", need_it_back=True)
    odb_model_inch.addRoute(path=route_layer.path)

    layers_to_remove = ["comp_+_top", "comp_+_bot"]
    for layer_to_remove in layers_to_remove:
        assert odb_model_mm.removeLayerByName(layer_to_remove) is not None, "Failed to remove layer {}".format(
            layer_to_remove)
        assert odb_model_inch.removeLayerByName(layer_to_remove) is not None, "Failed to remove layer {}".format(
            layer_to_remove)

    layers_thickness = [3, 50, 35, 900, 35, 50, 3]
    for layer_index in range(7):
        assert odb_model_mm.getLayerCopyByIndex(layer_index).thickness == layers_thickness[layer_index]
        assert odb_model_inch.getLayerCopyByIndex(layer_index).thickness == layers_thickness[layer_index]
