import logging
import math
import os

import cv2
import numpy as np
from UM.Job import Job
from UM.Message import Message
from UM.Logger import Logger
from cura.Utils.PCBHandler.PCBUtils.PcbModel import PcbModel
from cura.Utils.PCBHandler.PCBUtils.PcbUtils import LayerType, Layer
from cv2 import imread, imwrite
from tqdm import tqdm


class UnionPreviewImageJob(Job):
    def __init__(self, node: PcbModel):
        super().__init__()
        self.node = node
        self._result = []
        self._loading_message = None

    def run(self):
        if not isinstance(self.node, PcbModel):
            return

        if self.node.getRoutesCount() == 0:
            self.node.setPreviewImage("canceled")
            return
        self.node.setPreviewImage(None)

        self._loading_message = Message(self.node.getName(),
                                        lifetime=0,
                                        progress=0,
                                        dismissable=False,
                                        title='Updating Preview Image')
        self._loading_message.show()

        try:
            preview_image = self._calculateUnionPreviewImage()
            self.node.setPreviewImage(preview_image)
        except Exception as e:
            Logger.logException(logging.WARN, "Union preview failed : {}".format(str(e)))
            self.node.setPreviewImage("canceled")
        finally:
            self._loading_message.hide()

    def _calculateUnionPreviewImage(self):
        # if this is the first time preview is created, need to define a color for each drill and each layer,
        # other wise need to add color to the array when adding drills/layers and so on

        # if any layer or drill preview image size is different than the route image size, we will reset this layer
        # or drill preview image and cancel this job, so that next time it will be created again
        route = self.node.getRoute()
        global_image = imread(route.preview_image_path, cv2.THRESH_BINARY).astype(np.uint8)
        size_mismatch_detected = False
        layer_preview_images = {}
        for layer_id, layer in tqdm(enumerate(self.node.getStackup() + self.node.getDrills())):
            if not self._loading_message.visible:
                raise Exception("Loading message no longer visible")
            if layer.preview_image_path is None:
                continue
            if isinstance(layer, Layer) and layer.type == LayerType.Prepreg:
                continue
            layer_preview_images[layer_id] = imread(os.path.join(self.node.getTempDir(), layer.preview_image_path),
                                                    cv2.THRESH_BINARY).astype(np.uint8)
            if layer_preview_images[layer_id].shape != global_image.shape:
                Logger.log("w", "Preview image of layer {} size mismatch detected {} != {}", layer.name,
                           layer_preview_images[layer_id].shape, global_image.shape)
                layer.preview_image_path = None
                size_mismatch_detected = True
        if size_mismatch_detected:
            self.node.createPreview()
            return "canceled"

        for layer_id, layer in tqdm(enumerate(self.node.getStackup() + self.node.getDrills())):
            if not self._loading_message.visible:
                raise Exception("Loading message no longer visible")
            if layer.to_view is False:
                continue
            if isinstance(layer, Layer) and layer.type == LayerType.Prepreg:
                continue
            # the magic number 7 is to insure that together with modulo 180 in color map, each combination will get different color
            # Each layer is offset by a multiplier, in order to try and spread the various layers (and their combinations) across the
            # spectrum, since each value gets its own hue down the line.
            global_image += (layer_preview_images[layer_id] // 254) * ((layer_id + 1) * 7)

        assert global_image is not None, "No layer selected for view"

        # now lets make a colored picture.
        # 1) make a matrix to contain the new image
        # 2) add the numbers of each to the angle  in HSV space

        global_image = self.getColorFromColorMap(global_image)
        HSV_image = np.zeros((global_image.shape[0], global_image.shape[1], 3))
        HSV_image[:, :, 0] += global_image  # insert H

        # create S and V matrix where ever the original picture is not 0
        SV_matrix = np.zeros_like(global_image)
        SV_matrix[global_image != 0] = 1
        # SV_matrix = np.multiply(SV_matrix, global_image)

        # add the SV to the global HSV picture
        HSV_image[:, :, 1] += SV_matrix * 150
        HSV_image[:, :, 2] += SV_matrix * 255

        HSV_image = cv2.cvtColor(HSV_image.astype(np.uint8()), cv2.COLOR_HSV2RGB)

        union_preview_image_path = os.path.join(os.path.dirname(self.node.getTempDir()),
                                                (self.node.getName() + "unionized_preview.png").replace(" ", ""))
        imwrite(union_preview_image_path, HSV_image)
        del HSV_image
        del SV_matrix
        del global_image
        return union_preview_image_path

    @staticmethod
    def getOffset(layerBB, totalBB, res, unit):
        if unit != "metric":
            to_mm = 25.4
        else:
            to_mm = 1

        return [abs(int((layerBB[0][0] - totalBB[0][0]) * to_mm / res[1])),
                abs(int((layerBB[1][1] - totalBB[1][1]) * to_mm / res[0]))]

    @staticmethod
    def vectorBbToPixel(bounding_box, unit, res):
        if unit != "metric":
            to_mm = 25.4
        else:
            to_mm = 1

        return (
            abs(int(math.ceil((bounding_box[1][0] - bounding_box[0][0]) * to_mm / res[1]))),
            abs(int(math.ceil((bounding_box[1][1] - bounding_box[0][1]) * to_mm // res[0]))))

    @staticmethod
    def getColorFromColorMap(color):
        return np.uint8((color * 7) % 180)
