from UM import i18n_catalog
from UM.Job import Job
from UM.Message import Message
from cura.Utils.PCBHandler.PCBUtils.PcbModel import PcbModel


class RouteApplyJob(Job):
    def __init__(self, node: PcbModel, url=""):
        super().__init__()
        self.node = node
        self.urls = url
        self._result = []
        self._loading_message = None

    def run(self) -> None:
        self._loading_message = Message(self.node.getName(),
                                        lifetime=0,
                                        progress=0,
                                        dismissable=True,
                                        title=i18n_catalog.i18nc("@info:title", "Loading"))
        self._loading_message.setProgress(-1)
        self._loading_message.show()
        if self.urls != "":
            try:
                self.node.addRoute(path=self.urls, is_continue_callback=self.isMessageVisible)
            except Exception as e:
                self.setError(e)
                self._loading_message.hide()
                self.node.appendLayerFromPathToStackup(self.urls)
                message = Message(str("File can't be converted to route"),
                                  title="Loading Failed")
                message.show()
                return

        max_bounding_box_changed = self.node.updateNodeMeshData()

        if max_bounding_box_changed:
            self.node.detectDrillFmt()
        self.setResult(max_bounding_box_changed)

        self._loading_message.hide()
        return

    def isMessageVisible(self):
        # this flag passed into the mesh creator, and by changing it to False from outside, the process will be stopped
        return self._loading_message.visible
