# Copyright (c) 2018 Ultimaker B.V.
# Uranium is released under the terms of the LGPLv3 or higher.

from typing import TYPE_CHECKING, Optional

from PyQt5.QtCore import QObject  # For typing.
from UM.FileHandler.FileHandler import FileHandler
from UM.Logger import Logger
from UM.Math.Matrix import Matrix
from UM.Math.Vector import Vector

if TYPE_CHECKING:
    from UM.Qt.QtApplication import QtApplication


class GerberFileHandler(FileHandler):

    def __init__(self, application: "QtApplication", reader_type: str = "gerber_reader",
                 parent: QObject = None) -> None:
        super().__init__(application, reader_type=reader_type, parent=parent)

    def readerRead(self, reader, file_name, **kwargs):


        try:
            results = reader.read(file_name)
            if results is not None:
                if type(results) is not list:
                    results = [results]

                for result in results:
                    if kwargs.get("center", False):
                        # If the result has a mesh and no children it needs to be centered
                        if result.getMeshData() and len(result.getChildren()) == 0:
                            extents = result.getMeshData().getExtents()
                            move_vector = Vector(extents.center.x, extents.center.y, extents.center.z)
                            result.setCenterPosition(move_vector)

                        # Move all the meshes of children so that toolhandles are shown in the correct place.
                        for node in result.getChildren():
                            if node.getMeshData():
                                extents = node.getMeshData().getExtents()
                                m = Matrix()
                                m.translate(-extents.center)
                                node.setMeshData(node.getMeshData().getTransformed(m))
                                node.translate(extents.center)
                return results

        except OSError as e:
            Logger.logException("e", str(e))

        Logger.log("w", "Unable to read file %s", file_name)
        return None  # unable to read

    def getReaderForFile(self, file_name: str) -> Optional["FileReader"]:
        return self._readers['GerberReader'] if self._readers['GerberReader'].acceptsFile(
            file_name) else None

