# Copyright (c) 2015 Ultimaker B.V.
# Uranium is released under the terms of the LGPLv3 or higher.
import os

from UM.Message import Message
from UM.Math.Vector import Vector
from UM.Logger import Logger
from UM.Mesh.MeshReader import MeshReader

from UM.FileHandler.ReadFileJob import ReadFileJob

import math

from UM.i18n import i18nCatalog
i18n_catalog = i18nCatalog("uranium")


class ReadPCBJob(ReadFileJob):
    """A Job subclass that performs mesh loading.

    The result of this Job is a MeshData object.
    """

    def __init__(self, filename: str) -> None:
        super().__init__(filename)

        from UM.Qt.QtApplication import QtApplication
        self._application = QtApplication.getInstance()
        self._handler = QtApplication.getInstance().getGerberFileHandler()

    def run(self):
        self._loading_message = Message(", ".join([os.path.basename(name) for name in self._filename]),
                                        lifetime=0,
                                        progress=0,
                                        dismissable=False,
                                        title=i18n_catalog.i18nc("@info:title", "Loading"))
        self._loading_message.setProgress(-1)
        self._loading_message.show()
        reader = self._handler.getReaderForFile(self._filename)

        try:
            self.setResult(self._handler.readerRead(reader, self._filename))
        except:
            Logger.logException("e", "Exception occurred while loading file %s", self._filename)



        if not self._result:
            self._result = []
        self._loading_message.hide()
