# ------------------------------- ## TOOD when we have single git move both to the most upper part of the root
# General - StaticUtils.py
import collections
import os
import sys
import ctypes  # An included library with Python install.
import inspect
import types
import hashlib
# import cv2
import json
import subprocess
import re
import math
import functools
import datetime
import statistics
from copy import deepcopy  # deepcopy - create clone
import shutil  # deltree

import numpy as np  # math


class StaticUtils():
    ###########     General funcs     #################

    @staticmethod
    def MsgBox(text, title='', type=0):
        # type = 0 # ok only
        # type = 1  # ok / cancel
        # type = 2  # retry, abort, ignore
        # type = 3  # yes , no , cancel
        # type = 4  # yes , no
        # type = 5  # retry, cancel


        res = ctypes.windll.user32.MessageBoxW(0, text, title, type)

        # todo need to work on return val, to clearup what was selected
        return res

    ###########     genearl funcs     #################
    @staticmethod
    def trim(str, toRemove=None):
        strRes = str.rstrip(toRemove).lstrip(toRemove)
        return strRes

    @staticmethod
    def StringNonEmtpy(string):
        res = (string != None and len(string) > 0)
        return res

    #XXXXXXXXXXX --       DEBUG TOOLS        v

    @staticmethod
    def IsClassInstance(obj):
        res = isinstance(obj, object) and not (
                isinstance(obj, (int, np.int64, float, list, dict, str, tuple, set)) \
                or isinstance(obj, collections.Iterable)
        )
        return res

    @staticmethod
    def Print(someVar):

        def mod_retrieve_name(var):
            callers_local_vars = inspect.currentframe().f_back.f_back.f_locals.items()
            return [var_name for var_name, var_val in callers_local_vars if var_val is var]

        ## the name of the vairable with reflaction
        toPrint = mod_retrieve_name(someVar)
        if(len(toPrint) > 0):
            toPrint = toPrint[0]
        else:
            toPrint = ''

        isListOrDict = isinstance(someVar, collections.Iterable)
        if(isListOrDict):
            empty = len(someVar) <= 0
            if (empty):
                toPrint += ": " + str('<empty>')
            else:
                it = iter(someVar)
                first = next(it)

                isClass = StaticUtils.IsClassInstance(first)
                if (isClass):
                    toPrint += ": (" + str(type(first)) + ") \n-------------"
                    for item in someVar:
                        print(item)
                    print("")
                else:
                    toPrint += ": " + str(someVar)
        else:
            toPrint += ": " + str(someVar)

        print(toPrint)

    # XXXXXXXXXXX --       DEBUG TOOLS        ^

    #TODO - move to right section
    @staticmethod
    def StrInsertFront_List(str, list):
        result = [str + a for a in list]
        return result

    ###########     file sys funcs     #################

    @staticmethod
    # def CheckDirExist(fullpath):
    def DirExist(fullpath):
        boolRes = False
        if(fullpath != None):
            fullpath = fullpath + '///'
            fullpath = StaticUtils.FixDirPath_RemoveSlash(fullpath)
            boolRes = os.path.exists(fullpath) and (os.path.isdir(fullpath))
        return boolRes

    @staticmethod
    def FileOrDirExist(fullpath):
        fullpath = fullpath + '///'
        fullpath = StaticUtils.FixDirPath_RemoveSlash(fullpath)
        boolRes = os.path.exists(fullpath)
        return boolRes

    @staticmethod
    def FileExist(fullpath):
        boolRes = False
        if fullpath != None:
            boolRes = os.path.exists(fullpath) and (not os.path.isdir(fullpath))
        return boolRes



    '''
        list = ['c:/wcad','xxx.yyy']
        path = FindFirst(list, StaticUtils.FileExist)
    '''
    @staticmethod
    def FindFirst(list, FUNC):
        val = None
        for item in list:
            # if StaticUtils.FileExist()
            positive = FUNC(item)
            if (positive):
                val = item
                break
        return val

    ''' Usage
     pathToCheck = 'artworkengine/Engine.py'
     runningPath = StaticUtils.GetRunningPath()
     absStartPath = runningPath + "../"
     found, fullPath = GetFileExistAbs(pathToCheck, absStartPath):
    '''
    @staticmethod
    def GetFileExistAbs(pathToCheck, absStartPath):
        engine_path_from_env = None
        found = False
        res = found, engine_path_from_env
        if StaticUtils.StringNonEmtpy(pathToCheck):
            if not StaticUtils.FileExist(pathToCheck):
                absolotExpctedPath = absStartPath + pathToCheck
                if StaticUtils.FileExist(absolotExpctedPath):
                    engine_path_from_env = os.path.abspath(absolotExpctedPath)
                    found = True
                    res = found, engine_path_from_env
            else:
                res = True, pathToCheck
        return res

    @staticmethod
    def CheckFilesListExist(paths, force=False, message='file not found :\n'):
        nonFoundCounter = 0
        for fullpath in paths:
            boolFound = StaticUtils.FileExist(fullpath)
            if not boolFound:
                if force:
                    raise Exception(message + ' ' + fullpath)
                else:
                    print(message + ' ' + fullpath)
                    nonFoundCounter = nonFoundCounter + 1

        return nonFoundCounter == 0

    @staticmethod
    def FixDirPath_RemoveSlash(dirPath):
        res = dirPath
        cont = True
        while len(res) > 0 and cont:
            lastChar = res[-1]
            boolHasSlash = '/' == lastChar
            if boolHasSlash:
                res = res[:-1]
                cont = True
            else:
                cont = False
        return res

    @staticmethod
    def FixDirPath(dirPath, validate=False):
        res = dirPath
        lastChar = dirPath[-1]
        bool_has_slash = '/' == lastChar
        if not bool_has_slash:
            res = res + '/'

        if validate:
            bool_ok = os.path.exists(res) and os.path.isdir(res)
            if not bool_ok:
                raise Exception("path not found:\n " + res)

        return res

    @staticmethod
    def GetCurrWorkDir():
        cwd = os.getcwd()
        return cwd

    @staticmethod
    def NameExtSplit_Filename(nameOrPath : str) -> (str, str):
        NameNoExt = None
        Ext = None
        try:
            parts = os.path.splitext(nameOrPath)
            NameNoExt = parts[0]
            NameNoExt = os.path.basename(NameNoExt)

            Ext = parts[1]
            Ext = Ext[1:]
        except:
            pass

        return NameNoExt, Ext

    @staticmethod
    def IsComplied_ByName():
        runningScriptOrExe = (sys.argv[0])
        Name, Ext = StaticUtils.NameExtSplit_Filename(runningScriptOrExe)
        IsExec =  Ext.lower() == "exe"
        return IsExec

    @staticmethod
    def GetRunningPath():
        fullpath = os.path.abspath(sys.argv[0])
        parantFolder = os.path.dirname(fullpath)
        return StaticUtils.FixDirPath(parantFolder, True)
