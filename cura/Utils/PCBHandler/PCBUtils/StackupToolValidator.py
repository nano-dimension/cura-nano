from UM.Logger import Logger
from UM.Scene.SceneNodeDecorator import SceneNodeDecorator
from copy import deepcopy

from cura.Utils.PCBHandler.PCBUtils.PcbUtils import LayerErrors


class StackupToolValidator(SceneNodeDecorator):
    def __init__(self) -> None:
        super().__init__()

    def hasErrorInStackup(self) -> bool:
        result = False
        stackup = self._node.getStackup()
        for layer in stackup:
            layers_error = LayerErrors(layer.errors)
            if LayerErrors.TypeError in layers_error or LayerErrors.ThicknessError in layers_error:
                result = True
            if LayerErrors.ProcessError in layers_error and layer.name != 'Prepreg':
                result = True

        drills = self._node.getDrills()
        for i, drill in enumerate(drills):
            result |= drill.error

        return result

    def __deepcopy__(self, memo) -> "StackupToolValidator":
        copy = StackupToolValidator()
        copy.setNode(self.getNode())
        memo[id(self)] = copy
        return copy
