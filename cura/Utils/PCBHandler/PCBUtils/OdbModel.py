import os
import subprocess
import tarfile
from copy import deepcopy
from typing import Optional

from ArtworkLic import getVendorKey
from hasplib import haspChecker

from UM.Logger import Logger
from UM.Message import Message
from cura.Utils.PCBHandler.ODBPy.Layers import read_layers, ODB_MM_UNIT, ODB_UNITS_KEY
from cura.Utils.PCBHandler.ODBPy.StructuredTextParser import read_structured_text
from cura.Utils.PCBHandler.PCBUtils.GerberReadUtils import converUnit
from cura.Utils.PCBHandler.PCBUtils.Global.GlobalParamsHandler import GlobalParamsHandler
from cura.Utils.PCBHandler.PCBUtils.MessagesOnPcbLoad import PcbMessage, messageType
from cura.Utils.PCBHandler.PCBUtils.PcbModel import PcbModel
from cura.Utils.PCBHandler.PCBUtils.PcbUtils import LayerType, Drill, DrillType, Layer, _odb_drill_type_map
from cura.Utils.PCBHandler.gerberTools import read as gerber_read


class OdbModel(PcbModel):
    def __init__(self, path, parent: Optional["SceneNode"] = None, visible: bool = True, name: str = "ODB++ model",
                 no_setting_override: bool = False, settings=None, global_stack=None, temp_dir=None) -> None:
        super().__init__(parent=parent, visible=visible, name=name, no_setting_override=no_setting_override,
                         settings=settings, global_stack=global_stack, temp_dir=temp_dir)
        self._steps = []
        self._gerbers_extracted_path = None
        self._source_file_path = path

    def readStackupFromODB(self, global_stack):
        with tarfile.open(self._source_file_path, 'r') as odb_file:
            odb_file.extractall(self._temp_dir)
            self._odb_extracted_path = os.path.join(self._temp_dir, odb_file.members[0].name)
            self._steps = os.listdir(os.path.join(self._odb_extracted_path, "steps"))
            matrix_from_odb = read_layers(global_stack, self._odb_extracted_path)
            self._drills = matrix_from_odb.by_obj_type(Drill)
            self._routes = matrix_from_odb.by_type(LayerType.Route)
            for route in matrix_from_odb.by_type(LayerType.Route):
                matrix_from_odb.remove(route)
            self._stackup = matrix_from_odb.by_obj_type(Layer)
            self._stackup.sort(key=lambda x: x.stackup_index)

    def rereadRoutes(self):
        routes = deepcopy(self._routes)
        self._routes = []
        for route in routes:
            self.addRoute(path=route.path)

    def getGerbersExtractedPath(self):
        return self._gerbers_extracted_path

    def extractODB2Gerbers(self):
        global_params = GlobalParamsHandler.getGlobalParams()
        # run Artwork odb2gbr64
        self._gerbers_extracted_path = os.path.join(self._temp_dir, os.path.basename(self._source_file_path).split('.')[0], "gerbers")
        os.makedirs(self._gerbers_extracted_path, exist_ok=True)
        global_params = GlobalParamsHandler.getGlobalParams()

        for step in self._steps:
            cmd = [os.path.join(global_params.wcad_path,
                                'ODB2GBR64', 'odb2gbr64.exe'),
                   '-job:' + self._source_file_path,
                   "-workdir:" + self._gerbers_extracted_path,
                   "-outdir:" + self._gerbers_extracted_path,
                   '-step:' + step,
                   '-odb_symbols',
                   '-thrnum:4',
                   '-non_std',
                   '-butting',
                   '-unit:' + str(self._unit),
                   '-acsv:' + getVendorKey(),
                   '-acsp:' + global_params.artwork_key_path
                   # '-filter_sym',
                   # '-out_scale: local'
                   ]
            try:
                haspChecker.acquireArtwork()
                Logger.log("i", '[CMD] odb -> gbr: "' + '" "'.join(cmd) + '"')
                subprocess.check_call(cmd)
            except haspChecker.HaspLicenseException as e:
                Message("Cannot load ODB++: {}".format(e)).show()
                Logger.log("e", "extracting ODB++ to gerbers failed, {}".format(e))
            except:
                Logger.log("e", "extracting ODB++ to gerbers failed")

    def replaceNamesToAbsolutePaths(self):
        for layer in self._stackup + self._routes + self._drills:
            layer.setPath(os.path.join(self._gerbers_extracted_path, layer.name + ".gbr"))

    def updateDrillsStartEnd(self):
        was_issues = False
        for drill in self._drills:
            drill.start, was_issues = self.getDrillStartByNameForODB(drill, was_issues)
            drill.end, was_issues = self.getDrillEndByNameForODB(drill, was_issues)
            self.initDrillLayersToBeIn(drill)
            true_range = range(int(drill.start), int(drill.end) + 1)
            for layer_index in range(len(drill.layers)):
                if layer_index not in true_range:
                    drill.layers[layer_index]["state_bool"] = False
        if was_issues:
            PcbMessage(model_type=type(self).__name__, message_type=messageType.warning_message, file_name=self._name,
                       text="odbDrillBad").show()

    def getDrillStartByNameForODB(self, drill, was_issues):
        if drill.start is None:
            was_issues = True
            return 0, was_issues
        return [layer_index for layer_index, layer in enumerate(self._stackup) if layer.name == drill.start][
                   0], was_issues

    def getDrillEndByNameForODB(self, drill, was_issues):
        if drill.end is None:
            was_issues = True
            return self.getStackupCount() - 1, was_issues
        return [layer_index for layer_index, layer in enumerate(self._stackup) if layer.name == drill.end][
                   0], was_issues

    def splitDrillsByTool(self):
        temp_drills_list = deepcopy(self._drills)
        self._drills = []
        for original_drill_index, original_drill in enumerate(temp_drills_list):
            drill_cam = gerber_read(original_drill.path)
            drills_apertures_keys = list(drill_cam.apertures)
            drills_apertures = {}

            tools_text = read_structured_text(
                os.path.join(self._odb_extracted_path, 'steps', self._steps[0], "layers", original_drill.name,
                             "tools"))

            # TODO: check with artwork, we correlate the drill in the odb++ files with the d-codes in the gerber
            # files using the drill diameter. we must find a stronger way
            tools_info = tools_text.arrays
            units = tools_text.metadata.get(ODB_UNITS_KEY, "inch")
            units = "metric" if units == ODB_MM_UNIT else units

            for key in drills_apertures_keys:
                gerber_diameter = drill_cam.apertures[key].diameter
                found_drill = False
                for tool in tools_info:
                    diameter = converUnit(tool.attributes['DRILL_SIZE'], units, "metric") / 1000.0
                    if abs(diameter - gerber_diameter) < 0.001:
                        drills_apertures[key] = tool
                        found_drill = True
                        break
                assert found_drill, "Failed to find gerber drill for key {} in drill {}".format(key, original_drill.path)

            if len(drills_apertures) > 0:
                for aperture_index, aperture in enumerate(drills_apertures_keys):
                    temp_drill = deepcopy(drill_cam)
                    temp_drill.isolate_tool(aperture)
                    new_path = os.path.join(os.path.dirname(original_drill.path),
                                            "_".join(os.path.basename(original_drill.path).split(".")[:-1])
                                            + str(aperture)
                                            + ".gbr")
                    temp_drill.write(new_path)
                    drill_type = _odb_drill_type_map.get(drills_apertures[aperture].attributes['TYPE'])

                    try:
                        diameter = converUnit(drill_cam.apertures[aperture].diameter,
                                              drill_cam.apertures[aperture].units, "metric")
                    except:
                        diameter = 0

                    if drill_type is None:
                        if diameter <= self.defaults["minPTH"]:
                            drill_type = DrillType.VIA
                        else:
                            DrillType.PTH

                    self._drills.append(Drill(name="".join([original_drill.name, str(aperture)]),
                                              drill_type=drill_type,
                                              start=original_drill.start,
                                              end=original_drill.end,
                                              diameter=diameter,
                                              path=new_path,
                                              is_excellon=False))
                self._original_drills.append(original_drill.path)
            else:
                if len(drills_apertures) == 1 and original_drill.diameter is None:
                    original_drill.diameter = list(drill_cam.apertures.values())[0].diameter
                    original_drill.type = _odb_drill_type_map.get(
                        tools_info[aperture_index].attributes['TYPE'], DrillType.VIA \
                            if drill_cam.apertures[aperture].diameter <= self.defaults["minPTH"] else DrillType.PTH)


    def hasDrill(self, drill_name : str) -> bool:
        """
        check if we have a drill with the give name
        """
        if drill_name is None:
            return False
        for drill in self._drills:
            if drill.name == drill_name:
                return True
        return False

def Main():
    in_path = ''
    out_path = ''
    Model = OdbModel(in_path)
    Model.readStackupFromODB(output_path=out_path)
    print("matrix loaded")
    Model.extractODB2Gerbers(out_path)
    print("ODB++ extracted")
    Model.replaceNamesToAbsolutePaths()
    print("paths replaced")
    Model.addDielectricLayersToStackup()
    print("dielectric layers added")

    Model.splitDrillsByTool()
    print("drills split ")

    Model.updateDrillsStartEnd()
    print("drills updated ")

    print(Model)

    return


if __name__ == "__main__":
    Main()
