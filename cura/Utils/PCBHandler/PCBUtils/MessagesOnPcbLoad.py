import os
import sys
from enum import Enum

from PyQt5.QtWidgets import QFileDialog
from UM.Message import Message

_pcb_messages_map_file_notification_message = {
    "drillSlot": "PCB Reader: {#filename} has slots.\nSplit was not possible"
}
_pcb_messages_map_file_warning_message = {
    "NotRead": "PCB Reader: {#filename} could not read the file",
    "RouteNotValid": "{#filename} is not a valid route",
    "NoRoute": "Route was not found.\n"
               "Load route in the Route Manager tool,\n"
               "Preview is not available without Route",
    "RouteSmallApertures": "Route made of apertures less then 70um, not allowed",
    "Circle": "Non-circular drills were found in {#filename}",
    "ExcellonFormat": "The excellon {#filename} can not be loaded without route,\n"
                      "this file was added to stackup, please convert manually to drill after adding route",
    "FormatMismatch": "PCB model: there is mismatch between route to another route or another layer,\n"
                      "this may indicate that excellon file format won't be same as the route as well",
    "EmptyGerber": "{#filename} is an Empty Gerber",
    "OdbDrillBad": "The drills were not properly defined, will be set as through-holes"
}
_pcb_messages_map_file_error_message = {

}
_map_of_maps = {
    "notification_message": _pcb_messages_map_file_notification_message,
    "warning_message": _pcb_messages_map_file_warning_message,
    "error_message": _pcb_messages_map_file_error_message,
}


class messageType(Enum):
    notification_message = 0
    warning_message = 1
    error_message = 2


class PcbMessage(Message):
    def __init__(self, message_type: messageType, model_type: str = '', file_name: str = "", text: str = "", lifetime: int = 15,
                 dismissable: bool = True,
                 title="Loading File", folder: str = '', node=None):

        self._node = node
        full_message = _map_of_maps[message_type.name].get(text, text)  # type:str
        if file_name != "":
            full_message = full_message.replace("{#filename}", file_name)
        if model_type != "":
            full_message = ": ".join([model_type, full_message])

        super().__init__(text=full_message, lifetime=lifetime, dismissable=dismissable, title=title)

        if model_type == "OdbModel" and text == "noRoute":
            self.addAction("openRouteAction", "Load Route", icon="NO ICON", description="SuperAwesomeMessage")
            self.actionTriggered.connect(self._onActionTriggered)

    def _onActionTriggered(self, message, action):
        if action == "openRouteAction":
            dialog = QFileDialog()
            dialog.setWindowTitle("Open Gerber As Route")
            dialog.setFileMode(QFileDialog.AnyFile)
            dialog.setAcceptMode(QFileDialog.AcceptOpen)
            if sys.platform == "linux" and "KDE_FULL_SESSION" in os.environ:
                dialog.setOption(QFileDialog.DontUseNativeDialog)
            dialog.setDirectory(
                self._node.getGerbersExtractedPath() if type(self._node).__name__ == "OdbModel" else self._node._app_instance.getDefaultPath(
                    "dialog_load_path").toLocalFile())
            # if not dialog.exec_():
            #     pass
            self._node.appendRouteAndUpdateAll(dialog.getOpenFileUrls()[0])
            self.hide()
