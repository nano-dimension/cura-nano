import os.path

from typing import Optional

# from UM.Message import Message
from cura.Settings.SettingOverrideDecorator import SettingOverrideDecorator
from cura.Utils.PCBHandler.PCBUtils.GerberReadUtils import readGerberFile
from cura.Utils.PCBHandler.PCBUtils.PcbModel import PcbModel
from cura.Utils.PCBHandler.PCBUtils.PcbUtils import LayerType, _gerber_tools_layer_type_map
from cura.Utils.PCBHandler.gerberTools.common import onlyDetectIfExcellon


class GerberModel(PcbModel):
    def __init__(self, paths, parent: Optional["SceneNode"] = None, visible: bool = True, name: str = "Gerbers model",
                 no_setting_override: bool = False, settings=None, global_stack=None, temp_dir=None) -> None:
        super().__init__(parent=parent, visible=visible, name=name, no_setting_override=no_setting_override,
                         settings=settings, global_stack=global_stack, temp_dir=temp_dir)
        self._steps = []
        self._source_files_path = self.copyFilesToTemp(set(paths))

    def readStackupFromFiles(self):
        drills = []
        for file in self._source_files_path:
            if onlyDetectIfExcellon(file) == 'excellon':
                drills.append(file)
                continue

            layer = readGerberFile(file)
            if layer is None:
                continue

            if _gerber_tools_layer_type_map[layer.layer_class] == LayerType.Route:
                self.addRoute(layer, file)

            elif _gerber_tools_layer_type_map[layer.layer_class] in [LayerType.Signal, LayerType.Annotation,
                                                                     LayerType.SolderMask, LayerType.Unknown]:
                self.appendLayerFromPathToStackup(file)
        for file in drills:
            drills, pcb_message = self.addDrill(path=file)
            if pcb_message is not None:
                pcb_message.show()

        self._drills.sort(key=lambda x: x.diameter)

        if len(self._stackup) != 0:
            self._stackup.sort(key=lambda x: x.stackup_index)
            self.sortInternalLayers(self._stackup)

    @staticmethod
    def sortInternalLayers(stackup):
        internal_layers_indexes = [layer_index for layer_index, layer in enumerate(stackup) if layer.stackup_index == 3]
        if len(internal_layers_indexes)>1:
            stackup[internal_layers_indexes[0]:internal_layers_indexes[-1] + 1] = sorted(stackup[internal_layers_indexes[0]:
                                                                                             internal_layers_indexes[-1] + 1],key=lambda x: x.name)


    def setAllDrillsStartEnd(self):
        for drill in self._drills:
            drill.end = len(self._stackup) - 1
