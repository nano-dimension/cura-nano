import os
import subprocess
from copy import deepcopy
from time import time
from typing import List, Tuple
import re
import numpy as np
import scipy.ndimage
import cv2
from UM.Logger import Logger
from UM.Message import Message
from cura.Utils import PathUtils
from cura.Utils.MeshUtils import combineTwoTrimeshes
from cura.Utils.PCBHandler.PCBUtils.MessagesOnPcbLoad import PcbMessage, messageType
from cura.Utils.PCBHandler.gerberTools import read as gerber_read, excellon
from cura.Utils.PCBHandler.gerberTools.cam import FileSettings
from cura.Utils.PCBHandler.gerberTools.layers import PCBLayer
from cura.Utils.PCBHandler.gerberTools.primitives import Line, Circle
from hasplib import haspChecker
from shapely.geometry import Polygon
from tqdm import tqdm
import trimesh.creation
from ArtworkLic import getVendorKey
import threading
from cura.Utils.PCBHandler.PCBUtils.Global.GlobalParamsHandler import GlobalParamsHandler

GrbRip_lock = threading.Lock()
MIN_ROUTE_DIAMETER_MM = 0.02


def gbrRip(gbr_path,
           working_dir: str,
           bounding_box: Tuple[tuple, tuple],
           inverse: bool = True,
           dpi=1440,
           output_type: str = "tiff",
           new_name: str = '',
           unit: str = 'mm',
           enlarge_all_apertures_by_constant: float = 0.0) -> str:
    """
    1) create temp file path
    2) run artwork
    3) delete boarder
    4) save and return
    """

    # Artwork likes resolution_dpi and x_to_y_ratio

    # create an new name that is the same with tif ext
    name = os.path.basename(gbr_path).lower()
    real_name = new_name or name
    new_file_path = os.path.join(working_dir, real_name + '.' + output_type)

    global_params = GlobalParamsHandler.getGlobalParams()

    unit = makeUnitsFitToArtwork(unit)
    # create a bounding box string to ARTWORK argument

    bb = str(bounding_box[0][0]) + "," + str(bounding_box[0][1]) + " " + str(bounding_box[1][0]) + "," + str(
        bounding_box[1][1])

    # TODO: replace with a call to the engine once ADT-655 is done
    cmd = [os.path.join(global_params.wcad_path, 'GBRIP64', 'gbr2tiff64.exe'),
           gbr_path,
           '-274x',
           '-dpi:' + str(dpi),
           '-units:' + str(unit),
           '-wplot:"' + bb + '"',
           '-logcalls',
           '-ram:128',
           '-' + output_type + ':outputfile',
           '-pack:' + new_file_path,
           '-acsv:' + getVendorKey(),
           '-acsp:' + global_params.artwork_key_path
           ]

    if enlarge_all_apertures_by_constant != 0.0:
        cmd += ["-asizeup:{}".format(enlarge_all_apertures_by_constant)]

    # run Artwork GRBRIP
    if inverse:
        cmd.append('-inverse')
    Logger.log("d", '[CMD] gbr -> tif: "' + '" "'.join(cmd) + '"')
    try:
        haspChecker.acquireArtwork()
        with GrbRip_lock:
            subprocess.check_call(cmd)
    except haspChecker.HaspLicenseException as e:
        Message("Cannot create preview - license error: {}".format(e)).show()
        raise
    except:
        Logger.logException("e", "[CMD] gbr -> tif crashed. File:" + os.path.basename(gbr_path))
        return

    return new_file_path


def converUnit(measure, measure_unit, to_unit):
    """
    Convert the measure value from measure_unit to to_unit (if different)
    unit is either "inch" or "metric"
    """
    if measure_unit == to_unit:
        return measure
    return measure / 25.4 if to_unit == "inch" else measure * 25.4


def _fixCircleApertureWithSmallDiameter(layer_filename, layer_unit, min_aperture_diameter_mm):
    """
        parses the layer_filename gerber file, looking for Circle apertures with diameter < min_aperture_diameter_mm,
        (considering the layer_unit) and if found, writing back to the file a corrected tool with diameter
        min_aperture_diameter_mm
    """

    def replace_diameters(matchobj):
        tool_num = matchobj.group(1)
        diameter = converUnit(float(matchobj.group(2)), layer_unit, "metric")
        if diameter >= min_aperture_diameter_mm:
            return matchobj.group(0)
        return "ADD{}C,{:f}*".format(tool_num, converUnit(min_aperture_diameter_mm, "metric", layer_unit))

    with open(layer_filename, "r+") as gerber_file:
        gerber_txt = gerber_file.read()
        gerber_txt_replaced = re.sub(r"ADD(\d+)C,([\d\.]+)\*", replace_diameters, gerber_txt)
        gerber_file.seek(0)
        gerber_file.write(gerber_txt_replaced)
        gerber_file.flush()


def _getMaxApertureDiameterMetric(layer: PCBLayer):
    max_diameter = 0
    for p in layer.primitives:
        if hasattr(p, "aperture") and p.aperture.diameter is not None:
            diameter = converUnit(p.aperture.diameter, layer.cam_source.units, "metric")
            max_diameter = max(max_diameter, diameter)
    return max_diameter


def calculateRoutePreviewImage(route: PCBLayer, enlarge_all_apertures_by_constant: float):
    """
    For a given PCBLayer that may be a route, calculate the metric bounding box, and perform gbrRip
    to return the layer TIFF. also fixes cases where the route layer has 0 or very small aperture
    return (route image path, metric bounding box)
    """
    bounding_box = ((route.cam_source.bounding_box[0][0],
                     route.cam_source.bounding_box[1][0]),
                    (route.cam_source.bounding_box[0][1],
                     route.cam_source.bounding_box[1][1]))
    metric_bounding_box = mateUnitForBB(bounding_box, route.cam_source.units, "metric")

    # we need to be able to render the route anyway to proceed.
    # so we need the route to have some diameter

    max_aperture_diameter_mm = _getMaxApertureDiameterMetric(route)
    if max_aperture_diameter_mm < MIN_ROUTE_DIAMETER_MM:
        # so we do a work around for diameter 0 apertures
        _fixCircleApertureWithSmallDiameter(route.filename, route.cam_source.units, MIN_ROUTE_DIAMETER_MM)
        max_aperture_diameter_mm = MIN_ROUTE_DIAMETER_MM

    # increase the bounding box with the minimum aperture size to make sure it is painted
    metric_bounding_box = ((metric_bounding_box[0][0], metric_bounding_box[0][1]),
                           (metric_bounding_box[1][0], metric_bounding_box[1][1]))

    workdir = os.path.dirname(route.filename)
    route_preview_image = gbrRip(gbr_path=route.filename,
                                 working_dir=workdir,
                                 bounding_box=metric_bounding_box,
                                 unit="mm",
                                 new_name="outline",
                                 enlarge_all_apertures_by_constant=enlarge_all_apertures_by_constant)
    return route_preview_image, metric_bounding_box


def routeToMeshUsingGbrRip(route_image_path: str, metric_bounding_box, create_mesh_from_route: bool):
    """
    Updated method
    1. use gbrRip to extract TIFF at 1440 DPI from the layer
    2. read the TIFF as greyscale. use scipy.ndimage.label to get components
    3. choose border component (assume its the first component) and paint the rest in black
    4. use cv2.findContours to convert the route image to a list of vertices
    5. transfer the vertices to the metric bounding box of the layer
    6. use extrude_polygon to extrude the vertices to a mesh
    """

    width = metric_bounding_box[1][0] - metric_bounding_box[0][0]
    height = metric_bounding_box[1][1] - metric_bounding_box[0][1]

    if create_mesh_from_route:
        try:
            route_image = cv2.imread(route_image_path, 0)
            kernel = [[1, 1, 1], [1, 1, 1], [1, 1, 1]]
            labeled, nr_objects = scipy.ndimage.label(route_image, kernel)
            assert nr_objects > 0, "Gerber file does not contain route"

            ASSUME_THAT_FIRST_LABELED_COMPONENT_IS_ROUTE = 1
            outline = np.where(labeled == ASSUME_THAT_FIRST_LABELED_COMPONENT_IS_ROUTE, 255, 0)

            contours, hierarchy = cv2.findContours(outline.astype(np.uint8), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
            assert len(contours) == 1, "Gerber file does not contain route"
            vertices = contours[0]

            image_height, image_width = route_image.shape
            # transform the points from the image plane to the bounding box
            mesh_vertices = [((v[0][0] * width / image_width) - metric_bounding_box[0][0],
                              (v[0][1] * height / image_height) - metric_bounding_box[1][0]) for v in vertices]
            mesh = trimesh.creation.extrude_polygon(Polygon(mesh_vertices), 0.6)

            # Find the center of the bounding box in order to center the mesh accordingly
            center = mesh.bounds.mean(axis=0)
            mesh = mesh.apply_translation(-center)
            return mesh
        except Exception as e:
            Logger.logException(e, "Failed to create mesh from route shape")

    mesh = trimesh.creation.box((width, height, 0.6))
    center = mesh.bounds.mean(axis=0)
    mesh = mesh.apply_translation(-center)
    return mesh


def readGerberFile(path: str) -> PCBLayer:
    '''
    uses a PCB-tools lib (MIT)
    :param path: path to gerber file
    :return: layer, units of the layer
    '''
    try:
        camfile = gerber_read(path)
        layer = PCBLayer.from_cam(camfile)
        Logger.log("d", str(path.split("/")[-1] + " : " + layer.layer_class))
        return layer
    except:
        Logger.log("w", str(path.split("/")[-1] + " :reading Crashed"))
        file_name = os.path.basename(path)
        PcbMessage(message_type=messageType.warning_message, file_name=file_name,
                   text="Could not read Gerber file", title="Loading file {}".format(file_name)).show()


def readExcellonFile(path, settings=None) -> PCBLayer:
    '''
    uses a PCB-tools lib (MIT)
    :param settings:
    :param path: path to gerber file
    :return: layer
    '''
    try:
        camfile = excellon.read(path, settings)
        layer = PCBLayer.from_cam(camfile)
        return layer
    except:
        Logger.logException("w", message="Failed to read excellon file {}".format(path))


def drl2gbr(file: str, working_dir: str, settings: FileSettings):
    new_path = os.path.join(working_dir, os.path.basename(file) + '.gbr').lower()

    decode_format = str(settings.format)[1:-1].replace(", ", ".")
    xy_mode = settings.notation
    zero_suppression = settings.zero_suppression
    unit = makeUnitsFitToArtwork(settings.units)
    # decode_format = makeDrillFormatFitArtwork(decode_format)

    # TODO: @Daniel, this tries to save time by reusing existing file if it was already created.
    #       Do you still want it here, or do you prefer to recalculate it each time?
    #       I guess you want to recalculate, especially in case drill diameter was changed or something.
    #       In this case this should be removed
    # if os.path.isfile(new_path):
    #     return new_path

    # TODO: replace with a call to the engine once ADT-655 is done
    global_params = GlobalParamsHandler.getGlobalParams()
    wcad_path = global_params.wcad_path
    renderer_path = os.path.join(wcad_path,
                                 'GBRVU64', 'artwork', 'drl2gbr64.exe')
    cmd = [renderer_path,
           file,
           '-startdcode:200',
           '-drlformat:' + decode_format,
           '-drlunits:' + unit,
           '-drlxymode:' + xy_mode,
           '-drlzeroinc:' + zero_suppression,
           '-gbrformat:' + decode_format,
           '-gbrunits:' + unit,
           '-gbrxymode:' + xy_mode,
           '-gbrzerosup:' + zero_suppression,
           '-out:' + new_path,
           '-silence'
           ]

    Logger.log("w", str('[CMD] drl -> gbr: "' + '" "'.join(cmd) + '"'))

    haspChecker.acquireArtwork()
    subprocess.check_call(cmd)

    return new_path


def mateUnitForBB(bounding_box, bounding_box_units, obj_units):
    if obj_units == 'undefined':
        return
    if bounding_box_units != obj_units:
        bounding_box = \
            (
                tuple([x / 25.4 for x in bounding_box[0]]),
                tuple([x / 25.4 for x in bounding_box[1]])
            ) \
                if bounding_box_units == 'metric' else \
                (
                    tuple([x * 25.4 for x in bounding_box[0]]),
                    tuple([x * 25.4 for x in bounding_box[1]])
                )

    return bounding_box


def makeUnitsFitToArtwork(unit: str):
    unit = 'mm' if unit == "metric" else unit
    return unit
