import copy
import os
import shutil
from datetime import datetime
from enum import Enum, EnumMeta, IntEnum
from typing import Dict

import numpy as np

from UM.Logger import Logger
from UM.Math.Vector import Vector
from UM.Mesh.MeshBuilder import MeshBuilder
from cura.Utils.PCBHandler.PCBUtils.GerberReadUtils import readGerberFile
from cura.Utils.PCBHandler.gerberTools.cam import FileSettings
from trimesh.exchange.export import export_dict
from trimesh.exchange.load import load_kwargs


def resetDir(temp_dir):
    if os.path.isdir(temp_dir):
        shutil.rmtree(temp_dir)
    os.mkdir(temp_dir)


def copyToTempWithTimestamp(path, temp_dir):
    if path is not None and temp_dir is not None:
        if os.path.isfile(temp_dir):
            temp_dir = os.path.dirname(temp_dir)
        if not os.path.isfile(path):
            return None
    else:
        return None

    # Add a timestamp prefix in front of the filename to distinguish between copies
    temp_filename = os.path.join(temp_dir,
                                 datetime.now().strftime('%Y-%m-%d_%H-%M-%S.%f_') + os.path.basename(path).lower())
    return shutil.copyfile(path, temp_filename)


class LayerType(Enum):
    # layers stackup manger options

    Unknown = 0
    Annotation = 1
    SolderMask = 2
    Signal = 3
    Prepreg = 4
    # SMT = 5

    SolderPaste = 6
    Component = 7
    Drill = 8
    Route = 9
    Document = 10
    Mixed = 11  # Mixed plane & signal
    Mask = 12  # GenFlex additional information

    @classmethod
    def get(cls, value):
        if isinstance(value, cls):
            return value
        if isinstance(value, int):
            return cls(value)
        return cls[value]


class Polarity(Enum):
    """Polarity of a layer"""
    Positive = 1
    Negative = 2


class DrillType(Enum):
    PTH = 0
    TH = 1
    VIA = 2

    @classmethod
    def get(cls, value):
        if isinstance(value, int):
            return cls(value)
        return cls[value]


_odb_drill_type_map = {
    "VIA": DrillType.VIA,
    "PLATED": DrillType.PTH,
    "NON_PLATED": DrillType.TH

}

_gerber_tools_layer_type_map = {
    "TopSilk": LayerType.Annotation,
    "BottomSilk": LayerType.Annotation,
    "toppaste": LayerType.SolderPaste,
    "bottompaste": LayerType.SolderPaste,
    "TopSolderMask": LayerType.SolderMask,
    "BottomSolderMask": LayerType.SolderMask,
    "TopSignal": LayerType.Signal,
    "BottomSignal": LayerType.Signal,
    "InternalSignal": LayerType.Signal,
    "drill": LayerType.Drill,
    "outline": LayerType.Route,
    "unknown": LayerType.Unknown,
    "DOCUMENT": LayerType.Document,
}

_gerber_tools_layer_stackup_index_map = {
    "TopSilk": 0,
    "BottomSilk": 6,
    "TopSolderMask": 1,
    "BottomSolderMask": 5,
    "TopSignal": 2,
    "BottomSignal": 4,
    "InternalSignal": 3,
    "unknown": -1,
    "outline": -1,

}


class BitWiseFlags(EnumMeta):
    def __new__(cls, name, bases, attrs):
        base_items = {k: v for (k, v) in attrs.items() if isinstance(v, int)}
        curr_vals = base_items.values()
        assert all((n & (n - 1) == 0) for n in curr_vals)
        assert all(not name.startswith("Comb") for name in attrs._member_names)
        max_value = max(curr_vals)
        for i in set(range(1, 2 * max_value - 1)) - set(curr_vals):
            attrs["Comb" + str(i)] = i
        attrs["Nothing"] = 0
        attrs["All"] = 2 * max_value - 1
        cls = super(BitWiseFlags, cls).__new__(cls, name, bases, attrs)
        cls.base_flags = base_items  # type: dict[str, int]

        return cls


class LayerErrors(IntEnum, metaclass=BitWiseFlags):
    ThicknessError = 1
    TypeError = 2
    ThicknessWarning = 4
    TypeWarning = 8
    ProcessError = 16

    def __contains__(self, item):
        return (self.value & item.value) == item.value

    def __add__(self, other):
        return LayerErrors(self.value | other.value)

    def __sub__(self, other):
        return LayerErrors(self.value & ~other.value)


class Thickness(object):
    def __init__(self, thickness=0.0):
        self._thickness = float(thickness)  # casting to float for backward compatibility with str
        self._observers = []

    @property
    def thickness(self):
        return self._thickness

    @thickness.setter
    def thickness(self, value):
        self._thickness = value
        self.call_observers()

    def call_observers(self):
        for callback in self._observers:
            callback()

    def bind_to(self, callback):
        self._observers.append(callback)


class Layer():
    def __init__(self, name, global_stack=None, layer_type=LayerType.Unknown, path=None, polarity=Polarity.Positive,
                 stackup_index=-1,
                 start=0, end=0, thickness=0.0, layer_dielectric=0, unit="metric", fmt=None, zeroinc=None, route=0,
                 preview_image_path=None, to_view=True, selected=False, errors=LayerErrors.Nothing, signal_index=None):
        self.name = name
        self._global_stack = global_stack
        self._type = layer_type
        self.path = path
        self.polarity = polarity
        self.stackup_index = stackup_index
        self.start = start
        self.end = end
        self._thickness = Thickness(thickness)
        self._thickness.bind_to(self.checkThicknessRule)
        self.layer_dielectric = layer_dielectric
        self.unit = unit
        self.fmt = fmt
        self.zeroinc = zeroinc
        self.route = route
        self.preview_image_path = preview_image_path
        self.to_view = to_view
        self.selected = selected
        self.errors = errors  # type: LayerErrors
        self.empty_layer = False
        self.signal_index = signal_index

        self.checkThicknessRule()  # This checks and sets the errors

    @property
    def type(self):
        return self._type

    @type.setter
    def type(self, value):
        if self._type != value:
            self._type = value
            self._thickness.call_observers()

    @property
    def thickness(self):
        return self._thickness.thickness

    @thickness.setter
    def thickness(self, value):
        self._thickness.thickness = value

    def checkThicknessRule(self, override_type_to_check=None):
        """
        apply on the layer itself, check if there is any thickness errors, use the stack to get the rules to work by
        :param override_type_to_check: in case that someone from outside the layer itself want to dictate the layer to check itself as different type from
                                 what layer really is. this parameter will be not none but a name of a layer type to check by it's rules
        :return: None , add errors to the layer.
        """
        type_to_get_rules_of = (override_type_to_check or self.type).name
        all_rules_types = getTypesForStackUp(self._global_stack, True)
        rules = next(filter(lambda rule: rule["type_name"] == type_to_get_rules_of, all_rules_types),
                     all_rules_types[0])
        if rules["type_name"] == LayerType.Annotation.name:
            if self.stackup_index == 0:
                rules = rules["top"]
            else:
                rules = rules["bottom"]
        elif rules["type_name"] == LayerType.SolderMask.name:
            if self.stackup_index <= 1:
                rules = rules["top"]
            else:
                rules = rules["bottom"]

        min_value = rules.get(KeysLimits.MIN.value,
                              0) or 0  # the "or 0" is for the case 'minimum_value' exists and is None
        min_warning_value = rules.get(KeysLimits.MIN_WARNING.value, 0) or 0
        max_warning_value = rules.get(KeysLimits.MAX_WARNING.value, np.inf) or np.inf
        max_value = rules.get(KeysLimits.MAX.value, np.inf) or np.inf

        self.errors -= LayerErrors.ThicknessError
        self.errors -= LayerErrors.ThicknessWarning
        if not (min_value < self.thickness < max_value):
            self.errors += LayerErrors.ThicknessError
        elif not (min_warning_value < self.thickness < max_warning_value):
            self.errors += LayerErrors.ThicknessWarning

    def toDictionary(self):
        return {
            "name": self.name,
            "type": self.type.name,
            "path": self.path or "",
            "polarity": self.polarity.name,
            "stackup_index": self.stackup_index,
            "start": self.start,
            "end": self.end,
            "thickness": self.thickness,
            "layer_dielectric": self.layer_dielectric,
            "unit": self.unit,
            "fmt": self.fmt or "",
            "zeroinc": self.zeroinc or "",
            "route": self.route,
            "preview_image_path": self.preview_image_path or None,
            "to_view": self.to_view,
            "selected": self.selected,
            "material": 'conductive' if self.type != LayerType.Prepreg else "dielectric",
            "errors": self.errors.value,
            "signal_index":self.signal_index
        }

    @classmethod
    def fromDictionary(cls, dic: Dict):
        try:
            layer_type = LayerType[dic.get("type")]
        except KeyError as ke:
            raise Exception("Cannot deserialize layer type {}".format(ke))

        try:
            polarity = Polarity[dic.get("polarity")]
        except KeyError as ke:
            raise Exception("Cannot deserialize polarity {}".format(ke))

        return cls(name=dic.get("name"),
                   layer_type=layer_type,
                   path=dic.get("path", ""),
                   polarity=polarity,
                   stackup_index=dic.get("stackup_index"),
                   start=dic.get("start"),
                   end=dic.get("end"),
                   thickness=dic.get("thickness"),
                   layer_dielectric=dic.get("layer_dielectric"),
                   unit=dic.get("unit"),
                   fmt=dic.get("fmt"),
                   zeroinc=dic.get("zeroinc"),
                   route=dic.get("route"),
                   preview_image_path=dic.get("preview_image_path"),
                   to_view=dic.get("to_view"),
                   selected=dic.get("selected")
                   )

    def __deepcopy__(self, memodict):
        copy_layer = Layer(name=self.name,
                           layer_type=self.type,
                           path=self.path,
                           polarity=self.polarity,
                           stackup_index=self.stackup_index,
                           start=self.start,
                           end=self.end,
                           thickness=self.thickness,
                           layer_dielectric=self.layer_dielectric,
                           unit=self.unit,
                           fmt=self.fmt,
                           zeroinc=self.zeroinc,
                           route=self.route,
                           preview_image_path=self.preview_image_path,
                           to_view=self.to_view,
                           global_stack=self._global_stack,
                           signal_index=self.signal_index)
        copy_layer.checkThicknessRule()
        memodict[id(self)] = copy_layer
        return copy_layer

    def setPath(self, path):
        self.path = path


class Drill:
    def __init__(self, name, drill_type: DrillType, start=0, end=0, diameter=None, path=None, unit="metric",
                 layers=None, selected=False, bounding_box=None, preview_image_path=None, to_view=True,
                 is_excellon=True, original_path: str = None, settings=None, drill_holes=None, is_out_of_route=False):
        self.layers = layers or []
        self.name = name
        self.type = drill_type
        # when loading the ODB first time, this will be set as "DRILL" ,after on processing,
        # a real value (of DrillType(ENUM)) will be override
        self.unit = unit
        self.start = start
        self.end = end
        self.path = path
        self.diameter = diameter
        self.selected = selected
        self.bounding_box = bounding_box
        self.preview_image_path = preview_image_path
        self.to_view = to_view
        self.is_excellon = is_excellon
        self.original_path = original_path
        self.settings = settings
        self.drill_holes = drill_holes
        self._is_out_of_route = is_out_of_route

    """
    A layer reference in a ODB++ dataset
    start,end = start and end layer name
    """

    def __deepcopy__(self, memodict):
        copy_drill = Drill(name=self.name,
                           drill_type=self.type,
                           start=self.start,
                           end=self.end,
                           diameter=self.diameter,
                           path=self.path,
                           unit=self.unit,
                           selected=self.selected,
                           layers=self.layers,
                           bounding_box=self.bounding_box,
                           preview_image_path=copyToTempWithTimestamp(self.preview_image_path, self.preview_image_path),
                           to_view=self.to_view,
                           is_excellon=self.is_excellon,
                           original_path=self.original_path,
                           settings=self.settings,
                           drill_holes=self.drill_holes,
                           is_out_of_route=self._is_out_of_route
                           )
        memodict[id(self)] = copy_drill
        return copy_drill

    @property
    def Layers(self):
        return self._layers.layers

    def toDictionary(self):

        return {
            "name": self.name,
            "type": self.type.name if isinstance(self.type, DrillType) else DrillType.VIA.name,
            "unit": self.unit,
            "start": self.start,
            "end": self.end,
            "path": self.path or "",
            "diameter": "{:.02f}".format(self.diameter) if isinstance(self.diameter, float) else "unknown",
            "selected": self.selected,
            "layers": self.layers,
            "bounding_box": self.bounding_box,
            "preview_image_path": self.preview_image_path or None,
            "to_view": self.to_view,
            "original_path": self.original_path or None,
            "is_excellon": self.is_excellon,
            "settings": self.settings.toDictionary() if self.settings else None,
            "drillError": self.error,
            "is_out_of_route": self._is_out_of_route
        }

    @property
    def error(self):
        my_layers = self.layers
        if self.type in {DrillType.TH, DrillType.PTH}:
            for l1, l2 in zip(my_layers, my_layers[1:]):
                if not l1["state_bool"] and l2["state_bool"]:
                    return True
        return False

    @classmethod
    def fromDictionary(cls, dic: Dict):
        name = dic.get("name")

        try:
            drill_type = DrillType[dic["type"]]
        except KeyError as ke:
            raise Exception(
                "Cannot deserialize drill type {} for drill {}, error: {}".format(dic.get("type"), name, ke))

        try:
            diameter = float(dic.get("diameter"))
        except (ValueError, TypeError):
            diameter = 0

        if "bounding_box" not in dic:
            raise KeyError("Failed to deserialize drill {}, 'bounding_box' data is missing".format(name))
        bounding_box = dic["bounding_box"]
        if not isinstance(bounding_box, tuple) and bounding_box is not None:
            raise Exception("bounding box of drill {} is badly formatted: {}".format(name, bounding_box))

        # handle old AMT with no number_of_holes by trying to read the drill file again
        if dic.get("drill_holes") is None:
            try:
                drill = readGerberFile(dic.get("path")).cam_source
                dic["drill_holes"] = drill.primitives
            except Exception as e:
                Logger.logException("w", "Failed to get number of holes for drill {}".format(dic.get("path")))
                dic["drill_holes"] = 0

        return cls(name=name,
                   drill_type=drill_type,
                   unit=dic.get("unit"),
                   start=dic.get("start"),
                   end=dic.get("end"),
                   path=dic.get("path", ""),
                   diameter=diameter,
                   selected=dic.get("selected"),
                   layers=dic.get("layers"),
                   bounding_box=bounding_box,
                   preview_image_path=dic.get("preview_image_path"),
                   to_view=dic.get("to_view"),
                   original_path=dic.get("original_path"),
                   is_excellon=dic.get("is_excellon"),
                   settings=FileSettings.fromDictionary(dic.get("settings")),
                   drill_holes=dic.get("drill_holes")
                   )

    def setPath(self, path):
        self.path = path

    @property
    def isOutOfRoute(self):
        return self._is_out_of_route


def DrillState(state: bool):
    return {"state_bool": state}  # QML has a reserved word 'state' so we use 'state_bool' as key


class Route:
    def __init__(self, name, path=None, polygon=None, bounding_box=None, area=None, mesh=None, route_index=0,
                 unit="metric", fmt=None, zeroinc=None, settings=None, preview_image_path=None, to_view=True,
                 selected=False):
        self.name = name
        self.path = path
        self.polygon = polygon
        self.bounding_box = bounding_box
        self.area = area
        self.mesh = mesh
        self.route_index = route_index
        self.unit = unit
        self.fmt = fmt
        self.zeroinc = zeroinc
        self.settings = settings
        self.preview_image_path = preview_image_path
        self.to_view = to_view
        self.selected = selected

    def toDictionary(self):
        return {
            "name": self.name,
            "path": self.path or "",
            "polygon": "NOT SUPPORTED ATM",  # TODO: Add support for serialization of route polygons
            "bounding_box": self.bounding_box,
            "area": self.area,
            "mesh": export_dict(self.mesh),
            "route_index": self.route_index,
            "unit": self.unit,
            "fmt": self.fmt,
            "zeroinc": self.zeroinc,
            "settings": self.settings.toDictionary(),
            "preview_image_path": self.preview_image_path,
            "to_view": self.to_view,
            "selected": self.selected
        }

    def __deepcopy__(self, memodict):
        copy_route = Route(self.name,
                           self.path,
                           self.polygon,
                           self.bounding_box,
                           self.area,
                           self.mesh,
                           self.route_index,
                           self.unit,
                           self.fmt,
                           self.zeroinc,
                           self.settings,
                           self.preview_image_path,
                           self.to_view,
                           self.selected
                           )
        memodict[id(self)] = copy_route
        return copy_route

    @classmethod
    def fromDictionary(cls, dic: Dict):
        bounding_box = dictToTupleOfTuples(dic.get("bounding_box"))

        path = dic.get("path", "")

        return cls(name=dic.get("name"),
                   path=path,
                   polygon=None,  # TODO: Add support to deserialization of route polygons
                   bounding_box=bounding_box,
                   area=dic.get("area"),
                   mesh=load_kwargs(dic.get("mesh")),  # this is the way trimesh wanna get dict and make it a mesh
                   route_index=dic.get("route_index"),
                   unit=dic.get("unit"),
                   fmt=dic.get("fmt"),
                   zeroinc=dic.get("zeroinc"),
                   settings=FileSettings.fromDictionary(dic.get("settings")),
                   preview_image_path=dic.get("preview_image_path"),
                   to_view=dic.get("to_view"),
                   selected=dic.get("selected")
                   )


def dictToTupleOfTuples(bounding_box):
    if isinstance(bounding_box, list):  # from json from ThreeMFWorkspaceReader
        bounding_box = tuple(tuple(a) for a in bounding_box)
    if not isinstance(bounding_box, tuple) and bounding_box is not None:
        raise ValueError("bounding box of wrong format: {}".format(bounding_box))
    return bounding_box


defaults = {'SignalLayerThickness': '17',
            'SolderMaskThicknessTop': '50',
            'SolderMaskThicknessBottom': '50',
            'SolderAnnotationThicknessTop': '3',
            'SolderAnnotationThicknessBottom': '3',
            'PrepregThickness': '50',
            'minPTH': 0.7,
            }


def getAllTypes():
    temp = LayerType
    list = []
    for one in temp:
        list.append({'name': one.name, 'value': int(one.value)})
    return list


def getTypesForStackUp(global_stack=None, include_prepreg=False):
    if global_stack is None:
        from cura.CuraApplication import CuraApplication
        global_stack = CuraApplication.getInstance().getMachineManager().activeMachine

    """
    Unknown = 0
    Annotation  = 1
    SolderMask = 2
    Signal = 3
    Prepreg = 4
    """
    lst = [
        {
            'type_name': "Unknown",
            'value': 0,
            **getMinMax(global_stack, "unknown_layer_thickness")
        },
        {
            'type_name': "Annotation",
            'value': 1,
            'top': getMinMax(global_stack, "annotation_top_layer_thickness"),
            'bottom': getMinMax(global_stack, "annotation_bottom_layer_thickness"),
        },
        {
            'type_name': "SolderMask",
            'value': 2,
            'top': getMinMax(global_stack, "soldermask_top_layer_thickness"),
            'bottom': getMinMax(global_stack, "soldermask_bottom_layer_thickness")
        },
        {
            'type_name': "Signal",
            'value': -1,
            **getMinMax(global_stack, "signal_layer_thickness")
        }
    ]
    if include_prepreg:
        lst.append({
            'type_name': "Prepreg",
            'value': 4,
            **getMinMax(global_stack, "prepreg_layer_thickness")
        })
    return lst


class KeysLimits(Enum):
    MIN_WARNING = 'minimum_value_warning'
    MAX_WARNING = 'maximum_value_warning'
    MIN = 'minimum_value'
    MAX = 'maximum_value'


def getMinMax(stack, property_name):
    return {
        KeysLimits.MIN_WARNING.value: stack.getProperty(property_name, KeysLimits.MIN_WARNING.value),
        KeysLimits.MAX_WARNING.value: stack.getProperty(property_name, KeysLimits.MAX_WARNING.value),
        KeysLimits.MIN.value: stack.getProperty(property_name, KeysLimits.MIN.value),
        KeysLimits.MAX.value: stack.getProperty(property_name, KeysLimits.MAX.value)
    }


if __name__ == "__main__":
    layer = Layer(name="try", layer_type=LayerType.Signal, polarity=Polarity.Positive)
    print(layer.toDictionary())

    layer.thickness = 1
    print(layer.toDictionary())
    layer.thickness = 10
    print(layer.toDictionary())
    layer.thickness = 52
    print(layer.toDictionary())
    layer.thickness = 100
    print(layer.toDictionary())


def makePlaceHolderMesh():
    builder = MeshBuilder()
    builder.addCube(10.0, 3.0, 10.0, Vector(0, 0, 0))  # Cube of 10 by 10 by 3. Note the Y-Z inversion
    builder.calculateNormals()
    mesh = builder.build()
    return mesh
