class GlobalParams:

    def __init__(self,
                 wcad_path=None,
                 engine_path=None,
                 artwork_key_path=None,
                 engine_folder_path=None,
                 dev_mode=None
                 ):
        self.wcad_path = wcad_path
        self.engine_path = engine_path
        self.artwork_key_path = artwork_key_path
        self.engine_folder_path = engine_folder_path
        self.dev_mode = dev_mode


    def UpdateByEnvValues(self, env_values):
        global_params = self
        global_params.wcad_path = env_values.WCAD_PATH
        global_params.engine_path = env_values.ENGINE_PATH
        global_params.artwork_key_path = env_values.KEY_PATH

        ## uniqe value - the original envValues
        global_params.env_values = env_values

    ##TODO improve
    def __str__(self):
        return str(self.__dict__)
