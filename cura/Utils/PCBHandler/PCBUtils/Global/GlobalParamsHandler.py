from cura.Utils.PCBHandler.PCBUtils.Global.Consts import Consts
from cura.Utils.PCBHandler.PCBUtils.Utils.General.StaticUtils import StaticUtils
from cura.Utils.PCBHandler.PCBUtils.Global.GlobalParams import GlobalParams
from cura.Utils.PCBHandler.PCBUtils.Global.EnvValues import EnvValues, EnvValuesHelper
from typing import Tuple, Optional, Union, List, Dict
import sys
import os

from UM.Logger import Logger


class GlobalParamsHandler:
    g_instance = None

    ## need to change to name
    @staticmethod
    def getGlobalParamsSingleTime(env_values):
        """
            :return: GlobalParams object filled with the relevant information for the specific run
        """
        global_params = GlobalParams()

        global_params.UpdateByEnvValues(env_values)

        ##Start validation for env values - if not exist, value set to None
        if not StaticUtils.DirExist(global_params.wcad_path): global_params.wcad_path = None
        if not StaticUtils.FileExist(global_params.engine_path): global_params.engine_path = None
        if not StaticUtils.DirExist(global_params.engine_folder_path): global_params.engine_folder_path = None
        if not StaticUtils.FileExist(global_params.artwork_key_path): global_params.artwork_key_path = None

        global_params_by_cwd = GlobalParamsHandler.FindPathsBasedOnWorkDir()

        if not StaticUtils.DirExist(global_params_by_cwd.wcad_path): global_params_by_cwd.wcad_path = None
        if not StaticUtils.FileExist(global_params_by_cwd.engine_path): global_params_by_cwd.engine_path = None
        if not StaticUtils.DirExist(global_params_by_cwd.engine_folder_path): global_params_by_cwd.engine_folder_path = None
        if not StaticUtils.FileExist(global_params_by_cwd.artwork_key_path): global_params_by_cwd.artwork_key_path = None


        ##gen on the final output: update the final results, if not found by ENV - ovride by CurrWorkDir results
        global_params.wcad_path = global_params.wcad_path or global_params_by_cwd.wcad_path
        global_params.engine_path = global_params.engine_path or global_params_by_cwd.engine_path
        global_params.engine_folder_path = global_params.engine_folder_path or global_params_by_cwd.engine_folder_path
        global_params.artwork_key_path = global_params.artwork_key_path or global_params_by_cwd.artwork_key_path

        global_params.dev_mode = global_params_by_cwd.dev_mode


        Logger.log("i", "all EnvVars: {}".format(os.environ))

        message = '\n\nGlobal_Params:\n--------------------'
        for key in global_params.__dict__:
            val = global_params.__dict__.get(key, "")
            message += '\n' + key + ": " + str(val)
        message += '\n' + '--------------------\n'

        Logger.log("i", message)

        return global_params

    '''
    this is the main function for getting the singleton access
    '''
    @staticmethod
    def getGlobalParams():
        if GlobalParamsHandler.g_instance is None:
            env_values = EnvValuesHelper.LoadEnvValues()
            Logger.log("i", env_values)
            GlobalParamsHandler.g_instance = GlobalParamsHandler.getGlobalParamsSingleTime(env_values)
        return GlobalParamsHandler.g_instance

    '''
    genereate the global params based on CurrWorkDir 
    '''
    @staticmethod
    def FindPathsBasedOnWorkDir():
        work_dir = StaticUtils.GetCurrWorkDir()

        dev_mode_only = GlobalParamsHandler.getDevModeOnly()
        global_params = GlobalParams()

        global_params.dev_mode = dev_mode_only

        list_engine_op = StaticUtils.StrInsertFront_List(work_dir + "/",
                                                         Consts.ENGINE_REL_FOLDER_PATH_DEFAULTS_MAP[dev_mode_only])

        global_params.engine_path = StaticUtils.FindFirst(list_engine_op, StaticUtils.FileExist)

        ## not fonud with cwd - attempt find by running exe path
        if global_params.engine_path == None:
            curr_exec_path = os.path.abspath(sys.argv[0])
            exec_name = os.path.basename(curr_exec_path)

            expected_engine_as_exec_name = os.path.basename(Consts.ENGINE_REL_FOLDER_PATH_DEFAULTS_MAP[Consts.DevMode.release][0])
            if exec_name == expected_engine_as_exec_name:
                global_params.engine_path = curr_exec_path
            elif exec_name == Consts.MAIN_EXE_NAME:
                curr_exec_folder_path = os.path.dirname(curr_exec_path)
                global_params.engine_path = os.path.join(curr_exec_folder_path, Consts.ENGINE_REL_FOLDER_PATH_DEFAULTS_MAP[Consts.DevMode.release][0])


        found = StaticUtils.FileExist(global_params.engine_path)

        # Extra
        global_params.engine_folder_path = os.path.dirname(global_params.engine_path)

        global_params.artwork_key_path = os.path.join(global_params.engine_folder_path, Consts.KEY_FILE_NAME)

        global_params.wcad_path = os.path.join(global_params.engine_folder_path, Consts.WCAD_FOLDER_NAME)

        ##To absolute path
        global_params.artwork_key_path = os.path.abspath(global_params.artwork_key_path)
        global_params.engine_path = os.path.abspath(global_params.engine_path)
        global_params.wcad_path = os.path.abspath(global_params.wcad_path)
        global_params.engine_folder_path = os.path.abspath(global_params.engine_folder_path)

        return global_params

    @staticmethod
    def getDevModeOnly() -> Consts.DevMode:
        """
        This indicates whether the application runs in dev mode (opened by direct python.exe call),
        in release mode (opened by cura.exe call) or anything else (as of 2021-04-23 no other mode is supported)
        """
        prog_exec = sys.executable
        prog_exec_l = prog_exec.lower()
        if "python.exe" in prog_exec_l or "python3.exe" in prog_exec_l:  # Debug mode
            return Consts.DevMode.dev
        else:  # Installed version
            return Consts.DevMode.release
