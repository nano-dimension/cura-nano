# ------------------------------- ## TOOD when we have single git move both to the most upper part of the root
# Consts namings for global use
from enum import Enum
import os

class Consts:
    class DevMode(Enum):
        dev = 0
        release = 1

    WCAD_FILES_PATHS_LIST = {
        "exes": [
            "/GBRIP64/gbr2tiff64.exe                    ",
            "/GBRVU64/artwork/drl2gbr64.exe             ",
            "/STL2GDS64/stl2x64.exe                     ",
            "/QISMLIB/artwork/qismscript64.exe          ",
            "/ODB2GBR64/odb2gbr64.exe                   ",
        ]
    }

    WCAD_FOLDER_NAME = 'wcad'

    ENGINE_REL_FOLDER_PATH_DEFAULTS_MAP = {
        DevMode.dev: [os.path.join('artworkengine', 'Engine.py')],
        DevMode.release: [os.path.join('Engine', 'Engine.exe')]
    }

    MAIN_EXE_NAME = "FLIGHT Control.exe"


    KEY_FILE_NAME = 'nano_dim.key'

    ND_ENGINE_PATH_ENV_VAR_NAME = "ND_ENGINE_PATH"
    WCAD_PATH_ENV_VAR_NAME = "ND_WCAD_PATH"
    FLIGHT_CONTROL_INSTALL_PATH_VAR_NAME = "ND_FLIGHT_CONTROL_HOME"
    KEY_PATH_VAR_NAME = "ND_ENGINE_KEY_PATH"


    #----------------- output
    OUTPUT_FILE_EXT = '.pcbjc'
    JSON_SUB_OUTPUT_FILE_NAME = "pcbj.info"
