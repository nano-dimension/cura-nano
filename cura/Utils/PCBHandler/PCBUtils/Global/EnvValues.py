from cura.Utils.PCBHandler.PCBUtils.Utils.General.StaticUtils import StaticUtils

from cura.Utils.PCBHandler.PCBUtils.Global.Consts import Consts
import os


class EnvValues:

    def __init__(self,
                 ROOT_INSTALLER_PATH=None,
                 WCAD_PATH=None,
                 ENGINE_PATH=None,
                 KEY_PATH=None
                 ):
        self.ROOT_INSTALLER_PATH = ROOT_INSTALLER_PATH
        self.WCAD_PATH = WCAD_PATH
        self.ENGINE_PATH = ENGINE_PATH
        self.KEY_PATH = KEY_PATH
        pass

    def __str__(self):
        s = '\n\n' + 'EnvValues: ' + '\n---------------------------------' + '\n'

        s += 'ROOT_INSTALLER_PATH           :' + str(self.ROOT_INSTALLER_PATH) + '\n'
        s += 'WCAD_PATH                     :' + str(self.WCAD_PATH) + '\n'
        s += 'ENGINE_PATH                   :' + str(self.ENGINE_PATH) + '\n'
        s += 'KEY_PATH                      :' + str(self.KEY_PATH) + '\n'

        return s


class EnvValuesHelper():

    @staticmethod
    def LoadEnvValues():
        env_values = EnvValues()

        env_values.ROOT_INSTALLER_PATH = os.environ.get(Consts.FLIGHT_CONTROL_INSTALL_PATH_VAR_NAME)
        env_values.WCAD_PATH = os.environ.get(Consts.WCAD_PATH_ENV_VAR_NAME)
        env_values.ENGINE_PATH = os.environ.get(Consts.ND_ENGINE_PATH_ENV_VAR_NAME)
        env_values.KEY_PATH = os.environ.get(Consts.KEY_PATH_VAR_NAME)

        return env_values
