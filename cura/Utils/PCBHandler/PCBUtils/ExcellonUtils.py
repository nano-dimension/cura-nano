import os
from copy import deepcopy

from cura.Utils.PCBHandler.PCBUtils.GerberReadUtils import readGerberFile
from cura.Utils.PCBHandler.PCBUtils.PcbUtils import DrillType


def excellonSplit(layer, defaults ):
    '''
    get a excellon layer and split by sizes of circles inside
    :param defaults:
    :param layer:
    :return: list of dicts, keys : path , name, diameter, layer to start, layer to end , and type : plated not plated of via
    '''
    try:
        layer = layer.cam_source

        keys = list(layer.tools.keys())

        excellon_list = []
        if len(keys) > 1 and not (True in [type(drill).__name__=='DrillSlot' for drill in layer.hits]):
            for key in keys:
                temp_excellon = deepcopy(layer)
                temp_excellon.isolate_tool(key)
                excellon_list.append(temp_excellon)
                # temp.remove_tool(key)
                temp_excellon.filename = os.path.join(os.path.dirname(temp_excellon.filename),
                                                      str(key) + os.path.basename(temp_excellon.filename))
                temp_excellon.write()
            os.remove(layer.filename)

        else:
            excellon_list.append(layer)

        drills = []
        for excellon in excellon_list:
            to_mm = 25.4 if excellon.units == 'inch' else 1
            diameter = list(excellon.tools.values())[0].diameter * to_mm
            if diameter < defaults['minPTH'] and list(excellon.tools.values())[0].PLATED_YES == 'plated':
                drill_type = DrillType.VIA.value
            elif list(excellon.tools.values())[0].PLATED_YES == 'plated':
                drill_type = DrillType.PTH.value
            else:
                drill_type = DrillType.TH.value
            drills.append({"path": excellon.filename,
                           "name": os.path.basename(excellon.filename),
                           "start": 0,
                           "finish": 0,
                           "type": drill_type,
                           "diameter": str(diameter),
                           'unit': excellon.units,
                           'format': ".".join(map(str,excellon.format)),
                           'selected':False})
    except:
        '''
        in case this file can't be split, the file will be add ass one unit to the drill manger as  "unknown" 
        '''

        return [{"path": layer.filename,
                 "name": os.path.basename(layer.filename),
                 "start": 0,
                 "finish": 0,
                 "type": 0,
                 "diameter": "unknown",
                 'unit': layer.units,
                 'format': ".".join(map(str,excellon.format))}]

    return drills


def Main():
    in_path = r'C:\Users\daniel.s\Nano Dimension\Applications - Application\Switch 2\ODB Examples\LGA COUPON_REV B1\pcb\drill.gbr'

    defaults = {'SignalLayerThickness': '17',
                'SolderMaskThicknessTop': '50',
                'SolderMaskThicknessBottom': '50',
                'SolderAnnotationThicknessTop': '3',
                'SolderAnnotationThicknessBottom': '3',
                'PrepregThickness': '50',
                'minPTH': 0.7,
                }

    layer = readGerberFile(in_path)
    units = layer.units
    name = os.path.basename(in_path)
    drills = excellonSplit(layer, defaults)

    return

if __name__ == "__main__":
    Main()
