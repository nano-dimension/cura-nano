#------------------------------- ## TOOD when we have single git move both to the most upper part of the root

import sys
from cura.Utils.PCBHandler.PCBUtils.Global.Consts import Consts
from cura.Utils.PCBHandler.PCBUtils.Utils.General.StaticUtils import StaticUtils
from cura.Utils.PCBHandler.PCBUtils.Global.GlobalParamsHandler import GlobalParamsHandler

from UM.Logger import Logger

## check and report cases of issues with wcad
class ContentValidation():

    @staticmethod
    def ValidateAndQuitOnIssue_WCAD():
        global_params = GlobalParamsHandler.getGlobalParams()
        wcad_path = global_params.wcad_path
        wcad_valid = ContentValidation.CheckWcadLibValid(wcad_path)
        if not wcad_valid:
            text = "could not find and load in wcad lib from: " + wcad_path
            text += "\nLoading model files, will fail."

            Logger.log("e", text)
            StaticUtils.MsgBox(text,title='Warning',type = 0)
            sys.exit(1)

    @staticmethod
    def CheckWcadLibValid(wcad_path):
        res = False

        if not StaticUtils.DirExist(wcad_path):
            return res

        ## check list of files needs to be found
        wcad_files_exes = Consts.WCAD_FILES_PATHS_LIST["exes"]
        fullpath_wcad_files_exes = []
        for ref_path in wcad_files_exes:
            fullpath = StaticUtils.trim(wcad_path + ref_path)
            fullpath_wcad_files_exes.append(fullpath)

        allFound = StaticUtils.CheckFilesListExist(fullpath_wcad_files_exes)
        res = allFound

        return res

    @staticmethod
    def ValidateAndQuitOnIssue_EnginePath(dev_mode):
        global_params = GlobalParamsHandler.getGlobalParams()

        engine_path_from_env = global_params.engine_path

        found = StaticUtils.StringNonEmtpy(engine_path_from_env)
        if found:
            return engine_path_from_env
        else:
            orig_env_value = global_params.orig_env_values.ENGINE_PATH
            message = "Cannot determine Engine path, dev mode is {}, engine_path_from_env: {}".format(dev_mode.name,
                                                                                                      orig_env_value)
            Logger.log("e", message)
            StaticUtils.MsgBox(message, "Error")
            sys.exit(1)