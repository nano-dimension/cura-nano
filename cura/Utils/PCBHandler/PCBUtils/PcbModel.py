import copy
import os
import re
import tempfile
import threading
import typing
from copy import deepcopy
from datetime import datetime
from shutil import copytree
from typing import cast, Dict, List, Optional, Callable

from PyQt5.QtCore import QUrl

from UM.Logger import Logger
from UM.Math.Vector import Vector
from UM.Message import Message
from UM.Operations.OperationStack import OperationStack
from UM.Scene.SceneNode import SceneNode
from UM.Scene.SceneNodeDecorator import SceneNodeDecorator
from cura.CuraApplication import CuraApplication
from cura.Operations.AddLayerOperation import AddLayerOperation
from cura.Operations.RemoveLayerByIndexOperation import RemoveLayerByIndexOperation
from cura.Operations.SwapLayersOperation import SwapLayersOperation
from cura.Operations.UpdateLayerThicknessOperation import UpdateLayerThicknessOperation
from cura.Operations.UpdateLayerTypeOperation import UpdateLayerTypeOperation
from cura.Scene.BuildPlateDecorator import BuildPlateDecorator
from cura.Scene.ConvexHullDecorator import ConvexHullDecorator
from cura.Scene.CuraSceneNode import CuraSceneNode
from cura.Scene.SliceableObjectDecorator import SliceableObjectDecorator
from cura.Utils.MeshUtils import combineTwoTrimeshes, trimeshToMeshData
from cura.Utils.PCBHandler.PCBUtils.GerberReadUtils import readGerberFile, routeToMeshUsingGbrRip, \
    readExcellonFile, drl2gbr, mateUnitForBB, converUnit, calculateRoutePreviewImage
from cura.Utils.PCBHandler.PCBUtils.MessagesOnPcbLoad import PcbMessage, messageType
from cura.Utils.PCBHandler.PCBUtils.PcbUtils import Drill, DrillType, Layer, _gerber_tools_layer_type_map, LayerType, \
    _gerber_tools_layer_stackup_index_map, Route, makePlaceHolderMesh, DrillState, copyToTempWithTimestamp, \
    dictToTupleOfTuples, LayerErrors
from cura.Utils.PCBHandler.PcbModelDecorator import PcbModelDecorator
from cura.Utils.PCBHandler.gerberTools.cam import FileSettings, CamFile
from cura.Utils.PCBHandler.gerberTools.common import onlyDetectIfExcellon
from cura.Utils.PCBHandler.gerberTools.excellon import detect_excellon_format
from cura.Utils.PCBHandler.gerberTools.layers import PCBLayer
from cura.Utils.PathUtils import copyFileWithIncreasingNameIfExist

_gerber_thickness_defaults_map = {
    "TopSilk": 'SolderAnnotationThicknessTop',
    "BottomSilk": 'SolderAnnotationThicknessBottom',
    "TopSolderMask": 'SolderMaskThicknessTop',
    "BottomSolderMask": 'SolderMaskThicknessBottom',
    "TopSignal": 'SignalLayerThickness',
    "BottomSignal": 'SignalLayerThickness',
    "InternalSignal": 'SignalLayerThickness',
}
# constant for 2di
RECIPE_2DI_CRITERIA_NUM_OF_HOLES_PER_LAYER = 50
RECIPE_2DI_CRITERIA_MAX_SIGNALS = 10
RECIPE_2DI_CRITERIA_MAX_VIA_DIAMETER = 0.235


class PcbModel(CuraSceneNode):
    drl2grb_lock = threading.Lock()

    def __init__(self, parent: Optional["SceneNode"] = None, visible: bool = True, name: str = "PCB model",
                 no_setting_override: bool = False, settings=None, unit="mm", global_stack=None, temp_dir=None) -> None:
        super().__init__(parent=parent, visible=visible, name=name, no_setting_override=no_setting_override)
        self._global_stack = global_stack
        self._stackup = list()
        self._drills = list()
        self._original_drills = list()
        self._routes = list()
        self._unit = unit
        self.defaults = {'SignalLayerThickness': 17,
                         'SolderMaskThicknessTop': 50,
                         'SolderMaskThicknessBottom': 50,
                         'SolderAnnotationThicknessTop': 3,
                         'SolderAnnotationThicknessBottom': 3,
                         'PrepregThickness': 50,
                         'minPTH': 0.7,
                         'scale': 100.0,
                         } if settings is None else settings
        self.bounding_box = None
        self.bounding_box_unit = None
        self._temp_dir = self.makeTempDir(temp_dir)
        self._preview_image = None
        self.notify = None

        self.preview_job = None
        self._preview_job_lock = threading.Lock()

        pcb_scale = self.defaults.get("scale", 100.0)
        self.scale(Vector((pcb_scale / 100), 1, (pcb_scale / 100)))
        self._stackup_operation_stack = OperationStack()

    def getStackupOperationStack(self):
        return self._stackup_operation_stack

    def getPreviewLock(self):
        return self._preview_job_lock

    def toDictionary(self):
        return {
            "name": self.getName(),
            "stackup": [layer.toDictionary() for layer in self._stackup],
            "drills": [drill.toDictionary() for drill in self._drills],
            "original_drill_paths": self._original_drills,
            "routes": [route.toDictionary() for route in self._routes],
            "unit": self._unit,
            "defaults": self.defaults,
            "bounding_box": self.bounding_box,
            "bounding_box_unit": self.bounding_box_unit,
            "preview_image_path": self.getPreviewImage(),
        }

    @classmethod
    def fromDictionaryAndNode(cls, node_data: Dict, cura_node: CuraSceneNode, global_stack=None, temp_dir=None):
        # First construct the new node from existing data
        new_node = cls(parent=cura_node.getParent(),
                       visible=cura_node.isVisible(),
                       name=cura_node.getName(),
                       no_setting_override=True,  # We will add this decorator later along with the others
                       settings=node_data.get("defaults"),
                       unit=node_data.get("unit"),
                       global_stack=global_stack,
                       temp_dir=temp_dir
                       )

        new_node.setSelectable(True)  # This is used e.g. when deciding whether we can export the mesh

        # Set existing properties
        new_node.setMeshData(cura_node.getMeshData())

        # Setting meshdata does not apply scaling.
        new_node.scale(cura_node.getScale())

        new_node.setTransformation(cura_node.getLocalTransformation())

        # If there is no convex hull for the node, start calculating it and continue.
        if not new_node.getDecorator(ConvexHullDecorator):
            new_node.addDecorator(ConvexHullDecorator())
        for child in new_node.getAllChildren():
            if not child.getDecorator(ConvexHullDecorator):
                child.addDecorator(ConvexHullDecorator())

        # This is a PCB model - decorate it as such (apparently decorators are not serialized)
        new_node.addDecorator(PcbModelDecorator())

        # Add decorators from the original CuraSceneNode
        for decorator in cura_node.getDecorators():
            new_node.addDecorator(cast(SceneNodeDecorator, deepcopy(decorator)))

        # This node is deep copied from some other node which already has a BuildPlateDecorator,
        # but the deepcopy of BuildPlateDecorator produces one that's associated with build plate -1.
        # So, here we need to check if the BuildPlateDecorator exists or not
        # and always set the correct build plate number.
        target_build_plate = CuraApplication.getInstance().getMultiBuildPlateModel().activeBuildPlate
        build_plate_decorator = new_node.getDecorator(BuildPlateDecorator)
        if build_plate_decorator is None:
            build_plate_decorator = BuildPlateDecorator(target_build_plate)
            new_node.addDecorator(build_plate_decorator)
        build_plate_decorator.setBuildPlateNumber(target_build_plate)

        default_extruder_position = CuraApplication.getInstance().getMachineManager().defaultExtruderPosition
        default_extruder_id = CuraApplication.getInstance().getGlobalContainerStack().extruderList[
            int(default_extruder_position)].getId()
        new_node.callDecoration("setActiveExtruder", default_extruder_id)
        new_node.addDecorator(SliceableObjectDecorator())  # Has to be sliceable so that it can be moved

        # Add the PCB model data
        new_node._stackup = [Layer.fromDictionary(layer_dic) for layer_dic in node_data.get("stackup", [])]
        new_node._drills = [Drill.fromDictionary(drill_dic) for drill_dic in node_data.get("drills", [])]
        new_node._original_drills = node_data.get("original_drill_paths", [])
        new_node._routes = [Route.fromDictionary(route_dic) for route_dic in node_data.get("routes", [])]
        new_node.bounding_box = dictToTupleOfTuples(node_data.get("bounding_box"))
        new_node.bounding_box_unit = node_data.get("bounding_box_unit")
        new_node._preview_image = node_data.get("preview_image_path") if node_data.get(
            "preview_image_path") is not None else "canceled"

        return new_node

    @staticmethod
    def makeTempDir(temp_dir, name="PCBReader"):
        temp_dir = os.path.join(temp_dir.name, name, datetime.now().strftime('%Y-%m-%d_%H-%M-%S-%f'))
        os.makedirs(temp_dir)
        Logger.log("d", "Created a temporary directory at {}".format(temp_dir))
        return temp_dir

    def __deepcopy__(self, memo: Dict[int, object]) -> "PcbModel":
        # This is duplicated from CuraSceneNode in order to return the correct type.
        # There has got to be a cleaner way of doing this...
        copy = PcbModel(no_setting_override=True,
                        temp_dir=CuraApplication.getInstance().getTempDir())  # Setting override will be added later

        copy.setTransformation(self.getLocalTransformation(copy=False))
        copy.setMeshData(self._mesh_data)
        copy.setVisible(cast(bool, deepcopy(self._visible, memo)))
        copy._selectable = cast(bool, deepcopy(self._selectable, memo))
        copy._name = cast(str, deepcopy(self._name, memo))
        for decorator in self._decorators:
            copy.addDecorator(cast(SceneNodeDecorator, deepcopy(decorator, memo)))

        for child in self._children:
            copy.addChild(cast(SceneNode, deepcopy(child, memo)))
        self.calculateBoundingBoxMesh()
        attrs_shallow = {"_global_stack", "bounding_box", "bounding_box_unit", "_unit"}
        attrs_deep = {"_stackup", "_drills", "_original_drills", "_routes", "defaults"}

        for attr in attrs_shallow:
            value = getattr(self, attr)
            setattr(copy, attr, value)

        for attr in attrs_deep:
            value = deepcopy(getattr(self, attr))
            setattr(copy, attr, value)

        # Add pcb-specific properties
        copy.setPreviewImage(
            copyToTempWithTimestamp(self._preview_image, self._temp_dir) or "canceled")  # create a copy of the image
        for layer in copy._stackup:
            layer.checkThicknessRule(self._global_stack)
        copy.checkStackupTypes()

        return copy

    def setPreviewHook(self, notify):
        self.notify = notify

    def getPreviewImage(self):
        return self._preview_image

    def setPreviewImage(self, new_image):
        self._preview_image = new_image
        if self.notify:
            self.notify()

    def cleanPreview(self):
        self.setPreviewImage(None)
        return

    def createPreview(self,
                      reraster_all: bool = True,
                      post_job_callback: Callable = None):
        # cancel old preview job
        self.stopPreviewJob()

        # create new preview only when we have application
        from cura.Utils.PCBHandler.PreviewCreatorJob import PreviewCreatorJob
        self.preview_job = PreviewCreatorJob(node=self,
                                             reraster_all=reraster_all)

        # todo : connect signal to enable the process widget in the application
        self.preview_job.finished.connect(post_job_callback or self.updatePreviewImage)

        self.preview_job.start()

    def stopPreviewJob(self):
        """
        This stops the current preview job
        :param do_block_until_stopped: if True, will block the current thread until the preview thread stops
        """
        # just in case
        if self.preview_job:
            self.preview_job.cancel()
            self.preview_job = None

    def waitUntilPreviewDone(self):
        with self._preview_job_lock:
            pass  # Nothing to do here really, we just waited for the lock to be released
            Logger.log("d", "preview done")

    @staticmethod
    def updatePreviewImage(job):
        from cura.Utils.PCBHandler.UnionPreviewImageJob import UnionPreviewImageJob
        preview_job = UnionPreviewImageJob(job.getResult())
        preview_job.start()

    def getStackupAsDict(self) -> List[dict]:
        return [layer.toDictionary() for layer in self._stackup]

    def getDrillsAsDict(self) -> List[dict]:
        return [drill.toDictionary() for drill in self._drills]

    def getMaxBoundingBoxFromRouteInVector(self):
        return self.bounding_box

    def getMaxBoundingBoxUnits(self):
        return self.bounding_box_unit

    def getTempDir(self):
        return self._temp_dir

    def getSelectedDrill(self):
        for drill in self._drills:
            if drill.selected:
                return drill.preview_image_path
        return None

    def getDrillsForSerialization(self):
        new_drills_list = list()
        for drill in self._drills:
            intervals = list()
            start = None
            for layer_state_index, layer_state in enumerate(drill.layers):
                if start is None:
                    if layer_state['state_bool']:
                        start = layer_state_index
                else:
                    if not layer_state['state_bool']:
                        intervals.append([start, layer_state_index - 1])
                        start = None
                    elif layer_state_index == len(drill.layers) - 1:
                        intervals.append([start, layer_state_index])
                        start = None

            drill_dict = drill.toDictionary()
            drill_dict['layers_intervals'] = intervals
            del drill_dict["layers"]
            new_drills_list.append(drill_dict)
        return new_drills_list

    def getRoutesAsDict(self) -> List[dict]:
        return [route.toDictionary() for route in self._routes]

    def getStackup(self) -> List[Layer]:
        return self._stackup

    def getStackupCount(self) -> int:
        return len(self._stackup)

    def getNumberOfSignalLayers(self) -> int:
        """
        counts only the signal layers
        """
        self.updateSignalIndexOfLayers()
        num_of_signal_layers = 0
        for layer in self._stackup:
            if layer.signal_index != 0:
                num_of_signal_layers += 1
        return num_of_signal_layers

    def getDrills(self) -> List[Drill]:
        return self._drills

    def getDrillsCount(self) -> int:
        return len(self._drills)

    def getRoute(self) -> Route:
        return self._routes[0] if self._routes else None

    def getRoutes(self) -> List[Route]:
        return self._routes

    def getRoutesCount(self) -> int:
        return len(self._routes)

    def appendRouteAndUpdateAll(self, paths=None, onFinish=None):
        from cura.Utils.PCBHandler.RouteApplyJob import RouteApplyJob
        if paths is None:
            paths = ['']
        for url in paths:
            if url != '':
                new_path = self.copyFilesToTemp([url.toLocalFile()] if isinstance(url, QUrl) else [url])[0]
                route_update_job = RouteApplyJob(self, new_path)
            else:
                route_update_job = RouteApplyJob(self)
            route_update_job.finished.connect(self._onRouteJobFinished)
            if onFinish:
                route_update_job.finished.connect(onFinish)
            route_update_job.start()

    def _onRouteJobFinished(self, job):
        # re-render all images only needed if the ,main route did not changed
        pcb_node = job.node  # type PcbModel
        if pcb_node.getRoutesCount():
            self.createPreview()

    def _UnionPreviewImageForJob(self, job):
        from cura.Utils.PCBHandler.UnionPreviewImageJob import UnionPreviewImageJob
        preview_2_job = UnionPreviewImageJob(job.getResult())
        preview_2_job.start()

    def addLayerUndoable(self, layer, layer_index=-1, undo_callback=None, redo_callback=None):
        AddLayerOperation(self._stackup_operation_stack, self, layer, layer_index,
                          undo_callback=undo_callback, redo_callback=redo_callback).push()

    def removeLayerByIndexUndoable(self, layer_index, undo_callback=None, redo_callback=None):
        add_to_drills = []
        # save the drills that go through the layer in add_to_drills for later undo
        for drill_index, drill in enumerate(self._drills):
            if drill.layers[layer_index]["state_bool"]:
                add_to_drills.append(drill_index)
        RemoveLayerByIndexOperation(self._stackup_operation_stack, self, layer_index, add_to_drills,
                                    undo_callback=undo_callback, redo_callback=redo_callback).push()

    def updateLayerRoute(self, layer_index, route_index):
        layer = self._stackup[layer_index]
        layer.route = route_index

    def addRoute(self, layer: PCBLayer = None, path: str = None, is_continue_callback=None):
        if layer is None:
            if path is not None:
                layer = readGerberFile(path)

            else:
                return False
        if isinstance(layer, Route):
            self._routes.append(layer)

        name = os.path.basename(layer.filename)
        try:
            enlarge_all_apertures_by_constant = float(self._global_stack.getProperty("enlarge_all_apertures_by_constant", "value"))
            route_image_path, metrix_bounding_box = calculateRoutePreviewImage(layer, enlarge_all_apertures_by_constant)
            assert route_image_path is not None, "Failed to calculate route for layer {}".format(layer.filename)

            create_mesh_from_route = self._global_stack.getProperty("create_mesh_from_route", "value")
            mesh = routeToMeshUsingGbrRip(route_image_path, metrix_bounding_box, create_mesh_from_route)

            bounding_box = ((layer.cam_source.bounding_box[0][0], layer.cam_source.bounding_box[1][0]),
                            (layer.cam_source.bounding_box[0][1], layer.cam_source.bounding_box[1][1]))
            units = layer.cam_source.units

        except Exception as e:
            file_name = os.path.basename(path)
            Logger.logException("e", "Failed to add route from {}: {}".format(path, e))
            PcbMessage(model_type=type(self).__name__,
                       message_type=messageType.warning_message,
                       file_name=name,
                       title="Loading route from {}".format(file_name),
                       text="Invalid route : {}".format(e)).show()
            self.appendLayerFromPathToStackup(path)
            return

        fmt = str(layer.cam_source.format)[1:-1].replace(", ", ".")
        self._routes.append(Route(name=name,
                                  path=path,
                                  mesh=mesh,
                                  bounding_box=bounding_box,
                                  area=mesh.area,
                                  unit=units,
                                  fmt=fmt,
                                  zeroinc=layer.cam_source.zeros,
                                  settings=layer.cam_source.settings,
                                  preview_image_path=route_image_path))
        Logger.log("i", "Route {} added path {} bounding_box {} units {}".format(name, path, bounding_box, units))

    def readLayerFromPath(self, path=None, thickness=None) -> Layer:
        if path is None:
            Logger.log("w", "path added as route is empty")
            return
        pcb_layer = readGerberFile(path)
        file_name = os.path.basename(path)
        if pcb_layer is None:
            Logger.log("w", path.split("/")[-1] + " : could not read file")
            PcbMessage(message_type=messageType.warning_message, file_name=file_name,
                       text="Could not read gerber file", title="Loading file {}".format(file_name)).show()
            return None
        # if layer.primitives is empty its an empty gerber file
        if len(pcb_layer.primitives) == 0:
            Logger.log("w", path.split("/")[-1] + " is empty file")
            PcbMessage(message_type=messageType.warning_message, file_name=file_name,
                       text="Gerber file is empty", title="Loading file {}".format(file_name)).show()
            return None

        name = os.path.basename(path)
        layer_type = _gerber_tools_layer_type_map[pcb_layer.layer_class]

        # Currently we support layers up to LayerType.Prepreg, need to change here in order to support more layers
        if layer_type.value > LayerType.Prepreg.value:
            layer_type = LayerType.Unknown
            stackup_index = -1
        else:
            stackup_index = _gerber_tools_layer_stackup_index_map[pcb_layer.layer_class]
        fmt = str(pcb_layer.cam_source.format)[1:-1].replace(", ", ".")

        if layer_type == LayerType.Unknown:
            thickness = 0
        elif thickness is None:
            thickness = self.defaults.get(_gerber_thickness_defaults_map.get(pcb_layer.layer_class), 0)

        layer_dielectric = self.defaults['PrepregThickness']
        layer = Layer(name=name,
                      layer_type=layer_type,
                      stackup_index=stackup_index,
                      unit=pcb_layer.cam_source.units,
                      thickness=thickness,
                      layer_dielectric=layer_dielectric,
                      path=path,
                      fmt=fmt,
                      global_stack=self._global_stack)
        return layer

    def addLayerToStackup(self, layer: Layer, layer_index: int = None, add_to_drills: List[int] = None):
        layer_index = len(self._stackup) if layer_index is None else layer_index
        self._stackup.insert(layer_index, layer)
        self.appendLayerToDrills(layer_index, add_to_drills)
        self.checkStackupTypes()
        self.updateSignalIndexOfLayers()
        return layer

    def appendLayerFromPathToStackup(self, path):
        layer = self.readLayerFromPath(path)
        if layer is not None:
            self.addLayerToStackup(layer)
        else:
            Logger.log("d", "empty path - action ignored")

    def appendLayerToDrills(self, layer_index: int, add_to_drills: List[int] = None):
        """
            called when adding a new layer, add_to_drills by default it is "ON" for all drills
        """
        add_to_drills = add_to_drills if add_to_drills is not None else range(len(self._drills))
        for drill_index, drill in enumerate(self._drills):
            drill.layers.insert(layer_index, DrillState(drill_index in add_to_drills))

    def insertLayerToDrills(self, layer_index):
        for drill in self._drills:
            drill.layers.insert(layer_index, DrillState(True))

    def initAllDrillsLayersToBeIn(self):
        for drill in self._drills:
            self.initDrillLayersToBeIn(drill)
        return

    def initDrillLayersToBeIn(self, drill):
        drill.layers = []
        for _ in self._stackup:
            drill.layers.append(DrillState(True))

    def addDrill(self, path: str = None, settings: FileSettings = None):

        '''
        adds the drill, with override settings if present
        return list of the added drills or empty, error message

        TODO - return should be error_code instead of PCBMessage
        '''

        original_path = path
        is_excellon = True
        new_drills = []
        pcb_message = None
        drill_name = os.path.basename(path)

        if path is None or path == "":
            pcb_message = PcbMessage(model_type=type(self).__name__, message_type=messageType.warning_message,
                                     file_name=os.path.basename(path),
                                     text="NoPath")
            return new_drills, pcb_message

        # lets check if gerber
        if onlyDetectIfExcellon(path) != 'excellon':
            # if not we also support drill models so lets move them to stackup but we also want to update the user
            is_excellon = False
            file_name = os.path.basename(path)
            pcb_message = PcbMessage(model_type=type(self).__name__, message_type=messageType.warning_message,
                                     file_name=file_name,
                                     title="Loading drill {}".format(file_name),
                                     text="Drill format not supported. Adding as layer")
        else:
            if self.getRoutesCount() == 0:
                self.appendLayerFromPathToStackup(path)
                file_name = os.path.basename(path)
                pcb_message = PcbMessage(model_type=type(self).__name__,
                                         message_type=messageType.warning_message,
                                         file_name=file_name,
                                         title="Loading drill file {}".format(file_name),
                                         text="No route found. Drill added to stackup.")
                return new_drills, pcb_message

            ## check if drill not already exist
            for current_drill in self._drills:
                ##Error case
                if current_drill.name == drill_name:
                    pcb_message = PcbMessage(model_type=type(self).__name__, message_type=messageType.warning_message,
                                             file_name=str(drill_name or ''),
                                             text=("Drill already exists: " + str(drill_name or '')))
                    return new_drills, pcb_message

            tool_cam_file = readExcellonFile(path, settings or self._routes[0].settings)  # type PCBLayer
            if tool_cam_file is None:
                pcb_message = PcbMessage(model_type=type(self).__name__, message_type=messageType.warning_message,
                                         file_name=os.path.basename(path),
                                         text="Notype-EmptyDrill")
                return new_drills, pcb_message

            settings = settings or tool_cam_file.cam_source.settings

            with self.drl2grb_lock:
                path = drl2gbr(file=tool_cam_file.filename, working_dir=self.getTempDir(), settings=settings)

        layer = readGerberFile(path)  # type PCBLayer
        if layer is None:
            pcb_message = PcbMessage(model_type=type(self).__name__, message_type=messageType.warning_message,
                                     file_name=os.path.basename(path),
                                     text="Notype-EmptyDrill")
            return new_drills, pcb_message

        settings = layer.cam_source.settings
        self._original_drills.append(original_path)
        max_bb_unit = self.getMaxBoundingBoxUnits()
        if max_bb_unit is None:
            max_bb_unit = layer.cam_source.units

        per_tool_cam_files = self.splitPCBLayerToCamFiles(layer, self._temp_dir)

        drills_out_of_routes = []
        for tool_cam_file in per_tool_cam_files:
            new_drill = readGerberFile(tool_cam_file.filename)
            if new_drill is None or len(new_drill.primitives) == 0:
                Logger.log("i", "Drill isolated tool {} is empty. Not adding to drills".format(tool_cam_file.filename))
                continue

            drill_out_of_route = PcbModel.checkIfDrillsNotInRoute(new_drill, self._routes[0])
            if drill_out_of_route:
                drills_out_of_routes.append(new_drill)

            # the cam_source is different between pcb drill and pcb Gerber's so we have to adjust it after the tests
            tool_cam_file = new_drill.cam_source

            tool = list(tool_cam_file.apertures.values())[0]
            try:
                diameter = converUnit(tool.diameter, settings.units, "metric")
            except:
                diameter = 0

            new_drill = Drill(name=drill_name,
                              path=tool_cam_file.filename,
                              drill_type=DrillType.VIA if diameter <= self.defaults["minPTH"] else DrillType.PTH,
                              diameter=diameter,
                              unit=max_bb_unit,
                              is_excellon=is_excellon,
                              original_path=original_path,
                              settings=settings,
                              drill_holes=tool_cam_file.primitives,
                              is_out_of_route=drill_out_of_route)

            new_drills.append(new_drill)

            self._drills.append(new_drill)
            self.initDrillLayersToBeIn(self._drills[-1])

        if drills_out_of_routes:
            file_name = os.path.basename(path)
            PcbMessage(model_type=type(self).__name__, message_type=messageType.warning_message,
                       file_name=file_name,
                       title="Loading drill file {}".format(file_name),
                       text="Some drills are out of route. Check The drill excellon settings").show()

        return new_drills, pcb_message

    @staticmethod
    def checkIfDrillsNotInRoute(drill, route):
        route_bb = route.bounding_box
        drill_primitives = copy.deepcopy(drill.primitives)
        drill_out_of_route = False
        for primitive in drill_primitives:
            if route.unit == "inch":
                primitive.to_inch()
            else:
                primitive.to_metric()
            if hasattr(primitive, "start"):
                if primitive.start[0] < route_bb[0][0] or \
                        primitive.start[1] < route_bb[0][1] or \
                        primitive.end[0] > route_bb[1][0] or \
                        primitive.end[1] > route_bb[1][1]:
                    Logger.log("w", "Drill {}-{} position {} out of route bounding box {}".format(
                        drill.filename, primitive.start, primitive.end, route_bb))
                    drill_out_of_route = True
            elif hasattr(primitive, "position"):
                if primitive.position[0] < route_bb[0][0] or \
                        primitive.position[1] < route_bb[0][1] or \
                        primitive.position[0] > route_bb[1][0] or \
                        primitive.position[1] > route_bb[1][1]:
                    Logger.log("w", "Drill {} position {} out of route bounding box {}".format(
                        drill.filename, primitive.position, route_bb))
                    drill_out_of_route = True
            else:
                continue
        return drill_out_of_route

    def addDielectric(self, layer_index, thickness=None, unit="metric"):
        if thickness is None:
            thickness = self.defaults['PrepregThickness']
        layer = Layer(name='Prepreg', global_stack=self._global_stack, layer_type=LayerType.Prepreg,
                      thickness=thickness, unit=unit)
        self.addLayerToStackup(layer, layer_index)

    def addDielectricAfterLayerUndoable(self, layer_index, thickness=None, unit="metric", undo_callback=None,
                                        redo_callback=None):
        if thickness is None:
            thickness = self.defaults['PrepregThickness']
        layer = self._stackup[layer_index]
        dielectric_layer = Layer(name='Prepreg', route=layer.route, layer_type=LayerType.Prepreg, thickness=thickness,
                                 unit=unit)
        self.addLayerUndoable(dielectric_layer, layer_index + 1, undo_callback=undo_callback,
                              redo_callback=redo_callback)

    def isRouteAndStackupInSameFormat(self):
        if len(self._routes) == 0:
            return True
        route_format = self._routes[0].fmt
        for route in self._routes:
            if route.fmt != route_format:
                Logger.log("w", "route 0 format {} but route format is {}".format(route_format, route.fmt))
                return False
        for layer in self._stackup:
            if layer.fmt is None or layer.type == LayerType.Prepreg:
                continue
            if layer.fmt != route_format:
                Logger.log("w",
                           "route 0 format {} but layer {} format is {}".format(route_format, layer.name, layer.fmt))
                return False
        return True

    def updateDrillSettings(self, drill_index, new_settings) -> Drill:
        """
        Update a drill excellon settings
        return the updated drill. or None if nothing was changed
        """
        Logger.log("i", "update drill index {} to settings {}".format(drill_index, new_settings))

        if drill_index < 0 or drill_index > len(self._drills):
            return
        if self._drills[drill_index].settings == new_settings:
            return self._drills[drill_index]
        attrs_to_backup = {'layers', 'to_view'}
        backup = {
            drill.path: {
                attr: getattr(drill, attr) for attr in attrs_to_backup
            }
            for drill in self._drills
        }
        original_drill_path = self._drills[drill_index].original_path
        temp_drills = self._drills
        self._drills = []
        before_drill_added = True
        for drill in temp_drills:
            if os.path.basename(drill.original_path) != os.path.basename(original_drill_path):
                self._drills.append(drill)
            elif before_drill_added:
                _, pcb_message = self.addDrill(path=original_drill_path, settings=new_settings)
                if pcb_message is not None:
                    pcb_message.show()

                before_drill_added = False

        for drill in self._drills:
            if os.path.basename(drill.original_path) != os.path.basename(original_drill_path):
                for attr in attrs_to_backup:
                    setattr(drill, attr, backup[drill.path][attr])

        return self._drills[drill_index]

    def updateDrillDiameter(self, drill_index, new_diameter) -> Drill:
        """
        Update a drill diameter
        return <the updated drill, error message>. updated drill is None if the operation failed
        """
        Logger.log("i", "update drill index {} to diameter {}".format(drill_index, new_diameter))

        if drill_index < 0 or drill_index > len(self._drills):
            return None, "Invalid drill index"
        DRILL_DIAMETER_TOO_BIG = 200
        if new_diameter < 0 or new_diameter > DRILL_DIAMETER_TOO_BIG:
            return None, "Invalid drill diameter {}".format(new_diameter)
        drill = self._drills[drill_index]
        if drill.diameter == new_diameter:
            return self._drills[drill_index], None
        with open(drill.path, "r+") as drill_gerber_file:
            pcb_layer = readGerberFile(drill.path)
            drill_gerber_txt = drill_gerber_file.read()
            replace_diameter = "ADD\\1C,{:g}*".format(converUnit(new_diameter, "metric", pcb_layer.cam_source.units))
            gerber_txt_replaced = re.sub(r"ADD([\d]+)C,[\d\.]+\*", replace_diameter, drill_gerber_txt)
            if re.sub is None or drill_gerber_txt == gerber_txt_replaced:
                Logger.log("w", "failed to replace drill {} diameter", drill.name)
                return None, "Drill not found"
            drill_gerber_file.seek(0)
            drill_gerber_file.write(gerber_txt_replaced)
            drill_gerber_file.truncate()
            drill_gerber_file.flush()
        drill.preview_image_path = None
        drill.diameter = new_diameter
        return drill, None

    def addDielectricLayersToStackup(self):
        layer_index = 0
        while layer_index < len(self._stackup[:-2]):
            layer = self._stackup[layer_index]
            if layer.type == LayerType.Signal:
                if self._stackup[layer_index + 1].type == LayerType.Signal:
                    self.addDielectric(layer_index=layer_index + 1, thickness=layer.layer_dielectric, unit=layer.unit)
                    layer_index += 2
                else:
                    layer_index += 1
            else:
                layer_index += 1

    def makeDrillTH(self, drill_index):
        self._drills[drill_index].end = len(self._stackup) - 1

    def makeDemiRoute(self):
        self.setMeshData(makePlaceHolderMesh())
        PcbMessage(model_type=type(self).__name__,
                   message_type=messageType.warning_message,
                   text="No route found. Unable to present real size",
                   title="Loading Model",
                   node=self).show()
        self.setPreviewImage("canceled")

    def removeRouteByIndex(self, route_index, need_it_back=False):
        if self.getRoutesCount() == 0:
            return False
        route = None
        if need_it_back:
            route = deepcopy(self._routes[route_index])

        for layer in self._stackup:
            if layer.route == route_index:
                layer.route = 0
            elif layer.route > route_index:
                layer.route -= 1

        self._routes.remove(self._routes[route_index])
        return route or True

    def removeRouteByName(self, name, need_it_back=False):
        for layer_index, layer in enumerate(self._routes):
            if layer.name == name:
                return self.removeRouteByIndex(layer_index, need_it_back) or True

    def removeRouteByPath(self, path, need_it_back=False):
        for layer_index, layer in enumerate(self._routes):
            if layer.path == path:
                return self.removeRouteByIndex(layer_index, need_it_back) or True

    def getLayerCopyByIndex(self, layer_index) -> Layer:
        if layer_index >= self.getStackupCount():
            return None
        return deepcopy(self._stackup[layer_index])

    def getLayerIndexByName(self, name):
        for layer_index, layer in enumerate(self._stackup):
            if layer.name == name:
                return layer_index

    def getLayerIndexBySignalIndex(self, given_signal_index):
        for layer in self._stackup:
            if layer.signal_index == int(given_signal_index):
                return self.getLayerIndexByName(layer.name)

    def removeLayerByIndex(self, layer_index, need_it_back=False) -> Layer:
        if layer_index >= self.getStackupCount():
            return None

        layer = None
        if need_it_back:
            layer = deepcopy(self._stackup[layer_index])
        self._stackup.remove(self._stackup[layer_index])
        for drill in self._drills:
            if len(drill.layers) > layer_index:
                drill.layers.pop(layer_index)
        self.checkStackupTypes()
        self.updateSignalIndexOfLayers()
        return layer

    def removeLayerByName(self, name, need_it_back=False) -> Layer:
        for layer_index, layer in enumerate(self._stackup):
            if layer.name == name:
                return self.removeLayerByIndex(layer_index, need_it_back) or True

    def removeLayerByPath(self, path, need_it_back=False) -> Layer:
        # that function CAN NOT remove prepregs
        if path == "":
            return None
        for layer_index, layer in enumerate(self._stackup):
            if layer.path == path:
                return self.removeLayerByIndex(layer_index, need_it_back) or True

    def removeDrillByIndex(self, drill_index, need_it_back=False):
        if self.getDrillsCount() == 0:
            return False
        drill = None
        if need_it_back:
            drill = deepcopy(self._drills[drill_index])
        self._drills.remove(self._drills[drill_index])

        # TODO check
        self.checkStackupTypes()
        return drill or True

    def removeDrillByName(self, name, need_it_back=False):
        for drill_index, drill in enumerate(self._drills):
            if drill.name == name:
                drill = self.removeDrillByIndex(drill_index, need_it_back)
                return drill or True

    def removeDrillByPath(self, path, need_it_back=False):
        for drill_index, drill in enumerate(self._drills):
            if drill.path == path:
                drill = self.removeDrillByIndex(drill_index, need_it_back)
                return drill or True

    def updateNodeMeshData(self):
        if len(self._routes) == 0:
            self.makeDemiRoute()
            self.bounding_box = None
            self.bounding_box_unit = None
            return False
        self._routes.sort(key=lambda x: x.area, reverse=True)

        max_bounding_box = self._routes[0].bounding_box
        max_bounding_box_unit = self._routes[0].unit

        for route in self._routes[1:]:
            bounding_box = mateUnitForBB(route.bounding_box, route.unit, max_bounding_box_unit)
            max_bounding_box = (
                (min(bounding_box[0][0], max_bounding_box[0][0]), min(bounding_box[0][1], max_bounding_box[0][1])),
                (max(bounding_box[1][0], max_bounding_box[1][0]), max(bounding_box[1][1], max_bounding_box[1][1]))
            )

        # if the maximum bounding box calculate is different that the first route bounding box, a layer with other
        # route can in theory be "in the air" which is an error condition. we will still render a preview
        # on the maximum bounding box to allow the user to correct it
        if max_bounding_box != self._routes[0].bounding_box:
            Message(str("When using multiple routes they must be bounded in each other, from top to bottom."),
                    title="Invalid Routes").show()

        max_bounding_box_changed = max_bounding_box != self.bounding_box
        self.bounding_box = max_bounding_box
        self.bounding_box_unit = max_bounding_box_unit

        temp_mesh = self._routes[0].mesh
        for route in self._routes[1:]:
            temp_mesh = combineTwoTrimeshes(temp_mesh, route.mesh, auto_z=True)
        self.setMeshData(trimeshToMeshData(temp_mesh))

        return max_bounding_box_changed

    def swapLayers(self, from_index, to_index):
        # sanity
        if from_index not in range(len(self._stackup)) or to_index not in range(len(self._stackup)):
            Logger.log("w", "invalid layer indices to swap {} with {}, stackup length {}"
                       .format(from_index, to_index, len(self._stackup)))
            return
        self._stackup[from_index], self._stackup[to_index] = self._stackup[to_index], self._stackup[from_index]
        self.checkStackupTypes()

    def swapLayersUndoable(self, from_index, to_index, undo_callback=None, redo_callback=None):
        SwapLayersOperation(self._stackup_operation_stack, self, from_index, to_index, undo_callback=undo_callback,
                            redo_callback=redo_callback).push()

    def updateLayerThicknessUndoable(self, layer_index, thickness, undo_callback=None, redo_callback=None):
        layer = self._stackup[layer_index]
        if layer.thickness == thickness:
            Logger.debug("Not updating layer {} thickness to {} - unchanged", layer_index, thickness)
            return
        UpdateLayerThicknessOperation(self._stackup_operation_stack, self, layer_index, thickness,
                                      undo_callback=undo_callback,
                                      redo_callback=redo_callback).push()

    def updateLayerThickness(self, layer_index, thickness):
        if layer_index not in range(0, len(self._stackup)):
            return
        layer = self._stackup[layer_index]
        prev_thickness = layer.thickness
        layer.thickness = thickness
        self.checkStackupTypes()
        return prev_thickness

    def updateLayerDielectric(self, layer_index, layer_dielectric):
        if layer_index not in range(0, len(self._stackup)):
            return
        layer = self._stackup[layer_index]
        prev_layer_dielectric = layer.layer_dielectric
        layer.layer_dielectric = layer_dielectric
        return prev_layer_dielectric

    def updateLayerTypeUndoable(self, layer_index, layer_type, undo_callback=None, redo_callback=None):
        layer = self._stackup[layer_index]
        if layer.type == layer_type:
            Logger.debug("Not updating layer {} type to {} - unchanged", layer_index, layer_type)
            return
        UpdateLayerTypeOperation(self._stackup_operation_stack, self, layer_index, layer_type,
                                 undo_callback=undo_callback,
                                 redo_callback=redo_callback).push()

    def updateLayerType(self, layer_index, layer_type):
        """
        Update layer type
        return previous layer type if success. otherwise return None
        """
        if layer_index not in range(len(self._stackup)):
            Logger.log("w", "invalid layer index {}, stackup length {}"
                       .format(layer_index, len(self._stackup)))
            return
        layer = self._stackup[layer_index]
        prev_layer_type = layer.type
        layer.type = layer_type
        self.checkStackupTypes()
        self.updateSignalIndexOfLayers()
        return prev_layer_type

    def setRoutesIndex(self):
        for route_index, route in enumerate(self._routes):
            route.stackup_index = route_index

    def setSettingsAsDict(self):
        self.setSetting("stackup", list(map(vars, self._stackup)))
        self.setSetting("drills", list(map(vars, self._drills)))
        self.setSetting("routes", list(map(vars, self._routes)))

    def updateStackupFromGui(self, results):
        self.checkStackupTypes()
        return True

    def checkStackupTypes(self):
        for layer in self._stackup:
            if layer.empty_layer:
                layer.errors += LayerErrors.ProcessError
            else:
                layer.errors -= LayerErrors.ProcessError
        for layer in self._stackup:
            layer.errors -= LayerErrors.TypeError
            layer.errors -= LayerErrors.TypeWarning

        for layer in self._stackup:
            if layer.type.value not in {LayerType.Signal.value, LayerType.SolderMask.value, LayerType.Annotation.value,
                                        LayerType.Prepreg.value}:
                layer.errors += LayerErrors.TypeError

        for layer in self._stackup[1:-1]:
            if layer.type == LayerType.Annotation:
                layer.errors += LayerErrors.TypeError

        for layer in self._stackup[2:-2]:
            if layer.type == LayerType.SolderMask:
                layer.errors += LayerErrors.TypeError

        n = len(self._stackup) - 1
        if n >= 2:
            # allow soldermask to be not the highest/lowest layer only if there is annotation above / bellow
            if self._stackup[1].type == LayerType.SolderMask:
                if self._stackup[0].type != LayerType.Annotation:
                    self._stackup[1].errors += LayerErrors.TypeError

            if self._stackup[n - 1].type == LayerType.SolderMask:
                if self._stackup[n].type != LayerType.Annotation:
                    self._stackup[n - 1].errors += LayerErrors.TypeError

        # in case that the solder mask is in place 0 or -1, it means that there is no Annotation in this side of the board.
        # so the rules should be calculated as signal ( printable conductive layer ) and not considers that annotation layer should be inserted.
        # (inserted -> split to 3 sections).
        # the thickness checking is inside the layer it self , in a place that the layer do not know it's content and it's neighbors.
        # so the place to consider the neighborhood is here, the place the stack up check itself as a layers stack.
        if self._stackup:
            if self._stackup[0].type == LayerType.SolderMask:
                self._stackup[0].checkThicknessRule(override_type_to_check=LayerType.Signal)
            if self._stackup[-1].type == LayerType.SolderMask:
                self._stackup[-1].checkThicknessRule(override_type_to_check=LayerType.Signal)
        self.updateSignalIndexOfLayers()

    def updateDrillsFromGui(self, results):
        for drill_index, drill in enumerate(results):
            self._drills[drill_index].type = DrillType.get(drill['type'])
            self._drills[drill_index].layers = deepcopy(drill['layers'])
            self._drills[drill_index].selected = drill['selected']


    def detectDrillFmt(self):
        if len(self._original_drills) > 0:
            drill_example = readGerberFile(self._original_drills[0])
            container = [False]
            try:
                detect_excellon_format(filename=drill_example.filename, container=container)
                if container[0]:
                    for drill in self._drills:
                        drill.fmt = self._routes[0].fmt
                        drill.zeroinc = self._routes[0].zeroinc
                        drill.unit = self._routes[0].unit

            except:
                print("not excellon")

    @staticmethod
    def splitPCBLayerToCamFiles(layer: PCBLayer, temp_dir: str) -> List[CamFile]:
        """
        Given a PCBLayer split it to files for each tool
        Return a list of CamFile objects, each with the filename
        Write the files to temp_dir
        """
        if _gerber_tools_layer_type_map[layer.layer_class] == LayerType.Drill:
            # excellon file
            cam_source = layer.cam_source
            keys = list(cam_source.tools.keys())
        else:
            # eny other gerber
            cam_source = layer.cam_source
            keys = list(cam_source.apertures)

        cam_files_list = []
        if len(keys) > 1:
            for key in keys:
                temp_cam_file = deepcopy(cam_source)
                temp_cam_file.isolate_tool(key)
                cam_files_list.append(temp_cam_file)
                temp_cam_file.filename = os.path.join(temp_dir,
                                                      os.path.basename(temp_cam_file.filename) + "_" + str(key))
                temp_cam_file.write()
        else:
            cam_files_list.append(cam_source)
        return cam_files_list

    def copyFilesToTemp(self, files: list) -> list:
        new_files_list = []
        for file in files:
            new_files_list.append(
                copyFileWithIncreasingNameIfExist(file, os.path.join(self._temp_dir, os.path.basename(file))))
        return new_files_list

    def copyDirToTemp(self, files) -> list:
        new_files_list = []
        for file in files:
            new_files_list.append(copytree(file, os.path.join(self._temp_dir, os.path.basename(file))))
        return new_files_list

    def switchToViewInLayer(self, layer_index):
        self._stackup[layer_index].to_view = not self._stackup[layer_index].to_view

    def switchToViewInDrill(self, drill_index):
        self._drills[drill_index].to_view = not self._drills[drill_index].to_view

    def showAllLayers(self):
        for layer in self._stackup:
            layer.to_view = True

    def hideAllLayers(self):
        for layer in self._stackup:
            layer.to_view = False

    def hideAllDrills(self):
        for drill in self._drills:
            drill.to_view = False

    def showAllDrills(self):
        for drill in self._drills:
            drill.to_view = True

    def _onParentChanged(self, node: Optional["SceneNode"]) -> None:
        """
        In addition to the base-class signal, we want to check whether this node was actually removed from the scene.
        In this case, we want to cancel its PreviewJob
        """
        super()._onParentChanged(node)

        if not self.getParent():
            self.stopPreviewJob()

    def getPreferredRecipe(self):
        signal_list = list()
        for layer in self._stackup:
            if layer.type.name == 'Signal':
                signal_list.append(layer)
        if len(signal_list) >= RECIPE_2DI_CRITERIA_MAX_SIGNALS:
            return None
        # lets check if there is any drill with more than 50 holes and daim under 235 um moving through layer
        # TODO:need to understand primitives for odb
        for tool in self._drills:
            if tool.drill_holes is None and self._name.endswith('.tgz'):
                continue
            if len(tool.drill_holes) >= RECIPE_2DI_CRITERIA_NUM_OF_HOLES_PER_LAYER \
                    and tool.diameter <= RECIPE_2DI_CRITERIA_MAX_VIA_DIAMETER:
                return "High Quality Fine Vias"
        return None

    def getTotalThikness(self):
        return sum(layer.thickness for layer in self._stackup) / 1000.0

    def updateSignalIndexOfLayers(self):
        "lets define indexes for signals only"
        index = 0
        for layer in self._stackup:
            if layer.type.name == 'Signal':
                index += 1
                layer.signal_index = index
            else:
                layer.signal_index = 0


