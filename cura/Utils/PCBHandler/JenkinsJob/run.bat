REM - run.bat will be in the repo source and should run unit tests. - will be arrange after we have single git

@echo off
REM ------------------ input params ----------      v
set PYTHONPATH=%PYTHONPATH%"C:/Jenkins/workspace/flight_control_verification/cura-nano;C:/Jenkins/workspace/flight_control_verification/uranium-nano;C:/Jenkins/workspace/flight_control_verification/artworkengine"
set HOME_PATH=C:\Jenkins\workspace\flight_control_verification
set TESTS_DIR=%HOME_PATH%/cura-nano/cura/Utils/PCBHandler/tests
set TEST_NAME=TestPcbModel.py

REM --- mabye will change--
set ENV_PATH=C:\Jenkins\workspace\flight_control_verification\env
REM --- mabye will change--
REM ------------------ input params ----------      ^


REM ------------------ generated params ----------      v
set ND_ENGINE_PATH=%HOME_PATH%/artworkengine/Engine.py
set PATH=%PATH%;%PYTHONPATH%

set PYTHON_EXE=%ENV_PATH%/bin/python.exe
set TEST_PATH=%TESTS_DIR%/%TEST_NAME%
set PYTHON_PARAMS= -m pytest "%TEST_PATH%"
set EXTRA_TEST_ARGS=--capture=no                    REM      not tested yet
set RUN_CMD=%PYTHON_EXE% %PYTHON_PARAMS%
REM ------------------ generated params ----------      ^
 

REM ------------------ echo params ----------      v
echo PYTHONPATH: 	 %PYTHONPATH%
echo PATH:		     %PATH%	
echo ND_ENGINE_PATH: %ND_ENGINE_PATH%
echo ENV_PATH:		 %ENV_PATH%	
echo PYTHON_EXE:		 %PYTHON_EXE%	
echo TEST_PATH:		 %TEST_PATH%	
echo PYTHON_PARAMS:		 %PYTHON_PARAMS%	
echo RUN_CMD:		 %RUN_CMD%	
REM ------------------ echo params ----------      ^


REM ------------------ begin run ----------         v
cd/d %TESTS_DIR%
%RUN_CMD% 
REM ------------------ begin run ----------         ^






