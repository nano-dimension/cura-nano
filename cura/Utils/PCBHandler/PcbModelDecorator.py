from UM.Scene.SceneNodeDecorator import SceneNodeDecorator


class PcbModelDecorator(SceneNodeDecorator):
    def __init__(self) -> None:
        super().__init__()

    def isPcbModel(self) -> bool:
        return True

    def __deepcopy__(self, memo) -> "PcbModelDecorator":
        return type(self)()
