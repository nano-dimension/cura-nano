import os
import subprocess
import threading
import time
from typing import Tuple

from UM.Job import Job
from UM.Logger import Logger
from UM.Message import Message
from cura.Utils import PathUtils
from cura.CuraApplication import CuraApplication
from cura.Utils.PCBHandler.PCBUtils.GerberReadUtils import gbrRip, mateUnitForBB, makeUnitsFitToArtwork
from cura.Utils.PCBHandler.PCBUtils.MessagesOnPcbLoad import PcbMessage, messageType
from cura.Utils.PCBHandler.PCBUtils.PcbModel import PcbModel
from cura.Utils.PCBHandler.PCBUtils.PcbUtils import LayerType, Layer
from cura.Utils.PCBHandler.PCBUtils.PcbUtils import Drill, DrillType, Layer, _gerber_tools_layer_type_map, LayerType, \
    _gerber_tools_layer_stackup_index_map, Route, makePlaceHolderMesh, DrillState, copyToTempWithTimestamp, \
    dictToTupleOfTuples, LayerErrors
from hasplib import haspChecker
from tqdm import tqdm


class PreviewCreatorJob(Job):
    def __init__(self, node: PcbModel, reraster_all=True):
        super().__init__()
        self._is_cancelled = False
        self.node = node
        self._result = []
        self._loading_message = None
        self.reraster_all = reraster_all

    def run(self) -> None:
        with self.node.getPreviewLock():
            self._loading_message = Message(self.node.getName(),
                                            lifetime=0,
                                            progress=0,
                                            dismissable=True,
                                            title='Creating Preview')
            self._loading_message.setProgress(-1)
            self._loading_message.show()
            self.node.cleanPreview()

            CuraApplication.getInstance().getOnExitCallbackManager().addCallback(self.ExitApp)

            max_bb = self.node.getMaxBoundingBoxFromRouteInVector()
            max_bb_unit = self.node.getMaxBoundingBoxUnits()

            if max_bb is None:
                self._loading_message.hide()
                PcbMessage(model_type=type(self.node).__name__,
                           message_type=messageType.warning_message,
                           title="Creating preview",
                           text="No route found. Unable to create preview",
                           node=self.node).show()
                self.node.setPreviewImage("canceled")
                return

            self.node.setPreviewImage(None)
            if not self.node.isRouteAndStackupInSameFormat():
                PcbMessage(message_type=messageType.warning_message, text="Route and stackup have different format",
                           title="Format Mismatch").show()

            for layer in tqdm(self.node.getStackup() + self.node.getRoutes() + self.node.getDrills()):
                if not self._loading_message.visible or self._is_cancelled:
                    self.node.setPreviewImage("canceled")
                    self._loading_message.hide()

                    return

                if isinstance(layer, Layer) and layer.type == LayerType.Prepreg:
                    continue
                if layer.preview_image_path is not None and not self.reraster_all:
                    continue

                local_max_bb = mateUnitForBB(max_bb, max_bb_unit, layer.unit)
                try:
                    enlarge_all_apertures_by_constant = float(
                        CuraApplication.getInstance().getGlobalContainerStack().getProperty("enlarge_all_apertures_by_constant", "value"))
                    layer.preview_image_path = gbrRip(gbr_path=layer.path,
                                                      working_dir=self.node.getTempDir(),
                                                      bounding_box=local_max_bb,
                                                      unit=layer.unit,
                                                      enlarge_all_apertures_by_constant=enlarge_all_apertures_by_constant)
                    if layer.preview_image_path is None:
                        PcbMessage(message_type=messageType.error_message, title="stackup Error",
                                   text="Gerber {} is empty file - please remove it to process".format(
                                       layer.name)).show()
                        layer.empty_layer = layer.preview_image_path is None

                except haspChecker.HaspLicenseException as e:
                    self.setError(PermissionError("Cannot create preview, {}".format(e)))
                    self._loading_message.hide()
                    return

            self.node.checkStackupTypes()
            self.setResult(self.node)
            self._loading_message.hide()

            return

    def cancel(self):

        # call parent method
        super().cancel()

        # continue with own method
        self._is_cancelled = True

        try:
            self._loading_message.setText("Cancelling")
        except:
            pass

    def ExitApp(self):
        if self.node.preview_job is not None:
            # cancel current job
            self.node.stopPreviewJob()

            # wait for it to finish (note: the waiting happens where the lock is, i.e. on the node side)
            self.node.waitUntilPreviewDone()

        # Report we're done
        CuraApplication.getInstance().getOnExitCallbackManager().onCurrentCallbackFinished()
