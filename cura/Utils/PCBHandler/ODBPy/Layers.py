# !/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Parser for the ODB++ PCB matrix file
"""
import os.path
import os.path

from cura.Utils.PCBHandler.PCBUtils.PcbUtils import Layer, LayerType, Drill, Route

from .LineRecordParser import read_linerecords
from .StructuredTextParser import read_structured_text, StructuredText
from .Structures import polarity_map

__all__ = ["LayerSet", "parse_layers", "read_layers",
           "read_layer_components", "read_layer_features"]


class LayerSet(list):
    """
    A list of Layer objects with extra convenience functions
    """

    def by_type(self, layer_type):
        "Find all layers that have the given type"
        return LayerSet(filter(lambda l: l.type == layer_type, self))

    def by_obj_type(self, layer_type):
        "Find all layers that have the given type"
        return LayerSet(filter(lambda l: type(l) == layer_type, self))

    def by_name(self, name):
        "Get a layer by name or None if there is no such layer. Case-insensitive search"
        try:
            name_lower = name.lower()
            return next(filter(lambda l: l.name.lower() == name_lower, self))
        except StopIteration:
            return None

    def component_layers(self):
        """Get all component layers"""
        components = self.by_type(LayerType.Component)
        if len(components) in [2, 0]:  # Top, bottom or no components
            return components
        elif len(components) == 1:
            layer = components[0]
            # Top or bottom Layer? i.e. is it before or after the 1st signal layer
            first_signal_no = self.signal_layers()[0].index
            return [layer, None] if layer.index < first_signal_no else [None, layer]
        else:
            raise ValueError("Unknown length of component list: {}".format(components))

    def signal_layers(self):
        """Get all signal layers"""
        return self.by_type(LayerType.Signal)

    def top_components(self):
        """Get the top component layer, if any"""
        return self.component_layers()[0]

    def bottom_components(self):
        """Get the top component layer, if any"""
        return self.component_layers()[1]

    def __str__(self):
        return ("LayerSet([\n\t{}\n])".format(
            ",\n\t".join(
                map(str, self))))


_layer_type_map = {  # See ODB++ 7.0 spec page 38
    "COMPONENT": LayerType.Component,
    "SILK_SCREEN": LayerType.Annotation,
    "SOLDER_PASTE": LayerType.SolderPaste,
    "SOLDER_MASK": LayerType.SolderMask,
    "SIGNAL": LayerType.Signal,
    "DRILL": LayerType.Drill,
    "ROUT": LayerType.Route,
    "DOCUMENT": LayerType.Document,
    "MIXED": LayerType.Mixed,
    "MASK": LayerType.Mask,
    "POWER_GROUND": LayerType.Signal,
    "DIELECTRIC": LayerType.Prepreg
}

# TODO find a better place for all those enums
ODB_UNITS_KEY = "UNITS"
ODB_NUM_KEY = "NUM"
ODB_MM_UNIT = "MM"
ODB_FROM_OZ_TO_US = 34.79
ODB_FROM_INCH_TO_MM = 25.4

def parse_layers(global_stack, matrix, attrlists):
    layers_stack = LayerSet()
    for array in matrix.arrays:
        if array.name != "LAYER":
            continue
        layer_name = str(array.attributes["NAME"]).lower()

        if _layer_type_map.get(array.attributes["TYPE"]) == LayerType.Drill:
            layers_stack.append(Drill(
                name=layer_name,  # DipTrace seems to use lowercase for directories
                drill_type=_layer_type_map[array.attributes["TYPE"]],
                start=str(array.attributes.get("START_NAME", "")).lower() or None,
                end=str(array.attributes.get("END_NAME", "")).lower() or None,
                is_excellon=False
            ))
            continue

        units = attrlists[layer_name].metadata.get(ODB_UNITS_KEY, "inch")
        thickness = float(attrlists[layer_name].metadata.get(".copper_weight", 0.0))
        layer_dielectric = float(attrlists[layer_name].metadata.get(".layer_dielectric", 0.0))

        thickness = round(thickness * ODB_FROM_OZ_TO_US) if units == "inch" else round(thickness)
        layer_dielectric = round(1000.0 * layer_dielectric * ODB_FROM_INCH_TO_MM) if units == "inch" else round(1000.0 * layer_dielectric)

        if '.copper_weight' in attrlists[str(array.attributes["NAME"]).lower()].metadata.keys():
            layers_stack.append(Layer(
                name=layer_name,  # DipTrace seems to use lowercase for directories
                layer_type=_layer_type_map[array.attributes["TYPE"]],
                polarity=polarity_map[array.attributes["POLARITY"]],
                stackup_index=int(array.attributes["ROW"]),
                start=str(array.attributes.get("START_NAME", "")).lower() or None,
                end=str(array.attributes.get("END_NAME", "")).lower() or None,
                thickness=thickness,
                layer_dielectric=layer_dielectric,
                global_stack=global_stack
            ))
        else:
            layers_stack.append(Layer(
                name=str(array.attributes["NAME"]).lower(),  # DipTrace seems to use lowercase for directories
                layer_type=_layer_type_map[array.attributes["TYPE"]],
                polarity=polarity_map[array.attributes["POLARITY"]],
                stackup_index=int(array.attributes["ROW"]),
                start=str(array.attributes.get("START_NAME", "")).lower() or None,
                end=str(array.attributes.get("END_NAME", "")).lower() or None,
                thickness=0,
                layer_dielectric=0,
                global_stack=global_stack

            ))
    return layers_stack


def read_layers(global_stack, directory):
    matrix = read_structured_text(os.path.join(directory, "matrix/matrix"))
    layers_dir = os.path.join(directory, '\\'.join(["steps", str(matrix.arrays[0][1]["NAME"]), "layers"]))
    attrlists = {}
    for array in matrix.arrays:
        if str(array.name) != "LAYER":
            continue
        try:
            attrlists[str(array.attributes["NAME"]).lower()] = read_structured_text \
                (os.path.join(layers_dir, str(array.attributes["NAME"]).lower() + "\\attrlist"))
            keys = list(attrlists[str(array.attributes["NAME"]).lower()].metadata.keys())
            for key in keys:
                # odb sometimes come with spaces in keys of a layers dict , so i remove the spaces
                temp_layer = attrlists[str(array.attributes["NAME"]).lower()].metadata[key]
                del attrlists[str(array.attributes["NAME"]).lower()].metadata[key]
                attrlists[str(array.attributes["NAME"]).lower()].metadata[key.strip()] = temp_layer

        except IOError:
            attrlists[str(array.attributes["NAME"]).lower()] = StructuredText(
                {".copper_weight": 0, ".layer_dielectric": 0}, None)

    return parse_layers(global_stack, matrix, attrlists)


def read_layer_components(directory, layer):
    return read_linerecords(os.path.join(
        directory, "steps", "pcb", "layers", layer, "components.Z"))


def read_layer_features(directory, layer):
    return read_linerecords(os.path.join(
        directory, "steps", "pcb", "layers", layer, "features.Z"))


if __name__ == "__main__":
    # Parse commandline arguments
    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument("directory", help="The ODB++ directory")
    args = parser.parse_args()
    # Perform check
    print(read_layers(args.directory))
