from . import GerberReader
from UM.i18n import i18nCatalog
i18n_catalog = i18nCatalog("cura")

def getMetaData() :
    return {
        "gerber_reader": [
           {
                "extension": ['outline','g*','pho','phd','art','ncd',"drl","drd","tx*"],

                "description": "Gerber electrical board"
            }
        ]
    }

def register(app):
    return {'gerber_reader': GerberReader.GerberReader(app)}
