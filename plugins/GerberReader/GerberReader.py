import os
import re
import tempfile
from typing import Union, List

import UM.Application
from UM.FileHandler.FileReader import FileReader
from UM.Message import Message
from UM.MimeTypeDatabase import MimeTypeDatabase, MimeType
from UM.Scene.SceneNode import SceneNode
from cura.CuraApplication import CuraApplication
from cura.Utils.PCBHandler.PCBUtils.GerberModel import GerberModel
from cura.Utils.PCBHandler.PCBUtils.PcbUtils import makePlaceHolderMesh
from cura.Utils.PCBHandler.PcbModelDecorator import PcbModelDecorator
from cura.Utils.PCBHandler.PCBUtils.StackupToolValidator import StackupToolValidator

# ___ this copied from PCB-tools ___

'''
 _____________________________________debug only_____________________________________
# import matplotlib.pyplot as plt
# from matplotlib.path import Path
# from matplotlib.patches import PathPatch
# from descartes import PolygonPatch
# end of debug requierement

# def show_poly(self,poly):
#         if poly == None :
#             return
#         BLUE = '#6699cc'
#         GRAY = '#999999'
#         fig = plt.figure()
#         ax = fig.gca()
#         ax.add_patch(PolygonPatch(poly, fc=GRAY, ec=BLUE, alpha=0.5, zorder=2))
#         ax.axis('scaled')
#         plt.show()
 _____________________________________end of debug_____________________________________

'''


# todo : read defaults  from JSON


class GerberReader(FileReader):
    def __init__(self, application=None):
        super().__init__(application=application)
        self._application = application
        self._supported_extensions = ['.outline', '.g*', '.pho', '.phd', '.tx*', '.art', '.ncd', '.drl', ".drd"]
        MimeTypeDatabase.addMimeType(
            MimeType(
                name="gerber",
                comment="Gerber",
                suffixes=['outline', 'g*', "txt", 'pho', 'phd', 'art', 'ncd', 'drl', "drd"]
            )
        )
        self._global_container_stack = None

    def reset(self):
        pass

    def getGlobalContainerStack(self):
        return self._global_container_stack if self._global_container_stack is not None else self._application.getGlobalContainerStack()

    def setGlobalContainerStack(self, global_stack):
        self._global_container_stack = global_stack

    def read(self, files: str) -> Union[SceneNode, List[SceneNode]]:
        """Read mesh data from file and returns a node that contains the data
        Note that in some cases you can get an entire scene of nodes in this way (eg; 3MF)

        :return: node :type{SceneNode} or :type{list(SceneNode)} The SceneNode or SceneNodes read from file.
        """

        result = self._read(files)
        for file in files:
            UM.Application.Application.getInstance().getController().getScene().addWatchedFile(file)
        return result

    def getLoadingSettings(self):
        global_stack = self.getGlobalContainerStack()
        settings = {}

        settings['SignalLayerThickness'] = global_stack.getProperty(
            "signal_layer_thickness", "value")
        settings['SolderMaskThicknessTop'] = global_stack.getProperty(
            "soldermask_top_layer_thickness", "value")
        settings['SolderMaskThicknessBottom'] = global_stack.getProperty(
            "soldermask_bottom_layer_thickness", "value")
        settings['SolderAnnotationThicknessTop'] = global_stack.getProperty(
            "annotation_top_layer_thickness", "value")
        settings['SolderAnnotationThicknessBottom'] = global_stack.getProperty(
            "annotation_bottom_layer_thickness", "value")
        settings['PrepregThickness'] = global_stack.getProperty("prepreg_layer_thickness",
                                                                                "value")
        settings['minPTH'] = global_stack.getProperty("min_size_for_pth", "value")
        settings['scale'] = global_stack.getProperty("scale", "value")

        return settings

    def _read(self, files, no_setting_override: bool = False, temp_dir: tempfile.TemporaryDirectory = None):
        self.reset()
        global_stack = self.getGlobalContainerStack()

        if temp_dir is None:
            temp_dir = self._application.getTempDir()
        result_node = GerberModel(files, no_setting_override=no_setting_override, settings=self.getLoadingSettings(),
                                  global_stack=global_stack, temp_dir=temp_dir)
        result_node.readStackupFromFiles()
        # result_node.detectDrillFmt()
        result_node.addDielectricLayersToStackup()
        # try:
        result_node.updateNodeMeshData()
        # except:
        #     self.makePlaceHolderRoute(result_node)

        result_node.setName("PCB Model")
        result_node.addDecorator(PcbModelDecorator())  # mark the model as a pcb model
        result_node.checkStackupTypes()

        if result_node.getRoutesCount() and CuraApplication.getInstance() is not None:
            result_node.createPreview()

        # doing validation to errors
        if not result_node.hasDecoration("hasErrorInStackup"):
            result_node.addDecorator(StackupToolValidator())

        return result_node

    def acceptsFile(self, file_names):
        """Returns true if file_name can be processed by this plugin.
            This supports the * wildcard by using a regexp parser
        :return: boolean indication if this plugin accepts the file specified.
        """
        if isinstance(file_names, set) or isinstance(file_names, list):
            for file_name in file_names:
                if not self.checkExp(file_name):
                    return False
        else:
            return self.checkExp(file_names)
        return True

    def checkExp(self, file_names):
        ext = os.path.splitext(file_names.lower())[-1]
        for extension in self._supported_extensions:
            # replace wildcard in filename with equivalent regexp
            extension = extension.replace('*', '.*')
            if re.match(re.compile(extension), ext):
                return True
        return False

    @staticmethod
    def makePlaceHolderRoute(result_node):
        result_node.setMeshData(makePlaceHolderMesh())
        message = Message(str("Gerber Reader : there is no Route found.\n Please Load route in Route Manager Tool"),
                          title="File Missing")
        message.show()
