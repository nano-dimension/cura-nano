from shapely.geometry import Polygon,polygon

class GerberWriter:
    precision = (4,4)
    scale = 1.0

    def __init__(self):
        self.fileText = ''

    def write_gerber_header(self, f):
        self.emit_parameter(f, "G04", "Nano Dimension RCM Loader")
        self.emit_precision(f)
        self.emit_parameter(f, "MO", "MM")
        self.emit_parameter(f, "SR", "X1Y1I0J0")
        self.reset_gerber_state(f)


    def write_gerber_region(self,f,polygon):
        self.ensure_region(f,True)
        points= list(zip(*polygon.exterior.coords.xy))

        first_point =  (points[0])
        self.move_to(f,(first_point[0],first_point[1]))
        for (Ix,Iy) in (points):
            self.draw_to(f, (Ix, Iy))

        self.draw_to(f,(first_point[0],first_point[1]))

    def move_to(self,f,p):
        self.emit_command(f,self.emit_point(p)+"D02")

    def draw_to(self,f,p):
        self.emit_command(f,self.emit_point(p)+"D01")

    def emit_point(self, p):
        result = ''
        if self.X != p[0]:
            result += 'X%s' % GerberWriter.emit_coord(p[0])
            self.X = p[0]

        if self.Y != p[1]:
            result += 'Y%s' % GerberWriter.emit_coord(p[1])
            self.Y = p[1]

        return result

    def ensure_region(self,f,state=True):
        if self.region!=state:
            if state:
                self.emit_command(f,"G36");
                self.region=True
            else:
                self.emit_command(f,"G37");
                self.region=False

    def write_gerber_trailer(self,f):
        self.ensure_region(f,False)
        self.emit_command(f,"M02")

    @staticmethod
    def emit_command(f, symbol, value=""):
        if symbol == 'G04':
            f.write("G04 %s*" % (value.strip())+"\n")
        else:
            f.write( "%s%s*" % (symbol, value)+"\n")

    @staticmethod
    def emit_coord(d):
        s = d * GerberWriter.scale;
        result = ('%%%dd' % (GerberWriter.precision[0] + GerberWriter.precision[1])) % int(
            s * pow(10.0, GerberWriter.precision[1]));
        return result.strip();

    @staticmethod
    def emit_parameter(f, p, value):
         f.write( "%%%s%s*%%" % (p, value)+"\n")

    @staticmethod
    def emit_precision(f):
        GerberWriter.emit_parameter(f, "FS", "LAX%d%d" % (GerberWriter.precision[0], GerberWriter.precision[1]));

    def reset_gerber_state(self,f):
        self.emit_level(f,dark=True);
        self.region=False;
        self.X = -1.0;
        self.Y = -1.0;

    def emit_level(self, f, dark=True):
        if dark:
            self.emit_parameter(f, "LP", "D");
            self.level_dark = True;
        else:
            self.emit_parameter(f, "LP", "C");
            self.level_dark = False;

    def write_poly_gerber_file(self,fname,polygon:Polygon,holes):

        if polygon.is_empty :
            return "no points to draw in gerber"
        with open(fname,'w') as f:
            self.write_gerber_header(f)
            # self.write_gerber_apertures(f)
            if(polygon.type == "MultiPolygon"):
                for one_polygon in polygon:
                    self.write_gerber_region(f, one_polygon)
            else:
                self.write_gerber_region(f, polygon)
            if holes != None:
                self.emit_level(f, dark=False)
                self.write_gerber_region(f, polygon)

            self.write_gerber_trailer(f)
        f.close()
