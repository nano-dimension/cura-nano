import re
from enum import Enum

from UM.Logger import Logger
from UM.Mesh.MeshReader import \
    MeshReader  # This is the plug-in object we need to implement if we want to create meshes. Otherwise extend from FileReader.
from cura.Scene.CuraSceneNode import CuraSceneNode
from cura.Scene.UngroupableObjectDecorator import UngroupableObjectDecorator
from cura.Utils.MeshUtils import combineTwoTrimeshes, trimeshToMeshData
from shapely import affinity
from shapely.geometry import Polygon
from shapely.ops import unary_union
from trimesh.creation import extrude_polygon


class IDFReader(MeshReader):
    def __init__(self, app):
        super().__init__()
        self._supported_extensions = [".emn", ".emp"]
        self._application = app
        self.profile = list()
        self.scale = 1.0
        self.components_dict = list(dict())
        self.IDF_pairs = {"emn": "emp"}
        self.components_files_types = ['emp']
        self.settings = {"DrillsDiameterInMM": 3.2,
                         "DrillsNumber": 4,
                         "DrillsOffsetFromEdgeInMM": 1.51,
                         "HolesEnlargeInMM": 0.61,
                         "ExternalGapInMM": 0.072,
                         "MinimalComponentWidthInMM": 5.0,
                         "MinimalComponentLengthInMM": 5.0,
                         "MinimalWallInMM": 0.54,  # do not change any thing
                         "RegSquareness": RouteShape.profile,
                         "DefaultTopComponentThicknessInUM": 2500,
                         "DefaultTopRegistrationThicknessInUM": 500,
                         "DefaultBottomComponentThicknessInUM": 2700,
                         "DefaultBottomRegistrationThicknessInUM": 300}
        return

    def _read(self, file_name):
        # in case of second file read with this reader, the object need to be _reset
        self._reset()

        # load the IDF files by pairs
        if file_name.split('.')[-1] in (list(self.IDF_pairs.keys()) + list(self.IDF_pairs.values())):
            # TODO :  add open dialog if the pair do not exist
            # TODO :  add support to brd  and pro
            if not self.loadBOARDOUTLINE(file_name[:-3] + "emn"):
                Logger.log("w", "IDF_LOADER :  cant find any BOARD_OUTLINE")
                return
            if not self.loadPLACEMENT(file_name[:-3] + "emn"):
                Logger.log("w", "IDF_LOADER :  cant find any ELECTRICAL")
                return
            if not self.loadELECTRICAL(file_name[:-3] + "emp"):
                Logger.log("w", "IDF_LOADER :  cant find any ELECTRICAL")
                return
        Logger.log("w", "IDF_LOADER :  IDF has loaded")

        # create meshes for the the loaded RCM in self
        try:
            top_rcm_mesh, bottom_rcm_mesh = self.makeMeshes()
            Logger.log("w", "IDF_LOADER :  meshes created")

        except:
            Logger.log("w", "IDF_LOADER :  creating meshes failed")
            return
        # Put the mesh inside a scene node.
        result_node1 = CuraSceneNode()
        # convert mesh, trimesh to uranium - meshdata
        result_node1.setMeshData(mesh_data=trimeshToMeshData(top_rcm_mesh))
        result_node1.setName("Top RCM")
        result_node1.addDecorator(UngroupableObjectDecorator())  # make the node object and not a group

        result_node2 = CuraSceneNode()
        # convert mesh, trimesh to uranium - meshdata
        result_node2.setMeshData(mesh_data=trimeshToMeshData(bottom_rcm_mesh))
        result_node2.setName("Bottom RCM")
        result_node2.addDecorator(UngroupableObjectDecorator())  # make the node object and not a group

        return [result_node1, result_node2]

    def _reset(self):
        self.profile = list()
        self.components_dict = list(dict())
        self.components_polys = list()

    def getLoadingSettings(self):
        global_container_stack = self._application._global_container_stack
        settings = {'DrillsDiameterInMM': global_container_stack.getProperty("drills_diameter_in_mm", "value"),
                    'DrillsNumber': global_container_stack.getProperty("drills_number", "value"),
                    'DrillsOffsetFromEdgeInMM': global_container_stack.getProperty("drills_offset_from_edge_in_mm", "value"),
                    'HolesEnlargeInMM': global_container_stack.getProperty("holes_enlarge_in_mm", "value"),
                    'ExternalGapInMM': global_container_stack.getProperty("external_gap_in_mm", "value"),
                    'MinimalComponentWidthInMM': global_container_stack.getProperty("minimal_component_width_in_mm", "value"),
                    'MinimalComponentLengthInMM': global_container_stack.getProperty("minimal_component_length_in_mm", "value"),
                    'MinimalWallInMM': global_container_stack.getProperty("minimal_wall_in_mm", "value"),
                    'RegSquareness': global_container_stack.getProperty("reg_squareness", "value"),
                    'DefaultTopComponentThicknessInUM': global_container_stack.getProperty("default_top_component_thickness_in_um", "value"),
                    'DefaultTopRegistrationThicknessInUM': global_container_stack.getProperty("default_top_registration_thickness_in_um", "value"),
                    'DefaultBottomComponentThicknessInUM': global_container_stack.getProperty("default_bottom_component_thickness_in_um", "value"),
                    'DefaultBottomRegistrationThicknessInUM': global_container_stack.getProperty("default_bottom_registration_thickness_in_um", "value")}

        return settings

    def loadBOARDOUTLINE(self, path):
        # parse the file hold the outline and the placment of all the components
        # this function loads only the outline , as polygon, to self. profile
        try:
            with open(path, 'r') as src_file:
                lines = src_file.readlines()
            lines = [re.sub(' {2,}', ' ', line) for line in lines]
            if lines[2].split(' ')[-1] == "MM\n":
                self.scale = 1
            elif lines[2].split(' ')[-1] == "THOU\n":
                self.scale = 0.025
            outline_position = [lines.index(i) for i in lines if ("BOARD_OUTLINE" or "PANEL_OUTLINE") in i]
            outline = [line.split(" ") for line in lines[outline_position[0] + 2:outline_position[1]]]
            temp_poly = []
            for point in outline:
                temp_poly.append((float(point[1]) * self.scale, float(point[2]) * self.scale))
            self.profile = Polygon(temp_poly)
        except:
            return False
        return True

    def loadPLACEMENT(self, path):
        # parse the file hold the outline and the placement of all the components
        # this function loads all the components to dict,
        # that holds the name,place ,rotation, and if this component is actually placed.
        # later on the place will be replaced with the polygon it self that contain the component
        try:
            with open(path) as src_file:
                lines = src_file.readlines()
            lines = [re.sub(' {2,}', ' ', line) for line in lines]
            components_area = [lines.index(i) for i in lines if ("PLACEMENT" or "END_PLACEMENT") in i]
            components_position = [line.split(" ") for line in lines[components_area[0] + 1:components_area[1]]]
            # components_position = components_position[components_position != '']
            for i in range(int(len(components_position) / 2)):
                if components_position[i * 2 + 1][0] == '':
                    components_position[i * 2 + 1] = components_position[i * 2 + 1][1:]
                self.components_dict.append({components_position[i * 2][0] + " " + components_position[i * 2][1]:
                                                 [{"poly": [float(component) * self.scale for component in
                                                            components_position[i * 2 + 1][:2]]},
                                                  {"rotation": float(components_position[i * 2 + 1][3])},
                                                  {"side": components_position[i * 2 + 1][4]},
                                                  {"PLACE": components_position[i * 2 + 1][5] == 'PLACED\n'}]})
        except:
            return False
        return True

    def loadELECTRICAL(self, path):
        # parse the file hold the strucurr of all components used in the board.
        # this function loads all the components to list of dicts,
        # that holds the name and structure
        # later on the strucure will be rotated and inserted to the list of the components on the board itself.
        try:
            with open(path) as src_file:
                lines = src_file.readlines()
            lines = [re.sub(' {2,}', ' ', line) for line in lines]
            starts = []
            ends = []
            self.components_polys = []
            for i in range(len(lines)):
                if ".ELECTRICAL\n" == lines[i]:
                    starts.append(i)
                if ".END_ELECTRICAL\n" == lines[i]:
                    ends.append(i)

            for (i, j) in zip(starts, ends):
                current_lines = lines[i + 2:j]
                points = []
                for point in current_lines:
                    points.append((float(point.split(" ")[1]) * self.scale, float(point.split(" ")[2]) * self.scale))
                polygon = Polygon(points)

                self.components_polys.append({lines[i + 1].split(" ")[0] + ' ' + lines[i + 1].split(" ")[1]: polygon})
            self.applyComponents()
        except:
            return False
        return True

    def applyComponents(self, placement_sensitive=True):
        # after parsing IDF, this function takes each component on the board,place the real size of the component and rotate it.
        for component in self.components_dict:

            name = str(list(component.keys())[0])
            index = self.findPolygonInComponentsPolygons(name)
            poly = list(self.components_polys[index].values())[0]

            x1 = poly.bounds[0]
            y1 = poly.bounds[1]
            x2 = poly.bounds[2]
            y2 = poly.bounds[3]

            if abs(x1 - x2) < self.settings["MinimalComponentWidthInMM"]:
                poly = affinity.scale(poly, xfact=self.settings["MinimalComponentWidthInMM"] / abs(x1 - x2))
            if abs(y1 - y2) < self.settings["MinimalComponentLengthInMM"]:
                poly = affinity.scale(poly, yfact=self.settings["MinimalComponentLengthInMM"] / abs(y1 - y2))
            poly = poly.buffer(self.settings["HolesEnlargeInMM"])
            alpha = list(list(component.values())[0][1].values())[0]
            # poly = self.rotate_poly(poly,alpha)
            poly = affinity.rotate(poly, alpha, (0, 0))

            placement = list(list(component.values())[0][0].values())[0]
            # poly = self.move_poly(poly,(placement[0],placement[1]))
            poly = affinity.translate(poly, placement[0], placement[1])
            component[name][0]['poly'] = poly
        return

    def findPolygonInComponentsPolygons(self, name):
        for index in range(len(self.components_polys)):
            if name in self.components_polys[index].keys():
                return index
        return False

    def getComponentsPolygon(self, placement_sensitive=True):
        #   self.components_dict  is a list of dicts,
        #   contain all the components in the board.
        #   the stature is :
        #
        # [
        #       {
        #       'name' : {"polygon":[(point)],
        #               "rotation": float ,
        #               "side": str,
        #               "PLACE" : str}
        #       }
        #       ...
        # ]
        #
        # the loop append all top components to one temp list and the bottom to another
        # after creating only polygons list, createing a shapley polygon from it.
        # placement_sensitive - is a bool, for ignoring not placed components.

        poly_list_top = []
        poly_list_bot = []
        for component in self.components_dict:
            if component[str(list(component.keys())[0])][3]["PLACE"]:
                if component[str(list(component.keys())[0])][2]["side"] == "TOP":
                    if component[str(list(component.keys())[0])][0]['poly'].within(self.profile):
                        poly_list_top.append(component[str(list(component.keys())[0])][0]['poly'])
                elif component[str(list(component.keys())[0])][2]["side"] == "BOTTOM":
                    if component[str(list(component.keys())[0])][0]['poly'].within(self.profile):
                        poly_list_bot.append(component[str(list(component.keys())[0])][0]['poly'])
        return unary_union(poly_list_top), unary_union(poly_list_bot)

    def convertPcbPolygonToRCMPolygon(self, route_poly, top_poly=None, bottom_poly=None):

        new = route_poly.buffer(self.settings['DrillsDiameterInMM'] + 2 * self.settings['DrillsOffsetFromEdgeInMM'])
        route = new.difference(route_poly.buffer(self.settings["ExternalGapInMM"]))
        if self.settings['RegSquareness'] == RouteShape.bounding_polygon:
            route = route.convex_hull
        elif self.settings['RegSquareness'] == RouteShape.bounding_box:
            route = route.envelope
        top_poly = unary_union(affinity.scale(new.difference(top_poly), 1, -1))
        if top_poly.type == "MultiPolygon":
            max_poly = Polygon()
            for one_polygon in top_poly:
                if one_polygon.area > max_poly.area:
                    max_poly = one_polygon
            top_poly = max_poly
        top_route = affinity.scale(route, 1, -1)
        bottom_poly = new.difference(bottom_poly)
        return route, bottom_poly, top_route, top_poly

    def makeMeshes(self):
        # using this class 3 steps need be followed:
        # 1) create IDFReader object
        # 2) load IDF with .load_IDF(path)
        # 3) run(path1,path2) - gets two meshes,top/bottom of (route+components) .
        top, bottom = self.getComponentsPolygon()
        bottom_route, bottom_components, top_route, top_components = self.convertPcbPolygonToRCMPolygon(self.profile, top,
                                                                                                        bottom)

        bottom_route = extrude_polygon(bottom_route, self.settings["DefaultBottomRegistrationThicknessInUM"] / 1000)
        bottom_components = extrude_polygon(bottom_components,
                                            self.settings["DefaultBottomComponentThicknessInUM"] / 1000)
        top_route = extrude_polygon(top_route, self.settings["DefaultTopRegistrationThicknessInUM"] / 1000)
        top_components = extrude_polygon(top_components, self.settings["DefaultTopComponentThicknessInUM"] / 1000)
        top_rcm_mesh = combineTwoTrimeshes(top_components, top_route, auto_z=True)
        bottom_rcm_mesh = combineTwoTrimeshes(bottom_components, bottom_route, auto_z=True)
        return top_rcm_mesh, bottom_rcm_mesh


class RouteShape(Enum):
    profile = 1
    bounding_polygon = 2
    bounding_box = 3
