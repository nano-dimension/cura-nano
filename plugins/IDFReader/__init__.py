from . import IDFReader
from UM.Logger import Logger
from UM.i18n import i18nCatalog
i18n_catalog = i18nCatalog("cura")


# def getMetaData() :
#     return {
#         "mesh_reader": [
#             {
#                 "extension": "emn",
#                 "description": "IDF electrical board"
#             },{
#                 "extension": "emp",
#                 "description": "IDF electrical components"
#             }
#         ]
#     }
#

# def register(app):
#     return {'mesh_reader': IDFReader.IDFReader(app)}