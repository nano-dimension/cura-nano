# Copyright (c) 2018 Ultimaker B.V.
# Cura is released under the terms of the LGPLv3 or higher.

from . import Materials
from PyQt5.QtQml import qmlRegisterType
from UM.i18n import i18nCatalog

i18n_catalog = i18nCatalog("cura")


def getMetaData():
    return {
        "tool": {
            "id": "materials",
            "name": i18n_catalog.i18nc("@label", "Materials"),
            "description": i18n_catalog.i18nc("@info:tooltip", "Chosse material of Model"),
            "icon": "tool_icon.svg",
            "tool_panel": "Materials.qml",
            "weight": 7
        }
    }


def register(app):
    materials = Materials.Materials()
    return {"tool": materials}
