# Copyright (c) 2018 Ultimaker B.V.
# Cura is released under the terms of the LGPLv3 or higher.

from typing import List
import re
import traceback

from PyQt5.QtCore import QUrl

from UM.Event import Event
from UM.Scene.Selection import Selection
from UM.Signal import Signal
from UM.Logger import Logger
from UM.i18n import i18nCatalog

from cura.AmeTool import AmeTool
from cura.CuraApplication import CuraApplication

# for settings access
from cura.Operations.SetParentOperation import SetParentOperation
from cura.Settings.SetObjectExtruderOperation import SetObjectExtruderOperation

from .MaterialsModel import MaterialsModel

catalog = i18nCatalog("cura")


class Materials(AmeTool):

    Result = Signal()

    def __init__(self):
        """
        Start the tool and exposes properties we use for updates
        """

        super().__init__()

        self.setExposedProperties("ListofNodes")

    def isPCBTool(self):
        # This is a mesh-tool, not a PCB tool
        return False

    def _updateEnabled(self):

        super()._updateEnabled()

        self._updateNodes()

    def _updateNodes(self):
        # Initialize nodes - Assume there is only one non-pcb node selected, since otherwise the tool is disabled
        node = Selection.getSelectedObject(0)

        if not node or node.callDecoration('isPcbModel'):
            self.nodes = None
        elif node.callDecoration('isGroup'):
            self.nodes = node.getChildren()
        else:
            self.nodes = [node]

    #  Called when something happens in the scene while our tool is active.
    #  For instance, we can react to mouse clicks, mouse movements, and so on.
    def event(self, event):
        super().event(event)
        if event.type == Event.MousePressEvent and self._controller.getToolsEnabled():
            self.operationStopped.emit(self)
        return False  # we return false because we don't manipulate the scene

    @staticmethod
    def getSelectedActiveExtruder():
        """
        Gets the active extruder of the currently selected object.
        NB: We assume a single object is selected, since otherwise the tool is disabled

        :return: The active extruder of the currently selected object.
        """
        return Selection.getSelectedObject(0).callDecoration("getActiveExtruder")

    def getListofNodes(self) -> List[str]:
        """
        Return all the children nodes of the selected node
        :return:
        """
        try:
            materials_model = MaterialsModel()
            for child in self.nodes:
                material = re.findall(r"'(.*?)'", str(child.getPrintingExtruder()))[0]
                material_id = int(material[-4])-1
                materials_model.appendItem({'name': child.getName(), 'material': material_id})
            return materials_model

        except Exception:
            traceback.print_exc()
            Logger.error("failed to get children")

    def setExtruderFromQml(self, qml_input):
        file_index, extruder_index = (int(inp) for inp in qml_input.split("|"))

        # Assume there is only one non-pcb node selected, since otherwise the tool is disabled
        node = self.nodes[file_index]

        extruder_id = CuraApplication.getInstance().getGlobalContainerStack().extruderList[extruder_index].id

        # TODO: fix and enable undo/redo on material tool like stackup tool, and
        # instead of redo() call push() to push it to the correct (per-node) operation stack
        SetObjectExtruderOperation(node, extruder_id).redo()

    def addModels(self, fileUrls: List[QUrl]):
        """
        Add files at the urls to the selected group
        
        :param fileUrls:
        :return:
        """
        try:
            CuraApplication.getInstance().readLocalFilesAndAddToSelectedGroup(fileUrls,
                                                                              on_finished_callback=self._propertyChanged)
        except Exception as e:
            traceback.print_exc()
            Logger.error("failed to add models: {}".format(e.args[0]))

    def _propertyChanged(self):
        self.propertyChanged.emit()

    def removeModelByIndex(self, node_id: int):
        """
        Remove model at the given index from the group
        :param node_id: the index to delete
        :return:
        """
        if node_id > len(self.nodes):
            Logger.error("Trying to delete node out of scope!")
            return

        # TODO: fix and enable undo/redo on material tool like stackup tool, and
        # instead of redo() call push() to push it to the correct (per-node) operation stack
        SetParentOperation(node=self.nodes[node_id], parent_node=None, callback=self.propertyChanged).redo()

    def undo(self) -> None:
        # TODO: fix and enable undo/redo on material tool like stackup tool, and
        # instead of redo() call push() to push it to the correct (per-node) operation stack
        pass

    def redo(self) -> None:
        # TODO: fix and enable undo/redo on material tool like stackup tool, and
        # instead of redo() call push() to push it to the correct (per-node) operation stack
        pass
