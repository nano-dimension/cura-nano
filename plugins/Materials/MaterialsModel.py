from PyQt5.QtCore import Qt
from UM.Qt.ListModel import ListModel


class MaterialsModel(ListModel):
    def __init__(self,parent = None):
        super().__init__(parent)

        self.addRoleName(Qt.UserRole + 1, "name")
        self.addRoleName(Qt.UserRole + 2, "material")

        self._update()

    def _update(self):
        self.setItems([])
        return

