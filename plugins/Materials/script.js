function jsFunction() {
    console.log("Called JavaScript function!")
}
function muti_char(c, n){
    var s = "";
    
    while(s.length<n)
        s+=c;
    return s;
}

function print_object(name, obj, depth, prefix){
    if (!depth)
        depth = 0;
    if (!prefix)
        prefix = "";
    var max = 0;
    if (prefix=="") {
        var title = "------------ yuval zilber represents ------------";
        var title_len = title.length;
        var name_type = name + ": " + obj.toString();
        var obj_len = name_type.length;
        if (title_len % 2 != obj_len % 2) {
            title = "-" + title;
            title_len++;
        }

        var additional = muti_char("-", obj_len - title_len + 4);
        title = additional + title + additional;
        title_len = title.length;
        var obj_dashes = muti_char("-", (title_len - obj_len - 2) / 2);
        var border = muti_char("-", title_len);

        console.log(border);
        console.log(title);
        console.log(obj_dashes + " " + name_type + " " + obj_dashes);
        console.log(border);
    }
    for (var i in obj) {
        max = Math.max(max, i.length);
    }

    for (var i in obj){
        var spaces = muti_char(" ", max - i.toString().length);
        if (depth===0)
            print_value(prefix+name+"[\""+i+"\"]"+spaces, obj[i]);
        else
        {
            var obs = obj[i].toString();
            var str = prefix+name+"[\""+i+"\"] = "+obs;
            var is_parent = i.replace("_","").replace("_","").startsWith("parent");
            if (is_parent){
                console.log(str+" ... parent ...");
            }
            else if (obs.startsWith("Q") && obs.endsWith(")")){
                str+=" {";
                console.log(str);
                print_object(name+"[\""+i+"\"]", obj[i],depth-1, prefix+"    " )
                console.log(prefix+"}");
            }
            else{
                console.log(str);
            }
            
        }
    }
    
    if (prefix==="") {
        console.log(border);
    }
    return obj;
}
function print_value(name, value){
    console.log(name,"=", value);
    return value;
}
function stringify(val, depth, replacer, space) {
    depth = isNaN(+depth) ? 1 : depth;
    function _build(key, val, depth, o, a) { // (JSON.stringify() has it's own rules, which we respect here by using it for property iteration)
        return !val || typeof val != 'object' ? val : (a=Array.isArray(val), JSON.stringify(val, function(k,v){ if (a || depth > 0) { if (replacer) v=replacer(k,v); if (!k) return (a=Array.isArray(v),val=v); !o && (o=a?[]:{}); o[k] = _build(k, v, a?depth:depth-1); } }), o||(a?[]:{}));
    }
    return JSON.stringify(_build('', val, depth), null, space);
}