import QtQuick.Window 2.2
import QtQuick 2.8
import QtQml.Models 2.1
import QtQuick.Dialogs 1.2

import QtQuick.Controls 1.4


import UM 1.3 as UM
import Cura 1.0 as Cura
import ".."

Item {
    id: base

    width: UM.Theme.getSize("routes_width").width
    height: Math.max(Math.min(viewl.count, 5) * UM.Theme.getSize("layer_row").height + UM.Theme.getSize("routes_title_height").height - UM.Theme.getSize("12px").height, sidebar_buttons.height)
    visible: true
    property var sceneNodes: UM.ActiveTool.properties.getValue("ListofNodes")
    property var extruders: CuraApplication.getExtrudersModel()
    property var updated_once: false;
    ListModel {
        id: nodesModel

        Component.onCompleted: update()
        function update() {
            if (sceneNodes != undefined) {
                for (var i = 0; i < sceneNodes.count; i++) {
                    append(sceneNodes.getItem(i))
                }
                if (viewl.current == undefined) {
                    viewl.current = 0
                }
            }
        }
    }

    ListModel {
        id: extrudersModel
        Component.onCompleted: update()
        function update() {
            if (extruders != undefined) {
                for (var i = 0; i < extruders.count; i++) {
                    append(extruders.getItem(i));
                }
            }
        }
    }

    Rectangle {
        id: root
        visible: true
        width: UM.Theme.getSize("routes_width").width;

        Component {
            id: tr_template
            MouseArea {
                id: tr_template_body

                property bool selected: false
                
                acceptedButtons: Qt.LeftButton | Qt.RightButton
                hoverEnabled: true
                anchors { left: parent.left; right: parent.right }
                height: content.height-2
                width: content.width
                onClicked: {
                    if (mouse.button === Qt.RightButton){
                        menu.popup()
                    }
                }
                onPressed: {
                    viewl.current = index;
                    viewl.currentIndex = index;  // This updates viewl.currentItem so that it can be used
                                                 //      by highlight
                }
                onEntered: {
                    viewl.currentCB = index;
                }
                Menu {
                    id: menu
                }
                Rectangle {
                    id: content
                    border.width: UM.Theme.getSize("default_lining").width
                    border.color: UM.Theme.getColor("lining")
                    
                    width: tr_template_body.width;
                    height: UM.Theme.getSize("30px").height;
                    //color: "red" // UM.Theme.getColor("background_modal_row")
                    
                    color: {
                        if(extrudersModel.get(material).name.toLowerCase() === "dielectric"){
                            return UM.Theme.getColor("dielectric_color");
                        }
                        else{
                            return UM.Theme.getColor("conductive_color");
                        }
                    }
                    radius: 1

                    Row {
                        // padding for the left
                        Column {
                            width: UM.Theme.getSize("routes_filename_padding_left").width;
                            height: UM.Theme.getSize("24px").height;
                        }
                        anchors.verticalCenter: parent.verticalCenter
                        Column {
                            // filename is down here
                            Row {
                                Text {
                                    id: filename_lbl
                                    color: UM.Theme.getColor("text_modal")
                                    width: UM.Theme.getSize("routes_filename_width").width
                                    height: UM.Theme.getSize("24px").height
                                    text: name
                                    elide: Text.ElideMiddle
                                    verticalAlignment: Text.AlignVCenter
                                }   
                            }
                        }
                        
                        Column {
                            Row {
                                Cura.ND_tools_ComboBox {
                                    id: comboBox12
                                    currentIndex: material
                                    width: UM.Theme.getSize("routes_material_width").width; 
                                    height: UM.Theme.getSize("24px").height;
                                    textRole: "name"
                                    model: extrudersModel
                                    anchors.verticalCenter: parent.verticalCenter

                                    property int hope: 0
                                    property var extruder: CuraApplication.getExtrudersModel()
                                    popup.onOpened: {
                                        hope = viewl.currentCB
                                    }

                                    onActivated: {
                                        var tochange = ""+hope+"|"+currentIndex+""
                                        UM.ActiveTool.triggerActionWithData("setExtruderFromQml" , tochange)
                                        nodesModel.setProperty(hope, "material", currentIndex)
                                    }
                                    
                                    indicator: UM.RecolorImage {
                                        id: downArrow
                                        x: comboBox12.width - width - comboBox12.rightPadding
                                        y: comboBox12.topPadding + Math.round((comboBox12.availableHeight - height) / 2)

                                        source: UM.Theme.getIcon("arrow_bottom")
                                        width: UM.Theme.getSize("standard_arrow").width * screenScaleFactor
                                        height: UM.Theme.getSize("standard_arrow").height * screenScaleFactor
                                        sourceSize.width: width + 5
                                        sourceSize.height: width + 5
                                        visible: enabled
                                        color: UM.Theme.getColor("setting_control_button")
                                    }
                                    function guess(){
                                         
                                        //console.log(filename_lbl.text,"==?",name);
                                        if (nodesModel.count!=2)
                                            return;

                                        var my_index = -1;
                                        if (name === nodesModel.get(0).name)
                                            my_index = 0

                                        if (name === nodesModel.get(1).name)
                                            my_index = 1

                                        if (my_index == -1)
                                            return
                                        
                                        var ot_name = nodesModel.get(1 - my_index).name;

                                        if (name.length !== ot_name.length)
                                            return

                                        var count_changes = 0;
                                        var index_of_changed = -1;

                                        for (var i = 0; i < name.length; i++) {
                                            if (name[i] != ot_name[i])
                                            {
                                                count_changes++;
                                                index_of_changed = i;
                                            }
                                        }
                                        if (count_changes != 1)
                                            return

                                        var mat_s = name.substr(index_of_changed, 2);
                                        var mat_o = ot_name.substr(index_of_changed, 2);
                                        //console.log("mat_s =", mat_s)
                                        if ((mat_s === "di" && mat_o === "ci") || (mat_s === "DI" && mat_o === "CI")){
                                            hope = my_index
                                            currentIndex = 1
                                            var tochange = ""+hope+"|"+currentIndex+""
                                            //console.log("material["+hope+"] =",name, "-> DI")
                                            UM.ActiveTool.triggerActionWithData("reColor" , tochange)
                                            nodesModel.setProperty(hope, "material", currentIndex)
                                        }
                                    }
                                    Component.onCompleted:{
                                       //guess();
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        DelegateModel {
            id: visualModel
            model: nodesModel
            delegate: tr_template
        }
        Row {
            id: sidebar
            width: UM.Theme.getSize("routes_width").width
            height: parent.height
            
            // The +/- sidebar
            Column {
                id: sidebar_buttons
                width: UM.Theme.getSize("modal_sidebar").width
                // +
                Cura.ND_tools_Button {
                        icon.source: UM.Theme.getIcon("plus-svgrepo-com");
                        height: UM.Theme.getSize("modal_sidebar_button").height
                        width: UM.Theme.getSize("modal_sidebar_button").width
                        icon.height: UM.Theme.getSize("modal_sidebar_button").width/2
                        icon.width: UM.Theme.getSize("modal_sidebar_button").width/2
                        onClicked: { open_stl_dialog.open()}
                }
                // -
                Cura.ND_tools_Button {
                    icon.source: UM.Theme.getIcon("minus-svgrepo-com");
                    height: UM.Theme.getSize("modal_sidebar_button").height
                    width: UM.Theme.getSize("modal_sidebar_button").width
                    icon.height: UM.Theme.getSize("modal_sidebar_button").width/2
                    icon.width: UM.Theme.getSize("modal_sidebar_button").width/2
                    enabled: visualModel.items.count>1
                    onClicked: {
                        var index_del = viewl.current;
                        var rows = visualModel.model.count;
                        if (rows===0 || index_del<0 || index_del >=rows){
                            return;
                        }
                        UM.ActiveTool.triggerActionWithData("removeModelByIndex" , index_del)
                        viewl.current = Math.max(0, Math.min(index_del-1, rows-1))
                        viewl.currentIndex = viewl.current
                    }
                }
            }
            // List of selected Settings to override for the selected object
            Column {
                height: parent.height
                width: UM.Theme.getSize("routes_title_width").width
                //spacing: UM.Theme.getSize("default_margin").height
                x: UM.Theme.getSize("modal_sidebar").width
                ScrollView {
                    height: Math.min(viewl.count, 5) * UM.Theme.getSize("layer_row").height + UM.Theme.getSize("routes_title_height").height + 5
                    width: UM.Theme.getSize("routes_title_width").width;
                    style: UM.Theme.styles.scrollview
                    visible: true

                    Component {
                        id: highlight
                        Rectangle { // yz: selection shadow
                            width: (visualModel.items.count > 0 ? UM.Theme.getSize("400px").width : 0)
                            height: UM.Theme.getSize("layer_row").height
                            color: "white";
                            radius: 5
                            z: 5
                            opacity: 0.25
                            y: viewl.currentItem.y
                            Behavior on y {
                                SpringAnimation {
                                    spring: 2
                                    damping: 0.1
                                }
                            }
                        }
                    }

                    function returnToBounds() {}
                    ListView {
                        id: viewl
                        property int currentCB: 0
                        property int current: 0
                        anchors { fill: parent; margins: 0}
                        boundsBehavior: Flickable.StopAtBounds
                        headerPositioning: ListView.OverlayHeader
                        spacing: UM.Theme.getSize("default_lining").height
                        model: visualModel
                        cacheBuffer: 50
                        highlight: highlight
                        header: Rectangle {
                            id: headerItem
                            width: UM.Theme.getSize("routes_title_width").width;
                            height: UM.Theme.getSize("routes_title_height").height
                            radius: 2
                            color: UM.Theme.getColor("main_window_header_background")

                            Row {
                                anchors.verticalCenter: parent.verticalCenter

                                Column {
                                    width: UM.Theme.getSize("routes_filename_padding_left").width;
                                    height: UM.Theme.getSize("routes_title_height").height;
                                }
                                Column {
                                    anchors.verticalCenter: parent.verticalCenter
                                    Text {
                                        text: "File Name";
                                        width: UM.Theme.getSize("routes_title_filename_width").width
                                        color: UM.Theme.getColor("text_modal")
                                        font: UM.Theme.getFont("medium_bold")
                                        verticalAlignment: Text.AlignVCenter

                                    }
                                }
                                Column {
                                    anchors.verticalCenter: parent.verticalCenter
                                    Text {
                                        text: "Material"
                                        width: UM.Theme.getSize("routes_title_material_width").width;
                                        color: UM.Theme.getColor("text_modal")
                                        font: UM.Theme.getFont("medium_bold")
                                        verticalAlignment: Text.AlignVCenter
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }


     FileDialog {
         id: open_stl_dialog
         modality: Qt.WindowModal
         nameFilters: ["STL files (*.stl)"]
         selectMultiple: true
         folder:
         {
             //Because several implementations of the file dialog only update the folder when it is explicitly set.
             folder = CuraApplication.getDefaultPath("dialog_load_path");
             return CuraApplication.getDefaultPath("dialog_load_path");
         }
         onAccepted:
         {
             var f = folder;
             folder = f;
             CuraApplication.setDefaultPath("dialog_load_path", folder);
             UM.ActiveTool.triggerActionWithData("addModels" , fileUrls)
         }
    }

    Connections {
        target: UM.ActiveTool
        onPropertiesChanged: {
            nodesModel.clear()
            nodesModel.update()

            extrudersModel.clear()
            extrudersModel.update()
        }
    }

    Action
    {
        id: undoAction;
        text: catalog.i18nc("@action:inmenu menubar:edit", "&Undo");
        shortcut: StandardKey.Undo;
        onTriggered: UM.ActiveTool.triggerAction("undo");
        enabled: true;
    }

    Action
    {
        id: redoAction;
        text: catalog.i18nc("@action:inmenu menubar:edit", "&Redo");
        shortcut: StandardKey.Redo;
        onTriggered: UM.ActiveTool.triggerAction("redo");
        enabled: true;
    }

}
