//Copyright (c) 2017 Ultimaker B.V.
//This example is released under the terms of the AGPLv3 or higher.
//This allows you to use all of Uranium's built-in QML items.
import QtQuick 2.10 //This allows you to use QtQuicks built-in QML items.
import QtQuick.Controls 2.3 //Contains the "Label" element.
import UM 1.3 as UM
import Cura 1.0 as Cura
UM.Dialog //Creates a modal window that pops up above the interface.
{
    id: base
    title : "Excellon Settings Edit"
    width: 150
    height: 180
    minimumWidth: 300
    property var notation: UM.ActiveTool.properties.getValue("Notation")
    property var units: UM.ActiveTool.properties.getValue("Units")
    property var zero_suppression: UM.ActiveTool.properties.getValue("ZeroSuppression")
    property var format: UM.ActiveTool.properties.getValue("Format")
    property var formats: UM.ActiveTool.properties.getValue("Formats")

    minimumHeight: 70
    onAccepted:  UM.ActiveTool.triggerActionWithData("changeDrillSetting",""+notationcomboBox.currentText+"|"+
        unitscomboBox.currentText+"|"+
        zero_suppression_comboBox.currentText+"|"+
        formatcomboBox.currentText+"")

    Label{
        id: notationLabel
        width: 100;
        height: 20;
        //Positions the label on the top-left side of the window.
        anchors.top: base.top
        anchors.topMargin: 13
        anchors.left: base.left
        // anchors.leftMargin: 10
        text: "Notation"
    }
    ComboBox{
                id: notationcomboBox
                width: 80;
                height: 28 ;
                anchors.top: base.top
                anchors.right: parent.right
                model:["absolute", "incremental"]
                currentIndex: notation
                indicator: Canvas {
                                        }
    }


    Label{
        id: unitsLabel
        width: 100;
        height: 20;
        //Positions the label on the top-left side of the window.
        anchors.top: notationLabel.bottom
        anchors.topMargin: 13
        anchors.left: base.left
//        rightPadding:100
        // anchors.rightMargin: 40
        text: "Units:"
    }
    ComboBox{
                id: unitscomboBox
                width: 80;
                height: 28 ;
                anchors.topMargin: 5
                anchors.top: notationcomboBox.bottom
                anchors.right: parent.right
                model:["metric", "inch"]
                currentIndex: units
                indicator: Canvas {
                                        }
    }





    Label{
        id: zerosLabel
        width: 100;
        height: 20;
        //Positions the label on the top-left side of the window.
        anchors.top: unitsLabel.bottom
        anchors.topMargin: 13
        anchors.left: base.left

        text: "Zero Supression:"
    }

    ComboBox{
                id: zero_suppression_comboBox
                width: 80;
                height: 28 ;
                anchors.topMargin: 5
                anchors.top: unitscomboBox.bottom
                anchors.right: parent.right
                model:["leading", "trailing"]
                currentIndex:zero_suppression
                indicator: Canvas {}
    }





    Label{
        id: formatLabel
        width: 100;
        height: 20;
        //Positions the label on the top-left side of the window.
        anchors.top: zerosLabel.bottom
        anchors.topMargin: 13
        anchors.left: base.left
//        rightPadding:100
        // anchors.rightMargin: 40
        text: "Format:"
    }
    ComboBox{
                id: formatcomboBox
                width: 80;
                height: 28 ;
                anchors.topMargin: 5
                anchors.top: zero_suppression_comboBox.bottom
                anchors.right: parent.right
                model:formats
                currentIndex: format
                indicator: Canvas {
                                        }
    }


    Button{
        width: 80
        height: 20
        text:"Apply"
        anchors.bottom: parent.bottom
        anchors.right: parent.right
        anchors.rightMargin: 60

        onClicked:{
                accept()
        }
    }
}