from PyQt5.QtCore import Qt
from UM.Application import Application
from UM.FlameProfiler import pyqtSlot
from copy import deepcopy
from UM.Logger import Logger
from UM.Qt.ListModel import ListModel
from UM.Scene.Selection import Selection
from cura.Utils.PCBHandler.PCBUtils.PcbUtils import DrillType

class Layers(ListModel):
    def __init__(self, parent=None):
        super().__init__(parent)
        selected_object = Selection.getSelectedObject(0)
        selected_object_id = id(selected_object)
        self._node = Application.getInstance().getController().getScene().findObject(selected_object_id)
        self._layers = []
        self.addRoleName(Qt.UserRole + 1, "state_bool")


class DrillsPcb(ListModel):
    def __init__(self, parent=None):
        super().__init__(parent)

        self._items = []

        self._model = None
        # self._controller
        self._selected_object_id = None
        self._node = None
        self._stack = None
        self.addRoleName(Qt.UserRole + 1, "path")
        self.addRoleName(Qt.UserRole + 2, "name")
        self.addRoleName(Qt.UserRole + 3, "layers")
        self.addRoleName(Qt.UserRole + 4, "selected")
        self.addRoleName(Qt.UserRole + 5, "diameter")
        self.addRoleName(Qt.UserRole + 6, "type")
        self.addRoleName(Qt.UserRole + 7, "unit")
        self.addRoleName(Qt.UserRole + 8, "fmt")
        self.addRoleName(Qt.UserRole + 9, "to_view")
        self.addRoleName(Qt.UserRole + 10, "drillError")
        self.addRoleName(Qt.UserRole + 11, "is_excellon")
        self.addRoleName(Qt.UserRole + 12, "is_out_of_route")
        self._update()

    @pyqtSlot()
    def _update(self):

        self._selected_object_id = self.getSelectedObjectId()
        self._node = Application.getInstance().getController().getScene().findObject(self._selected_object_id)
        try:
            drill_list = self._node.getDrillsAsDict()  # selt._node.getSettings("stackup")
            for i, drill in enumerate(drill_list):

                try:
                    drill_type = DrillType[drill.get("type")]
                except KeyError as ke:
                    raise Exception("Cannot deserialize drill type {}".format(ke))

                drill["type"] = drill_type.value

                layers = Layers()
                layers.setItems(drill["layers"])
                drill["layers"] = layers
            self.setItems(drill_list)
        except Exception as e:
            Logger.log("d", "Failed loading drills: {}".format(e))

    def getSelectedObjectId(self):
        selected_object = Selection.getSelectedObject(0)
        selected_object_id = id(selected_object)
        return selected_object_id

    def getContainerID(self):
        selected_object = Selection.getSelectedObject(0)
        try:
            return selected_object.callDecoration("getStack").getId()
        except AttributeError:
            return ""

    @pyqtSlot()
    def forceUpdate(self) -> None:
        """Force updating the model."""
        to_update = []
        for i in range(len(self._items)):
            to_update.append({'selected': self._items[i]['selected'], 'type': self._items[i]['type'], 'layers': deepcopy(self._items[i]['layers'].items), 'drillError':True})
        self._node.updateDrillsFromGui(to_update)
