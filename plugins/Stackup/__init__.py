# Copyright (c) 2018 Ultimaker B.V.
# Cura is released under the terms of the LGPLv3 or higher.
from plugins.Stackup.PcbTypes import PcbTypes

from . import Stackup
from . import LayersPcb
from . import DrillsPcb
from PyQt5.QtQml import qmlRegisterType
from UM.i18n import i18nCatalog

i18n_catalog = i18nCatalog("cura")


def getMetaData():
    return {
        "tool": {
            "id": "stackup",
            "name": i18n_catalog.i18nc("@label", "Stackup"),
            "description": i18n_catalog.i18nc("@info:tooltip", "Organize the layer stackup of the pcb."),
            "icon": "tool_icon.svg",
            "tool_panel": "Stackup.qml",
            "weight": 5
        }
    }


def register(app):
    qmlRegisterType(LayersPcb.LayersPcb, "Cura", 1, 0,
                    "LayersPcb")
    qmlRegisterType(DrillsPcb.DrillsPcb, "Cura", 1, 0,
                    "DrillsPcb")
    qmlRegisterType(PcbTypes.PcbTypes, "Cura", 1, 0,
                    "PcbTypes")
    stackup = Stackup.Stackup()
    return {"tool": stackup}
