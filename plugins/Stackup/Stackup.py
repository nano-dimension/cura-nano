# Copyright (c) 2018 Ultimaker B.V.
# Cura is released under the terms of the LGPLv3 or higher.

import os

from UM.Application import Application
from UM.Event import Event
from UM.FlameProfiler import pyqtSlot
from UM.Job import Job
from UM.Logger import Logger
from UM.Message import Message
from UM.PluginRegistry import PluginRegistry
from UM.Scene.Selection import Selection
from UM.Signal import Signal
from UM.i18n import i18nCatalog
from cura.AmeTool import AmeTool
from cura.Utils.PCBHandler.PCBUtils.MessagesOnPcbLoad import PcbMessage, messageType
from cura.Utils.PCBHandler.PCBUtils.PcbModel import PcbModel
from cura.Utils.PCBHandler.PCBUtils.PcbUtils import getTypesForStackUp, LayerType, Drill
from cura.Utils.PCBHandler.RouteApplyJob import RouteApplyJob
from cura.Utils.PCBHandler.gerberTools.cam import FileSettings
from cura.Utils.PCBHandler.gerberTools.common import onlyDetectIfExcellon
from .PcbTypes import PcbTypes
from .RouteForStackup import RouteForStackup

catalog = i18nCatalog("cura")

format_options = [(i // 7, i % 7) for i in range(49) if (i // 7 != 0) and (i % 7 != 0)]


class Stackup(AmeTool):
    Result = Signal()

    def __init__(self):
        """Start the tool and exposes proprteis we use for updates"""
        super().__init__()
        self._marking_drills_window = None
        self._update_drill_diameter_windows = None
        self._settings_dialogue_window = None
        self._model = None
        self._controller = self._controller or None
        self.setExposedProperties("Node", "Route", "Types", "MaxVal", "MaxSignalLayerIndex", "Preview", "Window",
                                  "Notation", "Units", "ZeroSuppression", "Format", "Formats", "SelectedLayerIndex",
                                  "SupportsUndo")
        self._selected_drill_settings = None
        self._selected_object_id = None
        self._node = None
        self._stack = None
        self._preview_window = False
        self._error_message = Message()
        self._selected_drill = 0
        self._content_have_issues = False
        Selection.selectionChanged.connect(self.propertyChangedB)
        self._selectedLayerIndex = 0

    def getSupportsUndo(self):
        return True

    def getSelectedLayerIndex(self):
        return self._selectedLayerIndex

    #  Called when something happens in the scene while our tool is active.
    #   For instance, we can react to mouse clicks, mouse movements, and so on.
    def event(self, event):
        super().event(event)
        if event.type == Event.MousePressEvent and self._controller.getToolsEnabled():
            self.operationStopped.emit(self)
        return False  # we return false because we don't manipulate the scene

    # ___________________set_________________
    def setWindow(self, bool_s):
        self._preview_window = not self._preview_window

    def showErrorMessage(self, text):
        self._error_message.setText(text)
        self._error_message.show()

    # ___________________get_________________
    def getWindow(self) -> bool:
        return self._preview_window

    @staticmethod
    def getSelectedObjectId():
        selected_object = Selection.getSelectedObject(0)
        selected_object_id = id(selected_object)
        return selected_object_id

    def getContainerID(self):
        selected_object = Selection.getSelectedObject(0)
        try:
            return selected_object.callDecoration("getStack").getId()
        except AttributeError:
            return ""

    def getNode(self):
        self._selected_object_id = self.getSelectedObjectId()
        self._node = Application.getInstance().getController().getScene().findObject(self._selected_object_id)
        if not isinstance(self._node, PcbModel):
            self._node = None

        return self._node

    def getRoute(self) -> RouteForStackup:
        try:
            # Safety first
            if not self._node:
                self.showErrorMessage("Trying to access undefined node")
                return

            route_list = self._node.getRoutesAsDict()
            result = RouteForStackup()
            for route in route_list:
                result.appendItem(route)
            return result
        except:
            Logger.log("d", "There were no Routes to load")

    @staticmethod
    def getTypes() -> PcbTypes:
        try:
            types_list = getTypesForStackUp()
            result = PcbTypes()
            for i in types_list:
                result.appendItem(i)
            return result
        except:
            Logger.log("d", "There were no layers types to load")

    def getMaxVal(self) -> int:
        # Safety first
        if not self._node:
            self.showErrorMessage("Trying to access undefined node")
            return 0

        return self._node.getStackupCount()

    def getMaxSignalLayerIndex(self) -> int:
        'gives max val for just layers = signals and prepregs'

        # Safety first
        if not self._node:
            self.showErrorMessage("Trying to access undefined node")
            return 0

        layers_length = self._node.getNumberOfSignalLayers()

        return layers_length

    def getNotation(self):
        if self._selected_drill_settings:
            return 0 if self._selected_drill_settings.notation == 'absolute' else 1

    def getUnits(self):
        if self._selected_drill_settings:
            return 0 if self._selected_drill_settings.units == "metric" else 1

    def getZeroSuppression(self):
        if self._selected_drill_settings:
            return 0 if "leading" == self._selected_drill_settings.zero_suppression else 1

    def getFormat(self):
        if self._selected_drill_settings:
            return format_options.index(self._selected_drill_settings.format)

    @staticmethod
    def getFormats():
        return [str(f) for f in format_options]

    def getPreview(self) -> str:
        # Safety first
        if not self._node:
            self.showErrorMessage("Trying to access undefined node")
            return ""

        self._node.setPreviewHook(self.propertyChanged.emit)
        image_path = self._node.getPreviewImage()
        if image_path == "canceled":
            return "canceled"
        if image_path is not None:
            return "file:///" + image_path

        return ""

    # ______________________emits and updates ___________________
    @pyqtSlot()
    def _updateFromPy(self):
        self.propertyChanged.emit()
        Application.getInstance().getController().getScene().sceneChanged.emit(self._node)

    @pyqtSlot()
    def forceUpdateNoSceneChange(self):
        self.propertyChanged.emit()

    @pyqtSlot()
    def reloadImage(self):
        self._runPreviewCreateJob(None)

    def _emitPreview(self, job):
        self.propertyChangedB.emit()

    def _runUnionPreviewImageJob(self, pcb_item):
        from cura.Utils.PCBHandler.UnionPreviewImageJob import UnionPreviewImageJob

        if isinstance(pcb_item, Job):
            pcb_item = pcb_item.getResult()

        union_preview_job = UnionPreviewImageJob(pcb_item)
        union_preview_job.finished.connect(self._emitPreview)
        union_preview_job.start()

    def _runPreviewCreateJob(self, job):
        # Safety first

        if not self._node:
            self.showErrorMessage("Trying to access undefined node")
            return

        main_route_changed = job.getResult() if job is not None else False

        # re-render all images only needed if the main route changed
        self._node.createPreview(reraster_all=main_route_changed,
                                 post_job_callback=self._runUnionPreviewImageJob)

    # _________________________________________________________

    def itemToDrill(self, path: str):
        """
        :param path: garber layer to be inserted (forced) to drill manger,
                    if the engine do not think this file is a drill, it will force it
                    go into drills without diameter
        :return: nothing , apply by reference the CuraSceneNode
        """
        # Safety first
        if not self._node:
            self.showErrorMessage("Trying to access undefined node")
            return

        new_drills, _ = self._node.addDrill(path=path)
        if len(new_drills) > 0:
            self._node.removeLayerByPath(path)

        # TODO: make this operation undoable, so that we won't have to clear the undo/redo stack
        self._node.getStackupOperationStack().reset()

        self.propertyChanged.emit()
        self._runPreviewCreateJob(None)
        self.stackupChanged()

    def itemToRoutes(self, path: str):
        """
        :param path: gerber layer to be inserted (forced) to route manger,
                    if the engine do not think this file is a route, make a place holder on tray
        :return: nothing , apply by reference the CuraSceneNode
        """
        # Safety first
        if not self._node:
            self.showErrorMessage("Trying to access undefined node")
            return

        if self._node.removeLayerByPath(path) is None:
            return

        # TODO: make this operation undoable, so that we won't have to clear the undo/redo stack
        self._node.getStackupOperationStack().reset()

        self.propertyChanged.emit()
        route_update_job = RouteApplyJob(self._node, path)
        route_update_job.finished.connect(self._runPreviewCreateJob)
        route_update_job.finished.connect(self.stackupChanged)
        route_update_job.start()

    def drillToStackup(self, path: str):
        # Safety first
        if not self._node:
            self.showErrorMessage("Trying to access undefined node")
            return

        layer = self._node.readLayerFromPath(path=path)
        if layer is None:
            self.showErrorMessage("Invalid drill layer in file {}".format(path))
            return

        # TODO: make this operation undoable, so that we won't have to clear the undo/redo stack
        self._node.getStackupOperationStack().reset()

        self._node.addLayerToStackup(layer)
        self._node.removeDrillByPath(path=path)
        self.propertyChanged.emit()
        self._runPreviewCreateJob(None)
        self.stackupChanged()

    def removeLayerByIndex(self, layer_index):
        # Safety first
        if not self._node:
            self.showErrorMessage("Trying to access undefined node")
            return
        if not self._node.getStackupCount() > layer_index:
            self.showErrorMessage("Trying to access not exist layer")
            return

        self._node.removeLayerByIndexUndoable(layer_index, redo_callback=lambda: self._layerRemoved(layer_index),
                                              undo_callback=lambda: self._layerAdded(layer_index))

    def removeDrill(self, path: str):
        # Safety first
        if not self._node:
            self.showErrorMessage("Trying to access undefined node")
            return

        self._node.removeDrillByPath(path=path)
        self.propertyChanged.emit()
        self._runUnionPreviewImageJob(self._node)
        self.stackupChanged()

    def updateLayerRoute(self, args):
        if not self._node:
            self.showErrorMessage("Trying to access undefined node")
            return
        args_list = args.toVariant()
        self._node.updateLayerRoute(args_list[0], args_list[1])
        self.propertyChanged.emit()
        self._runUnionPreviewImageJob(self._node)
        self.stackupChanged()

    def addDielectric(self, layer_index):
        # Safety first
        if not self._node:
            self.showErrorMessage("Trying to access undefined node")
            return
        self._node.addDielectricAfterLayerUndoable(layer_index, undo_callback=lambda: self._layerAdded(layer_index),
                                                   redo_callback=lambda: self._layerRemoved(layer_index + 1))

    def selectLayer(self, index):
        if index not in range(self._node.getStackupCount()):
            Logger.warning("Invalid layers index to select {}".format(index))
            return
        self._selectLayer(index)

    def _selectLayer(self, layer_index=-1):
        layer_index = self._node.getStackupCount() - 1 if layer_index == -1 else layer_index
        self._selectedLayerIndex = max(0, min(layer_index, self._node.getStackupCount() - 1))
        Logger.debug("Selected layer {}".format(self._selectedLayerIndex))

    def _layerAdded(self, layer_index=-1):
        self._selectLayer(layer_index)
        self.stackupChanged()
        self.propertyChanged.emit()
        self._runPreviewCreateJob(None)

    def _layerRemoved(self, layer_index=-1):
        self._selectLayer(layer_index)
        self.stackupChanged()
        self.propertyChanged.emit()
        self._runUnionPreviewImageJob(self._node)

    def addLayer(self, urls: list):
        # Safety first
        if not self._node:
            self.showErrorMessage("Trying to access undefined node")
            return

        if isinstance(urls, list):
            for url in urls:
                new_path = self._node.copyFilesToTemp([url.toLocalFile()])[0]
                fmt = onlyDetectIfExcellon(new_path)
                if fmt == "excellon":
                    self._node.addDrill(path=new_path)
                    self.propertyChanged.emit()
                    self._runPreviewCreateJob(None)
                    self.stackupChanged()
                else:
                    layer = self._node.readLayerFromPath(new_path)
                    if layer is None:
                        PcbMessage(model_type=type(self).__name__, message_type=messageType.warning_message,
                                   file_name=os.path.basename(new_path),
                                   text="Notype-EmptyDrill").show()
                        continue
                    self._node.addLayerUndoable(layer,
                                                undo_callback=lambda: self._layerRemoved(self._node.getStackupCount()),
                                                redo_callback=lambda: self._layerAdded(self._node.getStackupCount()))


        else:
            PcbMessage(model_type=type(self).__name__, message_type=messageType.warning_message,
                       text="InvalidUrl").show()
            return

    def layersSwapped(self, to_index):
        self._selectedLayerIndex = to_index
        self.propertyChanged.emit()

    def swapLayers(self, args):
        args_dict = args.toVariant()
        from_index = int(args_dict.get("from_index", -1))
        to_index = int(args_dict.get("to_index", -1))
        if not {from_index, to_index}.issubset(set(range(self._node.getStackupCount()))):
            Logger.warning("Invalid layers index to swap {},{}".format(from_index, to_index))
            return
        self._node.swapLayersUndoable(from_index, to_index, undo_callback=lambda: self.layersSwapped(from_index),
                                      redo_callback=lambda: self.layersSwapped(to_index))

    def layerUpdated(self, layer_index):
        self._selectedLayerIndex = layer_index
        self.stackupChanged()
        self._updateFromPy()

    def addDielectricAfterSignalIfNeeded(self, layer_index):
        layers = self._node.getStackup()
        layer = layers[layer_index]
        if layer.type == LayerType.Signal:
            next_layer_index = layer_index + 1
            next_layer = layers[next_layer_index] if next_layer_index < len(layers) else None
            if not next_layer or next_layer.type != LayerType.Prepreg:
                self._node.addDielectricAfterLayerUndoable(layer_index,
                                                           undo_callback=lambda: self._layerAdded(next_layer_index),
                                                           redo_callback=lambda: self._layerRemoved(next_layer_index))

    def updateLayerThickness(self, args):
        args_dict = args.toVariant()
        layer_index = int(args_dict["layer_index"])
        thickness = float(args_dict["thickness"])
        self._node.updateLayerThicknessUndoable(layer_index, thickness,
                                                undo_callback=lambda: self.layerUpdated(layer_index),
                                                redo_callback=lambda: self.layerUpdated(layer_index))

    def updateLayerType(self, args):
        args_dict = args.toVariant()
        layer_index = int(args_dict["layer_index"])
        layer_type = LayerType(int(args_dict["layer_type"]))
        self._node.updateLayerTypeUndoable(layer_index, layer_type,
                                           undo_callback=lambda: self.layerUpdated(layer_index),
                                           redo_callback=lambda: self.layerUpdated(layer_index))
        if layer_type == LayerType.Signal:
            self.addDielectricAfterSignalIfNeeded(layer_index)

    def markFromToByDialogue(self, column: int):
        self._selected_drill = column
        if self._marking_drills_window is None:
            self._marking_drills_window = self._createMarkingDialogue()
        self._marking_drills_window.show()

    def _getSelectedDrill(self) -> Drill:
        return s[self._selected_drill]

    def updateDrillDiameterByDialogue(self, column: int):
        self._selected_drill = column

        if self._update_drill_diameter_windows is None:
            self._update_drill_diameter_windows = self._createChangeDrillDiameterDialogue()

        selected_drill = self._node.getDrills()[self._selected_drill]
        self._update_drill_diameter_windows.setProperty("currentDrillDiameter", str(selected_drill.diameter))
        self._update_drill_diameter_windows.show()

    def _createMarkingDialogue(self):
        qml_file_path = os.path.join(PluginRegistry.getInstance().getPluginPath(self.getPluginId()),
                                     "DrillMarkingDialogue.qml")
        component = Application.getInstance().createQmlComponent(qml_file_path)
        return component

    def _createChangeDrillDiameterDialogue(self):
        qml_file_path = os.path.join(PluginRegistry.getInstance().getPluginPath(self.getPluginId()),
                                     "UpdateDrillDiameter.qml")
        component = Application.getInstance().createQmlComponent(qml_file_path)
        return component

    def showDrillsSettingsDialog(self, column: int):
        # Safety first
        if not self._node:
            self.showErrorMessage("Trying to access undefined node")
            return

        self._selected_drill = column
        self._selected_drill_settings = self._node.getDrills()[column].settings
        if self._settings_dialogue_window is None:
            self._settings_dialogue_window = self._createSettingsDialogue()
        self.propertyChanged.emit()
        self._settings_dialogue_window.show()

    def _createSettingsDialogue(self):
        # Create a QML component from the SelectionInfo.qml file.
        qml_file_path = os.path.join(PluginRegistry.getInstance().getPluginPath(self.getPluginId()),
                                     "DrillSettingsDialogue.qml")
        component = Application.getInstance().createQmlComponent(qml_file_path)
        return component

    def changeDrillSetting(self, settings):
        # Safety first
        if not self._node:
            self.showErrorMessage("Trying to access undefined node")
            return

        notation, units, zero_suppression, fmt = settings.split("|")
        new_settings = FileSettings(notation=notation, units=units, zero_suppression=zero_suppression,
                                    format=(int(fmt.split(",")[0][-1]), int(fmt.split(",")[1][1])))
        if self._node.updateDrillSettings(self._selected_drill, new_settings) is not None:
            self._runPreviewCreateJob(None)
            self.stackupChanged()

    @staticmethod
    def unmarkInterval(bool_dict_of_layers_by_drill):
        """restart the drill interval to unmarked"""
        for i in range(len(bool_dict_of_layers_by_drill)):
            bool_dict_of_layers_by_drill[i]['state_bool'] = False
        return bool_dict_of_layers_by_drill

    def updateDrillFromToLayers(self, index: any):
        """
        lets mark layers but only signal layers
        """
        # Safety first
        if not self._node:
            self.showErrorMessage("Trying to access undefined node")
            return

        min_val_in_signals, max_val_in_signals = index.split("|")
        if not (len(min_val_in_signals) or len(max_val_in_signals)):
            return  # Strictly speaking we should not get here, but just in case we received empty string - do nothing
        if min_val_in_signals > max_val_in_signals:  # safety
            return

        min_val_in_stackup = self._node.getLayerIndexBySignalIndex(min_val_in_signals)
        max_val_in_stackup = self._node.getLayerIndexBySignalIndex(max_val_in_signals)

        if min_val_in_stackup is None or max_val_in_stackup is None:
            return

        layers_list = self._node._stackup
        drills_list = self._node.getDrillsAsDict()
        current_list = drills_list[self._selected_drill]['layers']
        current_list = Stackup.unmarkInterval(current_list)  # lets restart the marking
        current_list = self.updateDrillFromTOSignalS(current_list, layers_list, min_val_in_stackup, max_val_in_stackup)

        drills_list[self._selected_drill]['layers'] = current_list
        self._node.updateDrillsFromGui(drills_list)
        self._marking_drills_window.close()
        self.propertyChanged.emit()
        self.stackupChanged()

    def updateDrillDiameter(self, diameter: str):
        # Safety first
        if not self._node:
            self.showErrorMessage("Trying to access undefined node")
            return
        drill = self._node.getDrills()[self._selected_drill]
        updated_drill, err_message = self._node.updateDrillDiameter(self._selected_drill, float(diameter))
        if updated_drill is None:
            PcbMessage(model_type=type(self).__name__, message_type=messageType.warning_message,
                       file_name=os.path.basename(drill.path),
                       title="Updating drill {} diameter".format(drill.name),
                       text="Failed to update drill diameter : {}".format(err_message)).show()
            return
        self._node.createPreview(reraster_all=False)
        self.propertyChanged.emit()
        self.stackupChanged()

    def updateDrillFromTOSignalS(self, current_list, layers_list, min_val_in_stackup, max_val_in_stackup):
        for i in range(len(current_list)):
            if layers_list[i].type.name == 'Signal' or layers_list[
                i].type.name == 'Prepreg':  # lets check only siganl and prepregs
                current_list[i]['state_bool'] = int(min_val_in_stackup) <= i <= int(max_val_in_stackup)
                if current_list[i]['state_bool'] == True and (layers_list[i].signal_index is not None):  # safety
                    if i > 0:
                        if layers_list[i - 1].type.name == 'SolderMask':
                            current_list[i - 1]['state_bool'] = True
                            if i > 1:
                                if layers_list[i - 2].type.name == 'Annotation':
                                    current_list[i - 2]['state_bool'] = True
                    if i < len(layers_list) - 1:
                        if layers_list[i + 1].type.name == 'SolderMask':
                            current_list[i + 1]['state_bool'] = True
                            if i < len(layers_list) - 2:
                                if layers_list[i + 2].type.name == 'Annotation':
                                    current_list[i + 2]['state_bool'] = True
        return current_list

    def updateDrillFromTOSignalS(self, current_list, layers_list, min_val_in_stackup, max_val_in_stackup):
        for i in range(len(current_list)):
            if layers_list[i].type.name == 'Signal' or layers_list[i].type.name == 'Prepreg':  # lets check only siganl and prepregs
                current_list[i]['state_bool'] = int(min_val_in_stackup) <= i <= int(max_val_in_stackup)
                if current_list[i]['state_bool'] == True and (layers_list[i].signal_index is not None):  # safety
                    if i > 0:
                        if layers_list[i - 1].type.name == 'SolderMask':
                            current_list[i - 1]['state_bool'] = True
                            if i > 1:
                                if layers_list[i - 2].type.name == 'Annotation':
                                    current_list[i - 2]['state_bool'] = True
                    if i < len(layers_list) - 1:
                        if layers_list[i + 1].type.name == 'SolderMask':
                            current_list[i + 1]['state_bool'] = True
                            if i < len(layers_list) - 2:
                                if layers_list[i + 2].type.name == 'Annotation':
                                    current_list[i + 2]['state_bool'] = True
        return current_list

    def stackupChanged(self, _=None):
        # Safety first
        if not self._node:
            self.showErrorMessage("Trying to access undefined node")
            return

        Application.getInstance().getController().getScene().sceneChanged.emit(self._node)

    # ______________preview__________________
    def previewHideAllLayers(self):
        # Safety first
        if not self._node:
            self.showErrorMessage("Trying to access undefined node")
            return

        self._node.hideAllLayers()
        self.propertyChanged.emit()
        self._runUnionPreviewImageJob(self._node)

    def previewShowAllLayers(self):
        # Safety first
        if not self._node:
            self.showErrorMessage("Trying to access undefined node")
            return

        self._node.showAllLayers()
        self.propertyChanged.emit()
        self._runUnionPreviewImageJob(self._node)

    def previewHideAllDrills(self):
        # Safety first
        if not self._node:
            self.showErrorMessage("Trying to access undefined node")
            return

        self._node.hideAllDrills()
        self.propertyChanged.emit()
        self._runUnionPreviewImageJob(self._node)

    def previewShowAllDrills(self):
        # Safety first
        if not self._node:
            self.showErrorMessage("Trying to access undefined node")
            return

        self._node.showAllDrills()
        self.propertyChanged.emit()
        self._runUnionPreviewImageJob(self._node)

    def modifyPreviewForLayer(self, layer_index):
        # Safety first
        if not self._node:
            self.showErrorMessage("Trying to access undefined node")
            return

        self._node.switchToViewInLayer(layer_index)
        self.propertyChanged.emit()
        self._runUnionPreviewImageJob(self._node)

    def modifyPreviewForDrill(self, drill_index):
        # Safety first
        if not self._node:
            self.showErrorMessage("Trying to access undefined node")
            return

        self._node.switchToViewInDrill(drill_index)
        self.propertyChanged.emit()
        self._runUnionPreviewImageJob(self._node)

    @staticmethod
    def changestate():
        Application.getInstance().getController().setActiveToolcolor("PerObjectSettingsTool")

    def undo(self) -> None:
        if not self._node:
            Logger.log("w", "Can't undo - no selected node.")
            return
        self._node.getStackupOperationStack().undo()

    def redo(self) -> None:
        if not self._node:
            Logger.log("w", "Can't redo - no selected node.")
            return
        self._node.getStackupOperationStack().redo()
