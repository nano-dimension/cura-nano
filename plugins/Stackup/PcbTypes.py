from PyQt5.QtCore import Qt, pyqtSlot
from UM.Qt.ListModel import ListModel
from cura.Utils.PCBHandler.PCBUtils.PcbUtils import getTypesForStackUp, getMinMax
from UM.Application import Application


class PcbTypes(ListModel):
    def __init__(self, parent=None):
        super().__init__(parent)

        self._items = []
        self.addRoleName(Qt.UserRole + 1, "type_name")
        self.addRoleName(Qt.UserRole + 2, "value")
        self.addRoleName(Qt.UserRole + 3, "minimum_value_warning")
        self.addRoleName(Qt.UserRole + 4, "maximum_value_warning")
        self.addRoleName(Qt.UserRole + 5, "minimum_value")
        self.addRoleName(Qt.UserRole + 6, "maximum_value")
        self._update()

    @pyqtSlot(result="QVariantMap")
    def getPrepregRanges(self):
        return getMinMax(Application.getInstance().getMachineManager().activeMachine, "prepreg_layer_thickness")

    def _update(self):
        types_list = getTypesForStackUp()
        self.setItems(types_list)
        self.itemsChanged.emit()
