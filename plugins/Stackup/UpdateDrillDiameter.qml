//Copyright (c) 2017 Ultimaker B.V.
//This example is released under the terms of the AGPLv3 or higher.
//This allows you to use all of Uranium's built-in QML items.
import QtQuick 2.10 //This allows you to use QtQuicks built-in QML items.
import QtQuick.Controls 2.3 //Contains the "Label" element.
import UM 1.3 as UM
import Cura 1.0 as Cura

UM.Dialog //Creates a modal window that pops up above the interface.
{
    id: base
    title : "Update Drill Diameter"
    width: 300
    height: 70
    minimumWidth: 300
    property var currentDrillDiameter: 0.1
    minimumHeight: 70
    onAccepted:  UM.ActiveTool.triggerActionWithData("updateDrillDiameter",textInput.text)

    onVisibleChanged: {
        if (visible) {
            textInput.text = qsTr(currentDrillDiameter.toString())
            textInput.focus = true
            textInput.selectAll()
        }
    }

    Label
    {
        id: id_label

        //Positions the label on the top-left side of the window.
        anchors.top: base.top
        anchors.topMargin: 10
        anchors.left: base.left
        // anchors.leftMargin: 10

        text: "Drill Diameter (mm)"
    }

    Rectangle {
        width: 80;
        height: 20 ;
        color:"white"
        anchors.top: base.top
        anchors.topMargin: 10
        anchors.left: base.left
        x:150
        TextInput {
            id: textInput
            width: 80;
            height: 20;
            text: qsTr("1")
            font: UM.Theme.getFont("default")
            // anchors.top: base.top
            // anchors.topMargin: 10
            // anchors.left: base.left
            // x:80
            validator: DoubleValidator{bottom: 0.0; top: 100.0;}
            //anchors.leftMargin: 80
            //leftPadding: 80
        }
    }

    Button {
        width: 80
        height: 20
        text: "Apply"
        anchors.bottom: parent.bottom
        anchors.right: parent.right
        enabled: textInput.text
        onClicked: {
            accept()
        }
    }
}