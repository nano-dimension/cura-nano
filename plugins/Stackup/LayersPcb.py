from PyQt5.QtCore import Qt
from UM.Message import Message
from UM.Qt.ListModel import ListModel
from UM.Scene.Selection import Selection
from UM.Application import Application
from UM.FlameProfiler import pyqtSlot
from cura.Utils.PCBHandler.PCBUtils.PcbModel import PcbModel
from PyQt5.QtCore import QVariant


class LayersPcb(ListModel):

    def __init__(self, parent=None):
        super().__init__(parent)

        self._items = []
        self._decorator = None
        self._model = None
        self._selected_object_id = None
        self._node = None
        _selected_object_id = id(Selection.getSelectedObject(0))
        self._node = Application.getInstance().getController().getScene().findObject(_selected_object_id)
        if not isinstance(self._node, PcbModel):
            self._node = None
        self._stack = None
        self.addRoleName(Qt.UserRole + 1, "type")
        self.addRoleName(Qt.UserRole + 2, "path")
        self.addRoleName(Qt.UserRole + 3, "thickness")
        self.addRoleName(Qt.UserRole + 4, "name")
        self.addRoleName(Qt.UserRole + 5, "material")
        self.addRoleName(Qt.UserRole + 6, "route")
        self.addRoleName(Qt.UserRole + 7, "unit")
        self.addRoleName(Qt.UserRole + 8, "stackup_index")
        self.addRoleName(Qt.UserRole + 9, "to_view")
        self.addRoleName(Qt.UserRole + 10, "errors")
        self.addRoleName(Qt.UserRole + 11, "signal_index")
        self._update()

    @pyqtSlot()
    def _update(self):
        # Safety first
        if not self._node:
            Message("Trying to access undefined node").show()
            return

        self.setItems(self._node.getStackupAsDict())

    @pyqtSlot(result=QVariant)
    def getLayerErrorsEnum(self):
        return {
            "ThicknessError": 1,
            "TypeError": 2,
            "ThicknessWarning": 4,
            "TypeWarning": 8,
            "ProcessError": 16
        }

    def getContainerID(self):
        selected_object = Selection.getSelectedObject(0)
        try:
            return selected_object.callDecoration("getStack").getId()
        except AttributeError:
            return ""

    @pyqtSlot()
    def forceUpdate(self) -> None:
        """Force updating the model."""
        # Safety first
        if not self._node:
            Message("Trying to access undefined node").show()
            return

        self._node.updateStackupFromGui(self.items)
