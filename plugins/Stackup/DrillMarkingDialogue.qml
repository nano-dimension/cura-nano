//Copyright (c) 2017 Ultimaker B.V.
//This example is released under the terms of the AGPLv3 or higher.
//This allows you to use all of Uranium's built-in QML items.
import QtQuick 2.10 //This allows you to use QtQuicks built-in QML items.
import QtQuick.Controls 2.3 //Contains the "Label" element.
import UM 1.3 as UM
import Cura 1.0 as Cura
UM.Dialog //Creates a modal window that pops up above the interface.
{
    id: base
    title : "Mark Drill from Layer to Layer"
    width: 300
    height: 70
    minimumWidth: 300
    property var maxVal: UM.ActiveTool.properties.getValue("MaxSignalLayerIndex")
    minimumHeight: 70
    onAccepted:  UM.ActiveTool.triggerActionWithData("updateDrillFromToLayers",""+textInput.text+"|"+textInput1.text+"")

    onVisibleChanged: {
        if (visible)
            // restore default values
            if (textInput.text == "")
                textInput.text = qsTr("1")

            if (textInput1.text == "")
                textInput1.text = qsTr(maxVal.toString())
    }

        Label
    {
        id: id_label

        //Positions the label on the top-left side of the window.
        anchors.top: base.top
        anchors.topMargin: 10
        anchors.left: base.left
        // anchors.leftMargin: 10

        text: "Start"
    }
        Rectangle{
        width: 30;
        height: 20 ;
        color:"white"
        anchors.top: base.top
        anchors.topMargin: 10
        anchors.left: base.left
        x:80
    TextInput {
        id: textInput
        width: 20;
        height: 20 ;
        text: qsTr("1")
        font: UM.Theme.getFont("default")
        // anchors.top: base.top
        // anchors.topMargin: 10
        // anchors.left: base.left
        // x:80
        validator: IntValidator{bottom: 1; top: maxVal;}
        //anchors.leftMargin: 80
        //leftPadding: 80

        onTextChanged: {
            if (parseInt(textInput.text)<1){
                textInput.text="1"
            }
        }
    }
}
            Label
    {
        id: id_label1

        //Positions the label on the top-left side of the window.
        anchors.top: base.top
        anchors.topMargin: 10
        anchors.right: parent.right
        rightPadding:100
        // anchors.rightMargin: 40

        text: "Finish"
    }
    Rectangle{
        width: 30;
        height: 20 ;
        color:"white"
        anchors.top: base.top
        anchors.topMargin: 10
        anchors.right: parent.right
    TextInput {
        id: textInput1
        width: 20;
        height: 20 ;
        text: qsTr(maxVal.toString())
        font: UM.Theme.getFont("default")
        validator: IntValidator{bottom: 1; top: maxVal;}
        // anchors.rightMargin: 10
        onTextChanged: {
            if (parseInt(textInput1.text)<1){
                textInput1.text="1"
            }
        }
    }
    }


    Button{
            width: 80
    height: 20
    text:"Apply"
    anchors.bottom: parent.bottom
    anchors.right: parent.right
    enabled: textInput1.text && textInput.text
        onClicked:{
            accept()
           
        }
    }
}