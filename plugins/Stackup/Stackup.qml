import QtQuick.Window 2.2
import QtQuick 2.10
import QtQml.Models 2.1
import QtQuick.Dialogs 1.2
import QtQuick.Controls 2.3
import QtGraphicalEffects 1.0
import QtQuick.Layouts 1.1
import UM 1.2 as UM
import Cura 1.0 as Cura
import ".."

Item{
    id: root
    property int should_act: -1;
    property var p: UM.Theme.getSize("12px").width/12
    property var p2: {Math.min(p, 1.3)}
    property var p3: p2 / p

    property bool is_stackup_error: false;
    property var layerErrors: layersModel.getLayerErrorsEnum()


    ListModel{
        id: vias_types

        ListElement {
            name: "PTH"
        }
        ListElement {
            name: "TH"
        }
        ListElement {
            name: "VIA"
        }
    }
    function select_layer(index) {
        listView1.currentIndex = index
        UM.ActiveTool.triggerActionWithData("selectLayer", index)
    }

    function print_value(name, value){
        console.log(name,"=", value);
        return value;
    }
    function get_left_table_width() { 
        var w = UM.Theme.getSize("stackup_filename_title").width *p3;
        if(show_layer.checked){
            w += UM.Theme.getSize("51px").width *p3 + UM.Theme.getSize("txt_layer_thickness").width *p3 + UM.Theme.getSize("layer_type_cb").width *p3 + UM.Theme.getSize("route_combobox").width *p3 + UM.Theme.getSize("eye_icon").width *p3
        }
        return w;
    }
    function fix_number(number){
        var s = number.toString();
        s = s.split(".")[0];
        s = s.substring(0, 9);
        var res = "";
        var leading_zero = true;
        for (var i=0; i < s.length;i++){
            var c = s.charAt(i);
            if ('0' == c && leading_zero)
                continue;
            if ('0' <= c && c <= '9')
                res += c;
            leading_zero = false;
        }
        res = res || "0";
        return res;
    }

    function get_right_table_width() {
        var w = 0;
        var drills_count = Math.min(drillsModel.count, 8)
        if(!show_drill.checked)
            drills_count = 0;
        w = drills_count * 4628 / 70;
        return w;
    }

    function muti_char(c, n){
        var s = "";

        while(s.length < n)
            s+=c;
        return s;
    }

    function print_object(name, obj, depth, prefix){
        if (!depth)
            depth = 0;
        if (!prefix)
            prefix = "";
        var max = 0;
        if (prefix=="") {
            var title = "------------ yuval zilber represents ------------";
            var title_len = title.length;
            var name_str = (obj===undefined ? "#undefined#" : obj.toString());
            var name_type = name + ": " + name_str;
            var obj_len = name_type.length;
            if (title_len % 2 != obj_len % 2) {
                title = "-" + title;
                title_len++;
            }

            var additional = muti_char("-", obj_len - title_len + 4);
            title = additional + title + additional;
            title_len = title.length;
            var obj_dashes = muti_char("-", (title_len - obj_len - 2) / 2);
            var border = muti_char("-", title_len);

            console.log(border);
            console.log(title);
            console.log(obj_dashes + " " + name_type + " " + obj_dashes);
        }
        for (var i in obj) {
            max = Math.max(max, i.length);
        }

        for (var i in obj){
            var spaces = muti_char(" ", max - i.toString().length);
            if (depth === 0)
                print_value(prefix+name+"[\""+i+"\"]"+spaces, obj[i]);
            else
            {
                var obs = obj[i].toString();
                var str = prefix+name+"[\""+i+"\"] = "+obs;
                var is_parent = i.replace("_","").replace("_","").startsWith("parent");
                if (is_parent){
                    console.log(str+" ... parent ...");
                }
                else if (obs.startsWith("Q") && obs.endsWith(")")){
                    str+=" {";
                    console.log(str);
                    print_object(name+"[\""+i+"\"]", obj[i],depth-1, prefix+"    " )
                    console.log(prefix+"}");
                }
                else{
                    console.log(str);
                }
            }
        }

        if (prefix==="") {
            console.log(border);
        }
        return obj;
    }
    visible: true
    width: childrenRect.width
    height: childrenRect.height
    property var color_row : false
    property int header_heigth: 80
    property var node: UM.ActiveTool.properties.getValue("Node")
    property var routesPcb: UM.ActiveTool.properties.getValue("Route")
    property var previewPath: UM.ActiveTool.properties.getValue("Preview")
    property bool expanded_state: {
        var result = !!UM.ActiveTool.properties.getValue("Window") && previewPath && previewPath !== "canceled";
        if (result)
        {
            refreshimage()
        }
        return result;
    }
    property real view_x:0
    property real view_y:0
    property real page_x:0

    property var layer_is_moving: false;
    property var time_0: false;
    property var last_start: -1;
    property var last_end: -1;


    Cura.PcbTypes{ id:all_types }
    Cura.LayersPcb{ id:layersModel }


    UM.ToolModel{
         id: toolsModel
    }
    Component.onCompleted:{
        drillsModel._update();
        updatePython()
        updateQT()
    }

    ListModel{
        id: routesModel
        Component.onCompleted: update()
        function update()
        {
            if (routesPcb != undefined)
            {
                for (var i = 0; i < routesPcb.count; i++)
                {
                    append(routesPcb.getItem(i));
                }
            }
        }
    }
    function updateQT(){
        scrollview1.position = view_x
        scrollview3.position = view_y
        UM.ActiveTool.triggerAction("_updateFromPy")
    }
    function updatePython(){
        view_x = scrollview1.position
        view_y = scrollview3.position
        drillsModel.forceUpdate();
        layersModel.forceUpdate();
    }
    function updatePythonFast(){
        layersModel.forceUpdate();
    }
    function updateQTFast(){
        UM.ActiveTool.triggerAction("forceUpdateNoSceneChange")
    }
    function refreshimage(){
        image_shown.source = ""
        image_shown.source = previewPath === "canceled" ? "" : previewPath
    }
    function markAll(){
        var r = layersModel.count
        for (var t=0; t<r;t++){
            drillsModel.getItem(listView2.currentIndex).layers.setProperty(t,"state_bool",true)
        }
    }
    function unmarkAll(){
        var r = layersModel.count
        for (var t=0; t<r;t++){
            drillsModel.getItem(listView2.currentIndex).layers.setProperty(t,"state_bool",false)
        }
    }
    function markFromTo(){
        var r = layersModel.count
        var start = Math.min(listView1.roww_prev,listView1.roww)
        var stop = Math.max(listView1.roww_prev,listView1.roww)

        if (listView1.cloumn !== listView1.cloumn_prev) {
            drillsModel.getItem(listView1.cloumn_prev).layers.setProperty(listView1.roww_prev,"state_bool",false)
            listView1.roww_prev = listView1.i
            color_row = true
            drillsModel.getItem(listView1.cloumn).layers.setProperty(listView1.roww,"state_bool",false)
            drillsModel.setProperty(listView1.cloumn_prev,"selected",false)
            drillsModel.setProperty(listView1.cloumn,"selected",false)
            return
        }

        else{
            for (var t=0; t<r;t++){
                if ((t<stop) && (start<t)){
                    if(!drillsModel.getItem(listView1.cloumn).layers.getItem(t).state_bool){
                        drillsModel.getItem(listView1.cloumn).layers.setProperty(t,"state_bool",true)
                    }
                }
            }
            color_row = false
            listView1.roww_prev = -1
        }
    }
    Row {
        width: childrenRect.width  // width of all the stackup window
        height: {UM.Theme.getSize("400px").height*p3}
        Column {
            z:50
            id: button_colum
            width: UM.Theme.getSize("modal_sidebar").width*p3  // width of left toolbar (+, -, ^, ...)
            height: UM.Theme.getSize("400px").height*p3

            Cura.ExpandableComponent{
                id: base_box
                height: UM.Theme.getSize("modal_sidebar_button").height*p3
                width: UM.Theme.getSize("modal_sidebar_button").width *p3
                width_var: 520
                z:51
                dragPreferencesNamePrefix: "view/preview"
                expanded: expanded_state
                function toggleContent()
                {
                    if(previewPath!=="canceled"){
                        expanded = !expanded
                        refreshimage()
                        UM.ActiveTool.setProperty("Window", true)
                    }
                    else{
                        loading_timer.start();
                        UM.ActiveTool.triggerAction("reloadImage");
                    }
                }
                contentBackgroundColor: UM.Theme.getColor("background_modal")

                headerBackgroundColor: UM.Theme.getColor("background_modal_button")
                //headerHoverColor:
                //background.collapseButton.color: "blue"
                contentAlignment: Cura.ExpandableComponent.ContentAlignment.AlignLeft
                Timer {
                    id: loading_timer
                    interval: 300
                    running: true
                    repeat: true
                    onTriggered: {
                        var base = "Loading";
                        var sequence = ["",".","..","..."];
                        var i=0;
                        if (!base_box.disabledText){
                            base_box.disabledText = base + sequence[0];
                        }
                        for (;i<sequence.length;i++){
                            if (base_box.disabledText === base+sequence[i]){
                                base_box.disabledText = base+sequence[(i+1)%sequence.length]
                                break;
                            }
                        }
                        if (previewPath){
                            loading_timer.stop();
                        }
                    }
                }
                enabled: !!previewPath // && previewPath!=="canceled"
                headerPadding: enabled ? UM.Theme.getSize("default_margin").width : 0
                iconSize: UM.Theme.getSize("modal_sidebar_button").width *p3/2
                iconSizeW: UM.Theme.getSize("modal_sidebar_button").width *p3/2
                height_var: 600
                iconSource: previewPath==="canceled" ? UM.Theme.getIcon("yz_loading") : UM.Theme.getIcon("magnifying_glass")
                contentItem: Rectangle {
                    id: content
                    visible: true
                    Rectangle {
                        id: rect1
                        height: UM.Theme.getSize("40px").height*p3
                        z:2
                        Row {
                            RowLayout {
                                ToolButton {
                                    text: "Out"
                                    onClicked: {
                                        f.zoomOut()
                                    }
                                }
                                ToolButton {
                                    text: "Fit"
                                    onClicked: {
                                        f.fitToScreen();
                                    }
                                }
                                ToolButton {
                                    text: "In"
                                    onClicked: {
                                        f.zoomIn();
                                    }
                                }
                            }
                        }
                    }
                    Rectangle{
                        id: rect2
                        anchors.top: rect1.bottom
                        width: 500
                        color: UM.Theme.getColor("background_modal")
                        height: 510
                        Page {
                            anchors.fill: parent
                            Flickable {
                                id: f
                                anchors.fill: parent
                                boundsBehavior: Flickable.DragOverBounds
                                contentHeight: iContainer.height;
                                contentWidth: iContainer.width;
                                clip: true

                                property bool fitToScreenActive: true

                                property real minZoom: 0.1;
                                property real maxZoom: 20

                                property real zoomStep: 0.1
                                Rectangle{
                                    anchors.fill: parent
                                    color: UM.Theme.getColor("background_modal")
                                    MouseArea{
                                        anchors.fill: parent
                                        hoverEnabled : true
                                        onWheel: {

                                            image_shown.rotation += wheel.angleDelta.x / 120;
                                            if (Math.abs(image_shown.rotation) < 0.6)
                                                image_shown.rotation = 0;
                                            var scaleBefore = image_shown.scale;
                                            if (((image_shown.scale<=f.maxZoom)&&(image_shown.scale>=f.minZoom))){
                                                image_shown.scale += image_shown.scale * wheel.angleDelta.y / 120 / 10;
                                                if (image_shown.scale > f.maxZoom)
                                                    image_shown.scale = f.maxZoom
                                                if (image_shown.scale < f.minZoom)
                                                    image_shown.scale = f.minZoom
                                            }

                                        }
                                    }
                                }
                                onWidthChanged: {
                                    if (fitToScreenActive)
                                        fitToScreen();
                                }
                                onHeightChanged: {
                                    if (fitToScreenActive)
                                        fitToScreen();
                                }

                                Item {
                                    id: iContainer
                                    width: Math.max(image_shown.width * image_shown.scale, f.width)
                                    height: Math.max(image_shown.height * image_shown.scale, f.height)

                                    Image {
                                        id: image_shown
                                        property real prevScale: 1.0;
                                        asynchronous: true
                                        cache: false
                                        smooth: f.moving
                                        source: root.previewPath && previewPath!=="canceled" ? previewPath : ""
                                        anchors.centerIn: parent
                                        fillMode: Image.PreserveAspectFit
                                        transformOrigin: Item.Center
                                        onScaleChanged: {
                                            if ((width * scale) > f.width) {
                                                var xoff = (f.width / 2 + f.contentX) * scale / prevScale;
                                                f.contentX = xoff - f.width / 2
                                            }
                                            if ((height * scale) > f.height) {
                                                var yoff = (f.height / 2 + f.contentY) * scale / prevScale;
                                                f.contentY = yoff - f.height / 2
                                            }
                                            prevScale=scale;
                                        }
                                        onStatusChanged: {
                                            if (status===Image.Ready) {
                                                f.fitToScreen();
                                            }
                                        }
                                        //Behavior on scale { ScaleAnimator { } }
                                    }
                                }
                                function fitToScreen() {
                                    var s = Math.min(f.width / image_shown.width, f.height / image_shown.height, 1)
                                    image_shown.scale = s;
                                    f.minZoom = s;
                                    image_shown.prevScale = scale
                                    fitToScreenActive=true;
                                    f.returnToBounds();
                                }
                                function zoomIn() {
                                    image_shown.scale = Math.min(image_shown.scale*(1.0 + zoomStep), f.maxZoom)
                                    f.returnToBounds();
                                    fitToScreenActive=false;
                                    f.returnToBounds();

                                }

                                function zoomOut() {
                                    image_shown.scale = Math.max(image_shown.scale*(1.0 - zoomStep), f.minZoom)
                                    f.returnToBounds();
                                    fitToScreenActive=false;
                                    f.returnToBounds();
                                }

                                ScrollIndicator.vertical: ScrollIndicator { }
                                ScrollIndicator.horizontal: ScrollIndicator { }

                            }
                        }
                    }
                }
            }

            Cura.ND_tools_Button{
                id: btn_plus
                function hovered_mode() {return btn_plus.hovered;}

                height: UM.Theme.getSize("modal_sidebar_button").height*p3
                width: UM.Theme.getSize("modal_sidebar_button").width *p3
                icon.height: UM.Theme.getSize("modal_sidebar_button").width *p3/2
                icon.width: UM.Theme.getSize("modal_sidebar_button").width *p3/2
                icon.source: UM.Theme.getIcon("plus-svgrepo-com")
                onClicked: { bush.open(); }

            }
            Cura.ND_tools_Button {
                id: btn_minus
                height: UM.Theme.getSize("modal_sidebar_button").height*p3
                width:UM.Theme.getSize("modal_sidebar_button").width *p3
                icon.height: UM.Theme.getSize("modal_sidebar_button").width *p3/2
                icon.width: UM.Theme.getSize("modal_sidebar_button").width *p3/2
                icon.source: UM.Theme.getIcon("minus-svgrepo-com");
                enabled: listView1.count>0;
                onClicked: {
                    UM.ActiveTool.triggerActionWithData("removeLayerByIndex" , listView1.currentIndex);
                }
            }
            Cura.ND_tools_Button{
                id: arrow_up
                height: UM.Theme.getSize("modal_sidebar_button").height*p3
                width: UM.Theme.getSize("modal_sidebar_button").width *p3
                icon.height: UM.Theme.getSize("modal_sidebar_button").width *p3/2
                icon.width: UM.Theme.getSize("modal_sidebar_button").width *p3/2
                icon.source: UM.Theme.getIcon("up-arrow-stack");
                enabled: listView1.count>0;
                onClicked: {
                    if (listView1.currentIndex-1 >= 0)
                    {
                        UM.ActiveTool.triggerActionWithData("swapLayers",
                            { from_index: listView1.currentIndex, to_index: listView1.currentIndex-1 })
                    }
                }
            }
            Cura.ND_tools_Button {
                id: arrow_down
                height: UM.Theme.getSize("modal_sidebar_button").height*p3
                width: UM.Theme.getSize("modal_sidebar_button").width *p3
                icon.height: UM.Theme.getSize("modal_sidebar_button").width *p3/2
                icon.width: UM.Theme.getSize("modal_sidebar_button").width *p3/2
                icon.source: UM.Theme.getIcon("down-arrow-stack")
                enabled: listView1.count>0;

                onClicked: {
                    if (listView1.currentIndex+1 < layersModel.count)
                    {
                        UM.ActiveTool.triggerActionWithData("swapLayers",
                            { from_index: listView1.currentIndex, to_index: listView1.currentIndex+1 })
                    }
                }
            }
            Cura.ND_tools_Button {
                id:show_layer
                function normal_mode() {return show_layer.checked;}
                height: UM.Theme.getSize("modal_sidebar_button").height*p3
                width: UM.Theme.getSize("modal_sidebar_button").width *p3
                icon.height: UM.Theme.getSize("modal_sidebar_button").width *p3/2
                icon.width: UM.Theme.getSize("modal_sidebar_button").width *p3/2
                checkable: true
                checked: true
                icon.source: UM.Theme.getIcon("stackup_icon")
            }
            Cura.ND_tools_Button {
                id: show_drill
                function normal_mode() {return show_drill.checked;}
                height: UM.Theme.getSize("modal_sidebar_button").height*p3
                width: UM.Theme.getSize("modal_sidebar_button").width *p3
                icon.height: UM.Theme.getSize("modal_sidebar_button").width *p3/2
                icon.width: UM.Theme.getSize("modal_sidebar_button").width *p3/2
                enabled: drillsModel.count > 0
                checkable: drillsModel.count > 0
                checked: drillsModel.count > 0
                icon.source:UM.Theme.getIcon("drill_icon")
            }
        }
        Column {
            Item {
                id: header_frame
                clip: true
                visible: true
                z: 4
                width: {
                    var w = 0
                    w += root.get_left_table_width()
                    w += root.get_right_table_width()
                    w
                }
                height: UM.Theme.getSize("title_box_height").height*p3
                Rectangle {
                    id: headerI_row
                    width: childrenRect.width
                    height: UM.Theme.getSize("title_box_height").height*p3
                    z:3
                    color: UM.Theme.getColor("background_modal_header") // yz: header right
                    Rectangle {
                        width: childrenRect.width
                        height: UM.Theme.getSize("title_box_height").height*p3
                        z:3
                        color: UM.Theme.getColor("background_modal_header") // yz: header left
                        Row {
                            topPadding: 5
                            width: root.get_left_table_width() // yz: header left
                            height: UM.Theme.getSize("title_box_height").height*p3
                            z:3
                            Rectangle{
                                id: rec_padding_left1
                                height: UM.Theme.getSize("title_box_height").height*p3
                                width: UM.Theme.getSize("5px").width *p3
                                color: "transparent"
                            }
                            Cura.ND_tools_Text {
                                id: rect11
                                text: "File Name";
                                width: UM.Theme.getSize("stackup_filename_title").width *p3 - 6
                            }
                            Rectangle{
                                id: div2
                                visible: show_layer.checked
                                height: UM.Theme.getSize("30px").height*p3
                                width: UM.Theme.getSize("1px").width *p3
                                color: "#FAFAFA"
                                anchors.verticalCenter: parent.verticalCenter
                            }
                            Rectangle{
                                id: rec_padding_left2
                                visible: show_layer.checked
                                height: UM.Theme.getSize("title_box_height").height*p3
                                width: UM.Theme.getSize("21px").width *p3
                                color: "transparent"
                            }
                            Cura.ND_tools_Text {
                                text: "Thickness"
                                visible: show_layer.checked
                                enabled: show_layer.checked
                                width: UM.Theme.getSize("txt_layer_thickness").width *p3 + 1
                            }
                            Rectangle{
                                id: div3
                                visible: show_layer.checked
                                height: UM.Theme.getSize("30px").height*p3
                                width: UM.Theme.getSize("1px").width *p3
                                color: "#FAFAFA"
                                anchors.verticalCenter: parent.verticalCenter
                            }
                            Rectangle{
                                id: rec_padding_left3
                                visible: show_layer.checked
                                height: UM.Theme.getSize("title_box_height").height*p3
                                width: UM.Theme.getSize("45px").width *p3
                                color: "transparent"
                            }
                            Cura.ND_tools_Text {
                                text: "Type"
                                visible: show_layer.checked
                                enabled:show_layer.checked
                                width: UM.Theme.getSize("layer_type_cb").width *p3 - 35
                            }
                            Rectangle{
                                id: div4
                                visible: show_layer.checked
                                height: UM.Theme.getSize("30px").height*p3
                                width: UM.Theme.getSize("1px").width *p3
                                color: "#FAFAFA"
                                anchors.verticalCenter: parent.verticalCenter
                            }
                            Rectangle{
                                id: rec_padding_left4
                                visible: show_layer.checked
                                height: UM.Theme.getSize("title_box_height").height*p3
                                width: UM.Theme.getSize("5px").width *p3
                                color: "transparent"
                            }
                            Cura.ND_tools_Text {
                                text: "Route"
                                visible: show_layer.checked
                                enabled:show_layer.checked
                            }
                        }
                    }
                    ListView {
                        id: headerDrillsList
                        model: drillsModel
                        visible: show_drill.checked
                        enabled: show_drill.checked
                        x: root.get_left_table_width()
                        height: UM.Theme.getSize("title_box_height").height*p3
                        width: root.get_right_table_width()
                        orientation: ListView.Horizontal
                        ScrollBar.horizontal: scrollview3
                        cacheBuffer: 500

                        delegate: Row {
                            id: headerDrills
                            property int curindex: index

                            Column{
                                height: UM.Theme.getSize("title_box_height").height*p3
                                z:0
                                Rectangle{
                                    width: UM.Theme.getSize("51px").width *p3
                                    height: UM.Theme.getSize("10px").height*p3
                                    color: "transparent"
                                }
                                Row {
                                    height: childrenRect.height
                                    width: childrenRect.width

                                    MouseArea{
                                        id: diameter_hover
                                        height: UM.Theme.getSize("20px").height*p3
                                        width: UM.Theme.getSize("51px").width *p3
                                        hoverEnabled:true
                                        Cura.ToolTip{
                                            parent: diameter_hover
                                            visible: diameter_hover.containsMouse
                                            tooltipText: "Diameter in mm"
                                        }
                                        Rectangle{
                                            height: UM.Theme.getSize("20px").height*p3
                                            width: UM.Theme.getSize("51px").width *p3
                                            color: "transparent"
                                            border.width: UM.Theme.getSize("default_lining").width *p3
                                            border.color: UM.Theme.getColor("lining")
                                            radius: UM.Theme.getSize("default_radius").width *p3
                                            anchors.verticalCenter: parent.verticalCenter
                                            anchors.horizontalCenter: parent.horizontalCenter
                                            Text {
                                                anchors.horizontalCenter: parent.horizontalCenter
                                                horizontalAlignment: Text.AlignHCenter
                                                verticalAlignment: Text.AlignVCenter
                                                text: diameter || "";
                                                color: UM.Theme.getColor("text_modal")
                                                elide : Text.ElideRight
                                                width: UM.Theme.getSize("51px").width *p3
                                                height: UM.Theme.getSize("20px").height*p3
                                                font: UM.Theme.getFont("default")
                                            }
                                        }
                                    }
                                }
                                Row {
                                    height: childrenRect.height
                                    width: childrenRect.width;
                                    MouseArea{
                                        id:name_hover1
                                        height: childrenRect.height
                                        width: childrenRect.width;
                                        hoverEnabled:true
                                        Cura.ToolTip{
                                            parent: name_hover1
                                            visible: name_hover1.containsMouse
                                            tooltipText: "Show / Hide drill in the preview."
                                        }
                                        // eye - show drill?
                                        Cura.ND_tools_Button{
                                            id: drill_preview
                                            function normal_mode(){
                                                return to_view
                                            }
                                            normal_color: "#00ffffff"
                                            active_color: "#00ffffff"
                                            hovered_color: "#33ffffff"
                                            display : AbstractButton.IconOnly
                                            checkable : true
                                            checked: to_view || false
                                            icon.source: !to_view ? "":"iconfinder_eye-24_103177.svg"
                                            width: UM.Theme.getSize("eye_icon").width *p3
                                            leftPadding: 5
                                            height: UM.Theme.getSize("eye_icon").height*p3
                                            onClicked:{
                                                UM.ActiveTool.triggerActionWithData("modifyPreviewForDrill", index)
                                            }
                                        }
                                    }
                                }
                                Row {
                                    height: childrenRect.height
                                    width: childrenRect.width;
                                    MouseArea{
                                        id:name_hover2
                                        height: childrenRect.height
                                        width: childrenRect.width;
                                        hoverEnabled:true
                                        Cura.ToolTip{
                                            parent: name_hover2
                                            visible: name_hover2.containsMouse
                                            tooltipText: name || "[undefined]"
                                        }
                                        //  drill type
                                        Cura.ND_tools_ComboBox {

                                            left_padding: p==1 ? 5 : 1
                                            width: UM.Theme.getSize("48px").width *p3
                                            height: UM.Theme.getSize("30px").height *p3
                                            currentIndex: type

                                            background_color: UM.Theme.getColor("modal_title_background")
                                            font.pointSize: 11
                                            font.styleName: "Bold"
                                            flat: true
                                            model: vias_types
                                            onActivated: {
                                                drillsModel.setProperty(headerDrills.curindex,"type", currentIndex)

                                                updatePython()
                                                updateQT()
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            Flickable {
                id: flickable
                x: 0
                y: 0
                width: header_frame.width
                interactive: true
                height: UM.Theme.getSize("310px").height*p3
                maximumFlickVelocity : 20
                pixelAligned: true
                flickableDirection: Flickable.HorizontalAndVerticalFlick
                layer.enabled: true
                boundsBehavior: Flickable.StopAtBounds
                boundsMovement: Flickable.StopAtBounds
                onFlickEnded:{
                    view_x = scrollview1.position
                    view_y = scrollview3.position
                }
                onMovementEnded:{
                    view_x = scrollview1.position
                    view_y = scrollview3.position
                }
                ScrollBar.horizontal: ScrollBar {
                    id: scrollview3;
                    function returnToBounds(){}
                    visible: show_drill.checked && drillsModel.count > 8
                    x: 0
                    y: 0
                    height: UM.Theme.getSize("12px").height*p3
                    active: true
                    topPadding: 1
                    bottomPadding: 1
                    leftPadding: root.get_left_table_width();
                    rightPadding: 1
                    enabled: true
                    interactive: true
                    wheelEnabled: true;
                    orientation: Qt.Horizontal;
                    focusPolicy: Qt.WheelFocus;
                    policy: ScrollBar.AlwaysOn
                    snapMode: ScrollBar.NoSnap
                }
                ScrollBar.vertical: ScrollBar {
                    id: scrollview1
                    visible: layersModel.count > 8
                    property real pageSize : layersModel.count*40

                    interactive: false
                    height: UM.Theme.getSize("310px").height*p3
                    wheelEnabled: true
                    policy: ScrollBar.AlwaysOn
                    focusPolicy: Qt.WheelFocus
                    snapMode :ScrollBar.NoSnap
                    active: true
                }
                Row {
                    id : scrollview2
                    // todo: add "visible: needed"
                    width: header_frame.width
                    height: UM.Theme.getSize("310px").height*p3
                    z: 1
                    Component {
                        id: highlight
                        Rectangle { // yz: selection shadow
                            width: header_frame.width
                            height: UM.Theme.getSize("layer_row").height*p3
                            color: "white";
                            radius: 5
                            z: 5
                            opacity: 0.25
                            y: listView1.currentItem.y
                        }
                    }
                    ListView {
                        id: listView1
                        property var i : 0
                        property int current: 0
                        property int currentCB: 0
                        property var roww : -1
                        property var cloumn : -1
                        property var roww_prev : -1
                        property var cloumn_prev : -1
                        currentIndex: UM.ActiveTool.properties.getValue("SelectedLayerIndex")
                        z: 1
                        cacheBuffer: 1000
                        width: root.get_left_table_width();
                        height: UM.Theme.getSize("310px").height*p3
                        ScrollBar.vertical: scrollview1
                        interactive: true
                        flickableDirection: Flickable.VerticalFlick
                        boundsBehavior: Flickable.StopAtBounds
                        highlight: highlight
                        highlightFollowsCurrentItem: false
                        boundsMovement: Flickable.StopAtBounds
                        model: layersModel
                        onMovementEnded:{
                            view_x = scrollview1.position
                            view_y = scrollview3.position
                        }

                        delegate: Loader {
                            asynchronous:false
                            sourceComponent: Rectangle {
                                id: main_row
                                width: root.get_left_table_width()
                                height: UM.Theme.getSize("layer_row").height*p3
                                z: 1
                                property var ix : index
                                function isItError(error) {
                                    return (errors & error) === error;
                                }
                                MouseArea {
                                    id: dragArea
                                    drag.minimumY: listView1.y + headerI_row.height
                                    drag.maximumY: listView1.y + Math.min(UM.Theme.getSize("310px").height*p3 - UM.Theme.getSize("layer_row").height*p3, (listView1.model.count-1)*UM.Theme.getSize("layer_row").height*p3 ) + headerI_row.height
                                    property bool held: false
                                    property int from: 0
                                    property int to: 0
                                    acceptedButtons: Qt.LeftButton | Qt.RightButton
                                    hoverEnabled: true

                                    anchors { left: parent.left; right: parent.right }
                                    height: UM.Theme.getSize("layer_row").height*p3
                                    width: root.get_left_table_width()
                                    drag.target: {
                                        content_drag
                                        held ? content_drag : undefined
                                    }
                                    drag.axis: Drag.YAxis
                                    onClicked: {
                                        select_layer(index)
                                        if (mouse.button === Qt.RightButton && name!=="Prepreg")
                                            menu.popup()
                                    }
                                    Menu {
                                        id: menu
                                        Cura.ND_tools_MenuItem{
                                            text: "Add Dielectric"
                                            onTriggered: {
                                                if (listView1.current >= 0){
                                                    UM.ActiveTool.triggerActionWithData("addDielectric" , listView1.current)
                                                }
                                            }
                                        }
                                        Cura.ND_tools_MenuItem{
                                            text: "Move to Routes"
                                            onTriggered: {
                                                if (listView1.current >= 0){
                                                    var path_del = listView1.model.getItem(listView1.currentIndex).path
                                                    UM.ActiveTool.triggerActionWithData("itemToRoutes" , path_del)
                                                    updateQT()
                                                }
                                            }
                                        }
                                        Cura.ND_tools_MenuItem{
                                            text: "Move to Drills"
                                            onTriggered: {
                                                if (listView1.current >= 0){
                                                    var path_del = listView1.model.getItem(listView1.currentIndex).path
                                                    UM.ActiveTool.triggerActionWithData("itemToDrill" , path_del)
                                                    updateQT()
                                                }
                                            }
                                        }
                                        Cura.ND_tools_MenuItem{
                                            text: "Show All Layers"
                                            onTriggered: {
                                                if (listView1.current >= 0){
                                                    UM.ActiveTool.triggerAction("previewShowAllLayers")
                                                    updateQT()
                                                }
                                            }
                                        }
                                        Cura.ND_tools_MenuItem{
                                            text: "Hide All Layers"
                                            onTriggered: {
                                                if (listView1.current >= 0){
                                                    UM.ActiveTool.triggerAction("previewHideAllLayers")
                                                    updateQT()
                                                }
                                            }
                                        }
                                    }
                                    onPressed: {
                                        select_layer(index)
                                        listView1.current = index;
                                    }
                                    onPressAndHold:{
                                        from = index
                                        held = true ;
                                        listView1.i = index
                                    }
                                    onPositionChanged: parent
                                    onEntered: {
                                        view_x = scrollview1.position
                                        view_y = scrollview3.position
                                        listView1.currentCB = index;
                                        listView1.i = index
                                    }
                                    onReleased: {
                                        select_layer(index)
                                        if (held){
                                            held = false
                                            ////updatePython()
                                            ////updateQT()
                                        }
                                    }

                                    Rectangle {
                                        id: content_drag
                                        width: root.get_left_table_width()
                                        z:5
                                        anchors {
                                            horizontalCenter: parent.horizontalCenter
                                            verticalCenter: parent.verticalCenter
                                        }
                                        height: UM.Theme.getSize("layer_row").height*p3
                                        border.color: UM.Theme.getColor("lining_stackup")
                                        border.width: UM.Theme.getSize("default_lining").width *p3
                                        color: {
                                            name == "Prepreg" ? UM.Theme.getColor("dielectric_color") :
                                            isItError(layerErrors.ProcessError) ? UM.Theme.getColor("setting_validation_error_background") : UM.Theme.getColor("conductive_color")
                                        }

                                        Behavior on border.color {
                                            ColorAnimation { duration: 100 }
                                        }
                                        Drag.active: dragArea.held
                                        Drag.source: dragArea
                                        Drag.hotSpot.x: width / 2
                                        Drag.hotSpot.y: height / 2
                                        Drag.keys: ["pin"]
                                        Drag.onDragFinished:{
                                            updatePython()
                                            updateQT()
                                        }
                                        states: State {
                                            when: dragArea.held

                                            ParentChange {
                                                target: content_drag; parent: root
                                            }
                                            AnchorChanges {
                                                target: content_drag
                                                anchors { horizontalCenter: undefined; verticalCenter: undefined }
                                            }
                                        }

                                        Row {
                                            id: column
                                            anchors { fill: parent; margins: 5 }
                                            MouseArea{
                                                id: tooltip_filename_area
                                                hoverEnabled: true
                                                width: lbl_filename.width
                                                height: lbl_filename.height
                                                onClicked: {
                                                    mouse.accepted = false;
                                                }
                                                onPressed:{
                                                    mouse.accepted = false;
                                                }
                                                onPressAndHold:{
                                                    mouse.accepted = false;
                                                }
                                                onPositionChanged:{
                                                    mouse.accepted = false;
                                                }
                                                onEntered:{
                                                    dragArea.entered();
                                                }
                                                onReleased:{
                                                    mouse.accepted = false;
                                                }
                                                Cura.ToolTip {
                                                    parent: tooltip_filename_area
                                                    visible: model.name!=="Prepreg" && parent.containsMouse && lbl_filename.truncated
                                                    tooltipText: model.name

                                                    x: parent.x
                                                    y: Math.round(parent.y - parent.height *2)
                                                    targetPoint: Qt.point(parent.x, Math.round(parent.y + parent.height / 2))
                                                    background_color: UM.Theme.getColor("background_modal_tooltip")
                                                    text_color: UM.Theme.getColor("text_modal")
                                                }
                                                Text {
                                                    id: lbl_filename
                                                    width: UM.Theme.getSize("stackup_filename_title").width *p3;
                                                    height: UM.Theme.getSize("txt_layer_thickness").height *p3;
                                                    text: {
                                                            if (model.type === "Signal")
                                                                return ("(" + (layersModel.getItem(index)["signal_index"]) + ")" + " " + model.name + "   ");
                                                            else
                                                               return model.name + "   ";
                                                    }
                                                    elide: Text.ElideMiddle
                                                    color: UM.Theme.getColor("text_modal")
                                                    verticalAlignment: Text.AlignVCenter
                                                    anchors.verticalCenter: parent.verticalCenter
                                                }
                                            }
                                            Rectangle{
                                                id: input_container
                                                anchors.verticalCenter: parent.verticalCenter
                                                width:  childrenRect.width
                                                height: UM.Theme.getSize("txt_layer_thickness").height*p3
                                                color: {
                                                    isItError(layerErrors.ThicknessError) ?
                                                        UM.Theme.getColor("setting_validation_error_background") :
                                                    isItError(layerErrors.ThicknessWarning) ?
                                                        UM.Theme.getColor("setting_validation_warning_background") :
                                                    "transparent"
                                                }
                                                border.color: {
                                                    isItError(layerErrors.ThicknessError) ?
                                                        UM.Theme.getColor("setting_validation_error") :
                                                    isItError(layerErrors.ThicknessWarning) ?
                                                        UM.Theme.getColor("setting_validation_warning") :
                                                    name === "Prepreg" ?
                                                        UM.Theme.getColor("conductive_color") :
                                                    UM.Theme.getColor("lining")
                                                }
                                                border.width: UM.Theme.getSize("default_lining").width *p3
                                                radius: UM.Theme.getSize("setting_control_radius").width *p3
                                                visible: show_layer.checked
                                                Row {
                                                    anchors.verticalCenter: parent.verticalCenter
                                                    Rectangle{
                                                        color: "transparent"
                                                        width: UM.Theme.getSize("6px").width *p3
                                                        height: UM.Theme.getSize("txt_layer_thickness").height*p3
                                                    }

                                                    MouseArea {
                                                        id: input_mouse_area
                                                        width: UM.Theme.getSize("txt_layer_thickness").width *p3
                                                        height: UM.Theme.getSize("txt_layer_thickness").height*p3
                                                        hoverEnabled: true

                                                        cursorShape: Qt.IBeamCursor
                                                        TextInput {
                                                            mouseSelectionMode: TextInput.SelectCharacters
                                                            selectByMouse: true
                                                            selectionColor: UM.Theme.getColor("primary")//"#0CA9E3"
                                                            selectedTextColor: UM.Theme.getColor("background_modal")
                                                            anchors.verticalCenter: parent.verticalCenter
                                                            verticalAlignment: Text.AlignVCenter

                                                            id: textInput
                                                            width: UM.Theme.getSize("txt_layer_thickness").width *p3
                                                            height: UM.Theme.getSize("txt_layer_thickness").height*p3

                                                            text: qsTr(fix_number(thickness))

                                                            font: UM.Theme.getFont("default")
                                                            color: UM.Theme.getColor("text_modal")

                                                            onTextEdited: {
                                                                var value = fix_number(textInput.text);
                                                                listView1.model.setProperty(index, "thickness", value) ;
                                                            }
                                                            onEditingFinished:{
                                                                UM.ActiveTool.triggerActionWithData("updateLayerThickness",
                                                                    { "layer_index": index, "thickness": textInput.text })
                                                                parent.forceActiveFocus();
                                                            }

                                                            Text {
                                                                color: UM.Theme.getColor("text_modal")
                                                                height: UM.Theme.getSize("txt_layer_thickness").height*p3 ;
                                                                anchors.verticalCenter: parent.verticalCenter
                                                                verticalAlignment: Text.AlignVCenter
                                                                anchors.right: parent.right
                                                                text:"um"
                                                            }
                                                        }
                                                    }
                                                    Rectangle{
                                                        color: "transparent"
                                                        width: UM.Theme.getSize("6px").width *p3
                                                        height: UM.Theme.getSize("txt_layer_thickness").height*p3
                                                    }
                                                }
                                            }
                                            Rectangle{
                                                id: div1
                                                width: UM.Theme.getSize("vertical_div").width *p3
                                                height: textInput.height
                                                color: "transparent"
                                            }
                                            // Layer Type ComboBox
                                            Cura.ND_tools_ComboBox {
                                                id: cmb_layer_type
                                                property var error: false;
                                                property var warning: false;
                                                border.color: {
                                                    if (isItError(layerErrors.TypeError))
                                                        return UM.Theme.getColor("setting_validation_error");
                                                    else if (isItError(layerErrors.TypeWarning))
                                                        return UM.Theme.getColor("setting_validation_warning");
                                                    else if(name === "Prepreg")
                                                        return UM.Theme.getColor("conductive_color")
                                                    else
                                                        return UM.Theme.getColor("lining")
                                                }
                                                background_color: {
                                                    if (isItError(layerErrors.TypeError))
                                                        return UM.Theme.getColor("setting_validation_error_background");
                                                    else if (isItError(layerErrors.TypeWarning))
                                                        return UM.Theme.getColor("setting_validation_warning_background");
                                                    else
                                                        return content_drag.color
                                                }
                                                visible: show_layer.checked
                                                enabled: cmb_layer_type.model.count > 1 && name!=="Prepreg"
                                                indicator: UM.RecolorImage {
                                                    id: downArrow
                                                    x: cmb_layer_type.width - width - cmb_layer_type.rightPadding
                                                    y: cmb_layer_type.topPadding + Math.round((cmb_layer_type.availableHeight - height) / 2)

                                                    source: UM.Theme.getIcon("arrow_bottom")
                                                    width: UM.Theme.getSize("standard_arrow").width *p3 * screenScaleFactor
                                                    height: UM.Theme.getSize("standard_arrow").height*p3 * screenScaleFactor
                                                    sourceSize.width: width + 5
                                                    sourceSize.height: width + 5
                                                    visible: enabled
                                                    color: UM.Theme.getColor("setting_control_button")
                                                }
                                                width: UM.Theme.getSize("layer_type_cb").width *p3
                                                height: UM.Theme.getSize("layer_type_cb").height*p3
                                                currentIndex: {
                                                    var i = 0;
                                                    for (;i<all_types.count;i++){
                                                        if (all_types.items[i]["type_name"] == type){
                                                            break;
                                                        }
                                                    }
                                                    if (i==all_types.count){
                                                        return 0;
                                                    }
                                                    return i;
                                                }
                                                model: all_types
                                                textRole: "type_name"
                                                displayText: {if (name==="Prepreg") "Prepreg"; }
                                                property int hope: 0
                                                popup.onOpened:{
                                                    select_layer(listView1.currentCB)
                                                    hope = listView1.currentCB
                                                }
                                                onActivated: {
                                                    UM.ActiveTool.triggerActionWithData("updateLayerType",
                                                        { "layer_index": listView1.currentIndex, "layer_type": currentIndex })
                                                    parent.forceActiveFocus();
                                                }
                                                property var minimum_value_warning: Number.NEGATIVE_INFINITY
                                                property var maximum_value_warning: Number.POSITIVE_INFINITY
                                                property var minimum_value: Number.NEGATIVE_INFINITY
                                                property var maximum_value: Number.POSITIVE_INFINITY
                                            }
                                            anchors.verticalCenter: parent.verticalCenter
                                            Rectangle{
                                                id: div5
                                                width: UM.Theme.getSize("vertical_div").width *p3
                                                height: textInput.height
                                                color: "transparent"
                                            }
                                            Cura.ND_tools_ComboBox {
                                                // Route ComboBox/DropDownList
                                                id: cmb_route
                                                currentIndex: route
                                                width: UM.Theme.getSize("route_combobox").width *p3;
                                                height: UM.Theme.getSize("route_combobox").height*p3;
                                                border.color: {name === "Prepreg" ? UM.Theme.getColor("conductive_color") : UM.Theme.getColor("lining")}
                                                textRole: "name"
                                                model: routesModel
                                                visible: show_layer.checked
                                                property int hope: 0
                                                background_color: content_drag.color
                                                popup.onOpened:{
                                                    select_layer(listView1.currentCB)
                                                    hope = listView1.currentCB
                                                }
                                                indicator: UM.RecolorImage {
                                                    id: downArrow_route
                                                    x: cmb_route.width - width - cmb_route.rightPadding
                                                    y: cmb_route.topPadding + Math.round((cmb_route.availableHeight - height) / 2)

                                                    source: UM.Theme.getIcon("arrow_bottom")
                                                    width: UM.Theme.getSize("standard_arrow").width *p3 * screenScaleFactor
                                                    height: UM.Theme.getSize("standard_arrow").height*p3 * screenScaleFactor
                                                    sourceSize.width: width + 5
                                                    sourceSize.height: width + 5
                                                    visible: enabled
                                                    color: UM.Theme.getColor("setting_control_button")
                                                }
                                                clip: true
                                                onActivated: {
                                                    UM.ActiveTool.triggerActionWithData("updateLayerRoute", [hope, currentIndex])
                                                }
                                            }
                                            Rectangle{
                                                id: div6
                                                width: UM.Theme.getSize("vertical_div").width *p3 - 5
                                                height: textInput.height
                                                color: "transparent"
                                            }
                                            // show layer?
                                            Cura.ND_tools_Button{
                                                id: gerber_preview

                                                function normal_mode(){
                                                    return to_view
                                                }
                                                normal_color: content_drag.color
                                                active_color: "#00ffffff"
                                                hovered_color: "#33ffffff"

                                                display : AbstractButton.IconOnly
                                                checkable : true
                                                visible: show_layer.checked
                                                checked: to_view
                                                icon.source: to_view===false ? "":"iconfinder_eye-24_103177.svg"
                                                width: UM.Theme.getSize("eye_icon").width *p3
                                                height: UM.Theme.getSize("eye_icon").height*p3
                                                leftPadding:5
                                                onClicked:{
                                                    UM.ActiveTool.triggerActionWithData("modifyPreviewForLayer", index)
                                                }
                                            }
                                        }
                                    }

                                    DropArea {
                                        id: dragTarget
                                        keys: ["pin"]
                                        anchors { fill: dragArea; margins: 0}
                                        onEntered: {
                                            if (layer_is_moving){
                                                return;
                                            }
                                            if(listView1.current !== listView1.currentIndex){
                                                return;
                                            }

                                            listView1.current = index

                                            listView1.model.moveRow(
                                                layersModel.index(listView1.currentIndex,0),
                                                listView1.currentIndex,
                                                layersModel.index(index,0),
                                                index
                                            )
                                            view_x = scrollview1.position
                                            view_y = scrollview3.position
                                            //}

                                            layer_is_moving = false;
                                        }
                                    }
                                }
                            }
                        }
                    }
                    ListView{
                        id: listView2
                        property var curr_column: 0
                        x: root.get_left_table_width()
                        z: 0
                        width: root.get_right_table_width()
                        height: UM.Theme.getSize("310px").height*p3
                        visible: show_drill.checked
                        interactive: true
                        ScrollBar.horizontal: scrollview3
                        flickableDirection: Flickable.VerticalFlick
                        boundsBehavior: Flickable.StopAtBounds
                        maximumFlickVelocity:25
                        cacheBuffer: 200
                        orientation: ListView.Horizontal
                        snapMode :ListView.NoSnap
                        boundsMovement: Flickable.StopAtBounds
                        model: Cura.DrillsPcb{id:drillsModel}
                        delegate:
                        Loader {
                            asynchronous:false
                            sourceComponent:
                            MouseArea {
                                property int currentIndex2: index
                                id: mouseArea
                                hoverEnabled : true
                                width: UM.Theme.getSize("51px").width *p3
                                height: UM.Theme.getSize("310px").height*p3
                                acceptedButtons: Qt.LeftButton | Qt.RightButton
                                onClicked: {
                                    listView2.currentIndex = index
                                    if (is_excellon == false){
                                        excellonSettings.enabled = false
                                    }
                                    view_x = scrollview1.position
                                    view_y = scrollview3.position

                                    if (mouse.button === Qt.RightButton)
                                        menu_drill.popup()

                                }
                                Menu {
                                    id: menu_drill
                                    Cura.ND_tools_MenuItem{
                                        text: "Move to Stackup"
                                        onTriggered: {
                                            if (currentIndex2 >= 0){
                                                listView1.cloumn = -1;    // resetting column (cloumn) to prevent index out of bounds
                                                var path_del = drillsModel.getItem(currentIndex2).path
                                                UM.ActiveTool.triggerActionWithData("drillToStackup" , path_del)
                                                updateQT()
                                            }
                                        }
                                    }
                                    Cura.ND_tools_MenuItem{
                                        text: "Remove Drill"
                                        onTriggered: {
                                            if (currentIndex2 >= 0){
                                                listView1.cloumn = -1;    // resetting column (cloumn) to prevent index out of bounds
                                                var path_del = drillsModel.getItem(currentIndex2).path;
                                                var n = drillsModel.count;
                                                view_y = Math.min(1-8/Math.max(n-1, 8),scrollview3.position*n/(n-1));
                                                UM.ActiveTool.triggerActionWithData("removeDrill" , path_del);
                                                updateQT();
                                                scrollview3.width = 8/Math.max(n-1, 8)
                                            }
                                        }
                                    }
                                    Cura.ND_tools_MenuItem{
                                        text: "Mark this Drill"
                                        onTriggered: {
                                            if (currentIndex2 >= 0){
                                                markAll()
                                                updateQT()
                                            }
                                        }
                                    }
                                    Cura.ND_tools_MenuItem{
                                        text: "Unmark this Drill"
                                        onTriggered: {
                                            if (currentIndex2 >= 0){
                                                unmarkAll()
                                                updateQT()
                                            }
                                        }
                                    }
                                    Cura.ND_tools_MenuItem{
                                        text: "Mark From To"
                                        onTriggered: {
                                            if (currentIndex2 >= 0){
                                                UM.ActiveTool.triggerActionWithData("markFromToByDialogue" , currentIndex2)
                                                updatePython()
                                            }
                                        }
                                    }
                                    Cura.ND_tools_MenuItem{
                                        text: "Update Diameter"
                                        onTriggered: {
                                            if (currentIndex2 >= 0){
                                                UM.ActiveTool.triggerActionWithData("updateDrillDiameterByDialogue" , currentIndex2)
                                                updatePython()
                                            }
                                        }
                                    }
                                    Cura.ND_tools_MenuItem{
                                        id:excellonSettings
                                        text: "Excellon Settings"
                                        onTriggered: {
                                            if (currentIndex2 >= 0){
                                                UM.ActiveTool.triggerActionWithData("showDrillsSettingsDialog" , currentIndex2)
                                                updatePython()
                                            }
                                        }
                                    }
                                    Cura.ND_tools_MenuItem{
                                        text: "Show All Drills"
                                        onTriggered: {
                                            if (listView1.current >= 0){
                                                var path_del = listView1.model.getItem(listView1.current).path
                                                UM.ActiveTool.triggerAction("previewShowAllDrills")
                                                updateQT()
                                            }
                                        }
                                    }
                                    Cura.ND_tools_MenuItem{
                                        text: "Hide All Drills"
                                        onTriggered: {
                                            if (listView1.current >= 0){
                                                var path_del = listView1.model.getItem(listView1.current).path
                                                UM.ActiveTool.triggerAction("previewHideAllDrills")
                                                updateQT()
                                            }
                                        }
                                    }
                                }
                                onEntered: {
                                    listView2.curr_column = currentIndex2
                                    view_x = scrollview1.position
                                    view_y = scrollview3.position
                                }
                                onPositionChanged: {
                                    view_x = scrollview1.position
                                    view_y = scrollview3.position
                                }
                                ListView{
                                    id:listView3
                                    ScrollBar.vertical: scrollview1
                                    property int currentIndex: index
                                    flickableDirection: Flickable.VerticalFlick
                                    boundsBehavior: Flickable.StopAtBounds
                                    boundsMovement: Flickable.StopAtBounds
                                    snapMode: ListView.NoSnap
                                    cacheBuffer: 500
                                    Component.onCompleted: {
                                        //positionViewAtIndex(listView1.roww, ListView.Visible )
                                        scrollview1.position = view_x
                                        // scrollview3.position = view_y
                                    }
                                    model: layers
                                    width: UM.Theme.getSize("51px").width *p3
                                    height: UM.Theme.getSize("310px").height*p3
                                    delegate:
                                    Loader {
                                        asynchronous:false
                                        sourceComponent:
                                        Rectangle {
                                            id: drill_column
                                            width: UM.Theme.getSize("51px").width *p3
                                            height: UM.Theme.getSize("layer_row").height*p3
                                            opacity: selected ? 0.8 : 1

                                            radius: UM.Theme.getSize("setting_control_radius").width *p3
                                            Rectangle {
                                                property int currentIndexs: index
                                                width: UM.Theme.getSize("51px").width *p3
                                                height: UM.Theme.getSize("layer_row").height*p3

                                                border.color:{
                                                    drillError ? UM.Theme.getColor("setting_validation_error") :
                                                    is_out_of_route ? UM.Theme.getColor("setting_validation_warning") :
                                                    UM.Theme.getColor("lining_stackup")
                                                }
                                                border.width: UM.Theme.getSize("default_lining").width *p3
                                                color: {
                                                    drillError ? UM.Theme.getColor("setting_validation_error_background") :
                                                    is_out_of_route ?  UM.Theme.getColor("setting_validation_error_background"):
                                                    layersModel.getItem(index)["name"] == "Prepreg" ? UM.Theme.getColor("dielectric_color") : UM.Theme.getColor("conductive_color")
                                                }

                                                MouseArea {
                                                    id: mouseArea1
                                                    z:10000000
                                                    height: UM.Theme.getSize("layer_row").height*p3
                                                    hoverEnabled : true
                                                    width: UM.Theme.getSize("51px").width *p3
                                                    onEntered: {
                                                        listView1.i = index
                                                        view_x = scrollview1.position
                                                        view_y = scrollview3.position
                                                    }
                                                    onPositionChanged: {
                                                        view_x = scrollview1.position
                                                        view_y = scrollview3.position
                                                    }
                                                    Rectangle {
                                                        width: UM.Theme.getSize("1px").width *p3
                                                        height: UM.Theme.getSize("layer_row").height*p3
                                                        color: 'black'
                                                    }
                                                    RadioButton {
                                                        id: radioButton1
                                                        checked: state_bool
                                                        anchors.centerIn: parent
                                                        width: UM.Theme.getSize("51px").width *p3
                                                        onReleased: {
                                                            listView1.i = index
                                                            select_layer(listView1.i)
                                                            listView1.cloumn_prev = listView1.cloumn
                                                            listView1.cloumn = listView2.curr_column
                                                            if(radioButton1.checked){
                                                                if (listView1.roww_prev !== -1){
                                                                    listView1.roww = listView1.i
                                                                    listView2.model.getItem(listView1.cloumn).layers.setProperty(index,"state_bool",true)
                                                                    markFromTo()
                                                                    listView1.roww_prev = -1
                                                                    listView2.model.setProperty(listView1.cloumn,"selected",false)
                                                                    color_row = false
                                                                }
                                                                else{
                                                                    listView1.roww_prev = listView1.i
                                                                    listView2.model.setProperty(listView1.cloumn,"selected",true)
                                                                    color_row = true
                                                                    listView2.model.getItem(listView1.cloumn).layers.setProperty(index,"state_bool",true)
                                                                }
                                                            }
                                                            else {
                                                                if (selected ===true){
                                                                    listView1.roww = listView1.i
                                                                    listView2.model.setProperty(listView1.cloumn,"selected",false)
                                                                    color_row = false
                                                                    listView2.model.getItem(listView1.cloumn).layers.setProperty(index,"state_bool",false)
                                                                    if (listView1.roww_prev !== listView1.roww){
                                                                        markFromTo()
                                                                        listView2.model.getItem(listView1.cloumn).layers.setProperty(index,"state_bool",true)
                                                                    }
                                                                    listView1.roww_prev = -1
                                                                }
                                                                else{
                                                                    if (listView1.cloumn_prev ===-1){
                                                                        listView1.cloumn_prev =listView1.cloumn
                                                                    }
                                                                    listView1.roww = listView1.i
                                                                    listView2.model.setProperty(listView1.cloumn_prev,"selected",false)
                                                                    color_row = false
                                                                    listView2.model.getItem(listView1.cloumn).layers.setProperty(listView1.roww,"state_bool",false)
                                                                    if (listView1.cloumn !== listView1.cloumn_prev && listView1.roww_prev != -1){
                                                                        listView2.model.getItem(listView1.cloumn).layers.setProperty(listView1.roww,"state_bool",true)
                                                                        listView2.model.getItem(listView1.cloumn_prev).layers.setProperty(listView1.roww_prev,"state_bool",false)
                                                                    }
                                                                    listView1.roww_prev = -1
                                                                }
                                                            }
                                                            updatePython()
                                                            updateQT()
                                                        }
                                                        indicator:
                                                        Rectangle {
                                                            anchors.centerIn: parent
                                                            implicitWidth: 26
                                                            implicitHeight: UM.Theme.getSize("layer_row").height*p3
                                                            color: "transparent"
                                                            border.width: 0
                                                            Rectangle {
                                                                anchors.centerIn: parent
                                                                width: UM.Theme.getSize("22px").width *p3
                                                                height: UM.Theme.getSize("layer_row").height*p3
                                                                x: 6
                                                                y: 6
                                                                border.width: 0
                                                                visible: radioButton1.checked
                                                                color:"transparent"//radioButton1.down? "transparent":"white"
                                                                Item{
                                                                    width: UM.Theme.getSize("22px").width *p3
                                                                    height: UM.Theme.getSize("layer_row").height*p3
                                                                    RadialGradient  {
                                                                        horizontalOffset: 0
                                                                        angle: 0
                                                                        verticalOffset: 0
                                                                        anchors.fill: parent
                                                                        horizontalRadius: 20
                                                                        verticalRadius: 300
                                                                        gradient:
                                                                        Gradient  { ///["PTH", "TH","VIA"]
                                                                            GradientStop {
                                                                                position:0.0
                                                                                color: {
                                                                                    if(type===0)
                                                                                    {
                                                                                        "#000000"
                                                                                    }
                                                                                    else if(type===2)
                                                                                    {
                                                                                        "#ffffff"
                                                                                    }
                                                                                    else if(type===1)
                                                                                    {
                                                                                        "#000000"
                                                                                    }
                                                                                    else
                                                                                    {
                                                                                        "00FF00"
                                                                                    }
                                                                                }
                                                                            }

                                                                            GradientStop {
                                                                                position: 0.5
                                                                                color: {
                                                                                    if(type===0)
                                                                                    {
                                                                                        "#ffffff"
                                                                                    }
                                                                                    else if(type===2)
                                                                                    {
                                                                                        "#313131"
                                                                                    }
                                                                                    else if(type===1)
                                                                                    {
                                                                                        "transparent"
                                                                                    }
                                                                                }
                                                                            }

                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                        checkable: true
                                                        autoExclusive: false
                                                        wheelEnabled: false
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        FileDialog {
            id: bush
            modality: Qt.WindowModal
            nameFilters: [ "PCB Files (*.PCBPRJ *.BRD *.PCB *.GTO *.PLC *.TSK ST.PHO *.GTS *.STC *.TSM F.MASK *.SMT MT.PHO *.GTP *.CRC *.TSP F.PASTE *.SPT PT.PHO *.GTL *.CMP *.TOP F.CU *.TOP L1.PHO *.G? *.LY? *.IN? L3.PHO *.GP? *.GBL *.SOL *.BOT B.CU *.BOT L2.PHO L4.PHO *.GPB *.CRS *.BSP B.PASTE *.SPB PB.PHO *.GBS *.STS *.BSM B.MASK *.SMB MB.PHO *.GBO *.PLS *.BSK B.SILK *.SSB SB.PHO *.GD1 *.DIM *.MIL *.GML *.GG1 *.DIM *.MIL *.GML *.GKO *.GM1 *.GM3 *.FAB *.DRL *.TXT *.TAP)" ,
	                        "All files (*)" ]
            selectMultiple: true
            folder:
            {
                //Because several implementations of the file dialog only update the folder when it is explicitly set.
                folder = CuraApplication.getDefaultPath("dialog_load_path");
                return CuraApplication.getDefaultPath("dialog_load_path");
            }
            onAccepted:
            {
                var f = folder;
                folder = f;
                CuraApplication.setDefaultPath("dialog_load_path", folder);
                UM.ActiveTool.triggerActionWithData("addLayer" , fileUrls)
            }
        }
        Connections{
            target: UM.ActiveTool
            onPropertiesChanged:
            {   
                layersModel._update();
                drillsModel._update();
                scrollview1.position = view_x
                scrollview3.position = view_y
                listView1.currentIndex = UM.ActiveTool.properties.getValue("SelectedLayerIndex") || 0
            }
            onPropertiesChangedB:{
                refreshimage()
            }
        }
        Connections
        {
            target: Cura.MachineManager
            onGlobalContainerChanged:
            {
                outputDevice = Cura.MachineManager.printerOutputDevices.length >= 1 ? Cura.MachineManager.printerOutputDevices[0] : null;
            }
        }
    }

    Action
    {
        id: undoAction;
        text: catalog.i18nc("@action:inmenu menubar:edit", "&Undo");
        shortcut: StandardKey.Undo;
        onTriggered: UM.ActiveTool.triggerAction("undo");
        enabled: true;
    }

    Action
    {
        id: redoAction;
        text: catalog.i18nc("@action:inmenu menubar:edit", "&Redo");
        shortcut: StandardKey.Redo;
        onTriggered: UM.ActiveTool.triggerAction("redo");
        enabled: true;
    }
}
