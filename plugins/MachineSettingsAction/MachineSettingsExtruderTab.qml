// Copyright (c) 2019 Ultimaker B.V.
// Cura is released under the terms of the LGPLv3 or higher.

import QtQuick 2.10
import QtQuick.Controls 2.3

import UM 1.3 as UM
//import Cura 1.1 as Cura
import Cura 1.5 as Cura


//
// This component contains the content for the "Welcome" page of the welcome on-boarding process.
//
Item
{
    id: base
    UM.I18nCatalog { id: catalog; name: "cura" }

    anchors.left: parent.left
    anchors.right: parent.right
    anchors.top: parent.top

    property int labelWidth: 210 * screenScaleFactor
    property int controlWidth: (UM.Theme.getSize("setting_control").width * 3 / 4) | 0
    property var labelFont: UM.Theme.getFont("default")

    property int columnWidth: ((parent.width - 2 * UM.Theme.getSize("default_margin").width) / 2) | 0
    property int columnSpacing: 3 * screenScaleFactor
    property int propertyStoreIndex: manager ? manager.storeContainerIndex : 1  // definition_changes

    property string extruderStackId: ""
    property int  extruderPosition:0
    property var forceUpdateFunction: manager.forceUpdate

    function updateMaterialDiameter()
    {
        manager.updateMaterialForDiameter(extruderPosition)
                    console.log(extruderPosition);

    }
    Label
    {
        id: titleLabel
        anchors
        {
            top: parent.top
            left: parent.left
            right: parent.right
            margins: 5 * screenScaleFactor
        }
        font.pointSize: 14
        text: catalog.i18nc("@title:tab", "Select material:")
        color:"white"
    }

    Cura.MaterialsListForExtroder
            {
                id: materialListView
                extroderIndex123:extruderPosition
                width: base.columnWidth - UM.Theme.getSize("default_margin").width
                anchors.top: titleLabel.bottom
                anchors.left: parent.left
                anchors.right: parent.right
                anchors.margins: UM.Theme.getSize("default_margin").width
            }


}
