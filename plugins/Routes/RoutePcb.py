from PyQt5.QtCore import Qt
from UM.Logger import Logger
from UM.Message import Message
from UM.Qt.ListModel import ListModel
from UM.Scene.Selection import Selection


class RoutePcb(ListModel):
    # to keep the last RoutePcb instance created by the QML
    # meaning:
    # the QML is the one creating this instance and hold it.
    # the Python need to use it, so keep it in creation.
    # at any time the python holds the last
    # created RoutePcb instance cuz the QML can arbitrary decide to rebuild it
    instance = None

    @staticmethod
    def update_all():
        try:
            if RoutePcb.instance:
                RoutePcb.instance._update()
                Logger.info("Updated ROUTES PCB")
        except RuntimeError:
            Logger.warning("Couldn't update Routes PCB - the routes tool is already closed by now")


    def __init__(self, parent=None):
        super().__init__(parent)

        self.addRoleName(Qt.UserRole + 1, "name")
        self.addRoleName(Qt.UserRole + 2, "path")
        self._error_message = Message()

        # the route tool is only opened when a single pcb model is selected
        self._node = Selection.getSelectedObject(0)
        if self._node is not None and not self._node.callDecoration("isPcbModel"):
            self._node = None

        self._update()
        RoutePcb.instance = self

    def _update(self):
        # safety first
        if self._node is None:
            self._error_message = Message("PCB Model has no route")
            self._error_message.show()
            return

        lst = self._node.getRoutesAsDict()
        self.setItems(lst)
        return
