# Copyright (c) 2018 Ultimaker B.V.
# Cura is released under the terms of the LGPLv3 or higher.

from UM.Application import Application
from UM.Event import Event
from UM.FlameProfiler import pyqtSlot
from UM.Message import Message
from UM.Scene.Selection import Selection
from UM.Signal import Signal

from cura.AmeTool import AmeTool
from cura.Utils.PCBHandler.PCBUtils.PcbModel import PcbModel

from .RoutePcb import RoutePcb


class Routes(AmeTool):

    sumResult = Signal()

    def __init__(self):
        super().__init__()
        self._model = None
        self._selected_object_id = None
        self._node = None
        self._stack = None
        self._error_message = Message()

    def event(self, event):
        super().event(event)
        self._controller = Application.getInstance().getController()
        if event.type == Event.MousePressEvent and self._controller.getToolsEnabled():
            self.operationStopped.emit(self)
        return False

    @pyqtSlot()
    def _updateSelfNode(self):

        _selected_object_id = self.getSelectedObjectId()
        self._node = Application.getInstance().getController().getScene().findObject(_selected_object_id)

        if not isinstance(self._node, PcbModel):
            self._node = None

    @staticmethod
    def _emitTool(job=None):
        RoutePcb.update_all()

    def itemToSignals(self, path):
        # Safety first
        if not self._node:
            self._error_message = Message("Trying to access undefined node")
            self._error_message.show()
            return

        # TODO: make this operation undoable, so that we won't have to clear the undo/redo stack
        self._node.getStackupOperationStack().reset()

        self._node.removeRouteByPath(path=path)
        self._emitTool()
        self._node.appendLayerFromPathToStackup(path)
        self._node.appendRouteAndUpdateAll()

    @staticmethod
    def getSelectedObjectId():
        selected_object = Selection.getSelectedObject(0)
        selected_object_id = id(selected_object)
        return selected_object_id

    def addRoute(self, path):
        # Safety first
        if not self._node:
            self._error_message = Message("Trying to access undefined node")
            self._error_message.show()
            return

        self._node.appendRouteAndUpdateAll(path, self._emitTool)

    def removeRoute(self, path):
        # Safety first
        if not self._node:
            self._error_message = Message("Trying to access undefined node")
            self._error_message.show()
            return

        self._node.removeRouteByPath(path=path)
        self._emitTool()
        self._node.appendRouteAndUpdateAll()
