import QtQuick.Window 2.2
import QtQuick 2.8
import QtQml.Models 2.1
import QtQuick.Dialogs 1.2

import QtQuick.Controls 1.4 


import UM 1.3 as UM
import Cura 1.0 as Cura
import ".."

import QtQuick.Controls 2.3 as Ctrls

Item {
    id: base
    
    width: (list_body.items.count > 0 ? (UM.Theme.getSize("400px").width+UM.Theme.getSize("20px").width) : 0)+UM.Theme.getSize("modal_sidebar").width
    height: Math.min(Math.max(viewl.height, sidebar.height), 500)
    visible: true
    // property var layersPcb: UM.ActiveTool.properties.getValue("Routes")

    Component {
        id: highlight
        Rectangle { // yz: selection shadow
            width: (list_body.items.count > 0 ? UM.Theme.getSize("400px").width : 0)
            height: UM.Theme.getSize("layer_row").height
            color: "white";
            radius: 5
            z: 5
            opacity: 0.25
            y: viewl.currentItem.y
            Behavior on y {
                SpringAnimation {
                    spring: 2
                    damping: 0.1
                }
            }
        }
    }
    //ListModel {
    //    id: layersModel
    //
    //    Component.onCompleted: update()
    //    function update() {
    //        if (layersPcb != undefined) {
    //            for (var i = 0; i < layersPcb.count; i++) {
    //                append(print_value("layersPcb.getItem("+i+")[size:"+layersModel.count+"]",layersPcb.getItem(i)))
    //            }
    //        }
    //        console.log(layersModel.count);
    //    }
    //}

    function print_value(name, value){
        console.log(name,"=", value);
        return value;
    }

    Cura.RoutePcb{
        id: layersModel
        Component.onCompleted: {UM.ActiveTool.triggerAction("_updateSelfNode")}
    }


    Rectangle {
        id: root
        visible: true
        width: parent.width;

        Component {
            id: tr_template
            MouseArea {
                id: tr_template_body

                property bool selected: false

                acceptedButtons: Qt.LeftButton | Qt.RightButton
                hoverEnabled: true
                anchors { left: parent.left; right: parent.right }
                height: content.height-3
                width: content.width
                onClicked: {
                    if (mouse.button === Qt.RightButton){
                        menu.popup()
                    }
                }

                onPressed: {
                    viewl.currentIndex = index;
                    viewl.current = index;
                }
                Ctrls.Menu {
                    id: menu
                    width: menu_item.width+2
                    height: menu_item.height+2
                    bottomPadding : 1
                    leftPadding : 1
                    rightPadding : 1
                    topPadding : 1
                    Cura.ND_tools_MenuItem {
                        id: menu_item
                        text: "Move to Stackup"

                        background: Rectangle {
                            color: menu_item.hovered ?  UM.Theme.getColor("menu_item_background") : UM.Theme.getColor("menu_item_background_hovered")
                            //color: menu_item.hovered ?  "blue" : "red"
                            border.color: "#353637"
                            border.width: 1

                        }



                        onTriggered: {
                            if (viewl.current >= 0) {
                                var path_del = list_body.model.getItem(viewl.current).path
                                list_body.model.removeItem(viewl.current)
                                UM.ActiveTool.triggerActionWithData("itemToSignals" , path_del)
                                // UM.ActiveTool.triggerActionWithData("removeRoute" , path_del)
                            }
                        }
                    }
                }
                Rectangle {
                    id: content
                    border.width: UM.Theme.getSize("default_lining").width
                    border.color: UM.Theme.getColor("lining")
                    
                    width: tr_template_body.width;
                    height: UM.Theme.getSize("30px").width
                    color: UM.Theme.getColor("background_modal_row")
                    Behavior on color {
                        ColorAnimation { duration: 100 }
                    }
                    radius: 1

                    Row {
                        // padding for the left
                        anchors.verticalCenter: parent.verticalCenter
                        Rectangle {
                            anchors.verticalCenter: parent.verticalCenter
                            width: UM.Theme.getSize("20px").width;
                            height: UM.Theme.getSize("20px").height; // filename_lbl.height
                            color: "transparent"
                        }
                        // filename is down here
                        // filename is down here
                        Text {
                            anchors.verticalCenter: parent.verticalCenter
                            id: filename_lbl
                            color: UM.Theme.getColor("text_modal")
                            width: 390; //  all right side minus scrollbar
                            height: UM.Theme.getSize("20px").height;
                            text: name
                            verticalAlignment: Text.AlignVCenter
                            elide: Text.ElideMiddle
                        }

                    }
                }
            }
        }
    }

    DelegateModel {
        id: list_body
        model: layersModel
        delegate: tr_template
    }

    Row {
        id: sidebar
        width: childrenRect.width
        height: childrenRect.height

        // the +/- sidebar
        Column {
            width: UM.Theme.getSize("modal_sidebar").width
            // +
            Cura.ND_tools_Button {
                    icon.source: UM.Theme.getIcon("plus-svgrepo-com");
                    height: UM.Theme.getSize("modal_sidebar_button").height
                    width: UM.Theme.getSize("modal_sidebar_button").width
                    icon.height: UM.Theme.getSize("modal_sidebar_button").width/2
                    icon.width: UM.Theme.getSize("modal_sidebar_button").width/2
                    onClicked: { bush.open()}
            }
            // -
            Cura.ND_tools_Button {
                icon.source: UM.Theme.getIcon("minus-svgrepo-com");
                height: UM.Theme.getSize("modal_sidebar_button").height
                width: UM.Theme.getSize("modal_sidebar_button").width
                icon.height: UM.Theme.getSize("modal_sidebar_button").width/2
                icon.width: UM.Theme.getSize("modal_sidebar_button").width/2
                enabled: list_body.items.count>0
                onClicked: {
                    var index_del = viewl.current;
                    var rows = list_body.model.count;
                    if (rows===0 || index_del<0 || index_del >=rows){
                        return;
                    }
                    var path_del = list_body.model.getItem(index_del).path
                    list_body.model.removeItem(index_del)
                    UM.ActiveTool.triggerActionWithData("removeRoute" , path_del)
                    if (rows<=index_del+1)
                        viewl.current = Math.min(rows-2,list_body.model.count-1);
                }
            }
        }
        Column {
        // List of selected Settings to override for the selected object
            height: childrenRect.height
            
            width: childrenRect.width
            spacing: 0 //UM.Theme.getSize("default_margin").height

            ScrollView {
                height: Math.min(500, Math.min(viewl.count * (UM.Theme.getSize("section").height + UM.Theme.getSize("default_lining").height), maximumHeight) + 50 + 20)//height: parent.height
         
                width: (list_body.items.count > 0 ? UM.Theme.getSize("400px").width : 0)
                visible: true
                ListView {
                    id: viewl
                    
                    property int current: 0
                    headerPositioning: ListView.OverlayHeader
                    //anchors { margins: 0 }
                    highlight: highlight
                    boundsBehavior: Flickable.StopAtBounds
                    spacing: 0 //UM.Theme.getSize("default_lining").height
                    model: list_body
                    cacheBuffer: 250
                    width: (list_body.items.count > 0 ? UM.Theme.getSize("400px").width : 0)
                }
            }
        }
    }

    FileDialog {
        id: bush
        nameFilters: [ "Gerber Layer Files (*.g*)","Excellon Files (*.tx*)", "All files (*)" ]
        selectMultiple: true
        folder: {
            folder = CuraApplication.getDefaultPath("dialog_load_path");
            return CuraApplication.getDefaultPath("dialog_load_path");
        }
        onAccepted: {
            var f = folder;
            folder = f;

            CuraApplication.setDefaultPath("dialog_load_path", folder);
            UM.ActiveTool.triggerActionWithData("addRoute" , fileUrls)
        }

    }
//    Connections {
//        target: UM.ActiveTool
//        onPropertiesChanged: {
//            UM.ActiveTool.properties.getValue("Routes")
//            layersModel.clear()
//            layersModel.update()
//        }
//    }
}
