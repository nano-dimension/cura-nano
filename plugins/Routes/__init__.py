# Copyright (c) 2018 Ultimaker B.V.
# Cura is released under the terms of the LGPLv3 or higher.
from . import RoutePcb

from . import Routes
from PyQt5.QtQml import qmlRegisterType
from UM.i18n import i18nCatalog

i18n_catalog = i18nCatalog("cura")


def getMetaData():
    return {
        "tool": {
            "id": "routes",
            "name": i18n_catalog.i18nc("@label", "Routes"),
            "description": i18n_catalog.i18nc("@info:tooltip", "See routes of PCB."),
            "icon": "tool_icon.svg",
            "tool_panel": "Routes.qml",
            "weight": 4
        }
    }


def register(app):
    routes = Routes.Routes()
    qmlRegisterType(RoutePcb.RoutePcb, "Cura", 1, 0,
                    "RoutePcb")
    return {"tool": routes}
