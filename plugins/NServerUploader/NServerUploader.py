# Copyright (c) 2017 Ultimaker B.V.
# This example is released under the terms of the AGPLv3 or higher.

from UM.Logger import Logger
from UM.Message import Message
from UM.OutputDevice.OutputDevice import OutputDevice  # An interface to implement.
from UM.OutputDevice.OutputDeviceError import WriteRequestFailedError  # For when something goes wrong.
from UM.OutputDevice.OutputDevicePlugin import OutputDevicePlugin  # The class we need to extend.

from plugins.NServerUploader import UploaderJob


class NServerUploaderPlugin(OutputDevicePlugin):  # We need to be an OutputDevicePlugin for the plug-in system.
    ##  Called upon launch.
    #
    #   You can use this to make a connection to the device or service, and
    #   register the output device to be displayed to the user.
    def start(self):
        self.getOutputDeviceManager().addOutputDevice(
            NServerUploaderDevice())  # Since this class is also an output device, we can just register ourselves.
        # You could also add more than one output devices here.
        # For instance, you could listen to incoming connections and add an output device when a newY device is discovered on the LAN.

    ##  Called upon closing.
    #
    #   You can use this to break the connection with the device or service, and
    #   you should unregister the output device to be displayed to the user.
    def stop(self):
        self.getOutputDeviceManager().removeOutputDevice(
            "nserver_uploader_device")  # Remove all devices that were added. In this case it's only one.


class NServerUploaderDevice(OutputDevice):  # We need an actual device to do the writing.
    def __init__(self):
        super().__init__("nserver_uploader_device")  # Give an ID which is used to refer to the output device.

        # Optionally set some metadata.
        self.setName(
            "Server Uploader Device")
        self.setShortDescription("Upload Processed File")  # This is put on the save button.
        self.setDescription("Upload Processed File")
        self.setIconName("Upload")

    #  Called when the user clicks on the button to save to this device.
    #
    #   The primary function of this should be to select the correct file writer
    #   and file format to write to.
    #
    #   \param nodes A list of scene nodes to write to the file. This may be one
    #   or multiple nodes. For instance, if the user selects a couple of nodes
    #   to write it may have only those nodes. If the user wants the entire
    #   scene to be written, it will be the root node. For the most part this is
    #   not your concern, just pass this to the correct file writer.
    #   \param file_name A name for the print job, if available. If no such name
    #   is available but you still need a name in the device, your plug-in is
    #   expected to come up with a name. You could try `uuid.uuid4()`.
    #   \param limit_mimetypes Limit the possible MIME types to use to serialise
    #   the data. If None, no limits are imposed.
    #   \param file_handler What file handler to get the mesh from.
    #   \kwargs Some extra parameters may be passed here if other plug-ins know
    #   for certain that they are talking to your plug-in, not to some other
    #   output device.
    def requestWrite(self, nodes, file_name=None, limit_mimetypes=None, file_handler=None,
                     **kwargs):
        # The file handler is an object that provides serialisation of file types.
        # There's several types of files. If not provided, it is assumed that we want to save meshes.
        pcbjc_mode = kwargs.get("pcbjc_mode", False)
        if 'backend_file_path' not in kwargs:
            raise WriteRequestFailedError("Unexpected missing args")
        backend_file_path = kwargs['backend_file_path']

        job = UploaderJob.UploaderJob(
            backend_file_path)  # We'll create a Job, which gets run asynchronously in the background.

        job.finished.connect(self._onFinished)  # This way we can properly close the file stream.

        job.start()
        return None

    def _onFinished(self, job):
        if job.getServerError():
            Message(
                text = str(job.getServerError()),
                title = "Sending Job Error").show()
            Logger.log("d", str(job.getServerError()))
        elif not job.getResult():
            Message(
                text="No feedback from printer",
                title="Printer Message").show()
            Logger.log("d", "No feedback from printer")

        else:
            Res = job.getResult()
            try:
                Res = float(Res)
                Message(
                    text="Time Estimation: " + str(int(Res)) + ":" + str(int(60*(Res - int(Res)))) + "  Hours",
                    title="Printer Message").show()
                Logger.log("d", "Done Uploading file!")
            except Exception as e:
                Logger.log("d", "Error on handling Res %s Error: %s" , Res , str(e))


    def _onMessageActionTriggered(self, message, action):
        pass  # Do nothing
