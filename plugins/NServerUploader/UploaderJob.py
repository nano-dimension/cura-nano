import json
import os
import time
from enum import Enum

import requests
from UM.Application import Application
from UM.Job import Job
from UM.Logger import Logger
from UM.Message import Message


class ServerAnswers(Enum):
    got_answer = 200


class UploaderJob(Job):
    def __init__(self, backend_file_path, job_info_try_num=100, sleep_interval_in_sec=5):
        super().__init__()
        self._backend_file_path = backend_file_path
        self.job_info_try_num = job_info_try_num
        self.sleep_interval_in_sec = sleep_interval_in_sec
        self._message = None  # type: Optional[Message]
        self._server_error = None
        self.finished.connect(self._onFinished)

    def _onFinished(self, job: Job) -> None:
        if self == job and self._message is not None:
            self._message.hide()
            self._message = None

    def run(self) -> None:
        Job.yieldThread()
        global_stack = Application.getInstance().getGlobalContainerStack()
        get_job_info_url = global_stack.getProperty("server_host_name", "value") + "/printers/GetJobInfo"
        new_job_url = global_stack.getProperty("server_host_name", "value") + "/printers/NewJob"

        begin_time = time.time()
        JsonRsp = None
        with open(self._backend_file_path, 'rb') as f:
            files = {'myFile': (os.path.basename(self._backend_file_path), f, 'application/gzip')}
            try:
                tray_name = Application.getInstance().getPrintInformation().baseName

                res = requests.post(new_job_url, files=files,
                                    data={'JobName': tray_name,
                                          'UniqueString': global_stack.getProperty("printer_host_name", "value")})

                JsonRsp = res.json()
            except json.JSONDecodeError:
                Logger.log("d", "Error decoding Upload job response")
            except Exception as e:
                Logger.log("d", "Error  Upload job - error is %s", str(e))
            except:
                Logger.log("d", "Error  Upload job")
        end_time = time.time()
        if JsonRsp and "error" in JsonRsp:
            Logger.log("d", "got error  from server %s", JsonRsp["error"])
            self._server_error = "Got error response from server: %s."%JsonRsp["error"]
        else:
            Logger.log("d", "Writing file took %s seconds", end_time - begin_time)
            self._message = Message(title="Printer Communication",
                                    text="Job is Processing in the printer.\nTime estimation will be ready when done.",
                                    dismissable=True, progress=-1)
            self._message.show()
        while JsonRsp and "_id" in JsonRsp and self.job_info_try_num:
            if not self._message.visible:
                self.setResult(None)
                return
            self.job_info_try_num = self.job_info_try_num - 1
            payload = {'JobId': JsonRsp["_id"], 'PrinterIdentifier': global_stack.getProperty("printer_host_name", "value")}
            try:
                res = requests.get(get_job_info_url, params=payload)
                Logger.log("d", "got job info response: " + str(res.status_code))
                if res.status_code == ServerAnswers.got_answer.value:
                    GetJobInfoJson = res.json()
                    print(GetJobInfoJson)
                    if 'PrintTimeInHours' in GetJobInfoJson:
                        self.setResult(GetJobInfoJson['PrintTimeInHours'])
                        self._message.hide()

                        break

                time.sleep(self.sleep_interval_in_sec)

            except json.JSONDecodeError:
                Logger.log("d", "Error decoding get job info response")
            except Exception as e:
                Logger.log("d", "Error getting job info - error is %s", str(e))
            except:
                Logger.log("d", "Error getting job info")
            Logger.log("d", "waiting time reminded :" + str(self.job_info_try_num * self.sleep_interval_in_sec))

            if self.job_info_try_num == 0:
                self.setResult(None)
                self._message.hide()

    def getServerError(self):
        """Get the Server error.
        """
        return self._server_error