# TODO: Copyright
# TODO: License

from . import CuraNanoBackend

from UM.i18n import i18nCatalog
catalog = i18nCatalog("cura")

def getMetaData():
    return {}

def register(app):
    return { "backend": CuraNanoBackend.CuraNanoBackend() }

