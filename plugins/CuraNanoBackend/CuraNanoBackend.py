# TODO: License
# Copyright (c) 2018 Ultimaker B.V.
# Cura is released under the terms of the LGPLv3 or higher.

import sys
import argparse  # To run the engine in debug mode if the front-end is in debug mode.
import os
from collections import defaultdict
from typing import Dict, List, Optional, TYPE_CHECKING, cast, Callable, Any

import Arcus
import psutil
from PyQt5.QtCore import QObject, pyqtSlot
from UM.Backend.Backend import Backend, BackendState
from UM.Logger import Logger
from UM.Message import Message
from UM.PluginRegistry import PluginRegistry
from UM.Scene.Iterator.DepthFirstIterator import DepthFirstIterator
from UM.Scene.SceneNode import SceneNode
from UM.Settings.Interfaces import DefinitionContainerInterface
from UM.Settings.SettingInstance import SettingInstance
from UM.Signal import Signal
from UM.Tool import Tool  # For typing
from cura.BuildVolume import BuildVolume
from cura.CuraApplication import CuraApplication
from cura.Settings.ExtruderManager import ExtruderManager
from cura.Utils import PathUtils
from cura.Utils.PCBHandler.PCBUtils.PcbModel import PcbModel
from cura.Utils.PCBHandler.PCBUtils.Global.GlobalParamsHandler import GlobalParamsHandler
from cura.Utils.PCBHandler.PCBUtils.ContentValidation import ContentValidation

from .SerializeTrayJob import SerializeTrayJob, TraySerializationError, StartJobResult
from cura.Utils.PCBHandler.PCBUtils.Global.Consts import Consts

if TYPE_CHECKING:
    from cura.Machines.Models.MultiBuildPlateModel import MultiBuildPlateModel
    from cura.Machines.MachineErrorChecker import MachineErrorChecker

from UM.i18n import i18nCatalog

catalog = i18nCatalog("cura")


class CuraNanoBackend(QObject, Backend):
    backendError = Signal()

    def __init__(self) -> None:
        """Starts the back-end plug-in.

        This registers all the signal listeners and prepares for communication
        with the back-end in general.
        """

        super().__init__()

        application = CuraApplication.getInstance()


        globglobal_params = GlobalParamsHandler.getGlobalParams()
        ContentValidation.ValidateAndQuitOnIssue_EnginePath(globglobal_params.engine_path)

        application.getPreferences().addPreference("backend/command", self.getEngineCallCommand())



        # Listeners for receiving messages from the back-end.
        self._message_handlers["curaNano.proto.Progress"] = self._onProgress
        self._message_handlers["curaNano.proto.ProgressMessage"] = self._onProgressMessage
        self._message_handlers["curaNano.proto.Notification"] = self._onNotification
        self._message_handlers["curaNano.proto.ProcessingFinished"] = self._onProcessingFinished
        self._restart = False  # type: bool # Back-end is currently restarting?

        self._engine_is_fresh = True  # type: bool # Is the newly started engine used before or not?

        self._backend_log_max_lines = 20000  # type: int # Maximum number of lines to buffer

        self._error_message = Message()  # type: Optional[Message] # Pop-up message that shows errors.
        self._multi_build_plate_model = None  # type: Optional[MultiBuildPlateModel]
        self._machine_error_checker = None  # type: Optional[MachineErrorChecker]

        self._last_job_file_path = ""

        self._last_num_objects = defaultdict(int)
        self._postponed_scene_change_sources = []
        self._tool_active = False
        self._slicing = False
        self._tray_serialization_job = None  # type: Optional[SerializeTrayJob]
        self._start_slice_job_build_plate = None
        self._build_plates_to_be_sliced = []
        self._always_restart = True

        self._global_container_stack = None

        self._multi_build_plate_model = None
        self._scene = application.getController().getScene()
        self._scene.sceneChanged.connect(self._onSceneChanged)

        # create a temporary directory to be used later for temporary data storage
        self._temp_dir = os.path.join(application.getTempDir().name, "backendTempdir")
        if not os.path.isdir(self._temp_dir):
            os.mkdir(self._temp_dir)
            Logger.log("d", "Created a temporary directory at {}".format(self._temp_dir))

        # a callback to call (once!) after processing is complete
        self._post_process_callback = None

        application.initializationFinished.connect(self.initialize)

    def initialize(self) -> None:
        application = CuraApplication.getInstance()

        self._multi_build_plate_model = application.getMultiBuildPlateModel()

        self._global_container_stack = CuraApplication.getInstance().getMachineManager().activeMachine

        application.getMachineManager().globalContainerChanged.connect(self._onGlobalStackChanged)
        self._onGlobalStackChanged()

        self.backendQuit.connect(self._onBackendQuit)
        self.backendConnected.connect(self._onBackendConnected)

        application.getController().toolOperationStarted.connect(self._onToolOperationStarted)
        application.getController().toolOperationStopped.connect(self._onToolOperationStopped)
        self._machine_error_checker = application.getMachineErrorChecker()

    def close(self) -> None:
        """Terminate the engine process.

        This function should terminate the engine process.
        Called when closing the application.
        """

        # Terminate CuraEngine if it is still running at this point
        self._terminate()

        # remove the previously-created temporary directory
        # Logger.log("d", "removing the temporary directory at {}".format(self._temp_dir))
        # # self._temp_dir.cleanup()

    @pyqtSlot(result=str)
    def getLastJobFilename(self) -> str:
        return str(self._last_job_file_path)


    #TODO remvoe this funciton
    def getEngineCallCommand(self) -> List[str]:
        """
        return the command that runs the engine
        """

        global_params = GlobalParamsHandler.getGlobalParams()
        dev_mode = global_params.dev_mode
        engine_path = global_params.engine_path
        prog_exec = sys.executable
        if dev_mode == Consts.DevMode.dev:
            return [prog_exec,  # When debugging, sys.executable is our python
                    engine_path]

        elif dev_mode == Consts.DevMode.release:
            return [engine_path]
        else:
            raise NotImplementedError("Cannot determine Engine command, dev mode is {}".format(dev_mode.name))



    def getEngineCommand(self) -> List[str]:
        """Get the command that is used to call the engine.

        This is useful for debugging and used to actually start the engine.
        :return: list of commands and args / parameters.
        """
        command = CuraApplication.getInstance().getPreferences().getValue("backend/command")

        full_command_str = " ".join(command)

        # By now the plugin should be fully initialized, and can access its protocol file
        if "-l " not in full_command_str:
            command += ["-l", "{}".format(self.getProtoFile())]

        # Update to the current port (it may have changed due to zombie apocalypse)
        if "-p " in full_command_str:
            ind = command.index('-p')
            command.pop(ind)  # pop the flag
            command.pop(ind)  # pop the port

        # add the updated port
        command += (["-p", "{}".format(self._port)])

        parser = argparse.ArgumentParser(prog="cura", add_help=False)
        parser.add_argument("--debug", action="store_true", default=False,
                            help="Turn on the debug mode by setting this option.")

        known_args = vars(parser.parse_known_args()[0])
        if known_args["debug"]:
            command.append("-vvv")

        return command

    def _terminate(self) -> None:
        """Terminate the engine process.

        Start the engine process by calling _createSocket()
        """
        self._slicing = False
        self._reportProgress(progress=0.0,
                             state=BackendState.NotStarted)
        Logger.log("d", "Attempting to kill the engine process")

        if CuraApplication.getInstance().getUseExternalBackend():
            return

        # Also try to cancel existing serialization job
        if self._tray_serialization_job is not None:
            self._tray_serialization_job.cancel()

        if self._process is not None:  # type: ignore
            Logger.log("d", "Killing engine process")
            try:
                self._process.terminate()  # type: ignore

                Logger.log("d", "Engine process is killed. Received return code %s",
                           self._process.wait())  # type: ignore
                self._process = None  # type: ignore

            except Exception as e:  # terminating a process that is already terminating causes an exception, silently ignore this.
                Logger.log("d", "Exception occurred while trying to kill the engine %s", str(e))

    def _onSocketError(self, error: Arcus.Error) -> None:
        """Called when an error occurs in the socket connection towards the engine.

        :param error: The exception that occurred.
        """

        if CuraApplication.getInstance().isShuttingDown():
            return

        super()._onSocketError(error)
        if error.getErrorCode() == Arcus.ErrorCode.Debug:
            return

        self._terminate()
        self._createSocket()

        if error.getErrorCode() not in [Arcus.ErrorCode.BindFailedError, Arcus.ErrorCode.ConnectionResetError,
                                        Arcus.ErrorCode.Debug]:
            Logger.log("w", "A socket error caused the connection to be reset")

    def _onProgress(self, message: Arcus.PythonMessage) -> None:
        """
        Called when a progress message is received from the engine.

        :param message: The protobuf message containing the slicing progress.
        """
        self._reportProgress(message.progress)

    def _reportProgress(self,
                        progress: float,
                        state: BackendState = None) -> None:
        """
        update the progress value in the gui
        """
        if 0.0 <= progress:
            self.processingProgress.emit(min(progress, 1))
            self.setState(state or BackendState.Processing)

            # if progress > 1:
            #     msg = "Progress calculation error: received progress value {:.2f}%.\n".format(progress * 100) + \
            #           "Please check engine functionality"
            #     self._error_message.setText(msg)
            #     self._error_message.show()

        else:
            self.processingProgress.emit(0.0)
            self.stopProcessing()
            self.setState(state or BackendState.Error)

            self._activatePostProcessCallback()

    def _onProgressMessage(self, message: Arcus.PythonMessage) -> None:
        """
        Called when a progressMessage message is received from the engine.

        :param message: The protobuf message containing the message for the progress bar.
        """
        self._reportProgressMessage(message.message)

    def _reportProgressMessage(self,
                               message: str) -> None:
        """
        update the progress message in the gui
        """
        self.processingProgressMessage.emit(message)

    @staticmethod
    def _onNotification(message: Arcus.PythonMessage) -> None:
        """
        Called when a notification message is received from the engine.

        :param message: The protobuf message containing the notification string.
        """
        Message(text=message.message).show()

    def _onProcessingFinished(self, message: Arcus.PythonMessage) -> None:
        """Called when the engine sends a message that slicing is finished.

        :param message: The protobuf message signalling that slicing is finished.
        """
        self._reportProgress(progress=1.0,
                             state=BackendState.Done)

        self._last_job_file_path = message.job_file_path

        application = CuraApplication.getInstance()

        active_build_plate = application.getMultiBuildPlateModel().activeBuildPlate
        temp_message = {}
        time_ci = int(message.time_ci)
        time_di = int(message.time_di)
        temp_message['Time CI'] = time_ci
        temp_message['Time DI'] = time_di
        temp_message['Heat/Cool Time'] = int(message.cooling_heating)
        if -1 in [time_ci, time_di, message.di_material, message.ci_material]:
            application.getPrintInformation().setPreSliced(True)
        else:
            application.getPrintInformation()._onPrintDurationMessage(0, temp_message,
                                                                      [message.di_material, message.ci_material])

        self._activatePostProcessCallback()

    def _activatePostProcessCallback(self):
        # Do we need to follow up on the processing?
        if self._post_process_callback:
            try:
                callback, args, kwargs = self._post_process_callback  # Parse the callback and its arguments
                callback(*args, **kwargs)  # Call it
            except Exception as e:  # Oops?
                Message("Failed to run callback. Something bad happened: {}".format(e)).show()
                return
            finally:
                self._post_process_callback = None  # Only once(!)

    def _createSocket(self, protocol_file: str = None) -> None:
        """Creates a new socket connection."""
        try:
            if not protocol_file:
                protocol_file = self.getProtoFile()
                if protocol_file is None:
                    return

            super()._createSocket(protocol_file)
            self._engine_is_fresh = True
        except:
            return

    def getProtoFile(self):
        """Returns the default protocol file used for communication"""

        if not self.getPluginId():
            Logger.error("Can't create socket before CuraEngineBackend plug-in is registered.")
            return
        plugin_path = PluginRegistry.getInstance().getPluginPath(self.getPluginId())
        if not plugin_path:
            Logger.error("Could not get plugin path!", self.getPluginId())
            return
        return os.path.abspath(os.path.join(plugin_path, "CuraNano.proto"))

    def _onBackendConnected(self) -> None:
        """Called when the back-end connects to the front-end."""

        if self._restart:
            self._restart = False
            # self._onChanged()

    def _onBackendQuit(self) -> None:
        """Called when the back-end self-terminates.

        We should reset our state and start listening for new connections.
        """

        if not self._restart:
            if self._process:  # type: ignore
                Logger.log("d", "Backend quit with return code %s. Resetting process and socket.",
                           self._process.wait())  # type: ignore
                self._process = None  # type: ignore

    slicingStarted = Signal()
    """Emitted when the slicing process starts."""

    slicingCancelled = Signal()
    """Emitted when the slicing process is aborted forcefully."""

    @pyqtSlot()
    def stopProcessing(self) -> None:
        self.setState(BackendState.NotStarted)
        if self._slicing and self._process:  # We were already slicing. Stop the old job.
            try:
                pid_m = psutil.Process(self._process.pid)
                if pid_m is not None:
                    names2kill = pid_m.children(recursive=True)
                    self._terminate()
                    for p in names2kill:
                        try:
                            p.kill()
                        except (psutil.AccessDenied, psutil.ZombieProcess, psutil.NoSuchProcess):
                            pass
            except (psutil.AccessDenied, psutil.ZombieProcess, psutil.NoSuchProcess):
                pass
            except AttributeError as e:
                Logger.error("AttributeError: %s", e)
                raise e
            self._createSocket()

        if self._error_message:
            self._error_message.hide()

    def setPostProcessCallback(self, callback: Callable, *args, **kwargs):
        self._post_process_callback = (callback, args, kwargs)

    @pyqtSlot()
    def startProcessing(self) -> None:
        """
        Send the job to processing
        """
        self._slicing = True
        self.slicingStarted.emit()

        self._reportProgressMessage("Serializing Tray")
        self._tray_serialization_job = SerializeTrayJob(
            new_job_message=self._socket.createMessage("curaNano.proto.NewJob"),
            temp_dir=self._temp_dir)

        self._tray_serialization_job.finished.connect(self._onSerializationCompleted)
        self._tray_serialization_job.progress.connect(self._reportProgress)  # TODO: Add a wrapper for adding a message
        self._tray_serialization_job.start()

    def _onSerializationCompleted(self, job: SerializeTrayJob) -> None:
        """
        Called when the tray serialization job is completed.

        If the result is satisfactory, the resultant message containing the serialized data will be sent to the engine
        Otherwise an error message will be displayed

        :param job: The start slice job that was just finished.
        """

        # Note that cancelled slice jobs can still call this method.
        if self._tray_serialization_job is not job:
            return

        if self._error_message:
            self._error_message.hide()

        if job.isCancelled() or job.getError() or job.getResult() == StartJobResult.Error:
            if job.getError():
                error_msg = job.getError().args[0]
                Logger.error(error_msg)
                self._reportProgressMessage(str(error_msg))

            self.setState(BackendState.Error)
            self.backendError.emit(job)
            return

        application = CuraApplication.getInstance()
        if job.getResult() == StartJobResult.MaterialIncompatible:
            if application.platformActivity:
                self._error_message = Message(catalog.i18nc("@info:status",
                                                            "Unable to process with the current material as it is incompatible with the selected machine or configuration."),
                                              title=catalog.i18nc("@info:title", "Unable to process"))
                self._error_message.show()
                self.setState(BackendState.Error)
                self.backendError.emit(job)
            else:
                self.setState(BackendState.NotStarted)
            return

        if job.getResult() == StartJobResult.SettingError:
            if application.platformActivity:
                if not self._global_container_stack:
                    Logger.log("w", "Global container stack not assigned to CuraNanoBackend!")
                    return
                extruders = ExtruderManager.getInstance().getActiveExtruderStacks()
                error_keys = []  # type: List[str]
                for extruder in extruders:
                    error_keys.extend(extruder.getErrorKeys())
                if not extruders:
                    error_keys = self._global_container_stack.getErrorKeys()
                error_labels = set()
                for key in error_keys:
                    for stack in [
                                     self._global_container_stack] + extruders:  # Search all container stacks for the definition of this setting. Some are only in an extruder stack.
                        definitions = cast(DefinitionContainerInterface, stack.getBottom()).findDefinitions(key=key)
                        if definitions:
                            break  # Found it! No need to continue search.
                    else:  # No stack has a definition for this setting.
                        Logger.log("w", "When checking settings for errors, unable to find definition for key: {key}".format(key=key))
                        continue
                    error_labels.add(definitions[0].label)

                self._error_message = Message(
                    catalog.i18nc("@info:status", "Unable to Process with the current settings.<br/>The following settings have errors: {0}").format(
                        ", ".join(error_labels)),
                    title=catalog.i18nc("@info:title", "Unable to process"))
                self._error_message.show()
                self.setState(BackendState.Error)
                self.backendError.emit(job)
            else:
                self.setState(BackendState.NotStarted)
            return

        elif job.getResult() == StartJobResult.ObjectSettingError:
            errors = {}
            for node in DepthFirstIterator(application.getController().getScene().getRoot()):
                stack = node.callDecoration("getStack")
                if not stack:
                    continue
                for key in stack.getErrorKeys():
                    if not self._global_container_stack:
                        Logger.log("e", "CuraEngineBackend does not have global_container_stack assigned.")
                        continue
                    definition = cast(DefinitionContainerInterface, self._global_container_stack.getBottom()).findDefinitions(key=key)
                    if not definition:
                        Logger.log("e",
                                   "When checking settings for errors, unable to find definition for key {key} in per-object stack.".format(key=key))
                        continue
                    errors[key] = definition[0].label
            self._error_message = Message(catalog.i18nc("@info:status",
                                                        "Unable to process due to some per-model settings.<br/>The following settings have errors on one or more models: {error_labels}").format(
                error_labels=", ".join(errors.values())),
                title=catalog.i18nc("@info:title", "Unable to process"))
            self._error_message.show()
            self.setState(BackendState.Error)
            self.backendError.emit(job)
            return

        if job.getResult() == StartJobResult.BuildPlateError:
            if application.platformActivity:
                self._error_message = Message(
                    catalog.i18nc("@info:status", "Unable to process because the prime tower or prime position(s) are invalid."),
                    title=catalog.i18nc("@info:title", "Unable to process"))
                self._error_message.show()
                self.setState(BackendState.Error)
                self.backendError.emit(job)
            else:
                self.setState(BackendState.NotStarted)

        if job.getResult() == StartJobResult.ObjectsWithDisabledExtruder:
            self._error_message = Message(
                catalog.i18nc("@info:status", "Unable to process because there are objects associated with disabled Extruder %s.") % job.getMessage(),
                title=catalog.i18nc("@info:title", "Unable to process"))
            self._error_message.show()
            self.setState(BackendState.Error)
            self.backendError.emit(job)
            return

        if job.getResult() == StartJobResult.NothingToSlice:
            if application.platformActivity:
                self._error_message = Message(catalog.i18nc("@info:status", "Please review settings and check if your models:"
                                                                            "\n- Fit within the build volume"
                                                                            "\n- Are assigned to an enabled extruder"
                                                                            "\n- Are not all set as modifier meshes"),
                                              title=catalog.i18nc("@info:title", "Unable to process"))
                self._error_message.show()
                self.setState(BackendState.Error)
                self.backendError.emit(job)
            else:
                self.setState(BackendState.NotStarted)
            # self._invokeSlice()
            return

        if job.getResult() == StartJobResult.StackupError:
            self._error_message = Message(catalog.i18nc("@info:status", "Stackup is not valid"),
                                          title=catalog.i18nc("@info:title", "Unable to process"))
            self._error_message.show()
            self.setState(BackendState.Error)
            self.backendError.emit(job)
            return

        if job.isSuccessful():
            # if preview jobs existed, stop them in order to unblock GBRRIP for the engine
            # (TODO: When we move to qismLib this will no longer be needed)
            self._reportProgressMessage("Stopping Preview Jobs")
            for pcb_node in (node for node in
                             CuraApplication.getInstance().getController().getScene().getRoot().getChildren() if
                             isinstance(node, PcbModel)):
                Logger.info("Stopping Preview Job on model {}".format(pcb_node.getName()))
                pcb_node.stopPreviewJob()
                pcb_node.waitUntilPreviewDone()
                Logger.info("Stopped  Preview Job on model {}".format(pcb_node.getName()))

            self._reportProgressMessage("Stopped All Preview Jobs")
            self._socket.sendMessage(job.getArcusMessage())  # all done - the check is in the mail
            Logger.log("d", "sent job.getArcusMessage")

        else:  # Something bad happened
            self._reportProgress(progress=-1)

            job_error = job.getError()

            if job_error is None:  # Something really bad happened
                job_error_msg = "Unknown serialization error"
            else:
                job_error_msg = job_error.args[0]

            Logger.logException("e", job_error_msg)

            if isinstance(job_error, TraySerializationError):
                self._error_message.setText(job_error_msg)
                self._error_message.show()

            else:
                Message(job_error_msg).show()

        # In any case, we're done with this job
        self._tray_serialization_job = None

    def _onSceneChanged(self, source: SceneNode) -> None:
        """Listener for when the scene has changed.

        This should start a process if the scene is now ready to process.

        :param source: The scene node that was changed.
        """

        # self.setState(BackendState.NotStarted)
        if not source.callDecoration("isSliceable") and source != self._scene.getRoot():
            if source != self._scene.getRoot() and isinstance(source,
                                                              BuildVolume):  # or isinstance(source, ConvexHullDecorator):
                self.setState(BackendState.NotStarted)
            return

        # This case checks if the source node is a node that contains GCode. In this case the
        # current layer data is removed so the previous data is not rendered - CURA-4821
        if source.callDecoration("isBlockSlicing") and source.callDecoration("getLayerData"):
            self._stored_optimized_layer_data = {}

        build_plate_changed = set()
        source_build_plate_number = source.callDecoration("getBuildPlateNumber")
        if source == self._scene.getRoot():
            # we got the root node
            num_objects = self._numObjectsPerBuildPlate()
            for build_plate_number in list(self._last_num_objects.keys()) + list(num_objects.keys()):
                if build_plate_number not in self._last_num_objects or \
                        num_objects[build_plate_number] != self._last_num_objects[build_plate_number]:
                    self._last_num_objects[build_plate_number] = num_objects[build_plate_number]
                    build_plate_changed.add(build_plate_number)
        else:
            # we got a single scenenode
            if not source.callDecoration("isGroup"):
                mesh_data = source.getMeshData()
                if mesh_data is None or mesh_data.getVertices() is None:
                    return

            # There are some SceneNodes that do not have any build plate associated, then do not add to the list.
            if source_build_plate_number is not None:
                build_plate_changed.add(source_build_plate_number)

        if not build_plate_changed:
            return

        if self._tool_active:
            # do it later, each source only has to be done once
            if source not in self._postponed_scene_change_sources:
                self._postponed_scene_change_sources.append(source)
            return

        self.stopProcessing()
        for build_plate_number in build_plate_changed:
            if build_plate_number not in self._build_plates_to_be_sliced:
                self._build_plates_to_be_sliced.append(build_plate_number)
            # self.printDurationMessage.emit(source_build_plate_number, {}, [])
        self._reportProgress(progress=0.0,
                             state=BackendState.NotStarted)
        # self._clearLayerData(build_plate_changed)

        # self._invokeSlice()

    def _onToolOperationStopped(self, tool: Tool) -> None:
        """Called when the user stops using some tool.

        This indicates that we can safely start slicing again.

        :param tool: The tool that the user was using.
        """

        self._tool_active = False  # React on scene change again
        # Process all the postponed scene changes
        while self._postponed_scene_change_sources:
            source = self._postponed_scene_change_sources.pop(0)
            self._onSceneChanged(source)

    def _onToolOperationStarted(self, tool: Tool) -> None:
        """Called when the user starts using some tool.

        When the user starts using a tool, we should pause slicing to prevent
        continuously slicing while the user is dragging some tool handle.

        :param tool: The tool that the user is using.
        """

        self._tool_active = True  # Do not react on scene change
        # Restart engine as soon as possible, we know we want to slice afterwards
        if not self._engine_is_fresh:
            self._terminate()
            self._createSocket()
            pass

    def _numObjectsPerBuildPlate(self) -> Dict[int, int]:
        """Return a dict with number of objects per build plate"""

        num_objects = defaultdict(int)  # type: Dict[int, int]
        for node in DepthFirstIterator(self._scene.getRoot()):
            # Only count sliceable objects
            if node.callDecoration("isSliceable"):
                build_plate_number = node.callDecoration("getBuildPlateNumber")
                if build_plate_number is not None:
                    num_objects[build_plate_number] += 1
        return num_objects

    def _onGlobalStackChanged(self) -> None:
        """Called when the global container stack changes"""

        if self._global_container_stack:
            self._global_container_stack.propertyChanged.disconnect(self._onSettingChanged)

            for extruder in self._global_container_stack.extruderList:
                extruder.propertyChanged.disconnect(self._onSettingChanged)

        self._global_container_stack = CuraApplication.getInstance().getMachineManager().activeMachine

        if self._global_container_stack:
            self._global_container_stack.propertyChanged.connect(self._onSettingChanged)  # Note: Only starts slicing when the value changed.

            for extruder in self._global_container_stack.extruderList:
                extruder.propertyChanged.connect(self._onSettingChanged)

    def _onChanged(self, *args: Any, **kwargs: Any) -> None:
        """Called when anything has changed to the stuff that needs to be sliced.

        This indicates that we should probably re-slice soon.
        """
        self.stopProcessing()

    def _onSettingChanged(self, instance: SettingInstance, property: str) -> None:
        """A setting has changed, so check if we must reslice.

        :param instance: The setting instance that has changed.
        :param property: The property of the setting instance that has changed.
        """
        if property == "value":  # Only reslice if the value has changed.
            self._onChanged()
