# TODO: License
import copy
import math
import os
import sys
import time
from collections import defaultdict
from copy import deepcopy
from datetime import datetime
from enum import IntEnum
from typing import Tuple, cast, List, Dict
from itertools import combinations

import Arcus
from UM.Job import Job
from UM.Logger import Logger
from UM.Math.Matrix import Matrix
from UM.Math.Quaternion import Quaternion
from UM.Math.Vector import Vector
from UM.Mesh.MeshData import MeshData
from UM.Scene.Iterator.DepthFirstIterator import DepthFirstIterator
from UM.Scene.SceneNode import SceneNode
from UM.Settings.ContainerStack import ContainerStack  # For typing.
from UM.Settings.InstanceContainer import InstanceContainer
from UM.Settings.SettingDefinition import SettingDefinition
from UM.Settings.Validator import ValidatorState
from cura.CuraApplication import CuraApplication
from cura.Scene.CuraSceneNode import CuraSceneNode
from cura.Utils.MeshUtils import combineTwoTrimeshes, meshDataToTrimesh, trimeshToMeshData
from cura.Utils.PCBHandler.PCBUtils.PcbModel import PcbModel
from trimesh import Trimesh

RECIPE_2DI_CRITERIA_MAX_THICKNESS = 0.4


class StartJobResult(IntEnum):
    Finished = 1
    Error = 2
    SettingError = 3
    NothingToSlice = 4
    MaterialIncompatible = 5
    BuildPlateError = 6
    ObjectSettingError = 7  # When an error occurs in per-object settings.
    ObjectsWithDisabledExtruder = 8
    StackupError = 9


eps = sys.float_info.epsilon * 100  # a fairly arbitrary value for epsilon


class TraySerializationError(Exception):
    pass


class SerializeTrayJob(Job):
    """
    Job class that builds up the message of scene data to send to CuraEngine.
    The resultant message is saved internally, while the result of the job is success/failure
    and the error message if any
    """

    def __init__(self,
                 new_job_message: Arcus.PythonMessage,
                 temp_dir: str = None) -> None:
        super().__init__()

        self._new_job_message = new_job_message  # type: Arcus.PythonMessage
        self._temp_dir = temp_dir or os.path.join(os.getcwd(), "serializeTrayTempDir")
        self._tray_name = CuraApplication.getInstance().getPrintInformation().baseName
        self._scene = CuraApplication.getInstance().getController().getScene()  # type: Scene

        # fill in the high-level data in the message
        new_job_message.temp_dir = str(self._temp_dir)
        new_job_message.tray_name = self._tray_name

        self._is_cancelled = False  # type: bool

    def getArcusMessage(self) -> Arcus.PythonMessage:
        return self._new_job_message

    def cancel(self) -> None:
        super().cancel()
        self._is_cancelled = True

    def isCancelled(self) -> bool:
        return self._is_cancelled

    def setIsCancelled(self, value: bool):
        self._is_cancelled = value

    def isSuccessful(self) -> bool:
        return not self._is_cancelled and self._error is None

    def run(self) -> None:
        stack = CuraApplication.getInstance().getGlobalContainerStack()
        if not stack:
            self.setResult(StartJobResult.Error)
            return

        # Don't slice if there is a setting with an error value.
        if CuraApplication.getInstance().getMachineManager().stacksHaveErrors:
            self.setResult(StartJobResult.SettingError)
            return

        if CuraApplication.getInstance().getBuildVolume().hasErrors():
            self.setResult(StartJobResult.BuildPlateError)
            return

        # Wait for error checker to be done.
        while CuraApplication.getInstance().getMachineErrorChecker().needToWaitForResult:
            time.sleep(0.1)

        if CuraApplication.getInstance().getMachineErrorChecker().hasError:
            self.setResult(StartJobResult.SettingError)
            return

        # Don't slice if the buildplate or the nozzle type is incompatible with the materials
        if not CuraApplication.getInstance().getMachineManager().variantBuildplateCompatible and \
                not CuraApplication.getInstance().getMachineManager().variantBuildplateUsable:
            self.setResult(StartJobResult.MaterialIncompatible)
            return

        for extruder_stack in stack.extruderList:
            material = extruder_stack.findContainer({"type": "material"})
            if not extruder_stack.isEnabled:
                continue
            if material:
                if material.getMetaDataEntry("compatible") is False:
                    self.setResult(StartJobResult.MaterialIncompatible)
                    return

        # Don't slice if there is a per object setting with an error value.
        for node in DepthFirstIterator(self._scene.getRoot()):
            if not isinstance(node, CuraSceneNode) or not node.isSelectable():
                continue

            if self._checkStackForErrors(node.callDecoration("getStack")):
                self.setResult(StartJobResult.ObjectSettingError)
                return

            if node.callDecoration("hasErrorInStackup"):
                self.setResult(StartJobResult.StackupError)
                return

        self._serializeTray()
        Logger.log("d", "finished serializeTray")

    def setError(self, error: Exception) -> None:
        super().setError(error)
        self.setResult(None)

    def _serializeTray(self) -> None:
        """
        Serialize the meshes on the tray and serialize them
        We only want to iterate over top-level scene nodes, since groups are handled as 3D meshes
        """
        try:
            tray_can_be_processed, error_string, suggested_recipe = self._validate_tray_can_be_processed()
            if not tray_can_be_processed:
                self.setError(TraySerializationError(error_string))
                return

            child_nodes = [node for node in
                           CuraApplication.getInstance().getController().getScene().getRoot().getChildren() if
                           isinstance(node, CuraSceneNode)]
            child_nodes_num = len(child_nodes)
            for node_id, node in enumerate(child_nodes):
                is_non_printing_mesh = bool(node.callDecoration("isNonPrintingMesh"))
                if self._is_cancelled:
                    self.setError(TraySerializationError("Serialization cancelled successfully"))
                    return
                if getattr(node, "_outside_buildarea", False) and not is_non_printing_mesh:
                    self.setError(TraySerializationError("Mesh {} is outside the build area".format(node.getName())))
                    return

                self._serializeMesh(node, self._new_job_message, suggested_recipe)
                self.progress.emit(float(node_id + 1) / child_nodes_num)

            self.progress.emit(1)

        except Exception as e:
            Logger.logException("w", "Failed to serialize job")
            self.setError(e)

    def _serializeMesh(self, node, message: Arcus.PythonMessage, suggested_recipe):
        """
        serializes a CuraSceneNode representing an AME circuit into the growing arcus message to be sent to the engine

        :param node: the node to serialize
        :param message: the message (ProcessMessage)
        """
        Logger.log("d", " starting _serializeMesh")

        # This is the mapping of the printer axes to image axes. It will potentially change during coordinates change
        #      and PCB serialization
        axes_conversion_map = {"pixel_size_group_in_um": "PixelSizeYInUM",
                               "pixel_size_print_in_um": "PixelSizeXInUM"}

        global_stack = CuraApplication.getInstance().getMachineManager().activeMachine
        coordinate_transform_type = global_stack.getProperty("coordinate_transform", "value")
        if coordinate_transform_type == "disabled":
            printer_node, axes_conversion_map, were_axes_flipped = node, axes_conversion_map, False
        elif coordinate_transform_type == "dragonfly":
            # Transform the coordinates of each node to the printer coordinates.
            printer_node, axes_conversion_map, were_axes_flipped = transformNodeToDragonflyCoordinates(node,
                                                                                                       axes_conversion_map)
        else:
            raise ValueError("Cannot Serialize models: "
                             "transformation <{}> not implemented".format(coordinate_transform_type))

        # very slight position change to align with resolution axis
        printer_node = transformNodeToAlignWithResolution(printer_node, 18.0, 17.625)

        # add a CircuitModel message
        model_message = message.addRepeatedMessage("models")

        # generic data
        model_message.id = id(node)
        model_message.name = node.getName()

        # Bounding box of the node
        # NB: Due to the very strange axes flip they do, the bounding box is truly unorthodox.
        # We want the actual BB in cartesian coordinates, hence the very strange transformations
        # Note also that since the origin of the bounding box is the build plate center we add it (with inverted axes)
        aabb = printer_node.getBoundingBox()
        aabb_min = aabb.minimum
        aabb_max = aabb.maximum

        build_volume = CuraApplication.getInstance().getBuildVolume()
        build_volume_center = [build_volume.getWidth() / 2.0, build_volume.getDepth() / 2.0]

        lower_left = (aabb_min.x + build_volume_center[0], -aabb_max.z + build_volume_center[1])
        upper_right = (aabb_max.x + build_volume_center[0], -aabb_min.z + build_volume_center[1])

        bounding_box_data = model_message.addRepeatedMessage("metadata")
        bounding_box_data.name = "Bounding_box"
        bounding_box_data.value = str({"lower_left": lower_left,
                                       "upper_right": upper_right,
                                       "offset_from_tray": max(aabb_min.y, 0)}).encode("utf-8")

        # serialize models that are copy of another model.
        if node.callDecoration("isCopy"):
            self._serializeCopy(printer_node, model_message)

        # serialize pcb models
        elif node.callDecoration("isPcbModel"):
            axes_conversion_map = self._serializePCB(node=printer_node,
                                                     model_message=model_message,
                                                     axes_conversion_map=axes_conversion_map,
                                                     were_axes_flipped=were_axes_flipped)

        # anything else is a regular 3D model
        else:
            self._serialize3D(printer_node, model_message)

        # Finally, serialize the model settings. NB: This is done after model serialization, because in some cases
        #   we might want to change the settings during model serialization. e.g. flipping the axes pixel size
        #   based on PCB rotation
        stack = CuraApplication.getInstance().getGlobalContainerStack()
        if not stack:
            self.setError(None)
            return
        categories = {'resolution', 'raster', 'pcb_processing', '3d_processing', 'general_processing',
                      'conductive_processing', 'dielectric_processing'}
        recipe_settings = {
            'recipe': "High Quality Fine Vias",
            'use_shell_dielectric': True,
            'allow_layer_air': False,
            'Air from DI Shell to CI': 1,
            'Shell DI Width': 5,
            'Shell to Inner offset': 0
        }
        settings = {}
        for category_settings in [stack.getProperty(category, "children") for category in categories]:
            for setting in category_settings:
                key = setting.key
                value = stack.getProperty(key, "value")
                if key in axes_conversion_map:
                    settings[axes_conversion_map[key]] = float(value)
                else:
                    settings[key] = value

        if suggested_recipe is not None:
            settings = self._updateSettings(stack, settings, recipe_settings)

        process_settings = model_message.addRepeatedMessage("metadata")
        process_settings.name = "process_settings"
        process_settings.value = str(settings).encode("utf-8")

    def _serializeCopy(self, node: CuraSceneNode, model_message: Arcus.PythonMessage) -> None:
        origin_node = node.callDecoration("getSourceNode")

        Logger.log("d", " starting _serializeCopy")

        # marking the model as 3D
        model_message.type = 3  # TODO: enum

        if origin_node is not None:
            data = model_message.addRepeatedMessage("metadata")
            data.name = "copy_model"
            data.value = str({"model_id": id(origin_node)}).encode("utf-8")

    def _serializePCB(self, node: PcbModel,
                      model_message: Arcus.PythonMessage,
                      axes_conversion_map: Dict[str, str],
                      were_axes_flipped: bool) -> Dict[str, str]:
        """
        serializes a CuraSceneNode representing a PCB circuit into the growing arcus message to be sent to the engine

        Note: Due to the way we process the PCB circuits, we don't apply the transforms like in 3D Models, but decode
              and simplify them and send them along the circuit data

        Also Note: for the same reason, we have the engine render the images before rotating them, so each image is
                   rendered at such X/Y resolution that after rotation it will come out correctly. For this reason,
                   the third parameter to this function is the mapping of printer axes to XY axes in the image, and
                   for some rotations we flip them.

        :param node: the node to serialize
        :param model_message: the message (CircuitModel)
        :param axes_conversion_map: the mapping of printer axes to image axes
        :param were_axes_flipped: whether the axes were already flipped during previous calculations

        :return the serialized data representing the PCB circuit
        """
        Logger.log("d", " starting _serializePCB")

        # marking the model as a PCB
        model_message.type = 1  # TODO: enum within the protobuffer
        # now the pcb-specific data
        for key, value in zip(["stackup", "routes", "drills"],
                              [node.getStackupAsDict(), node.getRoutesAsDict(), node.getDrillsForSerialization()]):
            data = model_message.addRepeatedMessage("metadata")
            data.name = key
            data.value = str(value).encode("utf-8")

        # Finally - let's decode the transformation of the PCB.
        (z_rotation_deg, do_flipY) = self._getNodeZRotationYFlip(node)

        # ...and add it to the message
        rotation_data = model_message.addRepeatedMessage("metadata")
        rotation_data.name = "z_rotation_deg"
        rotation_data.value = str(z_rotation_deg).encode("utf-8")

        # The PCBs are rendered from original files, and we rotate them externally afterwards.
        # When model is rotated by odd multiples of 90deg relative to the default (Y=Group, X=Print) direction,
        # The pixel size needs to be swapped *before rendering*, and then rotated and swapped again.
        # As a result, if the part of the rotation made for printer coordinate transform was already accounted for
        # in a coordinate flip (that already happened), we have to revert it. It will then be flipped back
        # in the engine after rotation
        if _is_axes_flip_needed(z_rotation_deg) == were_axes_flipped:
            _flipDictValues(axes_conversion_map)

        do_flip_y_data = model_message.addRepeatedMessage("metadata")
        do_flip_y_data.name = "do_flip_y"
        do_flip_y_data.value = str(do_flipY).encode("utf-8")
        Logger.log("d", " finish _serializePCB")

        # Finally, return the axes conversion map explicitly (for readability)
        return axes_conversion_map

    def _getNodeZRotationYFlip(self, node: SceneNode) -> Tuple[int, bool]:
        """
        Get the accumulated rotation of the PCB around all its axes and decode into rotation around Z and flip around Y
        NB: We assume that the rotation around X and Y is 0 or pi, and around Z in quanta of pi/2

        :param node: The SceneNode representing our PCB
        :return: The rotation around Z, and whether to flip around the Y axis
        """

        # NB: Normally we would just break the transformation matrix to Euler angles. But Accumulated calculation
        # errors in the transform matrix will cause us grief if we decode it as it. So we clear those first!
        transform_data = node.getWorldTransformation().getTransposed().getData()

        # Clear those pesky epsilons
        transform_data[abs(transform_data) < eps] = 0

        # Now let's get the dang angles
        angles = Matrix(transform_data).getEuler()

        # Invert the axes back to Cartesian
        raw_angles = [angles.x, angles.z, -angles.y]

        # clear rotation around X by translating it into rotation around Y and Z
        # (assume rotations around X and Y are in quanta of pi)
        rotation_y_deg, rotation_z_deg = _transformRotationToYZDeg(raw_angles)

        if rotation_y_deg % 180 != 0:
            raise TraySerializationError("Rotation around Y has to be in quanta of 180 deg")

        if rotation_z_deg % 90 != 0:
            raise TraySerializationError("Rotation around Z has to be in quanta of 90 deg")

        return rotation_z_deg, (rotation_y_deg > 0)

    def _serialize3D(self, node: CuraSceneNode, model_message: Arcus.PythonMessage) -> None:
        """
        serializes a CuraSceneNode representing a 3D model circuit into the growing arcus message to be sent to the engine

        :param node: the node to serialize
        :param model_message: the message (circuitModel)
        """
        Logger.log("d", " starting _serialize3D")

        # marking the model as 3D
        model_message.type = 2  # TODO: enum

        # serialize the meshes into a dictionary (currently saving the STL and storing its path)
        extruders_to_meshes = {}
        # is this a group or a single mesh?
        if node.callDecoration("isGroup"):
            # first iteration on nodes: collect meshes by material - assume 1-level-grouping only
            material_nodes = defaultdict(list)
            for subNode in node.getChildren():
                material_nodes[subNode.getPrintingExtruder().name].append(subNode)

            # second iteration on materials: serialize
            for material, nodes in material_nodes.items():
                self._serialize3DMeshes(material=material,
                                        parent_node=node,
                                        nodes=nodes,
                                        extruders_to_meshes=extruders_to_meshes)

        else:
            # TODO: handle non-groups
            raise TraySerializationError("Cannot handle non-group in 3D mesh {}".format(node.getName()))

        extruder_data = model_message.addRepeatedMessage("metadata")
        extruder_data.name = "Extruders_to_meshes"
        extruder_data.value = str(extruders_to_meshes).encode("utf-8")

    def _serialize3DMeshes(self,
                           material: str,
                           parent_node: CuraSceneNode,
                           nodes: CuraSceneNode,
                           extruders_to_meshes: dict) -> None:
        """
        combined a list of CuraSceneNodes corresponding to the same material into one mesh and serializes it
          into a dictionary corresponding extruders to meshes, where each entry of the dictionary has info for the mesh,
          containing its path and the heights where it begins and ends

        For now we apply the transformation to the mesh and save it locally.
        TODO: Serialize meshes (after transformation) into the message and send it to the engine located on a server

        :param node: the nodes to serialize
        :param extruders_to_meshes: the dictionary to serialize into
        """

        build_volume = CuraApplication.getInstance().getBuildVolume()

        combined_trimesh = None
        for node in nodes:
            # First of all - apply the mesh transformation
            transform = node.getWorldTransformation()
            scale_rotation_mat = transform.getTransposed().getData()[:3, :3]

            # Get translation relative to the center of the tray
            translate = transform.getData()[:3, 3]

            # Transform relative to the corner (Note axis inversion)
            translate[0] += build_volume.getWidth() / 2.0
            translate[2] -= build_volume.getDepth() / 2.0

            # Apply the scale rotation and translation to the mesh STL
            mesh_data = node.getMeshData()
            vertices = mesh_data.getVertices()
            vertices = vertices.dot(scale_rotation_mat)
            vertices += translate

            transformed_mesh_data = MeshData(vertices=vertices, indices=mesh_data.getIndices())

            if combined_trimesh:
                combined_trimesh = combineTwoTrimeshes(combined_trimesh,
                                                       meshDataToTrimesh(transformed_mesh_data))
            else:
                combined_trimesh = meshDataToTrimesh(transformed_mesh_data)

        # We need to create a new SceneNode to pack the vertices and faces so that we can pass them to the STL writer
        #   and also in order to get the correct bounding box projection, which we will need for future processing
        combined_mesh_data = trimeshToMeshData(combined_trimesh,
                                               do_swap_axes=False)  # We started with Cura mesh - no need to swap axes

        temp_node = SceneNode()
        temp_node.setMeshData(combined_mesh_data)
        temp_node.setSelectable(True)  # Need that in order to be able to save it to STL

        # We will also need the net height (bottom to top, not counting the placement)
        mesh_extents = combined_mesh_data.getExtents()

        # Write the converted mesh to disk in STL form
        stl_writer = CuraApplication.getInstance().getMeshFileHandler().getWriter("STLWriter")
        file_name = "{}_{}-{}.stl".format(os.path.join(self._temp_dir,
                                                       datetime.now().strftime('%Y-%m-%d_%H-%M-%S.%f')),
                                          os.path.splitext(parent_node.getName())[0],
                                          material)

        with open(file_name, "wb") as stream:
            stl_writer.write(stream, [temp_node], 2)  # output mode is 2 = binary STL

        if not len(file_name):
            Logger.error("Failed to write STL data for material {} of mesh {}".format(material, parent_node.getName()))
            return

        mesh_info = {'path': file_name,
                     'bottom': max(mesh_extents.bottom, 0),  # If mesh starts below the tray we cut it off at the tray
                     'top': mesh_extents.top}

        extruders_to_meshes[material] = mesh_info

    def _checkStackForErrors(self, stack: ContainerStack) -> bool:
        """Check if a stack has any errors."""

        """returns true if it has errors, false otherwise."""

        top_of_stack = cast(InstanceContainer, stack.getTop())  # Cache for efficiency.
        changed_setting_keys = top_of_stack.getAllKeys()

        # Add all relations to changed settings as well.
        for key in top_of_stack.getAllKeys():
            instance = top_of_stack.getInstance(key)
            if instance is None:
                continue
            self._addRelations(changed_setting_keys, instance.definition.relations)
            Job.yieldThread()

        for changed_setting_key in changed_setting_keys:
            validation_state = stack.getProperty(changed_setting_key, "validationState")

            if validation_state is None:
                definition = cast(SettingDefinition, stack.getSettingDefinition(changed_setting_key))
                validator_type = SettingDefinition.getValidatorForType(definition.type)
                if validator_type:
                    validator = validator_type(changed_setting_key)
                    validation_state = validator(stack)
            if validation_state in (
                    ValidatorState.Exception, ValidatorState.MaximumError, ValidatorState.MinimumError,
                    ValidatorState.Invalid):
                Logger.log("w", "Setting %s is not valid, but %s. Aborting slicing.", changed_setting_key,
                           validation_state)
                return True
            Job.yieldThread()

        return False

    @staticmethod
    def _validate_nodes_for_process(child_nodes):
        """ validate the tray for process by "Mixed tray" criteria and 2di recipy-High Quality Fine Vias"""
        tray_recipes = list()
        total_thickness = list()

        for node in child_nodes:
            # TODO: the recipe check may need another argument that say "the user overrided the recipe check"
            # TODO: CI GROUPS
            total_thickness.append(node.getTotalThikness())

            if node.callDecoration("isPcbModel") is not None:
                tray_recipes.append(node.getPreferredRecipe())
            else:
                tray_recipes.append(None)

        if find_differences(total_thickness) > RECIPE_2DI_CRITERIA_MAX_THICKNESS:
            return False, "Models are not in the same height range", None
        if not len(set(tray_recipes)) == 1:
            return False, "The models require difference Recipe for processing", None

        if "High Quality Fine Vias" in tray_recipes:
            return True, None, tray_recipes[0]
        else:
            return True, "Process tray", None

    def _validate_tray_can_be_processed(self):
        """"
        returns nodes of Tray object ,meaning the models on the tray as child nodes for _validate_nodes_for_process static function
        """
        child_nodes = [node for node in
                       CuraApplication.getInstance().getController().getScene().getRoot().getChildren() if
                       isinstance(node, CuraSceneNode)]
        return SerializeTrayJob._validate_nodes_for_process(child_nodes)

    def _updateSettings(self, stack, settings, recipe_settings):

        for key in recipe_settings:
            if key not in stack.getContainer(0)._instances.keys():
                settings[key] = recipe_settings[key]

        return settings


def find_nth(haystack, needle, n):
    parts = haystack.split(needle, n + 1)
    if len(parts) <= n + 1:
        return -1
    return len(haystack) - len(parts[-1]) - len(needle)


def _flipDictValues(input_dict: Dict[str, str]) -> None:
    """
    Flip the two values of the dictionary at the input
    Assume there are exactly 2 keys in the dictionary
    :param input_dict - the dictionary to flip
    """
    key1, key2 = input_dict.keys()
    input_dict[key1], input_dict[key2] = input_dict[key2], input_dict[key1]


def _wrapAround2Pi(n: float) -> float:
    """
    This wraps an angle in  radians around 2pi

    :param n: the input angle
    :return: the wrapped angle, such that the result is between 0 and 2pi
    """
    while n < 0:
        n += 2 * math.pi

    while n >= 2 * math.pi:
        n -= 2 * math.pi

    return n


def _transformRotationToYZDeg(raw_angles: List[float]) -> Tuple[float]:
    """
    transform rotations around X axis to rotations around Y and Z axis.
    This only works while rotations around X are in quanta of pi

    NB: This was written for an earlier iteration and supports an arbitrary addition of 2pi,
    but since we are back to decomposing into Euler Angles we will get results between -pi and pi for all axes

    :param raw_angles: a list of the angles (in radians) to transform
    :return: a tuple containing the rotations *in degrees* around Y and Z respectively
    """

    # First wrap X rotation to prevent sign inconsistency
    raw_angles[0] = _wrapAround2Pi(raw_angles[0])

    # Now remove X rotations by translating them into Y and Z rotations
    while abs(raw_angles[0]) > eps:
        raw_angles = [angle - math.pi for angle in raw_angles]

        # safety
        if raw_angles[0] < -math.pi:
            raise TraySerializationError("Cannot transform X rotations that are not a multiple of pi")

    # Now wrap around 2pi and return Y and Z rotations in degrees.
    # The rounding will help take care of rounding errors later on, since we expect quanta of 90deg from here on
    return (round(math.degrees(_wrapAround2Pi(angle))) for angle in raw_angles[1:])


def transformNodeToDragonflyCoordinates(node: CuraSceneNode,
                                        axes_conversion_map: Dict[str, str]) -> Tuple[CuraSceneNode,
                                                                                      Dict[str, str],
                                                                                      bool]:
    """
    *TL;DR: We need to orient the image such that the printer can correctly print it*
    --------------
    Originally in the dragonfly printer the tray was supposed to be loaded from the side door, and so the
      Print and Group axes in the GIS were configured in order to receive the images that way.
    Somehow the design of the machine ended up being such that the monitor and keyboard were put on the front,
      the loading position was put there as well, and the side door was abandoned.
    We want the tray in Cura to represent the view from that door, and so we now rotate the node and set its position,
      as if we were looking at it from the side door.
    --------------

    Note: we want to calculate and use that transform that "virtually", without actually changing the nodes on the tray.
      In order to do that, we will create a duplicate node, and return it without adding it to the scene

    :param node: the CuraSceneNode to transform
    :param axes_conversion_map:  the mapping of printer axes to image axes

    :return: a duplicate node, with the appropriate transformation applied, the changes axes_axes_conversion_map, and
             a boolean indicating whether we applied an axes flip already, so that we don't do it again later
    """
    post_z_rotation_angle_deg = -90

    new_node = deepcopy(node)
    pos = new_node.getPosition()

    # Note again the axis confusion - the Y axis is the cartesian Z axis, around which we want to rotate
    new_node.rotate(Quaternion.fromAngleAxis(math.radians(post_z_rotation_angle_deg), Vector.Unit_Y),
                    transform_space=node.TransformSpace.World)

    new_node.setPosition(Vector(-pos.z, pos.y, pos.x))

    is_axes_flip_needed = _is_axes_flip_needed(post_z_rotation_angle_deg)

    if is_axes_flip_needed:
        _flipDictValues(axes_conversion_map)

    return new_node, axes_conversion_map, is_axes_flip_needed


def getAlignedCoord(coord, matrix_step):
    factor = 1000000
    factored_x = abs(int(coord * factor))
    step = int(matrix_step * factor)
    offset_with_factor = factored_x % step
    sign = -1 if coord < 0 else 1

    new_coord = (factored_x - offset_with_factor) * sign
    new_coord /= factor
    return new_coord


def transformNodeToAlignWithResolution(node: SceneNode, pixel_size_print_um: float, pixel_size_group_um: float):
    pos = node.getPosition()
    pixel_size_group_mm = pixel_size_group_um / 1000.0
    pixel_size_print_mm = pixel_size_print_um / 1000.0

    new_pos = pos.set(getAlignedCoord(pos.x, pixel_size_group_mm), pos.y,
                      getAlignedCoord(pos.z, pixel_size_print_mm))
    Logger.log("i", "Align node position {} -> {}".format((pos.x, pos.y, pos.z), (new_pos.x, new_pos.y, new_pos.z)))
    node.setPosition(new_pos)
    return node


def _is_axes_flip_needed(z_rotation_angle_deg: int) -> bool:
    """
    Checks whether the rotation necessitates a flip of the axes resolution swap
    :param z_rotation_angle_deg:
    :return:
    """
    if z_rotation_angle_deg % 90:
        raise Exception("Current logic only supports transformations in quanta of 90 degrees, "
                        "due to the required axes flip")

    return (z_rotation_angle_deg // 90) % 2 == 1


def find_differences(lst):
    """Find maximum difference in a list"""
    diff = [abs(i - j) for i, j in combinations(set(lst), 2)]
    if len(diff) == 0:
        return 0
    return max(diff)
