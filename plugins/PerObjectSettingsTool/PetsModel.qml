
import QtQuick 2.0

ListModel {
    ListElement {
        grb: "C:/gerbers/..."
        type: "Solder MAks"
        height: 12
        route_id: []
        drill: []
    }
    ListElement {
        name: "Penny"
        type: "Turtle"
        age: 4
        size: "Small"
    }
    ListElement {
        name: "Warren"
        type: "Rabbit"
        age: 2
        size: "Small"
    }
    ListElement {
        name: "Spot"
        type: "Dog"
        age: 9
        size: "Medium"
    }
    ListElement {
        name: "Schrödinger"
        type: "Cat"
        age: 2
        size: "Medium"
    }
    ListElement {
        name: "Joey"
        type: "Kangaroo"
        age: 1
        size: "Medium"
    }
    ListElement {
        name: "Kimba"
        type: "Bunny"
        age: 65
        size: "Large"
    }
    ListElement {
        name: "Rover"
        type: "Dog"
        age: 5
        size: "Large"
    }
    ListElement {
        name: "Tiny"
        type: "Elephant"
        age: 15
        size: "Large"
    }
}