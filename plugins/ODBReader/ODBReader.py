import os
import tempfile
import threading

from UM.Logger import Logger
from UM.Mesh.MeshReader import MeshReader
from UM.MimeTypeDatabase import MimeTypeDatabase, MimeType
from cura.CuraApplication import CuraApplication
from cura.Utils.PCBHandler.PCBUtils.GerberReadUtils import readGerberFile
from cura.Utils.PCBHandler.PCBUtils.MessagesOnPcbLoad import PcbMessage, messageType
from cura.Utils.PCBHandler.PCBUtils.OdbModel import OdbModel
from cura.Utils.PCBHandler.PCBUtils.PcbUtils import LayerType
from cura.Utils.PCBHandler.PCBUtils.StackupToolValidator import StackupToolValidator
from cura.Utils.PCBHandler.PcbModelDecorator import PcbModelDecorator


class ODBReader(MeshReader):
    def __init__(self, application=None):
        super().__init__(application=application)
        self._application = application
        self._supported_extensions = ['.tgz']
        MimeTypeDatabase.addMimeType(
            MimeType(
                name="ODB++",
                comment="ODB++",
                suffixes=['tgz']
            )
        )
        self._artwork_is_running = threading.Lock()
        self._global_container_stack = None

    def reset(self):
        pass

    def getGlobalContainerStack(self):
        return self._global_container_stack if self._global_container_stack is not None else self._application.getGlobalContainerStack()

    def setGlobalContainerStack(self, global_stack):
        self._global_container_stack = global_stack

    def getArtworkLock(self):
        return self._artwork_is_running

    LAYER_DEFAULT_THICKNESS_MAP = {
        LayerType.Signal: 'signal_layer_thickness',
        LayerType.SolderMask: 'soldermask_top_layer_thickness',
        LayerType.Annotation: 'annotation_top_layer_thickness',
        LayerType.Prepreg: 'prepreg_layer_thickness',
    }

    def _updateDefaultThickness(self, node: OdbModel):
        layer_dielectric_attr = self.LAYER_DEFAULT_THICKNESS_MAP.get(LayerType.Prepreg)
        default_layer_dielectric = int(self.getGlobalContainerStack().getProperty(layer_dielectric_attr, "value"))

        for layer_index in range(node.getStackupCount()):
            layer = node.getLayerCopyByIndex(layer_index)
            if layer.type not in self.LAYER_DEFAULT_THICKNESS_MAP.keys():
                continue
            if layer.thickness == 0:
                thickness_attr = self.LAYER_DEFAULT_THICKNESS_MAP.get(layer.type)
                node.updateLayerThickness(layer_index, int(self.getGlobalContainerStack().getProperty(thickness_attr, "value")))

            if layer.layer_dielectric == 0:
                node.updateLayerDielectric(layer_index, default_layer_dielectric)

    def _checkForEmptyGerbers(self, node: OdbModel, file_name):
        """the function checks for empty gerber : drills or layers"""
        model_name = os.path.basename(file_name)
        for gbr in os.listdir(node.getGerbersExtractedPath()):
            gerber_base_name = os.path.basename(gbr).split(".")[0]
            if gbr.endswith(".gbr"):
                odb_layer = readGerberFile(os.path.join(node.getGerbersExtractedPath(), gbr))
                if odb_layer is None:
                    Logger.log("w", node.getGerbersExtractedPath().split("/")[-1] + " : could not read file")
                    PcbMessage(message_type=messageType.warning_message, file_name=gbr,
                               text="Could not read Gerber file {}".format(gbr),
                               title="Loading ODB file {}".format(model_name)).show()
                    if node.hasDrill(gerber_base_name):  # if the gbr is drill
                        node.removeDrillByName(gerber_base_name)
                    else:
                        node.removeLayerByName(gerber_base_name)

                if not odb_layer.primitives:
                    Logger.log("w", node.getGerbersExtractedPath().split("/")[-1] + " is empty file")
                    PcbMessage(message_type=messageType.warning_message, file_name=gbr,
                               text="Gerber file {} is empty".format(gbr),
                               title="Loading ODB file {}".format(model_name)).show()
                    if node.hasDrill(gerber_base_name):  # if the gbr is drill
                        node.removeDrillByName(gerber_base_name)
                    else:
                        node.removeLayerByName(gerber_base_name)

    def getLoadingSettings(self):
        global_stack = self.getGlobalContainerStack()
        settings = {}

        settings['SignalLayerThickness'] = global_stack.getProperty(
            "signal_layer_thickness", "value")
        settings['SolderMaskThicknessTop'] = global_stack.getProperty(
            "soldermask_top_layer_thickness", "value")
        settings['SolderMaskThicknessBottom'] = global_stack.getProperty(
            "soldermask_bottom_layer_thickness", "value")
        settings['SolderAnnotationThicknessTop'] = global_stack.getProperty(
            "annotation_top_layer_thickness", "value")
        settings['SolderAnnotationThicknessBottom'] = global_stack.getProperty(
            "annotation_bottom_layer_thickness", "value")
        settings['PrepregThickness'] = global_stack.getProperty("prepreg_layer_thickness", "value")
        settings['minPTH'] = global_stack.getProperty("min_size_for_pth", "value")
        settings['scale'] = global_stack.getProperty("scale", "value")

        return settings

    def _read(self, file_name, no_setting_override: bool = False, temp_dir: tempfile.TemporaryDirectory = None) -> OdbModel:
        self.reset()

        global_stack = self.getGlobalContainerStack()
        if temp_dir is None:
            temp_dir = self._application.getTempDir()
        settings = self.getLoadingSettings()
        result_node = OdbModel(file_name, no_setting_override=no_setting_override,
                               settings=settings, global_stack=global_stack, temp_dir=temp_dir)

        result_node.readStackupFromODB(global_stack=global_stack)
        Logger.log("d", 'ODB stackup has loaded')

        self._updateDefaultThickness(result_node)
        Logger.log("d", '_updateDefaultThickness done ')

        with self.getArtworkLock():
            result_node.extractODB2Gerbers()  # there is log inside

        self._checkForEmptyGerbers(result_node, file_name)

        result_node.replaceNamesToAbsolutePaths()
        Logger.log("d", 'replaceNamesToAbsolutePaths done ')

        result_node.addDielectricLayersToStackup()
        Logger.log("d", 'addDielectricLayersToStackup done ')

        result_node.splitDrillsByTool()
        result_node.updateDrillsStartEnd()
        result_node.getDrillsForSerialization()
        result_node.rereadRoutes()

        if result_node.getRoutesCount() and CuraApplication.getInstance() is not None:
            result_node.createPreview()

        try:
            result_node.updateNodeMeshData()
            Logger.log("d", 'updateNodeMeshData done ')
        except:
            self.makePlaceHolderRoute(result_node)
            # there is log inside

        result_node.setName(file_name)
        result_node.addDecorator(PcbModelDecorator())  # mark the model as a pcb model
        result_node.checkStackupTypes()
        # doing validation to errors
        if not result_node.hasDecoration("hasErrorInStackup"):
            result_node.addDecorator(StackupToolValidator())
        return result_node

    @staticmethod
    def makePlaceHolderRoute(result_node):
        result_node.makeDemiRoute()

    def waitJobDone(self):
        with self._artwork_is_running:
            pass  # Nothing to do here really, we just waited for the lock to be released
