from . import ODBReader
from UM.i18n import i18nCatalog
i18n_catalog = i18nCatalog("cura")

def getMetaData() :
    return {
        "mesh_reader": [
           {
                "extension": ['tgz'
                              ],
                "description": "ODB++ electrical board"
            }
        ]
    }

def register(app):
    return {'mesh_reader': ODBReader.ODBReader(app)}
