// Copyright (c) 2019 Ultimaker B.V.
// Cura is released under the terms of the LGPLv3 or higher.

import QtQuick 2.10
import QtQuick.Controls 2.3

import UM 1.3 as UM
import Cura 1.1 as Cura

//
// This component contains the content for the "User Agreement" page of the welcome on-boarding process.
//
Item
{
    UM.I18nCatalog { id: catalog; name: "cura" }

    Label
    {
        id: titleLabel
        anchors.top: parent.top
        anchors.horizontalCenter: parent.horizontalCenter
        horizontalAlignment: Text.AlignHCenter
        text: catalog.i18nc("@label", "User Agreement")
        color: UM.Theme.getColor("primary_button")
        font: UM.Theme.getFont("huge")
        renderType: Text.NativeRendering
    }

    Label
    {
        id: disclaimerLineLabel
        anchors
        {
            top: titleLabel.bottom
            topMargin: UM.Theme.getSize("wide_margin").height
            left: parent.left
            right: parent.right
        }

        text: "LIMITATIONS ON LIABILITY. EXCEPT AS REQUIRED UNDER LOCAL LAW, THE LIABILITY OF NANO DIMENSION AND ITS LICENSORS, WHETHER IN CONTRACT, TORT (INCLUDING NEGLIGENCE) OR OTHERWISE, ARISING OUT OF OR IN CONNECTION WITH THE SOFTWARE OR DOCUMENTATION FURNISHED HEREUNDER AND ANY SERVICE SUPPLIED FROM TIME TO TIME SHALL NOT EXCEED THE LICENSE FEE YOU PAID FOR THE SOFTWARE (IF ANY) DURING THE TWELVE (12) MONTHS PRECEDING THE CIRCUMSTANCES FIRST GIVING RISE TO THE CLAIM OF LIABILITY. IN NO EVENT SHALL NANO DIMENSION OR ITS LICENSORS BE LIABLE FOR SPECIAL, INDIRECT, INCIDENTAL, PUNITIVE OR CONSEQUENTIAL DAMAGES (INCLUDING WITHOUT LIMITATION DAMAGES RESULTING FROM LOSS OF USE, LOSS OF DATA, LOSS OF PROFITS, LOSS OF GOODWILL OR LOSS OF BUSINESS) ARISING OUT OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THE SOFTWARE OR DOCUMENTATION FURNISHED HEREUNDER AND ANY SERVICE SUPPLIED FROM TIME TO TIME, EVEN IF NANO DIMENSION OR ITS LICENSORS HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES. Some jurisdictions may not allow the limitation or exclusion of liability for incidental or consequential damages, so some of the above may not apply to you. In such jurisdictions, liability is limited to the fullest extent permitted by law.
                THE PROVISIONS OF THIS SECTION ‎7 ALLOCATE THE RISK UNDER THIS AGREEMENT BETWEEN YOU AND NANO DIMENSION, AND YOU HEREBY ACKNOWLEDGE THAT NANO DIMENSION HAVE RELIED UPON THE LIMITATIONS SET FORTH IN THIS SECTION ‎7 IN DETERMINING WHETHER TO ENTER INTO THIS AGREEMENT."
        textFormat: Text.RichText
        wrapMode: Text.WordWrap
        font: UM.Theme.getFont("medium")
        color: UM.Theme.getColor("text")
        renderType: Text.NativeRendering
    }

    Cura.PrimaryButton
    {
        id: agreeButton
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        text: catalog.i18nc("@button", "Agree")
        onClicked:
        {
            CuraApplication.writeToLog("i", "User accepted the User-Agreement.")
            CuraApplication.setNeedToShowUserAgreement(false)
            base.showNextPage()
        }
    }

    Cura.SecondaryButton
    {
        id: declineButton
        anchors.left: parent.left
        anchors.bottom: parent.bottom
        text: catalog.i18nc("@button", "Decline and close")
        onClicked:
        {
            CuraApplication.writeToLog("i", "User declined the User Agreement.")
            CuraApplication.closeApplication() // NOTE: Hard exit, don't use if anything needs to be saved!
        }
    }
}
