// Copyright (c) 2018 Ultimaker B.V.
// Cura is released under the terms of the LGPLv3 or higher.

import QtQuick 2.10
import QtQuick.Controls 2.3
import QtQuick.Layouts 1.3

import UM 1.3 as UM
import Cura 1.0 as Cura

RowLayout
{
    property string enabledText: catalog.i18nc("@label:Should be short", "On")
    property string disabledText: catalog.i18nc("@label:Should be short", "Off")

    Cura.IconWithText
    {
        source: UM.Theme.getIcon("category_layer_height")
        text:
        {
            if (Cura.MachineManager.activeStack)
            {
                var resultMap = Cura.MachineManager.activeQualityDisplayNameMap
                var text = layerHeight.properties.label +" - " + layerHeight.properties.value + " [um]"

                return text
            }
            return ""
        }
        font: UM.Theme.getFont("medium")
        elide: Text.ElideMiddle

        UM.SettingPropertyProvider
        {
            id: layerHeight
            containerStack: Cura.MachineManager.activeStack
            key: "layer_height"
            watchedProperties: ["value","label"]
        }
    }

    Cura.IconWithText
    {
        source: UM.Theme.getIcon("category_experimental")
        text: recipe.properties.label + " - " + recipe.properties.value
        font: UM.Theme.getFont("medium")

        UM.SettingPropertyProvider
        {
            id: recipe
            containerStack: Cura.MachineManager.activeStack
            key: "recipe"
            watchedProperties: ["value","label"]
        }
    }

    /*Cura.IconWithText
    {
        source: UM.Theme.getIcon("category_support")
        text: supportEnabled.properties.value == "True" ? enabledText : disabledText
        font: UM.Theme.getFont("medium")

        UM.SettingPropertyProvider
        {
            id: supportEnabled
            containerStack: Cura.MachineManager.activeMachine
            key: "support_enable"
            watchedProperties: ["value"]
        }
    }*/

//    Cura.IconWithText
//    {
//        source: UM.Theme.getIcon("category_adhesion")
//        text: platformAdhesionType.properties.value != "skirt" && platformAdhesionType.properties.value != "none" ? enabledText : disabledText
//        font: UM.Theme.getFont("medium")
//
//        UM.SettingPropertyProvider
//        {
//            id: platformAdhesionType
//            containerStack: Cura.MachineManager.activeMachine
//            key: "adhesion_type"
//            watchedProperties: [ "value"]
//        }
//    }
}