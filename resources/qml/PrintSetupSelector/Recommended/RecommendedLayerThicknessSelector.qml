// Copyright (c) 2018 Ultimaker B.V.
// Cura is released under the terms of the LGPLv3 or higher.

import QtQuick 2.7
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4

import UM 1.2 as UM
import Cura 1.0 as Cura

Item
{
    id: layerHeightRow
    height: childrenRect.height

    property real labelColumnWidth: Math.round(width / 3)


    // We use a binding to make sure that after manually setting infillSlider.value it is still bound to the property provider
    Binding
    {
        target: layerHeightSlider
        property: "value"
        value: parseInt(layerHeight.properties.value)
    }

    // Here are the elements that are shown in the left column
    Cura.IconWithText
    {
        id: layerHeightRowTitle
        anchors.top: parent.top
        anchors.left: parent.left
        source: UM.Theme.getIcon("category_layer_height")
        text: layerHeight.properties.label || "" //catalog.i18nc("@label", "Infill") + " (%)"
        font: UM.Theme.getFont("medium")
        width: labelColumnWidth
        //border.width: UM.Theme.getSize("default_lining").width
        //border.color: UM.Theme.getColor("yzyzyz2")
    }

    Item
    {
        id: layerHeightSliderContainer
        height: childrenRect.height

        anchors
        {
            left: layerHeightRowTitle.right
            right: parent.right
            verticalCenter: layerHeightRowTitle.verticalCenter
        }

        Slider
        {
            id: layerHeightSlider

            width: parent.width
            height: UM.Theme.getSize("print_setup_slider_handle").height // The handle is the widest element of the slider

            minimumValue: 0
            maximumValue: 21
            stepSize: 1
            tickmarksEnabled: true


            // disable slider when gradual support is enabled
            enabled: remoteLayerThickness.properties.value != "True"

            // set initial value from stack
            value: parseInt(layerHeight.properties.value)

            style: SliderStyle
            {
                //Draw line
                groove: Item
                {
                    Rectangle
                    {
                        height: UM.Theme.getSize("print_setup_slider_groove").height
                        width: control.width - UM.Theme.getSize("print_setup_slider_handle").width
                        anchors.horizontalCenter: parent.horizontalCenter
                        anchors.verticalCenter: parent.verticalCenter
                        color: control.enabled ? UM.Theme.getColor("quality_slider_available") : UM.Theme.getColor("quality_slider_unavailable")
                    }
                }

                handle: Rectangle
                {
                    id: handleButton
                    color: control.enabled ? UM.Theme.getColor("primary") : UM.Theme.getColor("quality_slider_unavailable")
                    implicitWidth: UM.Theme.getSize("print_setup_slider_handle").width
                    implicitHeight: implicitWidth
                    radius: Math.round(implicitWidth / 2)
                }

                tickmarks: Repeater
                {
                    id: repeater
                    model: control.maximumValue / control.stepSize + 1

                    Rectangle
                    {
                        color: control.enabled ? UM.Theme.getColor("quality_slider_available") : UM.Theme.getColor("quality_slider_unavailable")
                        implicitWidth: UM.Theme.getSize("print_setup_slider_tickmarks").width
                        implicitHeight: UM.Theme.getSize("print_setup_slider_tickmarks").height
                        anchors.verticalCenter: parent.verticalCenter

                        // Do not use Math.round otherwise the tickmarks won't be aligned
                        x: ((styleData.handleWidth / 2) - (implicitWidth / 2) + (index * ((repeater.width - styleData.handleWidth) / (repeater.count-1))))
                        radius: Math.round(implicitWidth / 2)

                        Label
                        {
                            text: index
                            font: UM.Theme.getFont("default")
                            visible: (index % 3) == 0 // Only show steps of 3%
                            anchors.horizontalCenter: parent.horizontalCenter
                            y: UM.Theme.getSize("thin_margin").height
                            renderType: Text.NativeRendering
                            color: UM.Theme.getColor("quality_slider_available")
                        }
                    }
                }
            }

            onValueChanged:
            {
                // Don't round the value if it's already the same
                if (parseInt(layerHeight.properties.value) == layerHeightSlider.value)
                {
                    return
                }
                // Round the slider value to the nearest multiple of 10 (simulate step size of 10)
                var roundedSliderValue = Math.round(layerHeightSlider.value)

                // Update the slider value to represent the rounded value
                layerHeightSlider.value = roundedSliderValue

                // Update value only if the Recomended mode is Active,
                // Otherwise if I change the value in the Custom mode the Recomended view will try to repeat
                // same operation
                var active_mode = UM.Preferences.getValue("cura/active_mode")

                if (active_mode == 0 || active_mode == "simple")
                {
                    layerHeight.setPropertyValue("value",roundedSliderValue)
                }
            }
        }
    }

    //  Gradual Support Infill Checkbox
    CheckBox
    {
        id: enableRemoteLayerThicknessCheckBox
        property alias _hovered: enableRemoteLayerThicknessMouseArea.containsMouse
        anchors.top: layerHeightSliderContainer.bottom
        anchors.topMargin: UM.Theme.getSize("wide_margin").height
        anchors.left: layerHeightSliderContainer.left
        text: String(remoteLayerThickness.properties.label)
        style: UM.Theme.styles.checkbox
        enabled: recommendedPrintSetup.settingsEnabled
        visible: remoteLayerThickness.properties.enabled == "True"
        checked: remoteLayerThickness.properties.value == "True"
        MouseArea
        {
            id: enableRemoteLayerThicknessMouseArea
            anchors.fill: parent
            hoverEnabled: true
            enabled: true
            onClicked:
            {
                enableRemoteLayerThicknessCheckBox.checked = !enableRemoteLayerThicknessCheckBox.checked

                if (enableRemoteLayerThicknessCheckBox.checked)
                {
                  remoteLayerThickness.setPropertyValue("value",true)
                  layerHeight.setPropertyValue("value",layerHeight.properties.default_value)
                }else
                {
                  remoteLayerThickness.setPropertyValue("value",false)
                }
            }
            onEntered: base.showTooltip(enableRemoteLayerThicknessCheckBox, Qt.point(-layerHeightSliderContainer.x - UM.Theme.getSize("thick_margin").width, 0),
                    remoteLayerThickness.properties.description)
            onExited: base.hideTooltip()
        }
    }

    UM.SettingPropertyProvider
    {
        id: layerHeight
        containerStack: Cura.MachineManager.activeMachine
        key: "layer_height"
        watchedProperties: [ "value","default_value","label" ]
        storeIndex: 0
    }

    UM.SettingPropertyProvider
    {
        id: remoteLayerThickness
        containerStack: Cura.MachineManager.activeMachine
        key: "get_layer_thickness_from_printer"
        watchedProperties: ["label","description","value", "enabled"]
        storeIndex: 0
    }


}