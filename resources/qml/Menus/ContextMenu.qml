// Copyright (c) 2016 Ultimaker B.V.
// Cura is released under the terms of the LGPLv3 or higher.

import QtQuick 2.10
import QtQuick.Controls 1.1
import QtQuick.Dialogs 1.2
import QtQuick.Window 2.1

import UM 1.2 as UM
import Cura 1.0 as Cura

import QtQuick.Controls 2.3 as Ctrls

Ctrls.Menu
{
    id: base

    property bool shouldShowExtruders: machineExtruderCount.properties.value > 1;

    property var multiBuildPlateModel: CuraApplication.getMultiBuildPlateModel()

    // Selection-related actions.
    //Cura.ND_tools_MenuItem { functionalAction: Cura.Actions.centerSelection; width: base.width }
    Cura.ND_tools_MenuItem { functionalAction: Cura.Actions.deleteSelection; width: base.width }
    Cura.ND_tools_MenuItem { functionalAction: Cura.Actions.multiplySelection; width: base.width }
    Cura.ND_tools_MenuItem { functionalAction: Cura.Actions.removeCopyDecorator; width: base.width }
   // Extruder selection - only visible if there is more than 1 extruder
   // MenuSeparator { visible: base.shouldShowExtruders }
    // MenuItem { id: extruderHeader; text: catalog.i18ncp("@label", "Print Selected Model With:", "Print Selected Models With:", UM.Selection.selectionCount); enabled: false; visible: base.shouldShowExtruders }
    // Instantiator
    // {
    //     model: CuraApplication.getExtrudersModel()
    //     MenuItem {
    //         text: "%1: %2 - %3".arg(model.name).arg(model.material).arg(model.variant)
    //         visible: base.shouldShowExtruders
    //         enabled: UM.Selection.hasSelection && model.enabled
    //         checkable: true
    //         checked: Cura.ExtruderManager.selectedObjectExtruders.indexOf(model.id) != -1
    //         onTriggered: CuraActions.setExtruderForSelection(model.id)
    //         shortcut: "Ctrl+" + (model.index + 1)
    //     }
    //     onObjectAdded: base.insertItem(index, object)
    //     onObjectRemoved: base.removeItem(object)
    // }
    Ctrls.MenuSeparator {
        //visible: UM.Preferences.getValue("cura/use_multi_build_plate")
        visible: true
        padding: 0
        contentItem: Rectangle {
            implicitWidth: 200
            implicitHeight: 1
            color: UM.Theme.getColor("menu_item_text")
        }
        background: Rectangle{
            color: UM.Theme.getColor("menu_item_background_hovered")
        }
    }

    // Instantiator
    // {
    //     model: base.multiBuildPlateModel
    //     Ctrls.MenuItem {
    //         enabled: UM.Selection.hasSelection
    //         text: base.multiBuildPlateModel.getItem(index).name;
    //         onTriggered: CuraActions.setBuildPlateForSelection(base.multiBuildPlateModel.getItem(index).buildPlateNumber);
    //         checkable: true
    //         checked: base.multiBuildPlateModel.selectionBuildPlates.indexOf(base.multiBuildPlateModel.getItem(index).buildPlateNumber) != -1;
    //         visible: UM.Preferences.getValue("cura/use_multi_build_plate")
    //     }
    //     onObjectAdded: base.insertItem(index, object);
    //     onObjectRemoved: base.removeItem(object);
    // }

    Cura.ND_tools_MenuItem {
        enabled: UM.Selection.hasSelection
        text: "New build plate";
        onTriggered: {
            CuraActions.setBuildPlateForSelection(base.multiBuildPlateModel.maxBuildPlate + 1);
            checked = false;
        }
        checkable: true
        checked: false
        visible: UM.Preferences.getValue("cura/use_multi_build_plate")
    }
    Cura.ND_tools_MenuItem { functionalAction: Cura.Actions.selectAll; }
    Cura.ND_tools_MenuItem { functionalAction: Cura.Actions.arrangeAll; }
    Cura.ND_tools_MenuItem { functionalAction: Cura.Actions.deleteAll; }
    Cura.ND_tools_MenuItem { functionalAction: Cura.Actions.reloadAll; }
    Cura.ND_tools_MenuItem { functionalAction: Cura.Actions.resetAllTranslation; }
    Cura.ND_tools_MenuItem { functionalAction: Cura.Actions.resetAll; }

    // Group actions
    // MenuSeparator {}
    // MenuItem { functionalAction: Cura.Actions.groupObjects; }
    // MenuItem { functionalAction: Cura.Actions.mergeObjects; }
    // MenuItem { functionalAction: Cura.Actions.unGroupObjects; }

    Connections
    {
        target: UM.Controller
        onContextMenuRequested: base.popup();
    }

    Connections
    {
        target: Cura.Actions.multiplySelection
        onTriggered: multiplyDialog.open()
    }

    UM.SettingPropertyProvider
    {
        id: machineExtruderCount
        containerStack: Cura.MachineManager.activeMachine
        key: "machine_extruder_count"
        watchedProperties: [ "value" ]
    }

    Dialog
    {
        id: multiplyDialog
        modality: Qt.ApplicationModal

        title: catalog.i18ncp("@title:window", "Multiply Selected Model", "Multiply Selected Models", UM.Selection.selectionCount)


        onAccepted: CuraActions.multiplySelection(copiesXField.value, offsetXField.value, copiesYField.value, offsetYField.value)

        signal reset()
        onReset:
        {
            copiesXField.value = 1;
            copiesXField.focus = true;

            copiesYField.value = 1;

            offsetXField.value = 0;
            offsetYField.value = 0;
        }

        onVisibleChanged:
        {
            copiesXField.forceActiveFocus();
        }

        standardButtons: StandardButton.Ok | StandardButton.Cancel

        Column
        {
            Row
            {
                spacing: UM.Theme.getSize("default_margin").width

                Label
                {
                    text: catalog.i18nc("@label", "Objects in X")
                    anchors.verticalCenter: copiesXField.verticalCenter
                }

                SpinBox
                {
                    id: copiesXField
                    focus: true
                    minimumValue: 1
                    maximumValue: 100
                }

                Label
                {
                    text: catalog.i18nc("@label", "offset in X (mm)")
                    anchors.verticalCenter: copiesXField.verticalCenter
                    enabled: copiesXField.value > 1
                }

                SpinBox
                {
                    id: offsetXField
                    minimumValue: 0
                    maximumValue: 100
                    enabled: copiesXField.value > 1
                }
            }

            Row
            {
                spacing: UM.Theme.getSize("default_margin").width

                Label
                {
                    text: catalog.i18nc("@label", "Objects in Y")
                    //anchors.verticalCenter: copiesXField.verticalCenter
                }

                SpinBox
                {
                    id: copiesYField
                    minimumValue: 1
                    maximumValue: 100
                }

                Label
                {
                    text: catalog.i18nc("@label", "offset in Y (mm)")
                    anchors.verticalCenter: copiesYField.verticalCenter
                    enabled: copiesYField.value > 1
                }

                SpinBox
                {
                    id: offsetYField
                    minimumValue: 0
                    maximumValue: 100
                    enabled: copiesYField.value > 1
                }
            }
        }
    }

    // Find the index of an item in the list of child items of this menu.
    //
    // This is primarily intended as a helper function so we do not have to
    // hard-code the position of the extruder selection actions.
    //
    // \param item The item to find the index of.
    //
    // \return The index of the item or -1 if it was not found.
    function findItemIndex(item)
    {
        for(var i in base.items)
        {
            if(base.items[i] == item)
            {
                return i;
            }
        }
        return -1;
    }

    UM.I18nCatalog { id: catalog; name: "cura" }
}
