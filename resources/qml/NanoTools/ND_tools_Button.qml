import QtQuick 2.10
import QtQuick.Controls 2.3
import UM 1.2 as UM
//file: ND_tools_Button.qml
Button {
    id: base_b
    function normal_mode() {return !base_b.down;}
    function hovered_mode() {return false;}
    property var normal_color: UM.Theme.getColor("background_modal_button")
    property var active_color: UM.Theme.getColor("background_modal_button_active")
    property var hovered_color: UM.Theme.getColor("background_modal_button_hovered")
    
    icon.color: base_b.enabled ? UM.Theme.getColor("menu_item_text") : UM.Theme.getColor("menu_item_disabled")
    background: Rectangle {
        color: {
            if (base_b.enabled){
                if (base_b.hovered)
                    return base_b.hovered_color
                if (!normal_mode())
                    return base_b.active_color
            }

            return base_b.normal_color
        }
    }
}