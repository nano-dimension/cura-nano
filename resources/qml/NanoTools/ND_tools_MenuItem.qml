import QtQuick 2.10
import QtQuick.Controls 2.3
import UM 1.2 as UM
//file: ND_tools_MenuItem.qml
MenuItem{
    id: base_mi
    // the function / Action to act on triggered.
    // if it is a function i call it on trigger, otherwise it calls the "onTriggered" property of it.
    property var functionalAction: {
        text: ""
        onTriggered: function() {console.log("default onTriggered");}
    }

    background: Rectangle{
        color: base_mi.highlighted ?  UM.Theme.getColor("menu_item_background") : UM.Theme.getColor("menu_item_background_hovered")
    }
    onTriggered: {(typeof functionalAction) == "function" ? functionalAction() : functionalAction.onTriggered()}
    height: !visible ? 0 : implicitHeight
    property var sc_text: functionalAction.shortcut ? " ("+functionalAction.shortcut+")" : ""
    enabled: (functionalAction!==undefined && functionalAction.enabled!==undefined) ? functionalAction.enabled : true
    contentItem: Text {
        text: base_mi.text || (base_mi.functionalAction.text + base_mi.sc_text)

        font: base_mi.font
        opacity: 1.0
        color:  base_mi.enabled ? UM.Theme.getColor("menu_item_text") : UM.Theme.getColor("action_button_disabled_text")
        horizontalAlignment: Text.AlignLeft
        verticalAlignment: Text.AlignVCenter
        elide: Text.ElideRight
    }
}
