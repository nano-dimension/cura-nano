import QtQuick 2.10
import QtQuick.Controls 2.3
import UM 1.2 as UM
//file: ND_tools_ComboBox.qml
ComboBox {
    id: base_cb
    property var background_color: undefined
    property int left_padding: 0
    property int right_padding: 0
    property int popup_width: 0
    property alias border: my_background.border

    function print_value(name, value){
        console.log(name,"=", value);
        return value;
    }
    function muti_char(c, n){
        var s = "";

        while(s.length < n)
            s+=c;
        return s;
    }
    function print_object(name, obj, depth, prefix){
        if (!depth)
            depth = 0;
        if (!prefix)
            prefix = "";
        var max = 0;
        if (prefix=="") {
            var title = "------------ yuval zilber represents ------------";
            var title_len = title.length;
            var name_str = (obj===undefined ? "#undefined#" : obj.toString());
            var name_type = name + ": " + name_str;
            var obj_len = name_type.length;
            if (title_len % 2 != obj_len % 2) {
                title = "-" + title;
                title_len++;
            }

            var additional = muti_char("-", obj_len - title_len + 4);
            title = additional + title + additional;
            title_len = title.length;
            var obj_dashes = muti_char("-", (title_len - obj_len - 2) / 2);
            var border = muti_char("-", title_len);

            console.log(border);
            console.log(title);
            console.log(obj_dashes + " " + name_type + " " + obj_dashes);
        }
        for (var i in obj) {
            max = Math.max(max, i.length);
        }

        for (var i in obj){
            var spaces = muti_char(" ", max - i.toString().length);
            if (depth===0)
                print_value(prefix+name+"[\""+i+"\"]"+spaces, obj[i]);
            else
            {
                var obs = obj[i].toString();
                var str = prefix+name+"[\""+i+"\"] = "+obs;
                var is_parent = i.replace("_","").replace("_","").startsWith("parent");
                if (is_parent){
                    console.log(str+" ... parent ...");
                }
                else if (obs.startsWith("Q") && obs.endsWith(")")){
                    str+=" {";
                    console.log(str);
                    print_object(name+"[\""+i+"\"]", obj[i],depth-1, prefix+"    " )
                    console.log(prefix+"}");
                }
                else{
                    console.log(str);
                }

            }
        }

        if (prefix==="") {
            console.log(border);
        }
        return obj;
    }

    enabled: base_cb.model.count > 1

    popup.width: {(base_cb.width || 90)+30}
    indicator: Canvas {
    }

    background: Rectangle{
        id: my_background
        color: background_color ? background_color : base_cb.down ? UM.Theme.getColor("background_modal_button_hovered") : UM.Theme.getColor("background_modal_button")
        border.color: UM.Theme.getColor("lining")
        border.width: base_cb.enabled ? 1 : 0
        radius: 5
    }
    contentItem: Text {
        text: base_cb.displayText
        width: {base_cb.width || 90}
        color: UM.Theme.getColor("menu_item_text")
        leftPadding: base_cb.left_padding+10
        rightPadding: base_cb.right_padding
        verticalAlignment: Text.AlignVCenter
        //anchors.fill: parent
        elide: Text.ElideRight
    }
    delegate: ItemDelegate {
        id: yz_del_item
        width: {(base_cb.width || 90) + 30}
        contentItem: Text {
            id: yz_content
            text: model["type_name"] || base_cb.model[index] || model["name"]
            color: "white"
            elide: Text.ElideRight
            verticalAlignment: Text.AlignVCenter
        }

        background: Rectangle{
            color: yz_del_item.highlighted ? UM.Theme.getColor("background_modal_button_hovered") : UM.Theme.getColor("background_modal_button")
        }
        highlighted: base_cb.highlightedIndex === index
    }
    Component.onCompleted:{


    }

}