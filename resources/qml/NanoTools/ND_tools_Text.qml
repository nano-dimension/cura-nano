import QtQuick 2.10
import QtQuick.Controls 2.3
import UM 1.2 as UM
// file: ND_tools_Text.qml

Text {
    width: 90;
    lineHeightMode :Text.FixedHeight
    height: UM.Theme.getSize("title_box_height").height
    color: UM.Theme.getColor("text_modal")
    font: UM.Theme.getFont("medium_bold")
    verticalAlignment: Text.AlignVCenter

    anchors{
        verticalCenter: parent.verticalCenter
    }
}